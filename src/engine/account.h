#ifndef ENGINE_ACCOUNT_H
#define ENGINE_ACCOUNT_H

#include <engine/kernel.h>

class IAccount : public IInterface
{
	MACRO_INTERFACE("account", 0)
public:
	enum
	{
		ACCOUNT_STATUS_DISCONNECTED = 0,
		ACCOUNT_STATUS_WAITING,
		ACCOUNT_STATUS_ERROR,
		ACCOUNT_STATUS_OK,
		ACCOUNT_STATUS_OFFLINE,
		NUM_ACCOUNT_STATUS
	};

	virtual int GetStatus() = 0;
	virtual void Login(const char *pUsername, const char *pPassword) = 0;
	virtual void LoginCheck() = 0;
	virtual void Logout() = 0;
	virtual void Register(const char *pUsername, const char *pEmail, const char *pPassword) = 0;
	virtual void RequestPassword(const char *pEmail) = 0;

	virtual void Join(const NETADDR *pAddr) = 0;

	virtual char *Name() = 0;
	virtual const char *GetFormError(const char *pKey) = 0;
};
#endif
