#ifndef ENGINE_PLAYERS_H
#define ENGINE_PLAYERS_H

#include <engine/kernel.h>
#include <engine/external/json/json.h>

#include "api_service.h"

struct CApiPlayer: public CApiEntity
{
	unsigned m_NameHash;
	unsigned m_ClanHash;
  
	
	char *m_pSkin;
	char *m_pName;
	char *m_pClanName;
	char *m_pServerAddress;
	int m_ColorBody;
	int m_ColorFeet;
	
	bool m_UseColor;
	
	CApiPlayer() {
		m_pSkin = 0;
		m_pName = 0;
		m_pClanName = 0;
        m_pServerAddress = 0;
		m_NameHash = 0;
		m_ClanHash = 0;
		m_UseColor = 0;
		m_ColorBody = 0;
		m_ColorFeet = 0;
	}
	
	virtual bool Parse(const json_value *pEntry) {
		const json_value* pName = json_object_get(pEntry, "name");
		const json_value* pTeam = json_object_get(pEntry, "team_name");
		const json_value* pSkin = json_object_get(pEntry, "skin");
		const json_value* pID = json_object_get(pEntry, "id");    
    
   		 if (pID != &json_value_none && json_int_get(pID)) {
			 m_ID = json_int_get(pID);
		} else {
			 m_ID = 0;
		}
		
		if (pName != &json_value_none && json_string_get(pName)) {
			m_pName = str_create_source(json_string_get(pName));
			m_NameHash = str_quickhash(m_pName);
		} else {
			m_pName = str_create_source("");
		}
		
		
		if (pTeam != &json_value_none && json_string_get(pTeam)) {
			m_pClanName = str_create_source(json_string_get(pTeam));
			m_ClanHash = str_quickhash(m_pClanName);
		} else {
		}

		
		if (pSkin != &json_value_none && json_string_get(pSkin)) {
			m_pSkin = str_create_source(json_string_get(pSkin));
		} else {
			m_pSkin = str_create_source("default");
		}
		
		const json_value* pUseColors = json_object_get(pEntry, "use_color");

		if (pUseColors != &json_value_none && json_boolean_get(pUseColors)) {
			m_UseColor = true;
			
			// parse body and feet color_body
			const json_value* pColorBody = json_object_get(pEntry, "color_body");
			const json_value* pColorFeet = json_object_get(pEntry, "color_feet");
			
			if (pColorBody != &json_value_none) {
				m_ColorBody = json_int_get(pColorBody);
			}
			
			if (pColorFeet != &json_value_none) {
				m_ColorFeet = json_int_get(pColorFeet);
			}
		} else {
			m_UseColor = false;
		}

		const json_value* pSHP = json_object_get(pEntry, "server");

		if (pSHP != &json_value_none) {
			const json_value* pServer = json_object_get(pSHP, "server");
			if (pSHP != &json_value_none) {
				const json_value* pIP = json_object_get(pServer, "ip");
				if (pIP != &json_value_none) {
                    m_pServerAddress = str_create_source(json_string_get(pIP));
				}
			}
		}


		return true;
	};
	
	virtual ~CApiPlayer() {		
		//free strings
		mem_free(m_pSkin);
		mem_free(m_pClanName);
		mem_free(m_pName);
		mem_free(m_pServerAddress);
	}

    bool IsOnline() const {
        return true;
    }
};
class IApiPlayers : public IApiService {
	
	MACRO_INTERFACE("api_players", 0)
	public:		
		virtual void Reset() = 0;
		virtual void Search(const char *pTerm) = 0;
		virtual void Detail(int PlayerID) = 0;
		virtual const int NumEntries() const = 0;
		virtual const CApiPlayer *GetEntry(int Index) const = 0;
		virtual const CApiPlayer *GetDetailPlayer() const = 0;
};
#endif