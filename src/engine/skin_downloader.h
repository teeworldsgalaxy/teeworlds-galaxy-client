#ifndef ENGINE_SKIN_DOWNLOADER_H
#define ENGINE_SKIN_DOWNLOADER_H

#include <engine/kernel.h>

typedef void (SKIN_CALLBACK)(const char *pName, int DirType , void *pUser);

class ISkinDownloader : public IInterface {

	MACRO_INTERFACE("skin_downloader", 0)
	public:
		virtual void Download(const char *pName, SKIN_CALLBACK pCallback, void *pUser) = 0;
};
#endif
