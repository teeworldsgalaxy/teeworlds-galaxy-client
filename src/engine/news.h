#ifndef ENGINE_NEWS_H
#define ENGINE_NEWS_H

#include <engine/kernel.h>
#include <engine/external/json/json.h>

#include "api_service.h"

struct CNewsEntry: public CApiEntity {
	char * m_pHeadline;
	char * m_pContent;
	char * m_pDate;
	char * m_pAuthor;
	
	CNewsEntry() {
		m_pHeadline = 0;
		m_pContent = 0;
		m_pDate = 0;
		m_pAuthor = 0;
	};
	
	virtual ~CNewsEntry() {
		mem_free(m_pHeadline);
		mem_free(m_pContent);
		mem_free(m_pDate);
		mem_free(m_pAuthor);
	};
	
	virtual bool Parse(const json_value *pEntry) {
		const json_value* pHeadline = json_object_get(pEntry, "headline");
		const json_value* pContent = json_object_get(pEntry, "content");
		const json_value* pDate = json_object_get(pEntry, "date");
		const json_value* pAuthor = json_object_get(pEntry, "author");
		const json_value* pID = json_object_get(pEntry, "id");

		if (pHeadline != &json_value_none && json_string_get(pHeadline)) {
			m_pHeadline = str_create_source(json_string_get(pHeadline));
		} else {
			m_pHeadline = str_create_source("");
		}
		
		
		if (pContent != &json_value_none && json_string_get(pContent)) {
			m_pContent = str_create_source(json_string_get(pContent));
		} else {
			m_pContent = str_create_source("");
		}
		
		
		if (pDate != &json_value_none && json_string_get(pDate)) {
			m_pDate = str_create_source(json_string_get(pDate));
		} else {
			m_pDate = str_create_source("");
		}
    
   		if (pAuthor != &json_value_none && json_string_get(pAuthor)) {
			m_pAuthor = str_create_source(json_string_get(pAuthor));
		} else {
			m_pAuthor = str_create_source("");
		}

		if (pID != &json_value_none) {
			m_ID = json_int_get(pID);
		}

        return true;
	};
};

class INews : public IApiService {
	
	MACRO_INTERFACE("news", 0)
	public:
		enum {
			NEWS_STATUS_OUT_OF_DATE = 0,
			NEWS_STATUS_WAITING,
			NEWS_STATUS_ERROR,
			NEWS_STATUS_OK,
			NEWS_STATUS_ALREAD_READ,
			NUM_NEWS_STATUS
		};
		
		virtual int GetStatus() = 0;
		virtual void Refresh() = 0;
		virtual const int NumEntries() const = 0;
		virtual const CNewsEntry *GetEntry(int Index) const = 0;
};
#endif