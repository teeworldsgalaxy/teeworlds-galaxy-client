/*
 * Borrowed from DDNet (https://github.com/ddnet/ddnet)
 */
 
#ifndef ENGINE_UPDATER_H
#define ENGINE_UPDATER_H

#include "kernel.h"

class IUpdater : public IInterface
{
	MACRO_INTERFACE("updater", 0)
public:
	enum
	{
		CLEAN = 0,
		CHECKING,
		OK,
		NEED_UPDATE,
		DOWNLOADING,
		NEED_RESTART,
		FAIL,
	};
	
	virtual const char *CurrentVersion() = 0;
	virtual const char *NextVersion() = 0;

	virtual void Update() = 0;
	virtual void Restart() = 0;
	virtual int GetState() = 0;
	virtual void InitiateUpdate() = 0;

	virtual char *GetCurrentFile() = 0;
	virtual int GetCurrentPercent() = 0;
	virtual float GetProgress() = 0;
};

#endif
