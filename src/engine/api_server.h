#ifndef ENGINE_API_SERVER_H
#define ENGINE_API_SERVER_H

#include <engine/kernel.h>
#include <engine/external/json/json.h>

#include "api_service.h"
#include <base/tl/array.h>

struct CApiServerEntity: public CApiEntity
{

	/** TODO: add me **/
	CApiServerEntity() {
	}
	
	virtual bool Parse(const json_value *pEntry) {
    	return true;
	};
	
	virtual ~CApiServerEntity() {
	}
};
class IApiServer : public IApiService {
	
	MACRO_INTERFACE("api_server", 0)
	public:
		virtual void Join(const char *pAddress, int ClientID) = 0;
		virtual void Leave() = 0;
};
#endif