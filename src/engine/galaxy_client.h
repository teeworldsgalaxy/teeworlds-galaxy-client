#ifndef ENGINE_GALAXY_CLIENT_H
#define ENGINE_GALAXY_CLIENT_H

#include <engine/kernel.h>
#include <engine/friends.h>
#include <engine/news.h>
#include <engine/api.h>
#include <engine/skin_downloader.h>
#include <engine/account.h>
#include <engine/updater.h>
#include <engine/api_players.h>
#include <engine/api_server.h>

class IGalaxyClient : public IInterface {

	MACRO_INTERFACE("galaxy_client", 0)
	public:
		virtual IFriends *Friends() = 0;
		virtual INews *News() = 0;
		virtual IAccount *Account() = 0;
		virtual IAPI *Api() = 0;
		virtual ISkinDownloader *SkinDownloader() = 0;
		virtual IUpdater *Updater() = 0;
		virtual IApiPlayers *Players() = 0;
		virtual IApiServer *Server() = 0;
		virtual void Update() = 0;
};
#endif
