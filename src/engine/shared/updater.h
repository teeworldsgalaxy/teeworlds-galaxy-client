/*
 * Borrowed from DDNet (https://github.com/ddnet/ddnet)
 */

#ifndef ENGINE_SHARED_UPDATER_H
#define ENGINE_SHARED_UPDATER_H

#include <vector>
#include <string>
#include <engine/api.h>
#include <engine/updater.h>
#include <engine/shared/config.h>

#define CLIENT_EXEC "Teeworlds-Galaxy"
#define SERVER_EXEC "Teeworlds-Galaxy-Server"

#if defined(CONF_FAMILY_WINDOWS)
	#define PLAT_EXT ".exe"
	#define PLAT_NAME CONF_PLATFORM_STRING
#elif defined(CONF_FAMILY_UNIX)
	#define PLAT_EXT ""
	#if defined(CONF_ARCH_IA32)
		#define PLAT_NAME CONF_PLATFORM_STRING "-x86"
	#elif defined(CONF_ARCH_AMD64)
		#define PLAT_NAME CONF_PLATFORM_STRING "-x86_64"
	#else
		#define PLAT_NAME CONF_PLATFORM_STRING "-unsupported"
	#endif
#else
	#define PLAT_EXT ""
	#define PLAT_NAME "unsupported-unsupported"
#endif

#define PLAT_CLIENT_DOWN CLIENT_EXEC "-" PLAT_NAME PLAT_EXT
#define PLAT_SERVER_DOWN SERVER_EXEC "-" PLAT_NAME PLAT_EXT

#define PLAT_CLIENT_EXEC CLIENT_EXEC PLAT_EXT
#define PLAT_SERVER_EXEC SERVER_EXEC PLAT_EXT

class CUpdater : public IUpdater
{
	class IClient *m_pClient;
	class IStorage *m_pStorage;
	class IAPI *m_pApi;

	bool m_IsWinXP;

	int m_State;
	char m_CurrentFile[256];
	int m_Percent;

	volatile long m_TotalDownload; //TODO: synchronize them better
	volatile long m_CurrentDownload;

	bool m_ClientUpdate;
	bool m_ServerUpdate;

	std::vector<std::string> m_DownloadFiles;

	void AddDownloadFile(const char *pFile, int FileSize = 0);
	void FetchFile(const char *pFile, const char *pDestPath = 0);
	void FetchFileSize(const char *pFile);

	void ReplaceClient();
	void ReplaceServer();

	volatile int m_NumDownloads;

	char m_aNextVersion[8];

	unsigned long CRC(const char *pFile);

public:
	CUpdater();
	static void FileCallback(CApiRequest *pRequest, void *pUser);
	static void ManifestCallback(CApiRequest *pRequest, void *pUser);
	static void ProgressCallback(CApiRequest *pRequest, void *pUser);
	static void SizeCallback(CApiRequest *pRequest, void *pUser);

	char *GetCurrentFile() { return m_CurrentFile; };
	int GetCurrentPercent() {
		if (m_TotalDownload == 0)
			return 0;
		int size = (100L*m_CurrentDownload)/m_TotalDownload;
		return size <= 100 ? size : 100;
	}

	float GetProgress() {
		if (m_TotalDownload == 0)
			return 0.0f;
		float progress = (float)m_CurrentDownload / (float)m_TotalDownload;

		return progress < 1.0f ? progress : 1.0f;
	}

	virtual void InitiateUpdate();
	void Init(IAPI *pApi);
	virtual void Update();
	void WinXpRestart();
	virtual int GetState() { return m_State; };
	virtual void Restart();

	virtual const char *CurrentVersion() { return g_Config.m_GalaxyVersion;};
	virtual const char *NextVersion() { return m_aNextVersion;};
};

#endif
