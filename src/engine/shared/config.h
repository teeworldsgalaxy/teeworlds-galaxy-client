/* (c) Magnus Auvinen. See licence.txt in the root of the distribution for more information. */
/* If you are missing that file, acquire a complete release at teeworlds.com.                */
#ifndef ENGINE_SHARED_CONFIG_H
#define ENGINE_SHARED_CONFIG_H

struct CConfiguration
{
	#define MACRO_CONFIG_INT(Name,ScriptName,Def,Min,Max,Save,Desc) int m_##Name;
	#define MACRO_CONFIG_STR(Name,ScriptName,Len,Def,Save,Desc) char m_##Name[Len]; // Flawfinder: ignore
	#include "config_variables.h"
	#undef MACRO_CONFIG_INT
	#undef MACRO_CONFIG_STR
};

extern CConfiguration g_Config;

enum
{
	CFGFLAG_SAVE		= 1 << 0,
	CFGFLAG_READONLY	= 1 << 1,
	CFGFLAG_CLIENT		= 1 << 2,
	CFGFLAG_SERVER		= 1 << 3,
	CFGFLAG_STORE		= 1 << 4,
	CFGFLAG_MASTER		= 1 << 5,
	CFGFLAG_ECON		= 1 << 6,
};

#endif
