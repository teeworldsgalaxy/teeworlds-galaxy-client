#include <base/system.h>
#include <engine/api/meta_include.h>
#include <engine/shared/config.h>
#include "api.h"
#include <libwebsockets.h>

CAPI::CAPI() {
}

bool CAPI::Init()
{
	m_pStorage = Kernel()->RequestInterface<IStorage>();
	if(!curl_global_init(CURL_GLOBAL_DEFAULT)) {
		//create first worker threads
		for(int i = g_Config.m_GalaxyRequestsParallel; i >= 0; i--) {
			CAPIWorker *pWorker = new CAPIWorker();
			if (!pWorker->Init(this)) {
				m_apWorkers.delete_all();
				return false;
			}

			//TODO: just start a worker thread if needed
			m_apWorkers.add(pWorker);
			pWorker->Start();
		}
		return true;
	}
	return false;
}

CAPI::~CAPI()
{
	curl_global_cleanup();
}

void CAPI::Handle(CApiRequest *pRequest) {
	//Add to Queue, will be handled by workers
	dbg_msg("CAPI", "handle request, path='%s", pRequest->m_pPath);
	if (pRequest->m_Type == CApiRequest::REQUEST_TYPE_WEBSOCKET) {
		CWebsocketConnectionHandler *pHandler = new CWebsocketConnectionHandler();
		m_apWorkers.add(pHandler);
		pHandler->Init(this, (CWebsocketConnection*) pRequest);
		pHandler->Start();
	} else {
		m_lRequests.add(pRequest);
		pRequest->m_State = CApiRequest::REQUEST_STATE_SENDING;
	}
}

void CAPIWorker::MakeRequest(CApiRequest *pRequest)
{
	if(g_Config.m_Debug)
	{
		dbg_msg("API", "Got Request %p: %s", pRequest, pRequest->m_pPath);
	}

	char aCAFile[512];
	m_pApi->m_pStorage->GetBinaryPath("data/cert.pem", aCAFile, sizeof aCAFile);

	char aErr[CURL_ERROR_SIZE];
	curl_easy_setopt(m_pHandle, CURLOPT_ERRORBUFFER, aErr);

	if(pRequest->m_CanTimeout)
	{
		curl_easy_setopt(m_pHandle, CURLOPT_CONNECTTIMEOUT_MS, (long)g_Config.m_HTTPConnectTimeoutMs);
		curl_easy_setopt(m_pHandle, CURLOPT_LOW_SPEED_LIMIT, (long)g_Config.m_HTTPLowSpeedLimit);
		curl_easy_setopt(m_pHandle, CURLOPT_LOW_SPEED_TIME, (long)g_Config.m_HTTPLowSpeedTime);
	}
	else
	{
		curl_easy_setopt(m_pHandle, CURLOPT_CONNECTTIMEOUT_MS, 0);
		curl_easy_setopt(m_pHandle, CURLOPT_LOW_SPEED_LIMIT, 0);
		curl_easy_setopt(m_pHandle, CURLOPT_LOW_SPEED_TIME, 0);
	}

	curl_easy_setopt(m_pHandle, CURLOPT_FOLLOWLOCATION, 1L);
	curl_easy_setopt(m_pHandle, CURLOPT_MAXREDIRS, 4L);
	curl_easy_setopt(m_pHandle, CURLOPT_FAILONERROR, pRequest->m_FailOnError);

	if(pRequest->m_aQueryData.size() == 0)
	{
		curl_easy_setopt(m_pHandle, CURLOPT_URL, pRequest->m_pPath);

	}
	else
	{
		char aPath[4096];
		unsigned int offset = str_length(pRequest->m_pPath);
		//support for query data, we should add a warning here if the path is getting too long
		str_copy(aPath, pRequest->m_pPath, sizeof(aPath));

		char *pCur = aPath + offset;
		str_copy(pCur, "?", sizeof(aPath) - (pCur - aPath)); //init get
		pCur++;
		for(int i = 0; i < pRequest->m_aQueryData.size(); ++i)
		{

			CApiRequest::CKeyValue *pair = pRequest->m_aQueryData[i];
			char *escaped = curl_easy_escape(m_pHandle, pair->m_pKey, 0);
			if(escaped)
			{
				str_copy(pCur, escaped, sizeof(aPath) - (pCur - aPath));
				pCur += str_length(escaped);
				curl_free(escaped);
			}

			str_copy(pCur, "=", sizeof(aPath) - (pCur - aPath));
			pCur++;

			escaped = curl_easy_escape(m_pHandle, pair->m_pValue, 0);
			if(escaped)
			{
				str_copy(pCur, escaped, sizeof(aPath) - (pCur - aPath));
				pCur += str_length(escaped);
				curl_free(escaped);
			}

			if(i < pRequest->m_aQueryData.size() - 1)
			{
				str_copy(pCur, "&", sizeof(aPath) - (pCur - aPath));
				pCur++;
			}
		}

		curl_easy_setopt(m_pHandle, CURLOPT_URL, aPath);

		if(g_Config.m_Debug)
		{
			dbg_msg("API", "Added params to %p: '%s'", pRequest, aPath);
		}
	}


	curl_easy_setopt(m_pHandle, CURLOPT_NOPROGRESS, 0);
	curl_easy_setopt(m_pHandle, CURLOPT_PROGRESSDATA, pRequest);
	curl_easy_setopt(m_pHandle, CURLOPT_PROGRESSFUNCTION, &CAPI::ProgressCallback);
	curl_easy_setopt(m_pHandle, CURLOPT_HTTPHEADER, pRequest->m_pHeaderList);

	if(!pRequest->m_pRequestData)
	{
		curl_easy_setopt(m_pHandle, CURLOPT_POST, 0);
		curl_easy_setopt(m_pHandle, CURLOPT_POSTFIELDSIZE, 0);
		curl_easy_setopt(m_pHandle, CURLOPT_POSTFIELDS, 0);
		curl_easy_setopt(m_pHandle, CURLOPT_HTTPGET, 1L);
	}
	else
	{
		curl_easy_setopt(m_pHandle, CURLOPT_POST, 1);
		curl_easy_setopt(m_pHandle, CURLOPT_POSTFIELDSIZE, pRequest->m_RequestDataSize);
		curl_easy_setopt(m_pHandle, CURLOPT_POSTFIELDS, pRequest->m_pRequestData);
	}

	curl_easy_setopt(m_pHandle, CURLOPT_NOBODY, pRequest->m_HeadersOnly);
	curl_easy_setopt(m_pHandle, CURLOPT_HEADERFUNCTION, &CAPI::HeaderCallback);
	curl_easy_setopt(m_pHandle, CURLOPT_HEADERDATA, pRequest);
	curl_easy_setopt(m_pHandle, CURLOPT_CAINFO, aCAFile);

	curl_easy_setopt(m_pHandle, CURLOPT_SSL_VERIFYPEER, false);

	curl_easy_setopt(m_pHandle, CURLOPT_WRITEDATA, pRequest);
	curl_easy_setopt(m_pHandle, CURLOPT_WRITEFUNCTION, &CAPI::DataCallback);

	pRequest->Prepare(m_pApi);
	int ret = curl_easy_perform(m_pHandle);

	if(ret != CURLE_OK)
	{
		dbg_msg("API", "Request %p '%s' failed. libcurl error: %s", pRequest, pRequest->m_pPath, aErr);
		pRequest->Error((ret == CURLE_ABORTED_BY_CALLBACK));
	}
	else
	{
        curl_easy_getinfo (m_pHandle, CURLINFO_RESPONSE_CODE, &pRequest->m_StatusCode);
		pRequest->Complete();
	}

	if(pRequest->m_pfnCompCallback)
		pRequest->m_pfnCompCallback(pRequest, pRequest->m_pUser);
}

size_t CAPI::DataCallback(char *pData, size_t size, size_t nmemb, void *pUser)
{
	CApiRequest *pRequest = (CApiRequest *)pUser;
	size_t RealSize = size*nmemb;
	//dbg_msg("CAPI", "DataCallback %p: pData=%p size=%u", pRequest, pData, RealSize);
	return pRequest->AddData(pData, RealSize);
}

int CAPI::ProgressCallback(void *pUser, double DlTotal, double DlCurr, double UlTotal, double UlCurr)
{
	CApiRequest *pRequest = (CApiRequest *)pUser;
	pRequest->m_Current = DlCurr;
	pRequest->m_Size = DlTotal;
	pRequest->m_Progress = (100 * DlCurr) / (DlTotal ? DlTotal : 1);
	if(pRequest->m_pfnProgressCallback)
		pRequest->m_pfnProgressCallback(pRequest, pRequest->m_pUser);
	return pRequest->m_Abort ? -1 : 0;
}

size_t CAPI::HeaderCallback(char *pData, size_t size, size_t nmemb, void *pUser) {

	CApiRequest *pRequest = (CApiRequest *)pUser;

	CApiRequest::CResponseHeader *pHeader = new CApiRequest::CResponseHeader(pData, nmemb * size);
	pRequest->m_apResponseHeaders.add(pHeader);
	return nmemb * size;
}

// we are defining these here so that we do not have to include libcurl in engine/api.handled
// TODO: Move CApiRequests into own files anyway
void CApiRequest::AddHeader(const char *pHeader) {
	m_pHeaderList = curl_slist_append(m_pHeaderList, pHeader);
}

void CApiRequest::ClearHeaders() {
	if (m_pHeaderList) {
		curl_slist_free_all(m_pHeaderList);
		m_pHeaderList = NULL;
	}
}


//Used for worker thread synchronisation
CApiRequest* CAPI::FetchNextRequest() {

	Maybe<CApiRequest *> Front = m_lRequests.get_front();

	if (Front.IsFull()) {
		return Front.Value;
	} else {
		return nullptr;
	}
}


bool CAPIWorker::Init(CAPI *pApi)
{
	m_pApi = pApi;
	if(m_pHandle = curl_easy_init()) {
		return true;
	}
	return false;
}

void CAPIWorker::Start()
{
	if (m_pThHandle)
	{
		dbg_msg("CAPIWorker", "%p is waiting for the last worker thread to finish...", this);
		m_Running = false;
		sys_thread_wait(m_pThHandle);
	}
	m_Running = true;
	m_pThHandle = sys_thread_create_named(&CAPIWorker::Work, this, "API-Worker");
	sys_thread_detach(m_pThHandle);
}

void CAPIWorker::Stop()
{
	dbg_msg("API", "requesting worker %p to stop...", this);
	m_Running = false;
}

void CAPIWorker::Work(void *pUser)
{
	CAPIWorker *pWorker = (CAPIWorker *)pUser;
	CAPI *pApi = pWorker->m_pApi;
	dbg_msg("API", "Worker-Thread for %p started...", pWorker);
	while(pWorker->m_Running)
	{
		CApiRequest *pRequest = pApi->FetchNextRequest();
		if (pRequest) {
			pWorker->MakeRequest(pRequest);
			//clear apirequest
			delete pRequest;
		}
		else
		{
			//TODO: make this better ...
			sys_thread_sleep(20);
		}
	}
	dbg_msg("API", "Worker-Thread for %p stopped!", pWorker);
}

CAPIWorker::~CAPIWorker()
{
	if(m_pHandle)
		curl_easy_cleanup(m_pHandle);

	m_Running = false;
	if (m_pThHandle) {
		sys_thread_wait(m_pThHandle);
	}
}

CAPIWorker::CAPIWorker() {
	m_Running = false;
	m_pHandle = 0;
	m_pThHandle = 0;
}
/*
	WEBSOCKETS
*/
CWebsocketConnectionHandler::CWebsocketConnectionHandler() {
	m_Connected = false;
}

CWebsocketConnectionHandler::~CWebsocketConnectionHandler()
{
}

bool CWebsocketConnectionHandler::Init(CAPI *pApi, CWebsocketConnection *pConnection)
{
	if (!CAPIWorker::Init(pApi)) {
		return false;
	}
	m_pConnection = pConnection;
	return true;
}

void CWebsocketConnectionHandler::Start() {
    if (m_pThHandle) {
        dbg_msg("CAPIWorker", "Waiting for last worker thread to finish");
        m_Running = false;
        sys_thread_wait(m_pThHandle);
    }
    m_Running = true;
    m_pThHandle = sys_thread_create_named(&CWebsocketConnectionHandler::StartInitThread, this, "WSockConnHandler");
    sys_thread_detach(m_pThHandle);
}
void CWebsocketConnectionHandler::StartInitThread(void *pUser) {
    CWebsocketConnectionHandler *pHandler = (CWebsocketConnectionHandler *)pUser;
    pHandler->InitConnection();
}

void CWebsocketConnectionHandler::InitConnection() {

	struct lws_context_creation_info info;

	struct lws_protocols protocol;

	memset(&info, 0, sizeof info);
	info.port = CONTEXT_PORT_NO_LISTEN;
	info.iface = NULL;
	info.protocols = &protocol;;
	info.ssl_cert_filepath = NULL;
	info.ssl_private_key_filepath = NULL;
	info.extensions = NULL;
	info.gid = -1;
	info.uid = -1;
	info.options = 0;
	protocol.name  = "echo";
    protocol.callback = &CWebsocketConnectionHandler::LWSCallback;
    protocol.per_session_data_size = NULL;
    protocol.rx_buffer_size = 0;
    protocol.id = 0;
    protocol.user = NULL;

	m_pWSContext = lws_create_context(&info);

	dbg_msg("WS", "context created");

	if (m_pWSContext == NULL) {
		dbg_msg("WS", "context error");
		return;
	}


	struct lws_client_connect_info connectInfo;

	connectInfo.context = m_pWSContext;
	connectInfo.address = "api.teeworlds-galaxy.com";
	connectInfo.port = 4000;
	connectInfo.ssl_connection = 0;
	connectInfo.path = "/socket/websocket?token=undefined&vsn=2.0.0";
	connectInfo.host = "api.teeworlds-galaxy.com:4000";
	connectInfo.origin = "NULL";
	connectInfo.protocol = protocol.name;
	connectInfo.ietf_version_or_minus_one = -1;
	connectInfo.userdata = this;
	connectInfo.client_exts = NULL;
	connectInfo.method = NULL;
	connectInfo.parent_wsi = NULL;
	connectInfo.uri_replace_from = NULL;
	connectInfo.uri_replace_to = NULL;
	connectInfo.vhost = NULL;

	m_pWSHandle = lws_client_connect_via_info(&connectInfo);


	if (m_pWSContext == NULL || m_pWSHandle == nullptr) {
		dbg_msg("WS", "client connect error");
		return;
	}


	while(m_Running)
	{
		lws_service(m_pWSContext, 50);
        lws_callback_on_writable(m_pWSHandle);
	}

	lws_context_destroy(m_pWSContext);
}


int CWebsocketConnectionHandler::LWSCallback(struct lws * wsi, enum lws_callback_reasons reason, void * user, void * in, size_t len) {
    CWebsocketConnectionHandler* pHandler = (CWebsocketConnectionHandler*) user;

    switch (reason) {
        case LWS_CALLBACK_CLIENT_CONNECTION_ERROR:
            dbg_msg("WS", "CONNECTION_ERROR: %.*s", len, in);
            break;
        case LWS_CALLBACK_CLIENT_RECEIVE: {
            dbg_msg("WS", "Received: %.*s", len, in);
			CWebsocketMessage *pMsg = new CWebsocketMessage();
            pMsg->m_Size = len;
            unsigned char * pBuffer =  (unsigned char *)mem_alloc(pMsg->m_Size, 1);
            mem_copy(pBuffer, in, pMsg->m_Size);
            pMsg->m_pData = (char *) pBuffer;
            pHandler->m_pConnection->Receive(pMsg);
            break;
        }
        case LWS_CALLBACK_CLIENT_ESTABLISHED:
            dbg_msg("WS", "CLIENT_ESTABLISHED", len, in);
            lws_callback_on_writable(wsi);
            break;
        case LWS_CALLBACK_CLIENT_WRITEABLE: {

            //prepare to send message
            const CWebsocketMessage *pMsg = pHandler->m_pConnection->GetNextSendMessage();

            if (pMsg) {
                dbg_msg("WS", "Sending: %.*s", pMsg->m_Size, pMsg->m_pData);
                unsigned char *pBuffer =  (unsigned char *)mem_alloc(LWS_PRE + pMsg->m_Size, 1);
                mem_copy(pBuffer+LWS_PRE, pMsg->m_pData, pMsg->m_Size);
                lws_write(wsi, pBuffer+LWS_PRE, pMsg->m_Size, LWS_WRITE_TEXT);
                mem_free(pBuffer); // free Buffer
                lws_callback_on_writable(wsi);
                delete pMsg;
            }
            break;
        // case LWS_CALLBACK_GET_THREAD_ID:
        //return (int)sys_thread_id();
        }
        default:
            //dbg_msg("WS", "Callback: %d", reason);
            break;
    }

    return 0;
}
