/*
 * Borrowed from DDNet (https://github.com/ddnet/ddnet)
 */

#include <stdlib.h> // system

#include <base/system.h>
#include <engine/storage.h>
#include <engine/storage.h>
#include <engine/client.h>
#include <engine/api/net_request.h>
#include <engine/api/json_request.h>
#include <engine/external/json/json.h>

#include <game/version.h>
#include <engine/external/zlib/zlib.h>

#include "updater.h"

using std::string;
using std::vector;

#define EXECUTABLE

CUpdater::CUpdater()
{
	m_pClient = NULL;
	m_pStorage = NULL;
	m_State = CHECKING;
	m_Percent = 0;
	m_NumDownloads  = 0;
	m_pApi = NULL;
	m_aNextVersion[0] = 0;
	m_CurrentFile[0] = 0;
	m_TotalDownload = 0;
	m_CurrentDownload = 0;
}

void CUpdater::Init(IAPI *pApi)
{
	m_pApi = pApi;
	m_pClient = Kernel()->RequestInterface<IClient>();
	m_pStorage = Kernel()->RequestInterface<IStorage>();
	#if defined(CONF_FAMILY_WINDOWS)
		m_IsWinXP = os_compare_version(5, 1) <= 0;
	#else
		m_IsWinXP = false;
	#endif

	//TODO: Move me:
	InitiateUpdate();
}

void CUpdater::ProgressCallback(CApiRequest *pRequest, void *pUser)
{
	CUpdater *pUpdate = (CUpdater *)pUser;

	CDownloadRequest *pDownload = (CDownloadRequest *)pRequest;

	str_copy(pUpdate->m_CurrentFile, pDownload->m_pFilePath, sizeof(pUpdate->m_CurrentFile));
	pUpdate->m_Percent = pDownload->m_Progress;
}

void CUpdater::ManifestCallback(CApiRequest *pRequest, void *pUser) {
	CUpdater *pUpdate = (CUpdater *)pUser;
	CJSONRequest *pParsedRequest = (CJSONRequest *) pRequest;


	if(!pParsedRequest->Success()) {
		pUpdate->m_State = FAIL;

	} else {

		pUpdate->m_State = CHECKING;
		str_format(pUpdate->m_CurrentFile, sizeof(pUpdate->m_CurrentFile), "Calculating Differences...");

		const json_value *pCurrent = pParsedRequest->m_pPayload;
		bool needUpdate = false;

		if(pCurrent && pCurrent->type == json_object)
		{

			const char *pVersion = json_string_get(json_object_get(pCurrent, "version"));
			if(str_comp(pVersion, g_Config.m_GalaxyVersion))
			{
				str_copy(pUpdate->m_aNextVersion, pVersion, sizeof(pUpdate->m_aNextVersion));

				/* TODO
				 if(json_boolean_get(json_object_get(pCurrent, "client")))
					pUpdate->m_ClientUpdate = true;
				if(json_boolean_get(json_object_get(pCurrent, "server")))
					pUpdate->m_ServerUpdate = true;
				 */
				const json_value *pFiles = json_object_get(pCurrent, "files");


				if(pFiles->type == json_object)
				{
					for(int j = 0; j < json_object_size(pFiles); j++)
					{
						//dbg_msg("UPDATER", "%d", j);
						const char *pFilePath = json_object_get_index_key(pFiles, j);
						const json_value *pData = json_object_get_index_value(pFiles, j);

						const json_value *pCRC = json_object_get(pData, "crc32");
						const json_value *pSize = json_object_get(pData, "size");

						if (pCRC->type == json_integer && pSize->type == json_integer) {

							long CurrentCRC = pUpdate->CRC(pFilePath);
							long FileCRC = json_int_get_long(pCRC);
							int FileSize = json_int_get(pSize);

							if (FileCRC && CurrentCRC != FileCRC) {

                                needUpdate = true;
								//we need to update this file
								dbg_msg("UPDATER", "%s: Current crc %d, wanted %d, so we are downloading (%d Bytes)", pFilePath, CurrentCRC, FileCRC, FileSize);
								pUpdate->m_TotalDownload += FileSize;
								pUpdate->m_DownloadFiles.push_back(string(pFilePath));
							} else {
								//dbg_msg("UPDATER", "%s: CRC match", pFilePath);

							}
						}
					}
				}
			}
		}

		pUpdate->m_State = needUpdate ? NEED_UPDATE : OK;
	}
}

void CUpdater::FileCallback(CApiRequest *pRequest, void *pUser) {

	static LOCK Lock = sys_lock_create();
	sys_lock_wait(Lock);
	CUpdater *pUpdater = (CUpdater *)pUser;
	pUpdater->m_NumDownloads--;

	if(pRequest->Success() && pUpdater->m_NumDownloads == 0 && pUpdater->m_State != FAIL)
	{
		//we are done
		if(pUpdater->m_ClientUpdate) {
			pUpdater->ReplaceClient();
		}
		if(pUpdater->m_ServerUpdate) {
			pUpdater->ReplaceServer();
		}

		str_copy(g_Config.m_GalaxyVersion, pUpdater->m_aNextVersion, sizeof(g_Config.m_GalaxyVersion));

		pUpdater->Restart();
	} else if (pRequest->Success()) {
		pUpdater->m_CurrentDownload += pRequest->GetContentSize();
	}
	else
		pUpdater->m_State = FAIL;
	sys_lock_release(Lock);
}

void CUpdater::Restart() {
	if(!m_IsWinXP)
		m_pClient->Restart();
	else
		WinXpRestart();
}

void CUpdater::FetchFile(const char *pFile, const char *pDestPath)
{
	if(!pDestPath)
		pDestPath = pFile;

	CDownloadRequest *pRequest = new CDownloadRequest();
	pRequest->m_pUser = this;

	char aPath[512];
	str_format(aPath, sizeof(aPath), "%s/updater/files/%s/%s", g_Config.m_GalaxyAPIAdress, m_aNextVersion, PLAT_NAME);
	pRequest->AddQueryData("file", pFile, true);
	pRequest->SetPath(aPath);
	pRequest->m_pfnProgressCallback = &CUpdater::ProgressCallback;
	pRequest->m_pfnCompCallback = &CUpdater::FileCallback;
	pRequest->m_Secure = false; //no need for data only download but TODO: fix this with ssl certificates
	pRequest->SetFilePath(pDestPath, IStorage::STORAGETYPE_CLIENT);

	m_NumDownloads++;
	m_pApi->Handle(pRequest);
}

void CUpdater::ReplaceClient()
{
	dbg_msg("updater", "Replacing %s", m_pStorage->GetExecutableName());

	// replace running executable by renaming twice...
	if(!m_IsWinXP)
	{
		m_pStorage->RemoveBinaryFile(CLIENT_EXEC ".old");
		m_pStorage->RenameBinaryFile(m_pStorage->GetExecutableName(), CLIENT_EXEC ".old");
		m_pStorage->RenameBinaryFile(CLIENT_EXEC ".tmp", m_pStorage->GetExecutableName());
	}
	#ifndef CONF_FAMILY_WINDOWS
		char aPath[512];
		m_pStorage->GetBinaryPath(m_pStorage->GetExecutableName(), aPath, sizeof aPath);
		char aBuf[512];
		str_format(aBuf, sizeof aBuf, "chmod +x %s", aPath);
		if (system(aBuf))
			dbg_msg("updater", "Error setting client executable bit");
	#endif
}

void CUpdater::ReplaceServer()
{
	dbg_msg("updater", "Replacing " PLAT_SERVER_EXEC);

	//Replace running executable by renaming twice...
	m_pStorage->RemoveBinaryFile(SERVER_EXEC ".old");
	m_pStorage->RenameBinaryFile(PLAT_SERVER_EXEC, SERVER_EXEC ".old");
	m_pStorage->RenameBinaryFile(SERVER_EXEC ".tmp", PLAT_SERVER_EXEC);
	#ifndef CONF_FAMILY_WINDOWS
		char aPath[512];
		m_pStorage->GetBinaryPath(PLAT_SERVER_EXEC, aPath, sizeof aPath);
		char aBuf[512];
		str_format(aBuf, sizeof aBuf, "chmod +x %s", aPath);
		if (system(aBuf))
			dbg_msg("updater", "Error setting server executable bit");
	#endif
}

void CUpdater::InitiateUpdate()
{
	m_State = CHECKING;

	CJSONRequest *pRequest = new CJSONRequest();
	pRequest->m_pUser = this;

    char aPath[512];
	str_format(aPath, sizeof(aPath), "%s/updater/version_info/current/%s", g_Config.m_GalaxyAPIAdress, PLAT_NAME);
	pRequest->SetPath(aPath);
	pRequest->m_pfnCompCallback = &CUpdater::ManifestCallback;
	m_pApi->Handle(pRequest);
}

void CUpdater::Update()
{
	dbg_msg("updater", "Executing updates");
	m_State = DOWNLOADING;

	str_format(m_CurrentFile, sizeof(m_CurrentFile), "Calculating Differences...");

	while(!m_DownloadFiles.empty())
	{
		FetchFile(m_DownloadFiles.back().c_str());
		m_DownloadFiles.pop_back();
	}

	if(m_ServerUpdate)
		FetchFile(PLAT_SERVER_DOWN, SERVER_EXEC ".tmp");
	if(m_ClientUpdate)
		FetchFile(PLAT_CLIENT_DOWN, CLIENT_EXEC ".tmp");
}

void CUpdater::WinXpRestart()
{
	char aBuf[512];
	IOHANDLE bhFile = io_open(m_pStorage->GetBinaryPath("du.bat", aBuf, sizeof aBuf), IOFLAG_WRITE);
	if(!bhFile)
		return;
	char bBuf[512];
	str_format(bBuf, sizeof(bBuf), ":_R\r\ndel \""CLIENT_EXEC".exe\"\r\nif exist \""CLIENT_EXEC".exe\" goto _R\r\nrename \""CLIENT_EXEC".tmp\" \""CLIENT_EXEC".exe\"\r\n:_T\r\nif not exist \""CLIENT_EXEC".exe\" goto _T\r\nstart "CLIENT_EXEC".exe\r\ndel \"du.bat\"\r\n");
	io_write(bhFile, bBuf, str_length(bBuf));
	io_close(bhFile);
	shell_execute(aBuf);
	m_pClient->Quit();
}

unsigned long CUpdater::CRC(const char *pFile)
{
	IOHANDLE f = io_open(pFile, IOFLAG_READ);
	if(!f)
		return 0xFFFFFFFF;

	long len = io_length(f);
	if(len <= 0)
	{
		io_close(f);
		return 0xFFFFFFFF;
	}

	// take the CRC of the file and store it
	unsigned long Crc = 0;


	// read it in chunks
	while(len > 0)
	{
        unsigned char aBuf[4096];

        int NumWanted = len < sizeof(aBuf) ? len : sizeof(aBuf);
		unsigned int NumRead = io_read(f, aBuf, NumWanted);

        if (NumRead != NumWanted) {
            //error -> break
            break;
        } else {
            Crc = crc32(Crc, aBuf, NumRead);
            len -= NumRead;
        }
	}

	io_close(f);
	return Crc;
}
