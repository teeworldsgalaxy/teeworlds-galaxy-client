#ifndef ENGINE_SHARED_API_H
#define ENGINE_SHARED_API_H

#include <base/tl/list_thread_safe.h>
#include <base/tl/array.h>
#include <engine/api.h>
#define WIN32_LEAN_AND_MEAN
#include "curl/curl.h"
#include "curl/easy.h"

#include <libwebsockets.h>

class CAPIWorker;

class CAPI : public IAPI {

	friend class CAPIWorker;

	array<CAPIWorker*> m_apWorkers;

	list_ts<CApiRequest*> m_lRequests;

	IStorage *m_pStorage;

	static int ProgressCallback(void *pUser, double DlTotal, double DlCurr, double UlTotal, double UlCurr);
	static size_t DataCallback(char *pData, size_t size, size_t nmemb, void *pUser);
	static size_t HeaderCallback(char *pData, size_t size, size_t nitems, void *pUser);
	static void Worker(void *pUser);

	protected:
	CApiRequest* FetchNextRequest();

	public:
	~CAPI();
	CAPI();

	virtual IStorage *Storage() { return m_pStorage; };
	bool Init();
	virtual void Handle(CApiRequest *pRequest);
};

class CAPIWorker {
	friend class CAPI;

	protected:
		bool m_Running;
		CURL *m_pHandle;
		void *m_pThHandle;
		CAPI *m_pApi;

	public:
		virtual void Start();
		virtual void Stop();
		virtual void MakeRequest(CApiRequest *pRequest);

		static void Work(void *pUser);

		bool Init(CAPI *pApi);
		CAPIWorker();
		virtual ~CAPIWorker();
};


class CWebsocketConnectionHandler : public CAPIWorker {

	protected:
		CWebsocketConnection *m_pConnection;
		bool m_Connected;
		char m_aErr[CURL_ERROR_SIZE];

		struct lws_context *m_pWSContext;
		struct lws *m_pWSHandle;
	public:
		CWebsocketConnectionHandler();
		~CWebsocketConnectionHandler();

		bool Init(CAPI *pApi, CWebsocketConnection *pConnection);
		virtual void Start();
		static void StartInitThread(void *pUser);

		static int LWSCallback(struct lws * wsi, enum lws_callback_reasons reason, void * user, void * in, size_t len);

		static int ServiceCallback(struct libwebsocket_context *context,
            struct libwebsocket *wsi,
            int reason, void *user,
            void *in, size_t len);

		void HandleConnection();
		void InitConnection();

//		void Send(CWebsocketConnection::CMessage *pMessage);

		static int MessageCallback(struct libwebsocket *wsi_in, char *str, int str_size_in);
};
#endif
