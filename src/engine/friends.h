/* (c) Magnus Auvinen. See licence.txt in the root of the distribution for more information. */
/* If you are missing that file, acquire a complete release at teeworlds.com.                */
#ifndef ENGINE_FRIENDS_H
#define ENGINE_FRIENDS_H

#include <engine/shared/protocol.h>
#include <engine/external/json/json.h>

#include "kernel.h"
#include "api_players.h"

struct CApiFriend: public CApiEntity
{
	enum FriendStatus {
		STATUS_UNKNOWN = 0,
		STATUS_FRIEND,
		STATUS_REQUEST,
		STATUS_OWN_REQUEST
	};

	CApiPlayer *m_pFriend;
	CApiPlayer *m_pPlayer;

	FriendStatus m_Status;

	CApiFriend() {
		m_pFriend = 0;
		m_pPlayer = 0;
		m_Status = STATUS_UNKNOWN;
	}
	
	virtual bool Parse(const json_value *pEntry) {
		const json_value* pPlayer = json_object_get(pEntry, "player");
		const json_value* pFriend = json_object_get(pEntry, "friend");
		
		if (pPlayer != &json_value_none) {
            m_pPlayer = new CApiPlayer();
            m_pPlayer->Parse(pPlayer);
		}
        
        if (pFriend != &json_value_none) {
            m_pFriend = new CApiPlayer();
            m_pFriend->Parse(pFriend);
		}
        return true;
	};
	
	virtual ~CApiFriend() {		
    
        if (m_pFriend) {
            delete m_pFriend;
        }
        
        if (m_pPlayer) {
            delete m_pPlayer;
        }
	}

	const FriendStatus GetStatus() const {
		return m_Status;
	}
};

class IFriends : public IInterface
{
	MACRO_INTERFACE("friends", 0)
public:
	enum FRIEND_STATE
	{
		FRIEND_NO=0,
		FRIEND_CLAN,
		FRIEND_PLAYER,

		MAX_FRIENDS=128,
	};
	
	enum {
		STATUS_OUT_OF_DATE = 0,
		STATUS_WAITING,
		STATUS_ERROR,
		STATUS_OK,
		NUM_FRIENDS_STATUS
	};

	virtual int GetStatus() = 0;
	virtual int NumFriends() const = 0;
	virtual int NumRequests() const = 0;
	virtual const CApiFriend *GetFriend(int Index) const = 0;
	virtual const CApiFriend *GetRequest(int Index) const = 0;
	virtual FRIEND_STATE GetFriendState(const char *pName, const char *pClan) const = 0;
	virtual CApiFriend::FriendStatus GetFriendStatus(const int PlayerID) const = 0;
	virtual bool IsFriend(const char *pName, const char *pClan, bool PlayersOnly) const = 0;

	virtual void AddFriend(int64 ID) = 0;
	virtual void AddFriend(const CApiPlayer &Player) = 0;
	virtual void RemoveFriend(int64 ID) = 0;
	virtual void RemoveFriend(const CApiPlayer &Player) = 0;
	virtual void Refresh() = 0;
};

#endif
