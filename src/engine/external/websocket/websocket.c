#ifndef CURL_WEBSOCKET_H
#define CURL_WEBSOCKET_H

#ifndef CURL_WEBSOCKET_IMPORT
	#define CURL_WEBSOCKET_IMPORT
	#define WIN32_LEAN_AND_MEAN
	#include "curl/curl.h"
	#include "curl/easy.h"
#endif

#ifndef CURL_WEBSOCKET_SLEEP
	#define CURL_WEBSOCKET_SLEEP 10
#endif

struct websocket;

typedef size_t (WS_MSG_CALLBACK) (char *ptr, size_t size, size_t nmemb, void *userdata);

typedef enum {
	CONTINUATION = 0x0,
	TEXT_FRAME = 0x1,
	BINARY_FRAME = 0x2,
	CLOSE = 0x8,
	PING = 0x9,
	PONG = 0xa,
} OP_CODE;
struct frame {
	
	int fin;
	OP_CODE op_code;	
	unsigned char N0;
	unsigned int N;
	unsigned char use_mask;
	char mask[4];
};

struct message {
	unsigned int size;
	void* data;
};

const char *curl_websocket_strnstr(const char *haystack, const char *needle, size_t num){
	while (num > 0 && *haystack)	{
		const char *a = haystack;
		const char *b = needle;
		while (*a && *b && *a == *b) {
			a++;
			b++;
		}
		if(!(*b))
			return haystack;
		haystack++;
		num--;
	}
	return NULL;
}

typedef enum {
    WS_INIT = 0,
	WS_HANDSHAKE,
	WS_HANDLING,
	WS_ERROR,
	WS_CLOSING
} WS_STATE;

struct websocket {
	WS_MSG_CALLBACK *msg_callback;
	void *msg_data;
	
	CURL *handle;
	curl_socket_t socket;
	struct curl_slist *custom_headers;
	
	char *buffer;
	unsigned int buffer_size;
	
	WS_STATE state;
	
	unsigned char running;
	unsigned char should_stop;
	
};


struct websocket * curl_websocket_init(CURL *handle) {
	//create a new websocket handle and fill with 0 right from the start
	struct websocket* ws = (struct websocket*) calloc(1, sizeof(struct websocket));
	ws->handle = handle;
	//curl_easy_getinfo(handle, CURLINFO_ACTIVESOCKET,  &ws->socket);
	return ws;
}

size_t curl_websocket_recv(struct websocket *websocket, void * buffer , size_t buflen , size_t * n);
size_t curl_websocket_send(struct websocket *websocket, const void * buffer , size_t buflen , size_t * n);

CURLcode curl_websocket_setopt(struct websocket *websocket, int opt, long value) {
	switch (opt) {
		case CURLOPT_WRITEFUNCTION:
			websocket->msg_callback = (WS_MSG_CALLBACK*) value;
			return CURLE_OK;
		case CURLOPT_WRITEDATA:
			websocket->msg_data = (void*) value;
			return CURLE_OK;
		case CURLOPT_HTTPHEADER:
			if (websocket->state == WS_INIT) {
				websocket->custom_headers = (struct curl_slist *) value;
				return CURLE_OK;
			} else {
				return CURLE_UNKNOWN_OPTION;
			}
	}
	return CURLE_UNKNOWN_OPTION;
}

CURLcode curl_websocket_perform(struct websocket *websocket) {
	
	websocket->running = true;
	websocket->state = WS_HANDSHAKE;
	
	//init handshake
	struct curl_slist *headers = NULL;
	struct curl_slist *current = NULL;
	size_t num  = 0;
	char simple_buffer[1024];
	bool finished = false;
	int res = CURLE_OK;
	char *new_buf = NULL;
	const char *search = NULL;
	
	const char *url = NULL;
	curl_easy_getinfo(websocket->handle, CURLINFO_EFFECTIVE_URL, &url);
	
	if (websocket->custom_headers != NULL) {
		headers = websocket->custom_headers;
	} else {		
		headers = curl_slist_append(NULL ,    "GET ws://echo.websocket.org/?encoding=text HTTP/1.1");
		headers = curl_slist_append(headers , "Host: echo.websocket.org");
		headers = curl_slist_append(headers , "Connection: Upgrade");
		headers = curl_slist_append(headers , "Upgrade: websocket");
		headers = curl_slist_append(headers , "Sec-WebSocket-Version: 13");
		headers = curl_slist_append(headers , "Sec-WebSocket-Key: HF4DMUFGxzvG0DONyNmShg=="); //TODO: generate this
	}
	
	//send headers
	current = headers;
	while(current) { //TODO check sent sizes and error codes
		dbg_msg("WS", "curl_easy_send %s",current->data);
		 curl_easy_send(websocket->handle, current->data, strlen(current->data), &num); 
		dbg_msg("WS", "curl_easy_send %s, %d, %d",current->data, strlen(current->data), num);
		 curl_easy_send(websocket->handle, "\r\n", 2, &num);
		 current = current->next;
	}
	
	curl_easy_send(websocket->handle, "\r\n", 2, &num);
	
	//free list if not custom
	if (websocket->custom_headers == NULL) {
		curl_slist_free_all(headers);
	}
	
	
	//headers sent => wait for response:
	while (search == NULL && !websocket->should_stop) {
		
		res = curl_easy_recv(websocket->handle, simple_buffer, sizeof(simple_buffer), &num);
		
		
		if (res == CURLE_OK) {
			if (num == 0) {
				//error 
				websocket->should_stop = true;
				dbg_msg("WS", "curl_easy_recv num = 0");
			} else {
				dbg_msg("WS", "curl_easy_recv: %.*s", num, simple_buffer);
				//data => add to buffer	
				new_buf = (char *) malloc(websocket->buffer_size + num);
						
				if (websocket->buffer) {
					//we got some old data left => copy
					memcpy(new_buf, websocket->buffer, websocket->buffer_size);			
					free(websocket->buffer);
				}
					
				//copy new data:		
				memcpy( new_buf + websocket->buffer_size, simple_buffer, num);
				websocket->buffer = new_buf;				
				websocket->buffer_size += num;
				
				//search for body delimiter
				search = curl_websocket_strnstr(websocket->buffer, "\r\n\r\n", websocket->buffer_size);
			}
		} else if (res == CURLE_AGAIN) {
			//just try again //TODO wait for data (no busy wait)
		} else {
			dbg_msg("WS", "curl_easy_recv else");
			websocket->should_stop = true;
		}
	}
	
	if (search == NULL) {
		//unsuccessfull connection
		websocket->state = WS_ERROR;
		websocket->running = false;
		return CURLE_FAILED_INIT;
	}
	
	
	
	if (search == websocket->buffer+websocket->buffer_size-4) {
		//no more data available
		//we reset the buffer again
		free(websocket->buffer);
		websocket->buffer = 0;
		websocket->buffer_size = 0;
	} else {
		num = (websocket->buffer+websocket->buffer_size-4) - search;
		new_buf = (char *) malloc(num);
		//we got some old data left => copy
		memcpy(new_buf, search+4, num);			
		free(websocket->buffer);
		websocket->buffer = new_buf;				
		websocket->buffer_size = num;
	}
	
	//read incoming websocket data
	websocket->state = WS_HANDLING;
	while (!websocket->should_stop) {
		
		res = curl_easy_recv(websocket->handle, simple_buffer, sizeof(simple_buffer), &num);
		
		if (res == CURLE_OK) {
			if (num == 0) {
				//error 
				websocket->should_stop = true;
			} else {
				//data => add to buffer	
				new_buf = (char *) malloc(websocket->buffer_size + num);
						
				if (websocket->buffer) {
					//we got some old data left => copy
					memcpy(new_buf, websocket->buffer, websocket->buffer_size);			
					free(websocket->buffer);
				}
					
				//copy new data:		
				memcpy( new_buf + websocket->buffer_size, simple_buffer, num);
				websocket->buffer = new_buf;				
				websocket->buffer_size += num;
				
				//call with data
				if (websocket->msg_callback) {
					websocket->msg_callback(websocket->buffer, 1, websocket->buffer_size, websocket->msg_data);
				}
			}
		} else if (res == CURLE_AGAIN) {
			//just try again //TODO wait for data (no busy wait)
		} else {
			websocket->should_stop  = true;
		}
	}
	
	return CURLE_OK;
}

CURLcode curl_websocket_cleanup(struct websocket *websocket) {
	
	if (websocket->buffer) {
		free(websocket->buffer);
		websocket->buffer = 0;
	}
	
	websocket->should_stop = true;
	while (websocket->running) {
		//wait a moment
	}
	free(websocket);
	return CURLE_OK;
}

#endif