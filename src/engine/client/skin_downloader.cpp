#include <base/system.h>
#include <engine/client/skin_downloader.h>
#include <engine/api/net_request.h>

void CSkinDownloader::Init(IAPI *pApi) {
	m_pApi = pApi;
	m_Lock = sys_lock_create();
}

CSkinDownloader::~CSkinDownloader() {
	if (m_Lock) {
		sys_lock_destroy(m_Lock);
	}
}

bool CSkinDownloader::InProgress(const char *pSkinName) {
	sys_lock_wait(m_Lock);
	for(int i = 0; i < m_lpDownloads.size(); ++i) {
		if (str_comp(pSkinName, m_lpDownloads[i]->m_pName) == 0) {
			//we have found our request
			sys_lock_release(m_Lock);
			return true;
		}
	}
	sys_lock_release(m_Lock);
	return false;
}

void CSkinDownloader::Download(const char *pSkinName, SKIN_CALLBACK pCallback, void *pUser) {

	if (InProgress(pSkinName)) {
		return;
	}

	CDownloadRequest *pRequest = new CDownloadRequest();
	pRequest->m_pUser = this;

	char aPath[512];
	str_format(aPath, sizeof (aPath), "http://ddnet.tw/skins/skin/%s.png", pSkinName);
	pRequest->SetPath(aPath);
	pRequest->m_pfnCompCallback = &CSkinDownloader::CompletionCallback;
	pRequest->m_Secure = false; //no need for data only download but TODO: fix this with ssl certificates
	str_format(aPath, sizeof (aPath), "skins/%s.png", pSkinName);
	pRequest->SetFilePath(aPath, IStorage::TYPE_SAVE);

	CSkinDownload *pSkinDownload = new CSkinDownload(pSkinName, pCallback, pUser, pRequest);

	sys_lock_wait(m_Lock);
    m_lpDownloads.add(pSkinDownload);
	sys_lock_release(m_Lock);
	m_pApi->Handle(pRequest);
}

void CSkinDownloader::CompletionCallback(CApiRequest *pRequest, void *pUser) {
	CSkinDownloader *pSkins = (CSkinDownloader *) pUser;
	sys_lock_wait(pSkins->m_Lock);
	for(int i = 0; i < pSkins->m_lpDownloads.size(); ++i) {
		if (pSkins->m_lpDownloads[i]->m_pRequest == pRequest) {
			//we have found our request
			CSkinDownload *pSkinDownload = pSkins->m_lpDownloads[i];
			pSkinDownload->m_pCallback(pSkinDownload->m_pName, IStorage::TYPE_SAVE, pSkinDownload->m_pUser);
		}
	}
	sys_lock_release(pSkins->m_Lock);
}
