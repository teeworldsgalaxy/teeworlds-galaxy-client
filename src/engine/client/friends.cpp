/* (c) Magnus Auvinen. See licence.txt in the root of the distribution for more information. */
/* If you are missing that file, acquire a complete release at teeworlds.com.                */
#include <base/math.h>
#include <base/system.h>

#include <engine/config.h>
#include <engine/console.h>
#include <engine/shared/api.h>
#include <engine/shared/config.h>

#include "friends.h"

CFriends::CFriends()
{
	m_Status = STATUS_OUT_OF_DATE;

}
CFriends::~CFriends()
{
	m_apFriends.delete_all();
	m_apRequests.delete_all();
}

void CFriends::Init(IAPI *pApi)
{
	m_pApi = pApi;
}

const CApiFriend *CFriends::GetFriend(int Index) const
{
	return m_apFriends[Index];
}

const CApiFriend *CFriends::GetRequest(int Index) const
{
	return m_apRequests[Index];
}

IFriends::FRIEND_STATE CFriends::GetFriendState(const char *pName, const char *pClan) const {
    IFriends::FRIEND_STATE Result = FRIEND_NO;
    return FRIEND_NO;
}
CApiFriend::FriendStatus CFriends::GetFriendStatus(const int PlayerID) const
{
    for(int i = 0; i < m_apFriends.size(); i++) {
        const CApiFriend *pInfo =  m_apFriends[i];
        if (pInfo && pInfo->m_pFriend && pInfo->m_pFriend->m_ID == PlayerID) {
            return pInfo->GetStatus();
        }
    }

    for(int i = 0; i < m_apRequests.size(); i++) {
        const CApiFriend *pInfo =  m_apRequests[i];
        if (pInfo && pInfo->m_pPlayer && pInfo->m_pPlayer->m_ID == PlayerID) {
            return pInfo->GetStatus();
        }
    }

    for(int i = 0; i < m_apOwnRequests.size(); i++) {
        const CApiFriend *pInfo =  m_apOwnRequests[i];
        if (pInfo && pInfo->m_pFriend && pInfo->m_pFriend->m_ID == PlayerID) {
            return pInfo->GetStatus();
        }
    }

    return CApiFriend::STATUS_UNKNOWN;
}

bool CFriends::IsFriend(const char *pName, const char *pClan, bool PlayersOnly) const
{
	/*unsigned NameHash = str_quickhash(pName);
	unsigned ClanHash = str_quickhash(pClan);
	for(int i = 0; i < m_apFriends.size(); ++i)
	{
		if(m_apFriends[i]->m_ClanHash == ClanHash &&
			((!PlayersOnly && m_apFriends[i]->m_pName == 0) || m_apFriends[i]->m_NameHash == NameHash))
			return true;
	}*/
	return false;
}

void CFriends::AddFriend(int64 ID)
{
    CJSONRequest *pRequest = new CJSONRequest();
	pRequest->m_pUser = this;
    char aPath[512];
    str_format(aPath, sizeof(aPath), "%s/friends/add", g_Config.m_GalaxyAPIAdress);
    pRequest->SetPath(aPath);
    json_value * pRequestData = json_object_new(0);
    json_object_push(pRequestData, "friend", json_integer_new(ID));
    pRequest->SetRequestData(pRequestData);
    json_value_free(pRequestData);
	pRequest->m_pfnCompCallback = &CFriends::AddFriendCompletionCallback;
	m_pApi->Handle(pRequest);
}

void CFriends::AddFriend(const CApiPlayer &Player)
{
    AddFriend(Player.m_ID);
}

void CFriends::AddFriendCompletionCallback(CApiRequest *pRequest, void *pUser) {
    CFriends *pFriends = (CFriends *) pUser;
    pFriends->Refresh();
}

void CFriends::RemoveFriend(int64 ID)
{
	CJSONRequest *pRequest = new CJSONRequest();
	pRequest->m_pUser = this;
    char aPath[512];
    str_format(aPath, sizeof(aPath), "%s/friends/remove", g_Config.m_GalaxyAPIAdress);
    pRequest->SetPath(aPath);
    json_value * pRequestData = json_object_new(0);
    json_object_push(pRequestData, "friend", json_integer_new(ID));
    pRequest->SetRequestData(pRequestData);
    json_value_free(pRequestData);
	pRequest->m_pfnCompCallback = &CFriends::RemoveFriendCompletionCallback;
	m_pApi->Handle(pRequest);
}

void CFriends::RemoveFriend(const CApiPlayer &Player)
{
    RemoveFriend(Player.m_ID);
}

void CFriends::RemoveFriendCompletionCallback(CApiRequest *pRequest, void *pUser) {
    CFriends *pFriends = (CFriends *) pUser;
    pFriends->Refresh();
}

void CFriends::Refresh() {
	m_Status = IFriends::STATUS_WAITING;
	CJSONRequest *pRequest = new CJSONRequest();
	pRequest->m_pUser = this;
    char aPath[512];
    str_format(aPath, sizeof(aPath), "%s/friends", g_Config.m_GalaxyAPIAdress);
    pRequest->SetPath(aPath);
	pRequest->m_pfnCompCallback = &CFriends::FriendsCallback;
	m_pApi->Handle(pRequest);
}

void CFriends::FriendsCallback(CApiRequest *pRequest, void *pUser) {
	CFriends *pFriends = (CFriends *) pUser;

	CJSONRequest *pParsedRequest = (CJSONRequest *) pRequest;


	if (pParsedRequest->Success()) {
        if (pParsedRequest->m_pPayload) {
            const json_value * pEntries = pParsedRequest->m_pPayload;

			if (pEntries && pEntries != &json_value_none) {
                //clear entries
				pFriends->m_apFriends.delete_all();
				pFriends->m_apRequests.delete_all();
				pFriends->m_apOwnRequests.delete_all();

                const json_value* pFriendEntries = json_object_get(pEntries, "accepted");
                const json_value* pRequestEntries = json_object_get(pEntries, "requesting");
                const json_value* pOwnRequestEntries = json_object_get(pEntries, "requested");

                if (pFriendEntries && pFriendEntries != &json_value_none) {
                    const int size = json_array_length(pFriendEntries);

                    //add friends:
                    for (int i = 0; i < size; ++i) {
                        const json_value * pEntry = json_array_get(pFriendEntries, i);
                        if (pEntry && pEntry != &json_value_none) {
                            CApiFriend *ParsedEntry = new CApiFriend();
                            ParsedEntry->Parse(pEntry);
                            ParsedEntry->m_Status = CApiFriend::STATUS_FRIEND;
                            pFriends->m_apFriends.add(ParsedEntry);
                        }
                    }

                }

                if (pRequestEntries && pRequestEntries != &json_value_none) {
                    const int size = json_array_length(pRequestEntries);

                    //add requests:
                    for (int i = 0; i < size; ++i) {
                        const json_value * pEntry = json_array_get(pRequestEntries, i);
                        if (pEntry && pEntry != &json_value_none) {
                            CApiFriend *ParsedEntry = new CApiFriend();
                            ParsedEntry->Parse(pEntry);
                            ParsedEntry->m_Status = CApiFriend::STATUS_REQUEST;
                            pFriends->m_apRequests.add(ParsedEntry);
                        }
                    }
                }

                if (pOwnRequestEntries && pOwnRequestEntries != &json_value_none) {
                    const int size = json_array_length(pOwnRequestEntries);

                    //add requests:
                    for (int i = 0; i < size; ++i) {
                        const json_value * pEntry = json_array_get(pOwnRequestEntries, i);
                        if (pEntry && pEntry != &json_value_none) {
                            CApiFriend *ParsedEntry = new CApiFriend();
                            ParsedEntry->Parse(pEntry);
                            ParsedEntry->m_Status = CApiFriend::STATUS_OWN_REQUEST;
                            pFriends->m_apOwnRequests.add(ParsedEntry);
                        }
                    }
                }

				pFriends->m_Status = IFriends::STATUS_OK;
				return;
			}
		}
	}

	pFriends->m_Status = IFriends::STATUS_ERROR;
}


void CFriends::OnKernelEvent(CKernelEvent Event) {
	if (Event.m_ID == EVENT_ACCOUNT_LOGIN) {
		OnLogin();
	} else if(Event.m_ID == EVENT_ACCOUNT_LOGOUT) {
		OnLogout();
	}
}
void CFriends::OnLogin() {
	Refresh();
	//TODO: Start Websocket

}

void CFriends::OnLogout() {

}
