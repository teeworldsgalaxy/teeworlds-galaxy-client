#ifndef ENGINE_CLIENT_NEWS_H
#define ENGINE_CLIENT_NEWS_H

#include <engine/news.h>
#include <engine/api.h>
#include <base/tl/array.h>

class CNews : public INews {
	IAPI *m_pApi;

	int m_Status;

	array<CNewsEntry*> m_lpEntries;
	public:
		void Init(IAPI *pApi);
		virtual int GetStatus();
		virtual void Refresh();
		static void CompletionCallback(CApiRequest *pRequest, void *pUser);
		virtual const int NumEntries() const {
			return m_lpEntries.size();
		};

		virtual const CNewsEntry *GetEntry(int Index) const {
			return m_lpEntries[Index];
		};

		virtual void OnKernelEvent(CKernelEvent Event);
};
#endif
