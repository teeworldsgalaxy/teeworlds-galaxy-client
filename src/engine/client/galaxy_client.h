#ifndef ENGINE_CLIENT_GALAXY_CLIENT_H
#define ENGINE_CLIENT_GALAXY_CLIENT_H

#include <engine/client.h>
#include <engine/galaxy_client.h>
#include <engine/client/friends.h>
#include <engine/client/news.h>
#include <engine/shared/api.h>
#include <engine/client/skin_downloader.h>
#include <engine/client/account.h>
#include <engine/client/api_players.h>
#include <engine/shared/updater.h>
#include <engine/client/api_server.h>

class CGalaxyClient : public IGalaxyClient {
	
	class CAPI m_Api;
	class CFriends m_Friends;
	class CNews m_News;
	class CAccount m_Account;
	class CSkinDownloader m_SkinDownloader;
	class CUpdater m_Updater;
	class CApiPlayers m_ApiPlayers;
	class CApiServer m_ApiServer;

    int64 LastUpdate;
	public:
		void Init();
		void RegisterInterfaces();
		virtual IAPI *Api() {return &m_Api;};
		virtual IFriends *Friends() {return &m_Friends;};
		virtual INews *News() {return &m_News;};
		virtual IAccount *Account() {return &m_Account;};
		virtual ISkinDownloader *SkinDownloader() {return &m_SkinDownloader;};
		virtual IUpdater *Updater() {return &m_Updater;};
		virtual IApiPlayers *Players() {return &m_ApiPlayers;};
		virtual IApiServer *Server() {return &m_ApiServer;};
		virtual void Update();
};
#endif