#include <base/system.h>
#include <engine/client/news.h>
#include <engine/api/json_request.h>
#include <engine/shared/config.h>
#include <engine/external/json/json.h>

#include "api_server.h"

CApiServer::CApiServer() {
	m_pServer = 0;
}

CApiServer::~CApiServer() {
	if (m_pServer) {
		delete m_pServer;
	}
}

void CApiServer::Join(const char *pAddress, int ClientID) {
	CJSONRequest *pRequest = new CJSONRequest();
	json_value * pRequestData = json_object_new(0);
	json_object_push(pRequestData, "ip", json_string_new(pAddress));
	json_object_push(pRequestData, "client_id", json_integer_new(ClientID));
	pRequest->SetRequestData(pRequestData);
	json_value_free(pRequestData);
	pRequest->m_pUser = this;
	char aPath[512];
	str_format(aPath, sizeof(aPath), "%s/servers/join", g_Config.m_GalaxyAPIAdress);
	pRequest->SetPath(aPath);
	pRequest->m_pfnCompCallback = &CApiServer::ServerCallback;
	m_Status = IApiService::STATUS_WAITING;
	m_pApi->Handle(pRequest);
}

void CApiServer::Leave() {
	CJSONRequest *pRequest = new CJSONRequest();
    json_value * pRequestData = json_object_new(0);
    pRequest->SetRequestData(pRequestData);
    json_value_free(pRequestData);
	pRequest->m_pUser = this;
	char aPath[512];
	str_format(aPath, sizeof(aPath), "%s/servers/leave", g_Config.m_GalaxyAPIAdress);
	pRequest->SetPath(aPath);
	//pRequest->m_pfnCompCallback = &CApiServer::JoinCallback;
	m_Status = IApiService::STATUS_OK;
	m_pApi->Handle(pRequest);
	//delete current server
	if (m_pServer) {
		delete m_pServer;
		m_pServer = 0;
	}
}


void CApiServer::ServerCallback(CApiRequest *pRequest, void *pUser) {
	CApiServer *pSelf = (CApiServer *) pUser;

	//delete current server
	if (pSelf->m_pServer) {
		delete pSelf->m_pServer;
		pSelf->m_pServer = 0;
	}

	CJSONRequest *pParsedRequest = (CJSONRequest *) pRequest;
	if (pParsedRequest->Success()) {
		if (pParsedRequest->m_pPayload) {
			pSelf->m_pServer = new CApiServerEntity();
			pSelf->m_pServer->Parse(pParsedRequest->m_pPayload);
			pSelf->m_Status = STATUS_OK;
			return;
		}
	}

	pSelf->m_Status = STATUS_ERROR;
}