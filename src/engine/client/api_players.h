#ifndef ENGINE_CLIENT_API_PLAYERS_H
#define ENGINE_CLIENT_API_PLAYERS_H

#include <engine/news.h>
#include <engine/api.h>
#include <base/tl/array.h>
#include <engine/api_players.h>

class CApiPlayers : public IApiPlayers {
	array<CApiPlayer*> m_lpCurrentSearch;
    CApiPlayer* m_pCurrentDetailPlayer;

    CApiRequest *m_pCurrentRequest;
	public:

    CApiPlayers();
    ~CApiPlayers();
	virtual void Reset();

	virtual void Search(const char *Term);
	static void SearchCallback(CApiRequest *pRequest, void *pUser);

	virtual void Detail(int PlayerID);
	static void DetailCallback(CApiRequest *pRequest, void *pUser);

	virtual const int NumEntries() const {
		return m_lpCurrentSearch.size();
	};

	virtual const CApiPlayer *GetEntry(int Index) const {
		return m_lpCurrentSearch[Index];
	};

    virtual const CApiPlayer *GetDetailPlayer() const {
        return m_pCurrentDetailPlayer;
    };

	virtual void OnKernelEvent(CKernelEvent Event);
};
#endif
