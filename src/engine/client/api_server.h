#ifndef ENGINE_CLIENT_API_SERVER_H
#define ENGINE_CLIENT_API_SERVER_H

#include <engine/news.h>
#include <engine/api.h>
#include <base/tl/array.h>
#include <engine/api_server.h>

class CApiServer : public IApiServer {
	CApiServerEntity *m_pServer;
public:

    CApiServer();
	~CApiServer();
	virtual void Join(const char *pAddress, int ClientID);
	virtual void Leave();
	static void ServerCallback(CApiRequest *pRequest, void *pUser);
	virtual void OnKernelEvent(CKernelEvent Event) {

	};
};
#endif
