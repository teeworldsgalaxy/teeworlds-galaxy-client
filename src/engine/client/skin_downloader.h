#ifndef ENGINE_CLIENT_SKIN_DOWNLOADER_H
#define ENGINE_CLIENT_SKIN_DOWNLOADER_H

#include <engine/skin_downloader.h>
#include <engine/api.h>
#include <base/tl/array.h>
class CSkinDownloader : public ISkinDownloader {
	IAPI *m_pApi;

	LOCK m_Lock;
	struct CSkinDownload {

		char *m_pName;
		SKIN_CALLBACK *m_pCallback;
		void *m_pUser;

		CApiRequest *m_pRequest;
		CSkinDownload(const char *pName, SKIN_CALLBACK pCallback, void* pUser, CApiRequest *pRequest) {
			m_pCallback = pCallback;
			m_pName = str_create_source(pName);
			m_pRequest = pRequest;
			m_pUser = pUser;
		}

		~CSkinDownload() {
			mem_free(m_pName);
		}
	};

	array<CSkinDownload*> m_lpDownloads;
	public:
		~CSkinDownloader();
		void Init(IAPI *pApi);
		virtual void Download(const char *pName, SKIN_CALLBACK pCallback, void *pUser);
		bool InProgress(const char *pName);
		static void CompletionCallback(CApiRequest *pRequest, void *pUser);
};
#endif
