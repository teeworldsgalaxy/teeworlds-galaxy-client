#ifndef ENGINE_CLIENT_ACCOUNT_H
#define ENGINE_CLIENT_ACCOUNT_H

#include <engine/account.h>
#include <engine/api.h>
#include <engine/api/api_request.h>
#include <engine/api/json_request.h>

class CAccount : public IAccount {

	// TODO: put this into a better spot and maybe do something neat with inheritance
	class CJSONRequestBuilder
	{
		IAPI *m_pApi;
		CJSONRequest *m_pRequest;
		json_value *m_pData;

	public:
		CJSONRequestBuilder(IAPI *pApi);

		void AddString(const char *pField, const char *pString);
		void Dispatch(const char *pApiEndpoint, REQUEST_COMP pfnCompletionCallback, void *pUser);
	};


	IAPI *m_pApi;

	int m_Status;
    char m_aName[64];

    array<CJSONRequest::CFormError *> m_apFormErrors;

public:
	~CAccount();
	void Init(IAPI *pApi);
	virtual int GetStatus();
	virtual void Login(const char* pUsername, const char *pPassword);
	virtual void Logout();
	virtual void LoginCheck();
	virtual void Register(const char* pUsername, const char* pEmail, const char *pPassword);
	virtual void RequestPassword(const char* pEmail);

	virtual void Join(const NETADDR *pServer);

	virtual const char* GetFormError(const char *pKey);

	static void CompletionCallback(CApiRequest *pRequest, void *pUser);
	void ParseUserData(const json_value *pPayload);
	static void LoginCheckCallback(CApiRequest *pRequest, void *pUser);
	static void FormCallback(CApiRequest *pRequest, void *pUser);
	virtual char *Name() {
		return m_aName;
	};
};
#endif
