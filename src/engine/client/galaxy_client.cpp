
#include "galaxy_client.h"

void CGalaxyClient::RegisterInterfaces() {
	//GalaxyClient is already registered
	Kernel()->RegisterInterface(static_cast<IAPI*>(&m_Api));
	Kernel()->RegisterInterface(static_cast<IUpdater*>(&m_Updater));
	Kernel()->RegisterInterface(static_cast<IAccount*>(&m_Account));
	Kernel()->RegisterInterface(static_cast<IFriends*>(&m_Friends));
	Kernel()->RegisterInterface(static_cast<INews*>(&m_News));
	Kernel()->RegisterInterface(static_cast<ISkinDownloader*>(&m_SkinDownloader));
	Kernel()->RegisterInterface(static_cast<IApiPlayers*>(&m_ApiPlayers));
	Kernel()->RegisterInterface(static_cast<IApiServer*>(&m_ApiServer));
}

//Init Galaxy Client Components
void CGalaxyClient::Init() {
	m_Api.Init();
	m_Updater.Init(&m_Api);
	m_Account.Init(&m_Api);
	m_Friends.Init(&m_Api);
	m_News.Init(&m_Api);
	m_SkinDownloader.Init(&m_Api);
	m_ApiPlayers.SetApi(&m_Api);
	m_ApiServer.SetApi(&m_Api);
    LastUpdate = 0;
}

void CGalaxyClient::Update() {
    
    int64 Now = time_get();
    
    if (Now - LastUpdate > time_freq()*10) {
        if (m_Account.GetStatus() == IAccount::ACCOUNT_STATUS_OK) { 
            m_Friends.Refresh();        
            LastUpdate = Now;
        }
    }

	if (m_Account.GetStatus() == IAccount::ACCOUNT_STATUS_DISCONNECTED && g_Config.m_GalaxyToken[0] != 0 && m_Updater.GetState() == IUpdater::OK) {
		//we have a token but are not logged in?
		// => check this
		m_Account.LoginCheck();
	}
}