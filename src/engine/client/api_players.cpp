#include <base/system.h>
#include <engine/client/news.h>
#include <engine/api/json_request.h>
#include <engine/shared/config.h>
#include <engine/external/json/json.h>

#include "api_players.h"


CApiPlayers::CApiPlayers() {
  m_pCurrentRequest = 0;
    m_pCurrentDetailPlayer = 0;
}


CApiPlayers::~CApiPlayers() {
  m_lpCurrentSearch.delete_all();
}

void CApiPlayers::Reset() {
  m_Status = IApiService::STATUS_OK;
  if (m_pCurrentRequest) {
    m_pCurrentRequest->m_Abort = true;
  }
  m_pCurrentRequest = 0;
  m_pCurrentDetailPlayer = 0;
}

void CApiPlayers::Search(const char *pTerm) {
  m_Status = IApiService::STATUS_WAITING;
	CJSONRequest *pRequest = new CJSONRequest();
	pRequest->m_pUser = this;
  char aPath[512];
  str_format(aPath, sizeof(aPath), "%s/players/search", g_Config.m_GalaxyAPIAdress);
  pRequest->SetPath(aPath);
	pRequest->m_pfnCompCallback = &CApiPlayers::SearchCallback;

  pRequest->CApiRequest::AddQueryData("term", pTerm, true);

  //we might encounter a nullpointer exception here due to (missing) synchronisation
  if (m_pCurrentRequest) {
    m_pCurrentRequest->m_Abort = true;
  }

  m_pCurrentRequest = pRequest;
	m_pApi->Handle(pRequest);
}

void CApiPlayers::SearchCallback(CApiRequest *pRequest, void *pUser) {
    CApiPlayers *pSelf = (CApiPlayers *) pUser;

    if (pSelf->m_pCurrentRequest == pRequest) {
        pSelf->m_pCurrentRequest = 0; // reset current request
    } else {
        //dont parse anymore
        return;
    }

    CJSONRequest *pParsedRequest = (CJSONRequest *) pRequest;

    if (pParsedRequest->Success()) {
        if (pParsedRequest->m_pPayload) {
            const json_value * pEntries = pParsedRequest->m_pPayload;

            if (pEntries && pEntries != &json_value_none) {

                const int size = json_array_length(pEntries);

                //clear entries
                pSelf->m_lpCurrentSearch.delete_all();

                //add entries:
                for (int i = 0; i < size; ++i) {
                    const json_value * pEntry = json_array_get(pEntries, i);
                    if (pEntry && pEntry != &json_value_none) {
                        CApiPlayer *pParsedEntry = new CApiPlayer();
                        pParsedEntry->Parse(pEntry);
                        pSelf->m_lpCurrentSearch.add(pParsedEntry);
                    }
                }

                pSelf->m_Status = STATUS_OK;
                return;
            }
        }
    }

    pSelf->m_Status = STATUS_ERROR;
}

void CApiPlayers::Detail(int PlayerID) {

    m_Status = IApiService::STATUS_WAITING;

    CJSONRequest *pRequest = new CJSONRequest();
    pRequest->m_pUser = this;
    char aPath[512];
    str_format(aPath, sizeof(aPath), "%s/players/%d", g_Config.m_GalaxyAPIAdress, PlayerID);
    pRequest->SetPath(aPath);
    pRequest->m_pfnCompCallback = &CApiPlayers::DetailCallback;

    //we might encounter a nullpointer exception here due to (missing) synchronisation
    if (m_pCurrentRequest) {
        m_pCurrentRequest->m_Abort = true;
    }

    m_pCurrentRequest = pRequest;
    m_pApi->Handle(pRequest);

    //reset current player
    if (m_pCurrentDetailPlayer) {
        delete m_pCurrentDetailPlayer;
        m_pCurrentDetailPlayer = 0;
    }
}

void CApiPlayers::DetailCallback(CApiRequest *pRequest, void *pUser) {
	CApiPlayers *pSelf = (CApiPlayers *) pUser;
  if (pSelf->m_pCurrentRequest == pRequest) {
    pSelf->m_pCurrentRequest = 0; // reset current request
  } else {
    //dont parse anymore
    return;
  }

	CJSONRequest *pParsedRequest = (CJSONRequest *) pRequest;

	if (pParsedRequest->Success()) {
		if (pParsedRequest->m_pPayload) {
			const json_value * pEntry = pParsedRequest->m_pPayload;

            if (pEntry && pEntry != &json_value_none) {
                CApiPlayer *pParsedEntry = new CApiPlayer();
                pParsedEntry->Parse(pEntry);
                pSelf->m_pCurrentDetailPlayer = pParsedEntry;
				pSelf->m_Status = STATUS_OK;
				return;
			}
		}
	}

	pSelf->m_Status = STATUS_ERROR;
}

void CApiPlayers::OnKernelEvent(CKernelEvent Event) {
	if (Event.m_ID == EVENT_ACCOUNT_LOGIN) {

	} else if(Event.m_ID == EVENT_ACCOUNT_LOGOUT) {

	}
}