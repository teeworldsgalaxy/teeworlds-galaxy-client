#include <base/system.h>
#include <base/math.h>
#include <engine/client/news.h>
#include <engine/api/json_request.h>
#include <engine/shared/config.h>
#include <engine/external/json/json.h>

void CNews::Init(IAPI *pApi) {
	m_Status = INews::NEWS_STATUS_OUT_OF_DATE;
	m_pApi = pApi;
}

int CNews::GetStatus() {
	return m_Status;
}

void CNews::Refresh() {
	m_Status = INews::NEWS_STATUS_WAITING;
	CJSONRequest *pRequest = new CJSONRequest();
	pRequest->m_pUser = this;
    char aPath[512];
    str_format(aPath, sizeof(aPath), "%s/news/entries", g_Config.m_GalaxyAPIAdress);
    pRequest->SetPath(aPath);
	pRequest->m_pfnCompCallback = &CNews::CompletionCallback;
	m_pApi->Handle(pRequest);
}

void CNews::CompletionCallback(CApiRequest *pRequest, void *pUser) {
	CNews *pNews = (CNews *) pUser;

	CJSONRequest *pParsedRequest = (CJSONRequest *) pRequest;

	if (pParsedRequest->Success()) {
		if (pParsedRequest->m_pPayload) {
			const json_value * pEntries = pParsedRequest->m_pPayload;

			if (pEntries && pEntries != &json_value_none) {

				const int size = json_array_length(pEntries);

				//clear entries
				pNews->m_lpEntries.delete_all();

				//add entries:
				unsigned int HighestID = 0;
				for (int i = 0; i < size; ++i) {
					const json_value * pEntry = json_array_get(pEntries, i);
					if (pEntry && pEntry != &json_value_none) {
						CNewsEntry *ParsedEntry = new CNewsEntry();
						ParsedEntry->Parse(pEntry);
						HighestID = max(HighestID, ParsedEntry->m_ID);
						pNews->m_lpEntries.add(ParsedEntry);
					}
				}
				// check if we received any new news ;)
				if (g_Config.m_GalaxyLastNewsID < HighestID) {
					pNews->m_Status = INews::NEWS_STATUS_OK;
					// And save value ;)
					g_Config.m_GalaxyLastNewsID = HighestID;
				} else {
					pNews->m_Status = INews::NEWS_STATUS_ALREAD_READ;
				}

				return;
			}
		}
	}

	pNews->m_Status = INews::NEWS_STATUS_ERROR;
}

void CNews::OnKernelEvent(CKernelEvent Event) {
	if (Event.m_ID == EVENT_ACCOUNT_LOGIN) {
		Refresh();
	} else if(Event.m_ID == EVENT_ACCOUNT_LOGOUT) {
	}
}
