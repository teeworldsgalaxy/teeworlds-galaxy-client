/* (c) Magnus Auvinen. See licence.txt in the root of the distribution for more information. */
/* If you are missing that file, acquire a complete release at teeworlds.com.                */
#ifndef ENGINE_CLIENT_FRIENDS_H
#define ENGINE_CLIENT_FRIENDS_H

#include <engine/friends.h>
#include <engine/api.h>
#include <engine/api/meta_include.h>
#include <base/tl/array.h>

class CFriends : public IFriends
{
	array<CApiFriend*> m_apFriends;
	array<CApiFriend*> m_apRequests;
	array<CApiFriend*> m_apOwnRequests;
	IAPI *m_pApi;

	int m_Status;

	protected:
	CTopicWebsocketConnection *m_pWS;
public:
	CFriends();
	~CFriends();

	void Init(IAPI *pApi);
	static void FriendsCallback(CApiRequest *pRequest, void *pUser);
	static void AddFriendCompletionCallback(CApiRequest *pRequest, void *pUser);
	static void RemoveFriendCompletionCallback(CApiRequest *pRequest, void *pUser);

	int GetStatus() { return m_Status; };
	int NumFriends() const { return m_apFriends.size(); };
	int NumRequests() const { return m_apRequests.size(); };

	const CApiFriend *GetFriend(int Index) const;
	const CApiFriend *GetRequest(int Index) const;
	FRIEND_STATE GetFriendState(const char *pName, const char *pClan) const;
	CApiFriend::FriendStatus GetFriendStatus(const int PlayerID) const;
	bool IsFriend(const char *pName, const char *pClan, bool PlayersOnly) const;

	void AddFriend(int64 ID);
	void AddFriend(const CApiPlayer &Player);
	void RemoveFriend(int64 ID);
	void RemoveFriend(const CApiPlayer &Player);
	void Refresh();

	void OnKernelEvent(CKernelEvent Event);

	void OnLogin();
	void OnLogout();
};

#endif
