#include <base/system.h>
#include <engine/api/json_request.h>
#include <engine/client/account.h>
#include <engine/external/json/json.h>
#include <engine/shared/config.h>


// TODO: this helper thingy should go away from here
CAccount::CJSONRequestBuilder::CJSONRequestBuilder(IAPI *pApi) : m_pApi(pApi)
{
	m_pRequest = new CJSONRequest();
	m_pData = NULL;
}

void CAccount::CJSONRequestBuilder::AddString(const char *pField, const char *pString)
{
	if(m_pData == NULL)
		m_pData = json_object_new(0);
	json_object_push(m_pData, pField, json_string_new(pString));
}

void CAccount::CJSONRequestBuilder::Dispatch(const char *pApiEndpoint, REQUEST_COMP pfnCompletionCallback, void *pUser)
{
	char aPath[512];
	str_format(aPath, sizeof(aPath), "%s/%s", g_Config.m_GalaxyAPIAdress, pApiEndpoint);
	m_pRequest->SetPath(aPath);
	m_pRequest->m_pfnCompCallback = pfnCompletionCallback;
	m_pRequest->m_pUser = pUser;
	if(m_pData)
	{
		m_pRequest->SetRequestData(m_pData);
		json_value_free(m_pData);
	}
	m_pApi->Handle(m_pRequest);
}



void CAccount::Init(IAPI *pApi) {
	m_Status = IAccount::ACCOUNT_STATUS_DISCONNECTED;
	m_pApi = pApi;
	m_aName[0] = 0;
	LoginCheck();
}

int CAccount::GetStatus() {
	return m_Status;
}

CAccount::~CAccount() {
    m_apFormErrors.delete_all();
}

void CAccount::Login(const char *pUsername, const char *pPassword) {

	if(str_length(pUsername) == 0 || str_length(pPassword) == 0) {
		m_Status = IAccount::ACCOUNT_STATUS_ERROR;
	} else {
		m_Status = IAccount::ACCOUNT_STATUS_WAITING;
		CJSONRequestBuilder Builder(m_pApi);
		Builder.AddString("name", pUsername);
		Builder.AddString("password", pPassword);
		Builder.Dispatch("accounts/login", (REQUEST_COMP)&CAccount::FormCallback, (void*)this);
	}
}

void CAccount::LoginCheck() {
	if (g_Config.m_GalaxyToken[0] == 0) {
		m_Status = IAccount::ACCOUNT_STATUS_OFFLINE;
		return;
	}

	if (m_Status != IAccount::ACCOUNT_STATUS_OK) {
		m_Status = IAccount::ACCOUNT_STATUS_WAITING;
	}

	CJSONRequestBuilder Builder(m_pApi);
	Builder.Dispatch("accounts/login_check", (REQUEST_COMP)&CAccount::LoginCheckCallback, (void*)this);
}

void CAccount::Logout()
{

	dbg_msg("Logout", "Logging out");
	CJSONRequestBuilder Builder(m_pApi);
	Builder.Dispatch("logout", 0, (void*)this); // No callback needed

	m_Status = IAccount::ACCOUNT_STATUS_DISCONNECTED;
	m_aName[0] = '\0';
	g_Config.m_GalaxyToken[0] = 0;
	// dispatch logout event
	CKernelEvent LogoutEvent(EVENT_ACCOUNT_LOGOUT);
	Kernel()->Dispatch(LogoutEvent);
}

void CAccount::Register(const char *pUsername, const char *pEmail, const char *pPassword) {

	if(str_length(pUsername) == 0 || str_length(pEmail) == 0 || str_length(pPassword) == 0) {
		m_Status = IAccount::ACCOUNT_STATUS_ERROR;
	} else {
		m_Status = IAccount::ACCOUNT_STATUS_WAITING;
		CJSONRequestBuilder Builder(m_pApi);
		Builder.AddString("name", pUsername);
		Builder.AddString("email", pEmail);
		Builder.AddString("password", pPassword);
		Builder.Dispatch("accounts/register", (REQUEST_COMP)&CAccount::FormCallback, (void*)this);
	}
}

void CAccount::RequestPassword(const char *pEmail)
{
	if(!pEmail || str_length(pEmail) == 0) {
		m_Status = IAccount::ACCOUNT_STATUS_ERROR;
	} else {
		m_Status = IAccount::ACCOUNT_STATUS_WAITING;
		CJSONRequestBuilder Builder(m_pApi);
		Builder.AddString("email", pEmail);
		Builder.Dispatch("password-request", (REQUEST_COMP)&CAccount::FormCallback, (void*)this);
	}
}

void CAccount::Join(const NETADDR *pServer)
{
	// TODO
}

void CAccount::ParseUserData(const json_value *pPayload) {

	if(json_array_length(pPayload) > 0)
	{

        const json_value *pToken = json_object_get(pPayload, "token");
        if(pToken && pToken != &json_value_none)
		{
            str_copy(g_Config.m_GalaxyToken, json_string_get(pToken), sizeof(g_Config.m_GalaxyToken));
        } else if(g_Config.m_GalaxyToken[0] == 0){
            //no more to parse here
            return;
        }

        const json_value *pUserData = json_object_get(pPayload, "player");
        if(pUserData && pUserData != &json_value_none)
		{
            const json_value *pName = json_object_get(pUserData, "name");

            if(pName != &json_value_none)
			{
                if (m_Status == IAccount::ACCOUNT_STATUS_OK && str_comp_nocase(m_aName, json_string_get(pName))) {
                    //account name mismatch
					Logout();
					return;
				}
				str_copy(m_aName, json_string_get(pName), sizeof(m_aName));
				str_copy(g_Config.m_PlayerName, m_aName, sizeof(g_Config.m_PlayerName));
				m_Status = IAccount::ACCOUNT_STATUS_OK;
				// dispatch Login Event
				CKernelEvent LoginEvent(EVENT_ACCOUNT_LOGIN);
				Kernel()->Dispatch(LoginEvent);
			}
		}
	}
	else
	{
		Logout();
	}
}

void CAccount::FormCallback(CApiRequest *pRequest, void *pUser)
{
	CJSONRequest *pParsedRequest = (CJSONRequest *)pRequest;
	CAccount *pAccount = (CAccount *)pUser;

	// clear errors
	pAccount->m_apFormErrors.delete_all();
	if(pParsedRequest->Success())
	{
		if(pParsedRequest->m_pPayload)
		{
			pAccount->ParseUserData(pParsedRequest->m_pPayload);
		} else {
			pAccount->Logout();
		}
	}
	else
	{
		pAccount->m_Status = ACCOUNT_STATUS_ERROR;
		dbg_msg("acc/error", "server reported %i error%s:", pParsedRequest->m_apFormErrors.size(), pParsedRequest->m_apFormErrors.size() != 1 ? "s" : "");

		// save errors
		for(int i = 0; i < pParsedRequest->m_apFormErrors.size(); ++i)
		{
			pAccount->m_apFormErrors.add(
					new CJSONRequest::CFormError(pParsedRequest->m_apFormErrors[i]->m_pKey, pParsedRequest->m_apFormErrors[i]->m_pMessage)
			);
			dbg_msg("acc/error", "\t%s : %s", pParsedRequest->m_apFormErrors[i]->m_pKey, pParsedRequest->m_apFormErrors[i]->m_pMessage);
		}
	}
}

void CAccount::LoginCheckCallback(CApiRequest *pRequest, void *pUser)
{
	CJSONRequest *pParsedRequest = (CJSONRequest *)pRequest;
	CAccount *pAccount = (CAccount *)pUser;
	dbg_msg("LoginCheckCallback start", "%d", pAccount->m_Status);

	if(pParsedRequest->Success() && pParsedRequest->m_pPayload)
	{
        pAccount->ParseUserData(pParsedRequest->m_pPayload);
	}
	else
	{
		pAccount->Logout();
	}
}


const char *CAccount::GetFormError(const char *pKey)
{
	if(!pKey)
		return 0;

	for(int i = 0; i < m_apFormErrors.size(); ++i)
		if(m_apFormErrors[i]->m_pKey && !str_comp_nocase(pKey, m_apFormErrors[i]->m_pKey))
			return m_apFormErrors[i]->m_pMessage;

	return 0;
}
