#ifndef ENGINE_API_H
#define ENGINE_API_H

#include "engine/api/base_include.h"

class IAPI : public IInterface {
	MACRO_INTERFACE("api", 0)
	public:
		virtual void Handle(CApiRequest *pRequest) = 0;
		virtual IStorage *Storage() = 0;
};

#endif
