#ifndef ENGINE_API_SERVICE_H
#define ENGINE_API_SERVICE_H

#include <engine/kernel.h>
#include <engine/external/json/json.h>
#include <base/tl/promise.h>


struct CApiEntity {
  unsigned int m_ID;
  
  enum {
    TYPE_BASE = 0
  };
  
  CApiEntity () {
    m_ID = 0;
  };
  
  virtual ~CApiEntity() {
    
  };  
  
  virtual int GetType() {
    return TYPE_BASE;
  }

  virtual bool Parse(const json_value *pEntry) {
    return false;
  }
};


class IAPI;

class IApiService : public IInterface {
	
	//MACRO_INTERFACE("api_service", 0)
  protected:
    IAPI *m_pApi;
    int m_Status;
	public:
  
    void SetApi(IAPI *pApi) {
       m_pApi = pApi;
    };
    
    enum {
        STATUS_OUT_OF_DATE = 0,
        STATUS_WAITING,
        STATUS_ERROR,
        STATUS_OK,
        NUM_STATUS
    };
    
    IApiService() {
      m_Status = STATUS_OK;
    };
		
    virtual int GetStatus() {
        return m_Status;
    };
};
#endif