#pragma once

#include "base_include.h"
#include "file_request.h"

// we might want to add a upload request later
struct CDownloadRequest : public CFileRequest {

	CDownloadRequest() {
		Init();
	}

	CDownloadRequest(const char* aPath, REQUEST_TYPE Type) : CFileRequest(aPath, Type) {
		Init();
		m_Type = Type;
		SetPath(aPath);
	}

	void Init() {
		m_Type = REQUEST_TYPE_DOWNLOAD;
	}
};