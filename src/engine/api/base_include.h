#pragma once
#include <base/system.h>
#include <base/tl/array.h>
#include <base/tl/list_thread_safe.h>
#include <engine/kernel.h>
#include <engine/external/json/json.h>
#include <engine/external/json/json-builder.h>
#include <engine/storage.h>
#include <engine/shared/config.h>

struct CApiRequest;
struct CWebsocketConnection;

typedef void (*REQUEST_PROG)(CApiRequest *pRequest, void *pUser);
typedef void (*REQUEST_COMP)(CApiRequest *pRequest, void *pUser);
typedef void (*WS_MESSAGE) (CWebsocketConnection *pWSCon, void *pUser);
