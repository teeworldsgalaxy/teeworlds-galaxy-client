#pragma once

#include "base_include.h"
#include "api_request.h"
#include <engine/api.h>

struct CFileRequest : public CApiRequest {
	IOHANDLE m_File;
	int m_StorageType;
	char *m_pFilePath;

	bool m_Overriding;

	IStorage *m_pStorage;

	CFileRequest() {
		Init();
	}

	CFileRequest(const char* aPath, REQUEST_TYPE Type) : CApiRequest(aPath, Type) {
		Init();
		m_Type = Type;
		SetPath(aPath);
	}

	void Init() {
		m_File = 0;
		m_StorageType = IStorage::TYPE_SAVE;
		m_pFilePath = 0;
		m_Type = REQUEST_TYPE_FILE;
		m_Overriding = false;
        m_UseToken = false;
	}

	virtual void SetFilePath(const char* pFilePath, int Type) {
		sys_lock_wait(m_Lock);
		m_pFilePath = str_create_source(pFilePath);
		m_StorageType = Type;
		m_Type = REQUEST_TYPE_DOWNLOAD;
		sys_lock_release(m_Lock);
	}


	virtual void Error(bool AbortedByCallback = false) {
		CApiRequest::Error(AbortedByCallback);
		//we need to clear up files
		if (m_File) {
			io_close(m_File);
			//delete file here
			m_pStorage->RemoveFile(m_pFilePath, m_StorageType);
			m_File = 0;
		}
	}

	virtual size_t AddData(void *pData, size_t Size)
	{
		char aPath[1024] = {0};
		if(m_File == 0 && m_Overriding)
		{
			// we try to open the file just now to prevent data loss
			m_pStorage->GetCompletePath(m_StorageType, m_pFilePath, aPath, sizeof(aPath));
			if(aPath[0] == '\0')
				str_copy(aPath, m_pFilePath, sizeof(aPath));

			m_File = io_open_safe(aPath, IOFLAG_WRITE);
		}

		if(m_File == 0)
		{
			// file could not be opened
			dbg_msg("API-Request", "AddData: failed to open file for writing: '%s'", aPath);
			m_Abort = true;
			return 0;
		}
		else
		{
			io_write(m_File, pData, Size);
			return Size;
		}
	}

	virtual void Prepare(IAPI *pApi) {
		CApiRequest::Prepare(pApi);

		m_pStorage = pApi->Storage();
		char aPath[1024];
		m_pStorage->GetCompletePath(m_StorageType, m_pFilePath, aPath, sizeof(aPath));

		if (fs_exists(aPath))
		{
			m_Overriding = true; // file exists so we should only open it if we got data!
			if(g_Config.m_Debug)
				dbg_msg("API:filerequest", "prepare: '%s' does already exist!", aPath);
		}
		else
		{
			if(aPath[0] == '\0')
				str_copy(aPath, m_pFilePath, sizeof(aPath));
			m_File = io_open_safe(aPath, IOFLAG_WRITE);
			if(!m_File)
				dbg_msg("API:filerequest", "prepare: couldn't open file '%s' for writing", aPath);
		}
	}
	virtual void Complete()
	{
		CApiRequest::Complete();
		if(m_File)
		{
			io_close(m_File);
			m_State = REQUEST_STATE_FINISHED;
			m_File = 0;
			return;
		}
		m_State = REQUEST_STATE_ERROR;
	}

	virtual ~CFileRequest()
	{
		if(m_File)
		{
			io_close(m_File);
			m_File = 0;
		}
		mem_free(m_pFilePath);
	}
};
