#pragma once

#include "base_include.h"
#include "data_request.h"

struct CJSONRequest : public CDataRequest
{
	json_value *m_pParsedData;
	const json_value *m_pPayload;

	struct CFormError
	{

		const char *m_pKey;
		const char *m_pMessage;

		CFormError()
		{
			Init();
		}

		CFormError(const char *pKey, const char *pMessage)
		{
			Init();
			m_pKey = str_create_source(pKey);
			m_pMessage = str_create_source(pMessage);
		}

		void Init()
		{
			m_pKey = 0;
			m_pMessage = 0;
		}

		~CFormError()
		{
			mem_free((void *)m_pKey);
			mem_free((void *)m_pMessage);
		}
	};

	array<CFormError *> m_apFormErrors;

	CJSONRequest()
	{
		Init();
	}

	CJSONRequest(const char *aPath, REQUEST_TYPE Type) : CDataRequest(aPath, Type)
	{
		Init();
		m_Type = Type;
		SetPath(aPath);
	}

	void Init()
	{
		m_pParsedData = 0;
		m_pPayload = 0;
		m_Type = REQUEST_TYPE_JSON;
		m_FailOnError = false;
		AddHeader("Accept: application/json");
	}

	virtual ~CJSONRequest()
	{
		if(m_pParsedData)
		{
			json_value_free(m_pParsedData);
		}
		for(int i = 0; i < m_apFormErrors.size(); ++i)
		{
			delete m_apFormErrors[i];
			m_apFormErrors[i] = 0;
		}
	}

	virtual void SetRequestData(json_value *pJsonData)
	{
		char *pData = (char *)mem_alloc(json_measure(pJsonData), 1);

		const json_serialize_opts Options =
				{
						json_serialize_mode_packed,
						0,
						3  /* indent_size */
				};
		json_serialize_ex(pData, pJsonData, Options);
		//dbg_msg("API-Request", "Added Data: %s", pData);
		CDataRequest::SetRequestData(pData);
		AddHeader("Content-type: application/json");
	}

	virtual void AddQueryData(const char *pKey, json_value *pJsonData)
	{
		char *pData = (char *)mem_alloc(json_measure(pJsonData), 1);

		const json_serialize_opts Options =
				{
						json_serialize_mode_packed,
						0,
						3  /* indent_size */
				};
		json_serialize_ex(pData, pJsonData, Options);
		//dbg_msg("API-Request", "Added Data: %s", pData);
		CDataRequest::AddQueryData(pKey, pData, true);
		mem_free(pData);
	}

	virtual void Complete()
	{
		CDataRequest::Complete();

		m_State = REQUEST_STATE_FINISHED;

		if(m_pData)
		{
			// parse data
			m_pParsedData = json_parse((json_char *)m_pData, m_DataSize);

			if(m_pParsedData && m_pParsedData != &json_value_none)
			{
				const json_value *pErrors = json_object_get(m_pParsedData, "errors");
				if(pErrors && pErrors != &json_value_none)
				{
					if(pErrors->type == json_object) // the old code - TODO: remove this
					{
						// loop through errors
						for(int i = 0; i < pErrors->u.object.length; ++i)
						{
							const json_value *pMessages = pErrors->u.object.values[i].value;
							const int size = json_array_length(pMessages);
							if(size)
							{
								CFormError *pError = new CFormError(pErrors->u.object.values[i].name, json_string_get(json_array_get(pErrors->u.object.values[i].value, 0)));
								m_apFormErrors.add(pError);
							}
						}
					}
					else if(pErrors->type == json_array) // the new code
					{
						// loop through errors
						for(int i = 0; i < json_array_length(pErrors); ++i)
						{
							const json_value *entry = json_array_get(pErrors, i);

							if(entry->type == json_object)
							{
								// loop through errors
								for(int j = 0; j < entry->u.object.length; ++j)
								{
									CFormError *pError = new CFormError(
											entry->u.object.values[j].name,
											json_string_get(entry->u.object.values[j].value)
									);
									m_apFormErrors.add(pError);
								}
							}
							else
							{
								char aBuf[16];
								str_format(aBuf, sizeof(aBuf), "%i", i);
								CFormError *pError = new CFormError(aBuf, json_string_get(entry));
								m_apFormErrors.add(pError);
							}
						}
					}
				} else {
					m_pPayload = m_pParsedData;
					return;
				}
			}
			m_State = REQUEST_STATE_ERROR;
		} else {
            m_pParsedData = 0;
		}
	}
};
