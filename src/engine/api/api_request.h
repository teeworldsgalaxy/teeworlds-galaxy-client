#pragma once

#include "base_include.h"

struct CApiRequest
{
	friend class IAPI;

	enum REQUEST_STATE {
		REQUEST_STATE_INIT = 0,
		REQUEST_STATE_SENDING,
		REQUEST_STATE_WAITING,
		REQUEST_STATE_PARSING,
		REQUEST_STATE_FINISHED,
		REQUEST_STATE_ERROR,
		REQUEST_STATE_ABORTED,
		NUM_REQUEST_STATES
	};

	enum REQUEST_TYPE {
		REQUEST_TYPE_GET = 0,
		REQUEST_TYPE_POST,
		REQUEST_TYPE_FILE,
		REQUEST_TYPE_JSON,
		REQUEST_TYPE_DATA,
		REQUEST_TYPE_DOWNLOAD,
		REQUEST_TYPE_UPLOAD,
		REQUEST_TYPE_WEBSOCKET,
		NUM_REQUEST_TYPES
	};

	REQUEST_STATE m_State;
	REQUEST_TYPE m_Type;

	double m_Current;
	double m_Size;
	double m_Progress;

	REQUEST_PROG m_pfnProgressCallback;
	REQUEST_COMP m_pfnCompCallback;

	void *m_pUser;

	bool m_Abort;

	bool m_CanTimeout;

	char *m_pPath;

	bool m_Secure;

	long m_StatusCode;

	const char *m_pRequestData;
	class IAPI *m_pApi;

	struct curl_slist *m_pHeaderList;

	unsigned int m_RequestDataSize;

	bool m_UseToken;

	bool m_HeadersOnly;

	bool m_FailOnError;

	// TODO: move this out here, same goes for responseheaders
	struct CKeyValue
	{
		const char *m_pKey;
		const char *m_pValue;

		bool m_CascadeDeletion;

		CKeyValue()
		{
			Init();
		}

		CKeyValue(const char *pKey, const char *pValue, bool Copy = true, bool CascadeDeletion = true)
		{
			Init();
			if(Copy)
			{
				m_pKey = str_create_source(pKey);
				m_pValue = str_create_source(pValue);
				m_CascadeDeletion = true;
			}
			else
			{
				m_pKey = pKey;
				m_pValue = pValue;
				m_CascadeDeletion = CascadeDeletion;
			}
		}

		void Init()
		{
			m_pKey = 0;
			m_pValue = 0;
			m_CascadeDeletion = true;
		}

		~CKeyValue()
		{
			if(m_CascadeDeletion)
			{
				if(m_pKey)
				{
					mem_free((void *)m_pKey);
				}
				if(m_pValue)
				{
					mem_free((void *)m_pValue);
				}
			}
		}

	};

	struct CResponseHeader
	{
		const char *m_pKey;
		const char *m_pValue;

		CResponseHeader()
		{
			Init();
		}

		CResponseHeader(const char *pData, size_t Size)
		{
			Init();
			if(pData && *pData && *pData != '\n')
			{

				char *pTemp = (char *)mem_alloc(Size, 1);
				str_copy(pTemp, pData, Size);
				const char *pDelimiter = str_find(pTemp, ":");
				if(pDelimiter)
				{
					pTemp[(pDelimiter - pTemp)] = 0; //delimit key from header
					m_pKey = pTemp;
					if(*(pDelimiter + 1) == ' ')
					{
						m_pValue = pDelimiter + 2;
					}
					else
					{
						m_pValue = pDelimiter + 1;
					}
					//dbg_msg("Header:", "Key: %s, Value: %s", m_pKey, m_pValue);
				}
				else
				{

					m_pValue = pTemp;
					//dbg_msg("Header:", "Value: %s", m_pValue);
				}
			}
		}

		void Init()
		{
			m_pKey = 0;
			m_pValue = 0;
		}

		~CResponseHeader()
		{
			//data is just allocated in one variable
			if(m_pKey)
			{
				mem_free((void *)m_pKey);
			}
			else
			{
				mem_free((void *)m_pValue);
			}
		}
	};
	array<CResponseHeader *> m_apResponseHeaders;
	array<CKeyValue*> m_aQueryData;

	CApiRequest() {
		Init();
	}

	CApiRequest(const char* aPath, REQUEST_TYPE Type) {
		Init();
		m_Type = Type;
		SetPath(aPath);
	}

	void Init() {
		m_State = REQUEST_STATE_INIT;
		m_Type = REQUEST_TYPE_GET;
		m_Lock = sys_lock_create();
		m_pPath = 0;
		m_Abort = false;
		m_pfnProgressCallback = 0;
		m_pfnCompCallback = 0;
		m_pUser = 0;
		m_Current = 0.0f;
		m_Size = 0.0f;
		m_Progress = 0.0f;
		m_CanTimeout = true;
		m_Secure = true;
		m_pRequestData = 0;
		m_pHeaderList = 0;
		m_UseToken = true;
		m_HeadersOnly = false;
		m_FailOnError = true;
		//dbg_msg("CApiRequest", "INIT");
	}

	virtual ~CApiRequest() {
		sys_lock_wait(m_Lock);
		mem_free(m_pPath);
		mem_free((void*)m_pRequestData);
		ClearHeaders();
		for(int i = 0; i < m_apResponseHeaders.size(); ++i) {
			delete m_apResponseHeaders[i];
			m_apResponseHeaders[i] = 0;
		}
		for(int i = 0; i < m_aQueryData.size(); ++i) {
			delete m_aQueryData[i];
			m_aQueryData[i] = 0;
		}
		sys_lock_destroy(m_Lock);
	}

	virtual void SetPath(const char* aPath) {
		sys_lock_wait(m_Lock);
		m_pPath = str_create_source(aPath);
		sys_lock_release(m_Lock);
	}

	virtual void AddQueryData(const char* pKey, const char* pValue, bool Copy = false, bool CascadeDeletion = false) {

		CKeyValue *pData = new CKeyValue(pKey, pValue, Copy, CascadeDeletion);
		m_aQueryData.add(pData);
	}

	virtual void SetRequestData(const char* pData, bool Copy = false) {
		if (Copy) {
			m_pRequestData = str_create_source(pData);
		} else {
			m_pRequestData = pData;
		}

		m_RequestDataSize = str_length(m_pRequestData);
	}

	virtual void Prepare(class IAPI *pApi) {
		m_pApi = pApi;
		m_State = REQUEST_STATE_WAITING;

		if (m_UseToken && g_Config.m_GalaxyToken[0]) {
			char aBuf[4096];
			str_format(aBuf, sizeof(aBuf), "Authorization: BEARER %s", g_Config.m_GalaxyToken);
			AddHeader(aBuf);
		}
	}

	virtual bool Success() {
		return m_State == REQUEST_STATE_FINISHED && m_StatusCode < 400 ;
	}

	virtual size_t AddData(void *pData, size_t Size) {
		//Note that we did not really deal with any data
		return Size;
	}

	//TODO why dont we cache this instead?
	virtual unsigned int GetContentSize() {
		for(int i = 0; i < m_apResponseHeaders.size(); ++i) {
			if (m_apResponseHeaders[i]->m_pKey && !str_comp_nocase(m_apResponseHeaders[i]->m_pKey, "Content-Length")) {
				return str_toint(m_apResponseHeaders[i]->m_pValue);
			}
		}
		return 0;
	}


	virtual void Error(bool AbortedByCallback = false) {
		if (AbortedByCallback) {
			m_State = REQUEST_STATE_ABORTED;
		} else {
			m_State = REQUEST_STATE_ERROR;
		}

		dbg_msg("API-Request", "%p %s", this, AbortedByCallback ? "Aborted" : "Error");
	}

	virtual void Complete() {
		//dbg_msg("API-Request", "Complete");
		m_State = REQUEST_STATE_FINISHED;
	}

	virtual void AddHeader(const char *pHeader);

	virtual void ClearHeaders();

protected:
	LOCK m_Lock;
};
