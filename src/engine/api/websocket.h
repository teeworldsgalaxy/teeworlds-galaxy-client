#pragma once

#include <engine/external/json/json.h>
#include <engine/external/json/json.h>
#include <base/system.h>
#include "api_request.h"

struct CWebsocketMessage {
    CWebsocketMessage() {
        m_pData = 0;
        m_Size = 0;
    }
    CWebsocketMessage(const char *pData) {
        m_pData = pData;
        m_Size = str_length(pData);
    }
    ~CWebsocketMessage() {
        mem_free((void*) m_pData);
        m_Size = 0;
    }
    const char *m_pData;
    size_t m_Size;
};

struct CWebsocketConnection : public CApiRequest {

    list_ts<const CWebsocketMessage *> m_lpRecv; // The received messages
    list_ts<const CWebsocketMessage *> m_lpSend; // The messages to be sent!

    const CWebsocketMessage* GetNextSendMessage() {
        Maybe<const CWebsocketMessage *> Front = m_lpSend.get_front();
        if (Front.IsFull()) {
            return Front.Value;
        } else {
            return nullptr;
        }
    };

    const CWebsocketMessage* GetNextReceivedMessage() {
        Maybe<const CWebsocketMessage *> Front = m_lpRecv.get_front();
        if (Front.IsFull()) {
            return Front.Value;
        } else {
            return nullptr;
        }
    };

    void Send(const CWebsocketMessage* pMessage) {
        m_lpSend.push_back(pMessage);
    }

    void Receive(const CWebsocketMessage* pMsg) {
        m_lpRecv.push_back(pMsg);
    };

    void Init() {
        m_Type = REQUEST_TYPE_WEBSOCKET;
    }

    CWebsocketConnection() {
        Init();
    }

    CWebsocketConnection(const char* aPath) : CApiRequest(aPath, REQUEST_TYPE_WEBSOCKET) {
        Init();
        SetPath(aPath);
    }
};


class CTopicWebsocketConnection : public CWebsocketConnection {

public:
    struct CTopicMessage {
        char *m_pMessageRef = nullptr;
        int  m_MessageID = -1;
        char *m_pTopic = nullptr;
        char *m_pEvent = nullptr;
        const json_value *m_pData = nullptr;
        json_value *m_pSendData = nullptr;

        json_value *m_pJsonArr = nullptr;

        CTopicMessage() {}

        CTopicMessage(json_value *pJsonArr) {
            m_pJsonArr = pJsonArr;
        }

        ~CTopicMessage() {
            mem_free(m_pMessageRef);
            mem_free(m_pTopic);
            mem_free(m_pEvent);
            json_value_free(m_pJsonArr);
            json_value_free(m_pSendData);
        }

        static CTopicMessage *FromWebsocketMessage(const CWebsocketMessage *pMessage) {

            json_value *pJsonArr = json_parse(pMessage->m_pData, pMessage->m_Size);

            CTopicMessage *pTopicMessage = new CTopicMessage(pJsonArr);

            if (json_array_length(pJsonArr) >= 5) {

                const json_value *pValue = json_array_get(pJsonArr, 0);
                if (pValue->type == json_string) {
                    pTopicMessage->m_pMessageRef = str_create_source(json_string_get(pValue));
                }

                pValue = json_array_get(pJsonArr, 1);
                if (pValue->type == json_integer) {
                    pTopicMessage->m_MessageID = json_int_get(pValue);
                }

                pValue = json_array_get(pJsonArr, 2);
                if (pValue->type == json_string) {
                    pTopicMessage->m_pTopic = str_create_source(json_string_get(pValue));
                }

                pValue = json_array_get(pJsonArr, 3);
                if (pValue->type == json_string) {
                    pTopicMessage->m_pEvent = str_create_source(json_string_get(pValue));
                }

                pTopicMessage->m_pData = json_array_get(pJsonArr, 4);
                return pTopicMessage;
            }
            return nullptr;
        }

        static CWebsocketMessage *ToWebsocketMessage(CTopicMessage *pMessage) {
            json_value *pJsonArr = json_array_new(5);

            json_array_push(pJsonArr, pMessage->m_pMessageRef ? json_string_new(pMessage->m_pMessageRef) : json_null_new());
            json_array_push(pJsonArr, pMessage->m_MessageID >= 0 ? json_integer_new(pMessage->m_MessageID) : json_null_new());
            json_array_push(pJsonArr, pMessage->m_pTopic ? json_string_new(pMessage->m_pTopic) : json_null_new());
            json_array_push(pJsonArr, pMessage->m_pEvent ? json_string_new(pMessage->m_pEvent) : json_null_new());
            json_array_push(pJsonArr, pMessage->m_pSendData ? pMessage->m_pSendData : json_null_new());

            const json_serialize_opts Options =
                    {
                            json_serialize_mode_packed,
                            0,
                            3  /* indent_size */
                    };
            char *pData = (char *)mem_alloc(json_measure_ex(pJsonArr, Options), 1);

            json_serialize_ex(pData, pJsonArr, Options);

            pMessage->m_pSendData = nullptr; // do not free twice ;)
            json_builder_free(pJsonArr);
            return new CWebsocketMessage(pData);
        }
    };

    void Subscribe(const char *pTopic) {};
    void Unsubscribe(const char *pTopic) {};

    void SendTopicMessage(CTopicMessage *pMsg) {
        Send(CTopicMessage::ToWebsocketMessage(pMsg));
    }

    CTopicMessage *GetNextReceiveTopicMessage() {
        const CWebsocketMessage *pMsg = GetNextReceivedMessage();
        return pMsg != nullptr ? CTopicMessage::FromWebsocketMessage(pMsg) : nullptr;
    }
};
