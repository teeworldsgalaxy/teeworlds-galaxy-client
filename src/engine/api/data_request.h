#pragma once

#include "base_include.h"
#include "api_request.h"

struct CDataRequest : public CApiRequest {
	void *m_pData;
	size_t m_DataSize;

	CDataRequest() {
		Init();
	}

	CDataRequest(const char* aPath, REQUEST_TYPE Type) : CApiRequest(aPath, Type) {
		Init();
		m_Type = Type;
		SetPath(aPath);
	}

	void Init() {
		m_pData = 0;
		m_DataSize = 0;
		m_Type = REQUEST_TYPE_DATA;
	}

	virtual size_t AddData(void *pData, size_t Size) {
		char *pNewData = (char *) mem_alloc(m_DataSize + Size, 1);

		if (m_pData) {
			//we got some old data left => copy
			mem_copy(pNewData, m_pData, m_DataSize);
			mem_free(m_pData);
		}

		//copy new data:
		mem_copy( pNewData + m_DataSize, pData, Size);
		m_pData = pNewData;

		m_DataSize += Size;
		return Size;
	}

	virtual ~CDataRequest() {
		mem_free(m_pData);
	}
};
