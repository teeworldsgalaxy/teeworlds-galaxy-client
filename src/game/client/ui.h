/* (c) Magnus Auvinen. See licence.txt in the root of the distribution for more information. */
/* If you are missing that file, acquire a complete release at teeworlds.com.                */
#ifndef GAME_CLIENT_UI_H
#define GAME_CLIENT_UI_H
#include <base/vmath.h>
#include <base/system.h>
class CUIRect
{
	// TODO: Refactor: Redo UI scaling
	float Scale() const;
public:
	float x, y, w, h;

	void HSplitMid(CUIRect *pTop, CUIRect *pBottom) const;
	void HSplitPerc(float perc, CUIRect *pTop, CUIRect *pBottom) const;
	void HSplitTop(float Cut, CUIRect *pTop, CUIRect *pBottom) const;
	void HSplitBottom(float Cut, CUIRect *pTop, CUIRect *pBottom) const;
	void VSplitMid(CUIRect *pLeft, CUIRect *pRight) const;
	void VSplitPerc(float perc, CUIRect *pLeft, CUIRect *pRight) const;
	void VSplitLeft(float Cut, CUIRect *pLeft, CUIRect *pRight) const;
	void VSplitRight(float Cut, CUIRect *pLeft, CUIRect *pRight) const;

	void Margin(float Cut, CUIRect *pOtherRect) const;
	void VMargin(float Cut, CUIRect *pOtherRect) const;
	void HMargin(float Cut, CUIRect *pOtherRect) const;

	bool Align(CUIRect *pInner, int Align = 2 /* ALIGN_CENTER */, float MarginX = 0.0f, float MarginY = 0.0f, bool KeepRatio = true) const;
};

class CUI
{
	const void *m_pHotItem;
	const void *m_pActiveItem;
	const void *m_pLastActiveItem;
	const void *m_pBecommingHotItem;
	float m_MouseX, m_MouseY; // in gui space
	float m_MouseWorldX, m_MouseWorldY; // in world space
	unsigned m_MouseButtons;
	unsigned m_LastMouseButtons;

	CUIRect m_Screen;
	class IGraphics *m_pGraphics;
	class ITextRender *m_pTextRender;
	class CSounds *m_pSounds;
	class CRenderTools *m_pRenderTools;
	class IClient *m_pClient;


	class IInput *m_pInput;



  bool m_HoverSound;
  int64 m_HoverSoundTime;

public:
	// TODO: Refactor: Fill this in
	void SetPointers(	class IGraphics *pGraphics,
						 class ITextRender *pTextRender,
						 class CRenderTools *pRenderTools,
						 class IClient *pClient,
						 class IInput *pInput
	) { m_pGraphics = pGraphics; m_pTextRender = pTextRender; m_pRenderTools = pRenderTools; m_pClient = pClient; m_pInput=pInput;}
	class IGraphics *Graphics() { return m_pGraphics; }
	class ITextRender *TextRender() { return m_pTextRender; }
	class CRenderTools *RenderTools() const { return m_pRenderTools; };
	class IClient *Client() const { return m_pClient; };
	class IInput *Input() const { return m_pInput; }

  void SetSounds(class CSounds *pSounds) {
    m_pSounds = pSounds;
  }

	CUI();
	enum
	{
		CORNER_TL=1,
		CORNER_TR=2,
		CORNER_BL=4,
		CORNER_BR=8,

		CORNER_T=CORNER_TL|CORNER_TR,
		CORNER_B=CORNER_BL|CORNER_BR,
		CORNER_R=CORNER_TR|CORNER_BR,
		CORNER_L=CORNER_TL|CORNER_BL,

		CORNER_ALL=CORNER_T|CORNER_B
	};

	enum
	{
		ALIGN_LEGACY_LEFT = -1,
		ALIGN_LEGACY_RIGHT = 1,
		ALIGN_LEGACY_TOP = 0,
		ALIGN_CENTER = 2,
		ALIGN_TOP = 4,
		ALIGN_LEFT = 8,
		ALIGN_RIGHT = 16,
		ALIGN_BOTTOM = 32,
	};

	int Update(float mx, float my, float Mwx, float Mwy, int m_Buttons);

	float MouseX() const { return m_MouseX; }
	float MouseY() const { return m_MouseY; }
	float MouseWorldX() const { return m_MouseWorldX; }
	float MouseWorldY() const { return m_MouseWorldY; }
	int MouseButton(int Index) const { return (m_MouseButtons>>Index)&1; }
	int MouseButtonClicked(int Index) { return MouseButton(Index) && !((m_LastMouseButtons>>Index)&1) ; }

	void SetHotItem(const void *pID, bool HoverSound = false) { m_HoverSound = HoverSound && m_pBecommingHotItem != pID && m_pHotItem != pID; m_pBecommingHotItem = pID; }
	void SetActiveItem(const void *pID) { m_pActiveItem = pID; if (pID) m_pLastActiveItem = pID; }
	void ClearLastActiveItem() { m_pLastActiveItem = 0; }
	const void *HotItem() const { return m_pHotItem; }
	const void *NextHotItem() const { return m_pBecommingHotItem; }
	const void *ActiveItem() const { return m_pActiveItem; }
	const void *LastActiveItem() const { return m_pLastActiveItem; }

	int MouseInside(const CUIRect *pRect);
	void ConvertMouseMove(float *x, float *y);

	CUIRect *Screen();
	float PixelSize();
	void ClipEnable(const CUIRect *pRect);
	void ClipDisable();

	// TODO: Refactor: Redo UI scaling
	void SetScale(float s);
	float Scale();

	int DoRectLogic(const void *pID, const CUIRect *pRect, bool HoverSound = false);
	int DoClickableRectLogic(const class CUIClickable *pElem, const CUIRect *pRect, bool HoverSound = true);
	int DoButtonLogic(const void *pID, const char *pText /* TODO: Refactor: Remove */, int Checked, const CUIRect *pRect, bool HoverSound = true);
	int DoLinkLogic(const void *pID, const char *pUrl, const CUIRect *pRect);

	// TODO: Refactor: Remove this?
	void DoLabel(const CUIRect *pRect, const char *pText, float Size, int Align = 1, int MaxWidth = -1, const vec4 *Color = 0, const vec4 *pOutlineColor = 0);
	void DoLabelScaled(const CUIRect *pRect, const char *pText, float Size, int Align = 1, int MaxWidth = -1, const vec4 *Color = 0, const vec4 *pOutlineColor = 0);

    void DoSpinner(const CUIRect *pRect, int Align = 2, vec4 *pColor = 0);

	static vec4 Brightness(vec4 Color, float Brightness);
	CUIRect ConvertToUICoordinates(const CUIRect ScreenMapping, const CUIRect Rect);


	float DoScrollLogic(const void *pID, CUIRect *pView, float Current, float Step = 0.1f, bool Relative = true);
	bool DoInputTabLogic(const void *pCurrent, const void *pNext = 0, const void *pPrevious = 0);
	bool DoInputReturnLogic(const void *pCurrent, const void *pNext = 0);
	bool DoInputEscapeLogic(const void *pCurrent, const void *pNext = 0);
};

#endif
