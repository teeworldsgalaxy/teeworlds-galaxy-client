#include <base/system.h>
#include <engine/graphics.h>
#include <engine/storage.h>

#include "map_preview.h"


void CMapPreview::OnInit()
{
    mem_zero(m_apPreviewCache, sizeof(m_apPreviewCache));

	// load default texture
	CImageInfo Img;
	m_Default.m_Texture = -1;
	m_Default.m_Width = 1;
	m_Default.m_Height = 1;
	m_pCurrent = 0;
	if(Graphics()->LoadPNG(&Img, "data/maps/default.png", IStorage::TYPE_ALL))
	{
		m_Default.m_Texture = Graphics()->LoadTextureRaw(Img.m_Width, Img.m_Height, Img.m_Format, Img.m_pData, Img.m_Format, 0);
		m_Default.m_Width = Img.m_Width;
		m_Default.m_Height = Img.m_Height;
		mem_free(Img.m_pData);
	}
}

bool CMapPreview::SetCurrentMap(const char *pMap)
{
	if(m_pCurrent && str_comp_nocase(m_pCurrent->m_aMapName, pMap) == 0) // same map still
	{
		return false;
	}

	if(m_pCurrent && str_comp_nocase(m_pCurrent->m_aMapName, pMap) != 0) // map changed and we previously had one
	{
		m_pCurrent->m_CurrentImage++; // select the next image so that the user doesn't see the same every time the map gets selected
	}

	// check if we already have loaded the map
	for(int i = 0; i < MAX_PREVIEW_CACHE_SIZE; i++)
	{
        CMapPreviewInfo *pInfo = m_apPreviewCache[i];
		if(pInfo && !str_comp_nocase(pInfo->m_aMapName, pMap))
		{
            //swap to front
            //TODO: check if we should swap some more here
            m_apPreviewCache[i] = m_apPreviewCache[0];
            m_apPreviewCache[0] = pInfo;
            m_pCurrent = pInfo;
			return true;
		}
	}
    //load the map in the background
    CMapPreviewInfo *pInfo = new CMapPreviewInfo(Graphics());
    str_copy(pInfo->m_aMapName, pMap, sizeof(pInfo->m_aMapName));

    //release the map data if we would hit the limit
    if (m_apPreviewCache[MAX_PREVIEW_CACHE_SIZE-1] != 0) {
        CMapPreviewInfo *pLast = m_apPreviewCache[MAX_PREVIEW_CACHE_SIZE-1];
        if (pLast->m_Loading) {
            pLast->m_Loading = false; //cancel current loading
            sys_thread_wait(pLast->m_LoadingThread);
        }
        delete pLast;
    }
    for(int i = MAX_PREVIEW_CACHE_SIZE-1; i > 0; --i) {
        m_apPreviewCache[i] = m_apPreviewCache[i-1]; //make space at front, we should use a better algorithm here
    }
    m_apPreviewCache[0] = pInfo;
    m_pCurrent = pInfo;
    pInfo->m_Loading = true;
    pInfo->m_LoadingThread = sys_thread_create_named(CMapPreview::PreviewLoaderThread, pInfo, "MapPreview-Loader");
    return true;
}

const CMapPreview::CMapImageInfo &CMapPreview::GetCurrentImageInfo() {
	if (m_pCurrent && !m_pCurrent->m_Loading && m_pCurrent->m_aImages.size() > 0)	{
        return m_pCurrent->m_aImages[m_pCurrent->m_CurrentImage % m_pCurrent->m_aImages.size()];
	} else {
        return m_Default;
    }
}

void CMapPreview::NextImage()
{
    if (m_pCurrent)
        m_pCurrent->m_CurrentImage++;
}

void CMapPreview::PreviewLoaderThread(void *pUser)
{
    CMapPreviewInfo *pInfo = (CMapPreviewInfo *)pUser;

	for(int i = 0; pInfo->m_Loading; ++i)
	{
		CMapImageInfo MapImage;

		CImageInfo Img;
		char aBuf[512];
		str_format(aBuf, sizeof(aBuf), "data/maps/%s_%d.png", pInfo->m_aMapName, i);

		if(pInfo->m_pGraphics->LoadPNG(&Img, aBuf, IStorage::TYPE_ALL))
		{
			MapImage.m_Texture = pInfo->m_pGraphics->LoadTextureRaw(Img.m_Width, Img.m_Height, Img.m_Format, Img.m_pData, Img.m_Format, 0);
			MapImage.m_Width = Img.m_Width;
			MapImage.m_Height = Img.m_Height;
			mem_free(Img.m_pData);
            pInfo->m_aImages.add(MapImage);
			dbg_msg("mappreview", "loaded %s", aBuf);
		}
		else
		{
            // we have loaded all available images
			break;
		}

		sys_thread_sleep(5);
	}
    pInfo->m_Loading = false; //notify the main thread that we have finished loading
}
