#ifndef GAME_CLIENT_COMPONENTS_MAP_PREVIEW_H
#define GAME_CLIENT_COMPONENTS_MAP_PREVIEW_H
#include <base/vmath.h>
#include <base/tl/array.h>
#include <game/client/component.h>

class CMapPreview : public CComponent
{
public:
	// do this better and nicer

    struct CMapImageInfo
	{
        int m_Texture;
        int m_Width;
        int m_Height;
    };

	class CMapPreviewInfo
	{
		friend class CMapPreview;
		IGraphics *m_pGraphics;
	public:
		CMapPreviewInfo(IGraphics *pGraphics) : m_pGraphics(pGraphics) {
			m_CurrentImage = 0;
		}
		~CMapPreviewInfo() {
			// delete all textures
			for(int i = 0; i < m_aImages.size(); i++)
				if(m_aImages[i].m_Texture > 0)
					m_pGraphics->UnloadTexture(m_aImages[i].m_Texture);
		}

		char m_aMapName[512];
        array<CMapImageInfo> m_aImages;
        int m_CurrentImage;
		bool m_Loading;
		void *m_LoadingThread;
	};


    virtual void OnInit();
    //TODO: OnRelease -> free memory
	int Find(const char *pName);

	bool SetCurrentMap(const char *pMap);
	void NextImage();

    const CMapImageInfo& GetCurrentImageInfo();


private:
	enum
	{
		MAX_PREVIEW_CACHE_SIZE = 64
	};

	CMapPreviewInfo *m_pCurrent;
    CMapImageInfo m_Default;

	int m_ImageNumber;
	CMapPreviewInfo* m_apPreviewCache[MAX_PREVIEW_CACHE_SIZE];

	LOCK m_Lock;
	char m_aCurrentlyLoadingMap[512];
	static void PreviewLoaderThread(void *pUser);
};
#endif
