#include <engine/config.h>
#include <engine/friends.h>
#include <engine/graphics.h>
#include <engine/keys.h>
#include <engine/serverbrowser.h>
#include <engine/textrender.h>
#include <engine/shared/config.h>
#include <engine/galaxy_client.h>

#include <game/generated/client_data.h>
#include <game/generated/protocol.h>

#include <game/localization.h>
#include <game/version.h>
#include <game/client/render.h>
#include <game/client/ui.h>
#include <game/client/components/countryflags.h>


#include <game/client/animstate.h>
#include <game/localization.h>
#include <game/client/ui/elements/link.h>
#include <game/client/ui/elements/icon_button.h>
#include <game/client/ui/elements/button.h>
#include <game/client/ui/elements/indicator.h>
#include <game/client/ui/constants.h>
#include "skins.h"

#include "menus.h"
#include "../ui.h"
#include "../../../engine/client/serverbrowser.h"



void CMenus::RenderSectionLeftBottom(CUIRect MainView)
{
    CUIRect Friendlist, BottomBar;
    MainView.Margin(5.0f, &MainView);
    MainView.HSplitBottom(30.0f, &MainView, &BottomBar);
    BottomBar.HSplitBottom(5.0f, &BottomBar, 0);
    // calculate the boxes
    if(Client()->Galaxy()->Friends()->NumRequests() > 0 && m_FriendActiveActionsIndex == -1)
        MainView.HSplitBottom(min(MainView.h/2.0f, ms_ListheaderHeight*2+50.0f*Client()->Galaxy()->Friends()->NumRequests()), &Friendlist, &MainView);
    else
        Friendlist = MainView;

    // render the boxes
    // with a nice sliding effect - could maybe be made even prettier
    // @henri: disabling this for now for an easier user interface, but we might want to support this using the new shader feature for example?
  /* {
        static float s_SlideVal = 0.0f; // 0 means it's there, 1 means it's gone (slid out to the left)


        float Wanted = m_FriendActiveActionsIndex == -1 ? 1.0f : 0.0f;
        smooth_set(&s_SlideVal, Wanted, 30.0f, Client()->RenderFrameTime());

        // need to render the submenu first so that the listbox stays above it
        if(s_SlideVal < 1.0f )
        {


        }

        if(s_SlideVal > 0.0f && m_FriendActiveActionsIndex >= 0) // render the listbox after to have it always on top
        {
            CUIRect AlternateRect = Friendlist;
            AlternateRect.x -= s_SlideVal * AlternateRect.w * 1.01f;
            RenderSectionLeftBottomFriendsAction(AlternateRect);
        }
    }*/

    RenderFriendList(Friendlist);



    float ButtonRatio = 5.0f;
    CUIRect ButtonBox;
    ButtonBox.h = BottomBar.h;
    ButtonBox.w =  ButtonBox.h*ButtonRatio;

    BottomBar.Align(&ButtonBox);
    // friend adding
    static CUIButton AddFriendButton;
    AddFriendButton.SetText(Localize("Search Player")); // TODO: a nice search bar for friends and only a tiny '+' button to add friends
    AddFriendButton.Create(ButtonBox, RenderTools());
    if(AddFriendButton.Pressed())
    {
        m_Popup = POPUP_SEARCH_PLAYER;
    }
}


void CMenus::RenderFriendListEntry(CListboxItem Item, const CApiFriend *pInfo, int i, bool Request) {

    CUIRect Row;

    Item.m_Rect.Margin(6.0f, &Row);

    CUIRect Info, Actions;
    Row.VSplitPerc(0.6f, &Info, &Actions);

    CUIRect InfoTop, InfoBottom;

    Info.HSplitPerc(0.5f, &InfoTop, &InfoBottom);

    CUIRect IndicatorBox;

    float IndicatorSize = 2.0f;
    InfoTop.VSplitLeft(IndicatorSize, &IndicatorBox, &InfoTop);
    InfoBottom.VSplitLeft(IndicatorSize, 0, &InfoBottom);

    InfoTop.VSplitLeft(4.0f, 0, &InfoTop);
    InfoBottom.HSplitTop(2.0f, 0, &InfoBottom);
    InfoBottom.VSplitLeft(4.0f, 0, &InfoBottom);

    const CApiPlayer *pPlayer = Request ? pInfo->m_pPlayer : pInfo->m_pFriend;


    UI()->DoLabelScaled(&InfoTop, pPlayer->m_pName, InfoTop.h, CUI::ALIGN_LEFT, InfoTop.w);
    if(pPlayer->m_pClanName)
    {
        vec4 Color = STYLE_COLOR_BG_LIGHT;
        UI()->DoLabelScaled(&InfoBottom, pPlayer->m_pClanName, InfoBottom.h, CUI::ALIGN_LEFT,InfoBottom.w, &Color);
    }



    static CUIIndicator Indicator;
    static CUIIconButton Action1Button;
    static CUIIconButton Action2Button;

    Action2Button.SetID((char *) &Action2Button + i);
    Action1Button.SetID((char *) &Action1Button + i);
    Indicator.SetID((char *) &Indicator + i);

    Indicator.GetStyle().IndicatorSetRadius(IndicatorSize);


    CUIRect Action1, Action2;
    Actions.VSplitPerc(0.5f, &Action1, &Action2);

    Action1.Margin(2.5f, &Action1);
    Action2.Margin(2.5f, &Action2);

    if (Request) {


        Action1Button.SetEnabled(true);
        Action2Button.SetEnabled(true);

        Action1Button.GetStyle().IconSetIcon(SPRITE_GUIICON_PLUS);
        Action2Button.GetStyle().IconSetIcon(SPRITE_GUIICON_TIMES);

        Action1Button.GetTooltip().SetText("Add");

        Action1Button.Create(Action1, RenderTools());
        Action2Button.Create(Action2, RenderTools());
        if(Action1Button.Pressed())
        {
            Client()->Galaxy()->Friends()->AddFriend(pPlayer->m_ID);
        }

        Action2Button.GetTooltip().SetText("Reject");
        if(Action2Button.Pressed())
        {
            Client()->Galaxy()->Friends()->RemoveFriend(pPlayer->m_ID);
        }
    } else
    {

        Action2Button.GetStyle().IconSetIcon(SPRITE_GUIICON_LOG_IN);
        Action1Button.GetStyle().IconSetIcon(0);

        if (!pPlayer->IsOnline()) {
            Action2Button.GetTooltip().SetText(Localize("Offline"));
            Action2Button.SetEnabled(false);
            Indicator.GetStyle().BackgroundSetColor(STYLE_COLOR_FRIENDS_INDICATOR_OFFLINE);
        } else if (!pPlayer->m_pServerAddress) {
            Action2Button.GetTooltip().SetText(Localize("Lobby"));
            Action2Button.SetEnabled(false);
            Indicator.GetStyle().BackgroundSetColor(STYLE_COLOR_FRIENDS_INDICATOR_ONLINE);
        } else {
            /*
             * Player is online and on a server
             */
            Indicator.GetStyle().BackgroundSetColor(STYLE_COLOR_FRIENDS_INDICATOR_PLAYING);
            char aTooltip[512] = {0};
            Action1Button.GetTooltip().SetText(aTooltip);

            str_format(aTooltip, sizeof(aTooltip), "Join\n");

            // try to get real server information
            CServerInfo *pServerInfo = ServerBrowser()->GetInfo(pPlayer->m_pServerAddress);
            char aBuf[256] = {0};
            if (pServerInfo) {
                str_format(aBuf, sizeof(aBuf), "Playing %s on (%d/%d) %s [%s]",
                           pServerInfo->m_aGameType,
                           pServerInfo->m_NumClients, pServerInfo->m_MaxClients,
                           pServerInfo->m_aName, pPlayer->m_pServerAddress);

            } else {
                str_format(aBuf, sizeof(aBuf), "Playing on %s", pPlayer->m_pServerAddress);
            }
            str_append(aTooltip, aBuf, sizeof(aTooltip));

            // enable the button accordingly
            Action1Button.SetEnabled(
                    Client()->State() != IClient::STATE_ONLINE ||
                    str_comp_nocase(pPlayer->m_pServerAddress, g_Config.m_UiServerAddress)
            );


        }

        Action2Button.Create(Action2, RenderTools());
        if (Action2Button.Pressed()) {
            str_copy(g_Config.m_UiServerAddress, pPlayer->m_pServerAddress,
                     sizeof(g_Config.m_UiServerAddress));
            Client()->Connect(g_Config.m_UiServerAddress);
        }
    }

    Indicator.Create(IndicatorBox, UI()->RenderTools());
}

void CMenus::RenderFriendList(CUIRect UpperBox)
{

    CUIRect Divider;
    UpperBox.HSplitTop(5.0f, &Divider, &UpperBox);
    UpperBox.HSplitTop(10.0f, 0, &UpperBox);
    DoDivider(&Divider);
    static float s_ScrollVal = 0.0f;
    static int s_Listbox = 0;

    static int s_Selected = 0;
    int OldSelected = s_Selected;

    int NumFriends = Client()->Galaxy()->Friends()->NumFriends();
    int NumRequests = Client()->Galaxy()->Friends()->NumRequests();
    int Num = NumFriends + NumRequests;

    UiDoListboxStart(&s_Listbox, &UpperBox, 30.0f, Localize("Friends"), "", Num, 1, -1, s_ScrollVal);

    for(int i = 0; i < NumRequests; ++i)
    {
        const CApiFriend *pInfo = Client()->Galaxy()->Friends()->GetRequest(i);
        CListboxItem Item = UiDoListboxNextItem(pInfo->m_pPlayer);

        if(!Item.m_Visible || !pInfo || !pInfo->m_pPlayer) {
            continue;
        }

        RenderFriendListEntry(Item, pInfo, i, true);


    }

    for(int i = 0; i < NumFriends; ++i)
    {
        const CApiFriend *pInfo = Client()->Galaxy()->Friends()->GetFriend(i);
        CListboxItem Item = UiDoListboxNextItem(pInfo->m_pFriend);

        if(!Item.m_Visible || !pInfo || !pInfo->m_pFriend) {
            continue;
        }

        RenderFriendListEntry(Item, pInfo, i+NumRequests, false);

    }

    s_Selected = UiDoListboxEnd(&s_ScrollVal, 0);

    if (OldSelected != s_Selected && s_Selected >= 0 && s_Selected < Num) {


        const CApiPlayer *pSelected = 0;
        if (s_Selected < NumRequests) {
            pSelected = Client()->Galaxy()->Friends()->GetRequest(s_Selected)->m_pPlayer;
        } else {
            pSelected = Client()->Galaxy()->Friends()->GetFriend(s_Selected-NumRequests)->m_pFriend;
        }

        if (pSelected->m_ID) {
            Client()->Galaxy()->Players()->Detail(pSelected->m_ID);
            m_Popup = POPUP_PLAYER;
        }
    }
}

void CMenus::RenderSectionLeftBottomFriendsAction(CUIRect View)
{
    CUIRect Button;
    const CApiPlayer *pF = Client()->Galaxy()->Friends()->GetFriend(m_FriendActiveActionsIndex)->m_pFriend;

    // heading
    View.Margin(5.0f, &View);
    View.HSplitTop(25.0f, &Button, &View);
    {
        CUIRect Tee;
        Button.VSplitRight(Button.h, &Button, &Tee);
        RenderSectionLeftBottomFriendTee(Tee, Client()->Galaxy()->Friends()->GetFriend(m_FriendActiveActionsIndex));
    }
    UI()->DoLabelScaled(&Button, Localize("Friend Actions"), 17.0f, 0);

    // name
    View.HSplitTop(5.0f, 0, &View);
    View.HSplitTop(20.0f, &Button, &View);
    UI()->DoLabelScaled(&Button, pF->m_pName, 13.0f);

    // clan
    View.HSplitTop(5.0f, 0, &View);
    View.HSplitTop(20.0f, &Button, &View);
    UI()->DoLabelScaled(&Button, pF->m_pClanName, 13.0f);

    // remove friend button
    View.HSplitTop(5.0f, 0, &View);
    View.HSplitTop(20.0f, &Button, &View);
    {
        static CUIButton RemoveButton;
        RemoveButton.SetText(Localize("Remove Friend"));
        RemoveButton.Create(Button, RenderTools());
        if(RemoveButton.Pressed())
        {
            Client()->Galaxy()->Friends()->RemoveFriend(pF->m_ID);
            m_FriendActiveActionsIndex = -1;
            return;
        }
    }

    View.HSplitBottom(20.f, &View, &Button);
    static CUIButton CloseButton;
    CloseButton.SetText(Localize("Close"));
    CloseButton.Create(Button, RenderTools());
    if(CloseButton.Pressed())
    {
        m_FriendActiveActionsIndex = -1;
        return;
    }

}

void CMenus::RenderSectionLeftBottomFriendTee(CUIRect& Tee, const CApiFriend *pInfo)
{
    static float Position = 0.0f;
    static vec2 TargetPos = vec2(0.0f, 0.0f);
    static vec2 CurrentPos = TargetPos;

    if(distance(CurrentPos, TargetPos) > 0.001f)
    {
        CurrentPos += normalize(TargetPos - CurrentPos) * 200.0f * Client()->RenderFrameTime();
    }
    else
    {
        TargetPos = vec2((float)(rand() % (int)UI()->Screen()->w), (float)(rand() % (int)UI()->Screen()->h));
    }
    const float WalkTimeMagic = 100.0f;
    Position = fmodf(Position + Client()->RenderFrameTime() * 3.0f, 1.0f);
    CAnimState State;
    State.Set(&g_pData->m_aAnimations[ANIM_BASE], 0);

    bool InAir = false;
    bool Stationary = true;
    bool WantOtherDir = false;
    if(InAir)
        State.Add(&g_pData->m_aAnimations[ANIM_INAIR], 0, 1.0f); // TODO: some sort of time here
    else if(Stationary)
        State.Add(&g_pData->m_aAnimations[ANIM_IDLE], 0, 1.0f); // TODO: some sort of time here
    else if(!WantOtherDir)
        State.Add(&g_pData->m_aAnimations[ANIM_WALK], Position, 1.0f);

    const CSkins::CSkin *pSkin = m_pClient->m_pSkins->Get(m_pClient->m_pSkins->Find(pInfo->m_pFriend->m_pSkin));
    CTeeRenderInfo SkinInfo;
    if(pInfo->m_pFriend->m_UseColor)
    {
        SkinInfo.m_Texture = pSkin->m_ColorTexture;
        SkinInfo.m_ColorBody = m_pClient->m_pSkins->GetColorV4(pInfo->m_pFriend->m_ColorBody);
        SkinInfo.m_ColorFeet = m_pClient->m_pSkins->GetColorV4(pInfo->m_pFriend->m_ColorFeet);
    }
    else
    {
        SkinInfo.m_Texture = pSkin->m_OrgTexture;
        SkinInfo.m_ColorBody = vec4(1.0f, 1.0f, 1.0f, 1.0f);
        SkinInfo.m_ColorFeet = vec4(1.0f, 1.0f, 1.0f, 1.0f);
    }

    SkinInfo.m_Size = 50.0f * UI()->Scale();

    Tee.Align(&Tee, CUI::ALIGN_CENTER, 10.0f, 10.0f);

    vec2 Pos = vec2(Tee.x + Tee.w / 2, Tee.y + Tee.h / 2);
    RenderTools()->RenderTee(&State, &SkinInfo, 0, normalize(CurrentPos - Pos), Pos);
}


void CMenus::RenderTee(CUIRect Tee, const char *pSkinName, bool UseColor, int ColorBody, int ColorFeet) {
    CAnimState State;
    State.Set(&g_pData->m_aAnimations[ANIM_BASE], 0);

    /* bool InAir = false;
     bool Stationary = true;
     bool WantOtherDir = false;
     if(InAir)
       State.Add(&g_pData->m_aAnimations[ANIM_INAIR], 0, 1.0f); // TODO: some sort of time here
     else if(Stationary)
       State.Add(&g_pData->m_aAnimations[ANIM_IDLE], 0, 1.0f); // TODO: some sort of time here
     else if(!WantOtherDir)
       State.Add(&g_pData->m_aAnimations[ANIM_WALK], Position, 1.0f);*/
    State.Add(&g_pData->m_aAnimations[ANIM_IDLE], 0, 1.0f);

    const CSkins::CSkin *pSkin = m_pClient->m_pSkins->Get(m_pClient->m_pSkins->Find(pSkinName));
    CTeeRenderInfo SkinInfo;
    if(UseColor)
    {
        SkinInfo.m_Texture = pSkin->m_ColorTexture;
        SkinInfo.m_ColorBody = m_pClient->m_pSkins->GetColorV4(ColorBody);
        SkinInfo.m_ColorFeet = m_pClient->m_pSkins->GetColorV4(ColorFeet);
    }
    else
    {
        SkinInfo.m_Texture = pSkin->m_OrgTexture;
        SkinInfo.m_ColorBody = vec4(1.0f, 1.0f, 1.0f, 1.0f);
        SkinInfo.m_ColorFeet = vec4(1.0f, 1.0f, 1.0f, 1.0f);
    }

    SkinInfo.m_Size = 50.0f*UI()->Scale();

    Tee.Align(&Tee, CUI::ALIGN_CENTER, 10.0f, 10.0f);

    vec2 Pos = vec2(Tee.x + Tee.w/2, Tee.y + Tee.h/2);
    RenderTools()->RenderTee(&State, &SkinInfo, 0, vec2(1,0), Pos);
}