/* (c) Magnus Auvinen. See licence.txt in the root of the distribution for more information. */
/* If you are missing that file, acquire a complete release at teeworlds.com.                */
#include <base/math.h>

#include <engine/config.h>
#include <engine/demo.h>
#include <engine/friends.h>
#include <engine/graphics.h>
#include <engine/serverbrowser.h>
#include <engine/textrender.h>
#include <engine/shared/config.h>

#include <game/generated/protocol.h>
#include <game/generated/client_data.h>

#include <game/localization.h>
#include <game/client/animstate.h>
#include <game/client/gameclient.h>
#include <game/client/render.h>
#include <game/client/ui.h>
#include <game/client/ui/constants.h>
#include <game/client/ui/elements/button.h>

#include "menus.h"
#include "motd.h"
#include "voting.h"

void CMenus::RenderGame(CUIRect MainView)
{
	CUIRect Button, ButtonBar;
	MainView.HSplitTop(50.0f, &ButtonBar, &MainView);

    RenderTools()->DrawUIRect(&ButtonBar, STYLE_COLOR_POPUP_BG, 0, 0);

	// button bar
	ButtonBar.HSplitTop(12.5f, 0, &ButtonBar);
	ButtonBar.HSplitTop(25.0f, &ButtonBar, 0);
    ButtonBar.VSplitPerc(0.8f, &ButtonBar, 0);
    ButtonBar.VMargin(10.0f, &ButtonBar);
	// online menus



    {
        // Options and info
        ButtonBar.VSplitLeft(5.0f, 0, &ButtonBar);
        ButtonBar.VSplitLeft(100.0f, &Button, &ButtonBar);

        static CUIButton PlayersBtn;
        PlayersBtn.SetText(Localize("Players"));
        PlayersBtn.Create(Button, RenderTools());
        if(PlayersBtn.Pressed())
            m_Popup = POPUP_SERVER_PLAYERS;


        ButtonBar.VSplitLeft(5.0f, 0, &ButtonBar);
        ButtonBar.VSplitLeft(100.0f, &Button, &ButtonBar);
        static CUIButton InfoBtn;
        InfoBtn.SetText(Localize("Server info"));
        InfoBtn.Create(Button, RenderTools());
        if(InfoBtn.Pressed())
            m_Popup = POPUP_SERVER_INFO;

        ButtonBar.VSplitLeft(5.0f, 0, &ButtonBar);
        ButtonBar.VSplitLeft(100.0f, &Button, &ButtonBar);
        static CUIButton VotesBtn;
        VotesBtn.SetText(Localize("Call vote"));
        VotesBtn.Create(Button, RenderTools());
        if(VotesBtn.Pressed())
            m_Popup = POPUP_SERVER_VOTES;
    }


    {
        ButtonBar.VSplitLeft(20.0f, 0, &ButtonBar);

        if(m_pClient->m_Snap.m_pLocalInfo && m_pClient->m_Snap.m_pGameInfoObj) {
            if (m_pClient->m_Snap.m_pLocalInfo->m_Team != TEAM_SPECTATORS) {

                ButtonBar.VSplitLeft(5.0f, 0, &ButtonBar);
                ButtonBar.VSplitLeft(100.0f, &Button, &ButtonBar);

                static CUIButton SpectateBtn;
                SpectateBtn.SetText(Localize("Spectate"));
                SpectateBtn.Create(Button, RenderTools());
                if (SpectateBtn.Pressed()) {
                    m_pClient->SendSwitchTeam(TEAM_SPECTATORS);
                    m_pClient->m_pMenus->SetActive(false);
                }
            }

            if(m_pClient->m_Snap.m_pGameInfoObj->m_GameFlags & GAMEFLAG_TEAMS)
            {
                if(m_pClient->m_Snap.m_pLocalInfo->m_Team != TEAM_RED)
                {
                    ButtonBar.VSplitLeft(5.0f, 0, &ButtonBar);
                    ButtonBar.VSplitLeft(100.0f, &Button, &ButtonBar);

                    static CUIButton JoinRedBtn;
                    JoinRedBtn.SetText(Localize("Join red"));
                    JoinRedBtn.Create(Button, RenderTools());
                    if(JoinRedBtn.Pressed()) {
                        m_pClient->SendSwitchTeam(TEAM_RED);
                        m_pClient->m_pMenus->SetActive(false);
                    }
                }

                if(m_pClient->m_Snap.m_pLocalInfo->m_Team != TEAM_BLUE)
                {
                    ButtonBar.VSplitLeft(5.0f, 0, &ButtonBar);
                    ButtonBar.VSplitLeft(100.0f, &Button, &ButtonBar);

                    static CUIButton JoinBlueBtn;
                    JoinBlueBtn.SetText(Localize("Join blue"));
                    JoinBlueBtn.Create(Button, RenderTools());
                    if(JoinBlueBtn.Pressed()) {
                        m_pClient->SendSwitchTeam(TEAM_BLUE);
                        m_pClient->m_pMenus->SetActive(false);
                    }
                }

            } else {
                // Only one team
                if(m_pClient->m_Snap.m_pLocalInfo->m_Team != 0)
                {
                    ButtonBar.VSplitLeft(5.0f, 0, &ButtonBar);
                    ButtonBar.VSplitLeft(100.0f, &Button, &ButtonBar);

                    static CUIButton JoinGameBtn;
                    JoinGameBtn.SetText(Localize("Join game"));
                    JoinGameBtn.Create(Button, RenderTools());
                    if(JoinGameBtn.Pressed()) {
                        m_pClient->SendSwitchTeam(0);
                        m_pClient->m_pMenus->SetActive(false);
                    }
                }
            }
        }
    }


    ButtonBar.VSplitRight(120.0f, &ButtonBar, &Button);
    RenderTools()->DrawUIRect(&Button, STYLE_COLOR_POPUP_BG, 0, 0);

    bool Recording = DemoRecorder()->IsRecording();
    static CUIButton DemoBtn;
    DemoBtn.SetText(Localize(Recording ? "Stop record" : "Record demo"));
    DemoBtn.Create(Button, RenderTools());
    if(DemoBtn.Pressed()) {
        if(!Recording)
            Client()->DemoRecorder_Start("demo", true);
        else
            Client()->DemoRecorder_Stop();
    }



    /*
     * CUIRect Button, ButtonBar;
	MainView.HSplitTop(45.0f, &ButtonBar, &MainView);

	// button bar
	ButtonBar.HSplitTop(10.0f, 0, &ButtonBar);
	ButtonBar.HSplitTop(25.0f, &ButtonBar, 0);
	ButtonBar.VMargin(10.0f, &ButtonBar);

	// online menus
	ButtonBar.VSplitLeft(10.0f, 0, &ButtonBar);
	ButtonBar.VSplitLeft(120.0f, &Button, &ButtonBar);

    static CUIButton PlayersBtn;
    PlayersBtn.SetText(Localize("Players"));
    PlayersBtn.Create(Button, RenderTools());
    if(PlayersBtn.Pressed())
        m_Popup = POPUP_SERVER_PLAYERS;


    ButtonBar.VSplitLeft(10.0f, 0, &ButtonBar);
    ButtonBar.VSplitLeft(120.0f, &Button, &ButtonBar);
    static CUIButton InfoBtn;
    InfoBtn.SetText(Localize("Server info"));
    InfoBtn.Create(Button, RenderTools());
    if(InfoBtn.Pressed())
        m_Popup = POPUP_SERVER_INFO;

    ButtonBar.VSplitLeft(10.0f, 0, &ButtonBar);
    ButtonBar.VSplitLeft(120.0f, &Button, &ButtonBar);
    static CUIButton VotesBtn;
    VotesBtn.SetText(Localize("Call vote"));
    VotesBtn.Create(Button, RenderTools());
    if(VotesBtn.Pressed())
        m_Popup = POPUP_SERVER_VOTES;


    ButtonBar.VSplitLeft(100.0f, 0, &ButtonBar);
    ButtonBar.VSplitLeft(150.0f, &Button, &ButtonBar);
    bool Recording = DemoRecorder()->IsRecording();
    static CUIButton DemoBtn;
    DemoBtn.SetText(Localize(Recording ? "Stop record" : "Record demo"));
    DemoBtn.Create(Button, RenderTools());
    if(DemoBtn.Pressed()) {
        if(!Recording)
            Client()->DemoRecorder_Start("demo", true);
        else
            Client()->DemoRecorder_Stop();
    }


    // Render Buttons onto scoreboard

    float w = 700.0f;

    CUIRect ScoreBoardMapping;
    ScoreBoardMapping.w = 400*3.0f*Graphics()->ScreenAspect();
    ScoreBoardMapping.h = 400*3.0f;

    CUIRect BaseButton;
    BaseButton.x = 10;
    BaseButton.y = 10;
    BaseButton.w = 80;
    BaseButton.h = 20;


    // Spectate Position
    CUIRect Box = BaseButton;
    Box.x += ScoreBoardMapping.w/2;
    Box.y += 150+760+10+50+10;

    Box = UI()->ConvertToUICoordinates(ScoreBoardMapping, Box);
    Box.w = BaseButton.w;
    Box.h = BaseButton.h;
    Box.x -= Box.w/2;

    if(m_pClient->m_Snap.m_pLocalInfo && m_pClient->m_Snap.m_pGameInfoObj) {
        if (m_pClient->m_Snap.m_pLocalInfo->m_Team != TEAM_SPECTATORS) {
            static CUIButton SpectateBtn;
            SpectateBtn.SetText(Localize("Spectate"));
            SpectateBtn.Create(Box, RenderTools());
            if (SpectateBtn.Pressed()) {
                m_pClient->SendSwitchTeam(TEAM_SPECTATORS);
                m_pClient->m_pMenus->SetActive(false);
            }
        }
    }


    // Render Join Btn
    if(m_pClient->m_Snap.m_pLocalInfo && m_pClient->m_Snap.m_pGameInfoObj)
    {
        if(m_pClient->m_Snap.m_pGameInfoObj->m_GameFlags & GAMEFLAG_TEAMS)
        {
            if(m_pClient->m_Snap.m_pLocalInfo->m_Team != TEAM_RED)
            {
                CUIRect Box = BaseButton;
                Box.x += ScoreBoardMapping.w/2-w/2;
                Box.y += 150;
                Box = UI()->ConvertToUICoordinates(ScoreBoardMapping, Box);
                Box.w = BaseButton.w;
                Box.h = BaseButton.h;
                static CUIButton JoinRedBtn;
                JoinRedBtn.SetText(Localize("Join"));
                JoinRedBtn.Create(Box, RenderTools());
                if(JoinRedBtn.Pressed()) {
                    m_pClient->SendSwitchTeam(TEAM_RED);
                    m_pClient->m_pMenus->SetActive(false);
                }
            }

            if(m_pClient->m_Snap.m_pLocalInfo->m_Team != TEAM_BLUE)
            {
                CUIRect Box = BaseButton;
                Box.x += ScoreBoardMapping.w/2+w/2;
                Box.y += 150;
                Box = UI()->ConvertToUICoordinates(ScoreBoardMapping, Box);
                Box.w = BaseButton.w;
                Box.h = BaseButton.h;

                static CUIButton JoinBlueBtn;
                JoinBlueBtn.SetText(Localize("Join"));
                JoinBlueBtn.Create(Box, RenderTools());
                if(JoinBlueBtn.Pressed()) {
                    m_pClient->SendSwitchTeam(TEAM_BLUE);
                    m_pClient->m_pMenus->SetActive(false);
                }
            }

        } else {
            // Only one team
            if(m_pClient->m_Snap.m_pLocalInfo->m_Team != 0)
            {
                CUIRect Box = BaseButton;
                Box.x += ScoreBoardMapping.w/2;
                Box.y += 150;
                Box = UI()->ConvertToUICoordinates(ScoreBoardMapping, Box);
                Box.w = BaseButton.w;
                Box.h = BaseButton.h;
                Box.x -= Box.w/2;

                static CUIButton JoinGameBtn;
                JoinGameBtn.SetText(Localize("Join"));
                JoinGameBtn.Create(Box, RenderTools());
                if(JoinGameBtn.Pressed()) {
                    m_pClient->SendSwitchTeam(0);
                    m_pClient->m_pMenus->SetActive(false);
                }
            }
        }
    }
     */
}


/**
 * Ingame Popups
 **/

void CMenus::RenderPopupServerInfo(CUIRect Screen) {

    CUIRect MainView;

    Screen.Margin(50.0f, &MainView);

    // render background
    CUIRect Temp, TabBar;
    MainView.VSplitRight(120.0f, &MainView, &TabBar);

    RenderTools()->DrawUIRect(&MainView, STYLE_COLOR_POPUP_BG, 0, 0);

    CUIRect CloseButtonRect;
    MainView.HSplitTop(10.0f, &CloseButtonRect, &MainView);

    CloseButtonRect.VSplitRight(6.0f, &CloseButtonRect, 0);

    static int s_CloseButton = 0;
    int HoverOrClick = DoIcon(&s_CloseButton, SPRITE_GUIICON_TIMES, &CloseButtonRect,
                              CUI::ALIGN_CENTER | CUI::ALIGN_RIGHT, 3.0f);
                              
    if (HoverOrClick > 0) {
        m_Popup = POPUP_NONE;
    }

    if(!m_pClient->m_Snap.m_pLocalInfo) {
         // Show loading spinner
         UI()->DoSpinner(&MainView);
         return;
     }


     // fetch server info
	CServerInfo CurrentServerInfo;
	Client()->GetServerInfo(&CurrentServerInfo);

	CUIRect View, ServerInfo, GameInfo, Motd;

	float x = 0.0f;
	float y = 0.0f;

	char aBuf[1024];

	// set view to use for all sub-modules
	MainView.Margin(10.0f, &View);

	// serverinfo
	View.HSplitTop(View.h/2/UI()->Scale()-5.0f, &ServerInfo, &Motd);
	ServerInfo.VSplitLeft(View.w/2/UI()->Scale()-5.0f, &ServerInfo, &GameInfo);
	RenderTools()->DrawUIRect(&ServerInfo, vec4(1,1,1,0.25f), CUI::CORNER_ALL, 10.0f);

	ServerInfo.Margin(5.0f, &ServerInfo);

	x = 5.0f;
	y = 0.0f;

	TextRender()->Text(0, ServerInfo.x+x, ServerInfo.y+y, 32, Localize("Server info"), 250);
	y += 32.0f+5.0f;

	mem_zero(aBuf, sizeof(aBuf));
	str_format(
		aBuf,
		sizeof(aBuf),
		"%s\n\n"
		"%s: %s\n"
		"%s: %d\n"
		"%s: %s\n"
		"%s: %s\n",
		CurrentServerInfo.m_aName,
		Localize("Address"), g_Config.m_UiServerAddress,
		Localize("Ping"), m_pClient->m_Snap.m_pLocalInfo->m_Latency,
		Localize("Version"), CurrentServerInfo.m_aVersion,
		Localize("Password"), CurrentServerInfo.m_Flags &1 ? Localize("Yes") : Localize("No")
	);

	TextRender()->Text(0, ServerInfo.x+x, ServerInfo.y+y, 20, aBuf, 250);

	{
		CUIRect Button;
		int IsFavorite = ServerBrowser()->IsFavorite(CurrentServerInfo.m_NetAddr);
		ServerInfo.HSplitBottom(20.0f, &ServerInfo, &Button);
		static int s_AddFavButton = 0;
		if(DoButton_CheckBox(&s_AddFavButton, Localize("Favorite"), IsFavorite, &Button))
		{
			if(IsFavorite)
				ServerBrowser()->RemoveFavorite(CurrentServerInfo.m_NetAddr);
			else
				ServerBrowser()->AddFavorite(CurrentServerInfo.m_NetAddr);
		}
	}

	// gameinfo
	GameInfo.VSplitLeft(10.0f, 0x0, &GameInfo);
	RenderTools()->DrawUIRect(&GameInfo, vec4(1,1,1,0.25f), CUI::CORNER_ALL, 10.0f);

	GameInfo.Margin(5.0f, &GameInfo);

	x = 5.0f;
	y = 0.0f;

	TextRender()->Text(0, GameInfo.x+x, GameInfo.y+y, 32, Localize("Game info"), 250);
	y += 32.0f+5.0f;

	if(m_pClient->m_Snap.m_pGameInfoObj)
	{
		mem_zero(aBuf, sizeof(aBuf));
		str_format(
			aBuf,
			sizeof(aBuf),
			"\n\n"
			"%s: %s\n"
			"%s: %s\n"
			"%s: %d\n"
			"%s: %d\n"
			"\n"
			"%s: %d/%d\n",
			Localize("Game type"), CurrentServerInfo.m_aGameType,
			Localize("Map"), CurrentServerInfo.m_aMap,
			Localize("Score limit"), m_pClient->m_Snap.m_pGameInfoObj->m_ScoreLimit,
			Localize("Time limit"), m_pClient->m_Snap.m_pGameInfoObj->m_TimeLimit,
			Localize("Players"), m_pClient->m_Snap.m_NumPlayers, CurrentServerInfo.m_MaxClients
		);
		TextRender()->Text(0, GameInfo.x+x, GameInfo.y+y, 20, aBuf, 250);
	}

	// motd
	Motd.HSplitTop(10.0f, 0, &Motd);
	RenderTools()->DrawUIRect(&Motd, vec4(1,1,1,0.25f), CUI::CORNER_ALL, 10.0f);
	Motd.Margin(5.0f, &Motd);
	y = 0.0f;
	x = 5.0f;
	TextRender()->Text(0, Motd.x+x, Motd.y+y, 32, Localize("MOTD"), -1);
	y += 32.0f+5.0f;
	TextRender()->Text(0, Motd.x+x, Motd.y+y, 16, m_pClient->m_pMotd->m_aServerMotd, (int)Motd.w);
}


void CMenus::RenderPopupServerPlayers(CUIRect Screen)
{

    CUIRect MainView;
    Screen.Margin(50.0f, &MainView);

    // render background
    CUIRect Temp, TabBar;
    MainView.VSplitRight(120.0f, &MainView, &TabBar);
    RenderTools()->DrawUIRect(&MainView, STYLE_COLOR_POPUP_BG, 0, 0);

    CUIRect CloseButtonRect;
    MainView.HSplitTop(10.0f, &CloseButtonRect, &MainView);

    CloseButtonRect.VSplitRight(6.0f, &CloseButtonRect, 0);

    static int s_CloseButton = 0;
    int HoverOrClick = DoIcon(&s_CloseButton, SPRITE_GUIICON_TIMES, &CloseButtonRect,
                              CUI::ALIGN_CENTER | CUI::ALIGN_RIGHT, 3.0f);
                              
    if (HoverOrClick > 0) {
        m_Popup = POPUP_NONE;
    }

    if(!m_pClient->m_Snap.m_pLocalInfo) {
         // Show loading spinner
         UI()->DoSpinner(&MainView);
         return;
     }

	CUIRect Button, ButtonBar, Options, Player;

	// player options
	MainView.Margin(10.0f, &Options);
	RenderTools()->DrawUIRect(&Options, vec4(1.0f, 1.0f, 1.0f, 0.25f), CUI::CORNER_ALL, 10.0f);
	Options.Margin(10.0f, &Options);
	Options.HSplitTop(50.0f, &Button, &Options);
	UI()->DoLabelScaled(&Button, Localize("Player options"), 34.0f, -1);

	// headline
	Options.HSplitTop(34.0f, &ButtonBar, &Options);
	ButtonBar.VSplitRight(220.0f, &Player, &ButtonBar);
	UI()->DoLabelScaled(&Player, Localize("Player"), 24.0f, -1);

	ButtonBar.HMargin(1.0f, &ButtonBar);
	float Width = ButtonBar.h*2.0f;
	ButtonBar.VSplitLeft(Width, &Button, &ButtonBar);
	Graphics()->TextureSet(g_pData->m_aImages[IMAGE_GUIICONS].m_Id);
	Graphics()->QuadsBegin();
	RenderTools()->SelectSprite(SPRITE_GUIICON_MUTE);
	IGraphics::CQuadItem QuadItem(Button.x, Button.y, Button.w, Button.h);
	Graphics()->QuadsDrawTL(&QuadItem, 1);
	Graphics()->QuadsEnd();

	ButtonBar.VSplitLeft(20.0f, 0, &ButtonBar);
	ButtonBar.VSplitLeft(Width, &Button, &ButtonBar);
	Graphics()->TextureSet(g_pData->m_aImages[IMAGE_GUIICONS].m_Id);
	Graphics()->QuadsBegin();
	RenderTools()->SelectSprite(SPRITE_GUIICON_FRIEND);
	QuadItem = IGraphics::CQuadItem(Button.x, Button.y, Button.w, Button.h);
	Graphics()->QuadsDrawTL(&QuadItem, 1);
	Graphics()->QuadsEnd();

	// options
	static int s_aPlayerIDs[MAX_CLIENTS][2] = {{0}};
	for(int i = 0, Count = 0; i < MAX_CLIENTS; ++i)
	{
		if(!m_pClient->m_Snap.m_paInfoByTeam[i])
			continue;

		int Index = m_pClient->m_Snap.m_paInfoByTeam[i]->m_ClientID;
		if(Index == m_pClient->m_Snap.m_LocalClientID)
			continue;

		Options.HSplitTop(28.0f, &ButtonBar, &Options);
		if(Count++%2 == 0)
			RenderTools()->DrawUIRect(&ButtonBar, vec4(1.0f, 1.0f, 1.0f, 0.25f), CUI::CORNER_ALL, 10.0f);
		ButtonBar.VSplitRight(220.0f, &Player, &ButtonBar);

		// player info
		Player.VSplitLeft(28.0f, &Button, &Player);
		CTeeRenderInfo Info = m_pClient->m_aClients[Index].m_RenderInfo;
		Info.m_Size = Button.h;
		RenderTools()->RenderTee(CAnimState::GetIdle(), &Info, EMOTE_NORMAL, vec2(1.0f, 0.0f), vec2(Button.x+Button.h/2, Button.y+Button.h/2));

		Player.HSplitTop(1.5f, 0, &Player);
		Player.VSplitMid(&Player, &Button);
		CTextCursor Cursor;
		TextRender()->SetCursor(&Cursor, Player.x, Player.y, 14.0f, TEXTFLAG_RENDER|TEXTFLAG_STOP_AT_END);
		Cursor.m_LineWidth = Player.w;
		TextRender()->TextEx(&Cursor, m_pClient->m_aClients[Index].m_aName, -1);

		TextRender()->SetCursor(&Cursor, Button.x,Button.y, 14.0f, TEXTFLAG_RENDER|TEXTFLAG_STOP_AT_END);
		Cursor.m_LineWidth = Button.w;
		TextRender()->TextEx(&Cursor, m_pClient->m_aClients[Index].m_aClan, -1);

		// ignore button
		ButtonBar.HMargin(2.0f, &ButtonBar);
		ButtonBar.VSplitLeft(Width, &Button, &ButtonBar);
		Button.VSplitLeft((Width-Button.h)/4.0f, 0, &Button);
		Button.VSplitLeft(Button.h, &Button, 0);
		if(g_Config.m_ClShowChatFriends && !m_pClient->m_aClients[Index].m_Friend)
			DoButton_Toggle(&s_aPlayerIDs[Index][0], 1, &Button, false);
		else
			if(DoButton_Toggle(&s_aPlayerIDs[Index][0], m_pClient->m_aClients[Index].m_ChatIgnore, &Button, true))
				m_pClient->m_aClients[Index].m_ChatIgnore ^= 1;

		// friend button
		ButtonBar.VSplitLeft(20.0f, &Button, &ButtonBar);
		ButtonBar.VSplitLeft(Width, &Button, &ButtonBar);
		Button.VSplitLeft((Width-Button.h)/4.0f, 0, &Button);
		Button.VSplitLeft(Button.h, &Button, 0);
		if(DoButton_Toggle(&s_aPlayerIDs[Index][1], m_pClient->m_aClients[Index].m_Friend, &Button, true))
		{
			//if(m_pClient->m_aClients[Index].m_Friend)
			//	m_pClient->Friends()->RemoveFriend(m_pClient->m_aClients[Index].m_aName);
			//else
			//	m_pClient->Friends()->AddFriend(m_pClient->m_aClients[Index].m_aName);
		}
	}
}



void CMenus::RenderServerControlServer(CUIRect MainView)
{
	static int s_VoteList = 0;
	static float s_ScrollValue = 0;
	CUIRect List = MainView;
	UiDoListboxStart(&s_VoteList, &List, 24.0f, "", "", m_pClient->m_pVoting->m_NumVoteOptions, 1, m_CallvoteSelectedOption, s_ScrollValue);

	for(CVoteOptionClient *pOption = m_pClient->m_pVoting->m_pFirst; pOption; pOption = pOption->m_pNext)
	{
		CListboxItem Item = UiDoListboxNextItem(pOption);

		if(Item.m_Visible)
			UI()->DoLabelScaled(&Item.m_Rect, pOption->m_aDescription, 16.0f, -1);
	}

	m_CallvoteSelectedOption = UiDoListboxEnd(&s_ScrollValue, 0);
}

void CMenus::RenderServerControlKick(CUIRect MainView, bool FilterSpectators)
{
	int NumOptions = 0;
	int Selected = -1;
	static int aPlayerIDs[MAX_CLIENTS];
	for(int i = 0; i < MAX_CLIENTS; i++)
	{
		if(!m_pClient->m_Snap.m_paInfoByTeam[i])
			continue;

		int Index = m_pClient->m_Snap.m_paInfoByTeam[i]->m_ClientID;
		if(Index == m_pClient->m_Snap.m_LocalClientID || (FilterSpectators && m_pClient->m_Snap.m_paInfoByTeam[i]->m_Team == TEAM_SPECTATORS))
			continue;
		if(m_CallvoteSelectedPlayer == Index)
			Selected = NumOptions;
		aPlayerIDs[NumOptions++] = Index;
	}

	static int s_VoteList = 0;
	static float s_ScrollValue = 0;
	CUIRect List = MainView;
	UiDoListboxStart(&s_VoteList, &List, 24.0f, "", "", NumOptions, 1, Selected, s_ScrollValue);

	for(int i = 0; i < NumOptions; i++)
	{
		CListboxItem Item = UiDoListboxNextItem(&aPlayerIDs[i]);

		if(Item.m_Visible)
		{
			CTeeRenderInfo Info = m_pClient->m_aClients[aPlayerIDs[i]].m_RenderInfo;
			Info.m_Size = Item.m_Rect.h;
			Item.m_Rect.HSplitTop(5.0f, 0, &Item.m_Rect); // some margin from the top
			RenderTools()->RenderTee(CAnimState::GetIdle(), &Info, EMOTE_NORMAL, vec2(1,0), vec2(Item.m_Rect.x+Item.m_Rect.h/2, Item.m_Rect.y+Item.m_Rect.h/2));
			Item.m_Rect.x +=Info.m_Size;
			UI()->DoLabelScaled(&Item.m_Rect, m_pClient->m_aClients[aPlayerIDs[i]].m_aName, 16.0f, -1);
		}
	}

	Selected = UiDoListboxEnd(&s_ScrollValue, 0);
	m_CallvoteSelectedPlayer = Selected != -1 ? aPlayerIDs[Selected] : -1;
}

void CMenus::RenderPopupServerVotes(CUIRect Screen)
{
    CUIRect MainView;
    Screen.Margin(50.0f, &MainView);
    
    if (m_EscapePressed) {
        m_Popup = POPUP_NONE;
    }

    // render background
    CUIRect Temp, TabBar;
    MainView.VSplitRight(120.0f, &MainView, &TabBar);
    RenderTools()->DrawUIRect(&MainView, ms_ColorTabbarActive, CUI::CORNER_B | CUI::CORNER_TL, 3.0f);

    MainView.HSplitTop(10.0f, 0, &MainView);

    CUIRect Button;

    static int s_ControlPage = 0;
    const char *aTabs[] = {
            Localize("Change settings"),
            Localize("Kick player"),
            Localize("Move player to spectators")
        };

    int NumTabs = (int) (sizeof(aTabs) / sizeof(*aTabs));

    for (int i = 0; i < NumTabs; i++) {
        TabBar.HSplitTop(26, &Button, &TabBar);
        if (DoButton_MenuTab(aTabs[i], aTabs[i], s_ControlPage == i, &Button, CUI::CORNER_R))
            s_ControlPage = i;
        TabBar.HSplitTop(5, &Button, &TabBar);
    }
    

	// render background
	CUIRect Bottom, Extended;
	MainView.HSplitBottom(90.0f, &MainView, &Extended);

	// tab bar

	// render page
	MainView.HSplitBottom(ms_ButtonHeight + 5*2, &MainView, &Bottom);
	Bottom.HMargin(5.0f, &Bottom);

	if(s_ControlPage == 0)
		RenderServerControlServer(MainView);
	else if(s_ControlPage == 1)
		RenderServerControlKick(MainView, false);
	else if(s_ControlPage == 2)
		RenderServerControlKick(MainView, true);

	// vote menu
	{
		CUIRect Button;
		Bottom.VSplitRight(120.0f, &Bottom, &Button);

		static int s_CallVoteButton = 0;
		if(DoButton_Menu(&s_CallVoteButton, Localize("Call vote"), 0, &Button))
		{
			if(s_ControlPage == 0)
				m_pClient->m_pVoting->CallvoteOption(m_CallvoteSelectedOption, m_aCallvoteReason);
			else if(s_ControlPage == 1)
			{
				if(m_CallvoteSelectedPlayer >= 0 && m_CallvoteSelectedPlayer < MAX_CLIENTS &&
					m_pClient->m_Snap.m_paPlayerInfos[m_CallvoteSelectedPlayer])
				{
					m_pClient->m_pVoting->CallvoteKick(m_CallvoteSelectedPlayer, m_aCallvoteReason);
					SetActive(false);
				}
			}
			else if(s_ControlPage == 2)
			{
				if(m_CallvoteSelectedPlayer >= 0 && m_CallvoteSelectedPlayer < MAX_CLIENTS &&
					m_pClient->m_Snap.m_paPlayerInfos[m_CallvoteSelectedPlayer])
				{
					m_pClient->m_pVoting->CallvoteSpectate(m_CallvoteSelectedPlayer, m_aCallvoteReason);
					SetActive(false);
				}
			}
			m_aCallvoteReason[0] = 0;
		}

		// render kick reason
		CUIRect Reason;
		Bottom.VSplitRight(40.0f, &Bottom, 0);
		Bottom.VSplitRight(160.0f, &Bottom, &Reason);
		Reason.HSplitTop(5.0f, 0, &Reason);
		const char *pLabel = Localize("Reason:");
		UI()->DoLabelScaled(&Reason, pLabel, 14.0f, -1);
		float w = TextRender()->TextWidth(0, 14.0f, pLabel, -1);
		Reason.VSplitLeft(w+10.0f, 0, &Reason);
		static float s_Offset = 0.0f;
		DoEditBox(&m_aCallvoteReason, &Reason, m_aCallvoteReason, sizeof(m_aCallvoteReason), 14.0f, &s_Offset, false, CUI::CORNER_ALL);

		// extended features (only available when authed in rcon)
		if(Client()->RconAuthed())
		{
			// background
			Extended.Margin(10.0f, &Extended);
			Extended.HSplitTop(20.0f, &Bottom, &Extended);
			Extended.HSplitTop(5.0f, 0, &Extended);

			// force vote
			Bottom.VSplitLeft(5.0f, 0, &Bottom);
			Bottom.VSplitLeft(120.0f, &Button, &Bottom);
			static int s_ForceVoteButton = 0;
			if(DoButton_Menu(&s_ForceVoteButton, Localize("Force vote"), 0, &Button))
			{
				if(s_ControlPage == 0)
					m_pClient->m_pVoting->CallvoteOption(m_CallvoteSelectedOption, m_aCallvoteReason, true);
				else if(s_ControlPage == 1)
				{
					if(m_CallvoteSelectedPlayer >= 0 && m_CallvoteSelectedPlayer < MAX_CLIENTS &&
						m_pClient->m_Snap.m_paPlayerInfos[m_CallvoteSelectedPlayer])
					{
						m_pClient->m_pVoting->CallvoteKick(m_CallvoteSelectedPlayer, m_aCallvoteReason, true);
						SetActive(false);
					}
				}
				else if(s_ControlPage == 2)
				{
					if(m_CallvoteSelectedPlayer >= 0 && m_CallvoteSelectedPlayer < MAX_CLIENTS &&
						m_pClient->m_Snap.m_paPlayerInfos[m_CallvoteSelectedPlayer])
					{
						m_pClient->m_pVoting->CallvoteSpectate(m_CallvoteSelectedPlayer, m_aCallvoteReason, true);
						SetActive(false);
					}
				}
				m_aCallvoteReason[0] = 0;
			}

			if(s_ControlPage == 0)
			{
				// remove vote
				Bottom.VSplitRight(10.0f, &Bottom, 0);
				Bottom.VSplitRight(120.0f, 0, &Button);
				static int s_RemoveVoteButton = 0;
				if(DoButton_Menu(&s_RemoveVoteButton, Localize("Remove"), 0, &Button))
					m_pClient->m_pVoting->RemovevoteOption(m_CallvoteSelectedOption);


				// add vote
				Extended.HSplitTop(20.0f, &Bottom, &Extended);
				Bottom.VSplitLeft(5.0f, 0, &Bottom);
				Bottom.VSplitLeft(250.0f, &Button, &Bottom);
				UI()->DoLabelScaled(&Button, Localize("Vote description:"), 14.0f, -1);

				Bottom.VSplitLeft(20.0f, 0, &Button);
				UI()->DoLabelScaled(&Button, Localize("Vote command:"), 14.0f, -1);

				static char s_aVoteDescription[64] = {0};
				static char s_aVoteCommand[512] = {0};
				Extended.HSplitTop(20.0f, &Bottom, &Extended);
				Bottom.VSplitRight(10.0f, &Bottom, 0);
				Bottom.VSplitRight(120.0f, &Bottom, &Button);
				static int s_AddVoteButton = 0;
				if(DoButton_Menu(&s_AddVoteButton, Localize("Add"), 0, &Button))
					if(s_aVoteDescription[0] != 0 && s_aVoteCommand[0] != 0)
						m_pClient->m_pVoting->AddvoteOption(s_aVoteDescription, s_aVoteCommand);

				Bottom.VSplitLeft(5.0f, 0, &Bottom);
				Bottom.VSplitLeft(250.0f, &Button, &Bottom);
				static float s_OffsetDesc = 0.0f;
				DoEditBox(&s_aVoteDescription, &Button, s_aVoteDescription, sizeof(s_aVoteDescription), 14.0f, &s_OffsetDesc, false, CUI::CORNER_ALL);

				Bottom.VMargin(20.0f, &Button);
				static float s_OffsetCmd = 0.0f;
				DoEditBox(&s_aVoteCommand, &Button, s_aVoteCommand, sizeof(s_aVoteCommand), 14.0f, &s_OffsetCmd, false, CUI::CORNER_ALL);
			}
		}
	}
}
