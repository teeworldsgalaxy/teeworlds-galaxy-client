#include <engine/config.h>
#include <engine/friends.h>
#include <engine/graphics.h>
#include <engine/keys.h>
#include <engine/serverbrowser.h>
#include <engine/textrender.h>
#include <engine/shared/config.h>
#include <engine/galaxy_client.h>

#include <game/generated/client_data.h>
#include <game/generated/protocol.h>

#include <game/localization.h>
#include <game/version.h>
#include <game/client/render.h>
#include <game/client/ui.h>
#include <game/client/components/countryflags.h>


#include <game/client/animstate.h>
#include <game/localization.h>
#include <game/client/ui/elements/link.h>
#include <game/client/ui/elements/button.h>
#include <game/client/ui/constants.h>
#include "skins.h"

#include "menus.h"
#include "../ui.h"
#include "../../../engine/client/serverbrowser.h"



void CMenus::RenderLoginForm(CUIRect LoginForm) {
    char aBuf[128];

    CUIRect LoginSubmit, InputUsername, InputPassword, Button;
    vec4 ButtonColor = vec4(0.078f, 0.612f, 0.902f, 1.0f);
	vec4 ButtonColorError = vec4(0.917f, 0.2078f, 0.2745f, 1.0f);

	static int s_AccountStatus = Client()->Galaxy()->Account()->GetStatus();

	static float s_UsernameOffset = 0.0f;

	static bool s_UsernameError = false;
	static bool s_Error = false;

	static char s_aPassword[64] = {0};
	static float s_PasswordOffset = 0.0f;
	static bool s_PasswordError = false;

	if (s_AccountStatus != Client()->Galaxy()->Account()->GetStatus())
	{
		s_AccountStatus = Client()->Galaxy()->Account()->GetStatus();
		if(s_AccountStatus == IAccount::ACCOUNT_STATUS_ERROR)
		{
			s_Error = true;
		}
	}
	if(s_AccountStatus == IAccount::ACCOUNT_STATUS_OK)
	{
		mem_zero(&s_aPassword, sizeof(s_aPassword)); // just for security
		g_Config.m_UiPage = PAGE_INTERNET;
		ServerBrowser()->Refresh(IServerBrowser::TYPE_INTERNET);
		m_Popup = POPUP_NEWS;
		Render();
		return;
	}

	bool Submitted = 	s_AccountStatus == IAccount::ACCOUNT_STATUS_WAITING ||
						Client()->Galaxy()->Updater()->GetState() == IUpdater::CHECKING ||
						Client()->Galaxy()->Updater()->GetState() == IUpdater::FAIL ||
						Client()->Galaxy()->Updater()->GetState() == IUpdater::DOWNLOADING;


	//LOGIN

	//LOGIN FORM
	LoginForm.HSplitPerc(0.5f, &LoginForm, &LoginSubmit);

	//USERNAME
	LoginForm.HSplitTop(50.0f, &InputUsername, &LoginForm);


	CUIEditBoxStyle EditBoxStyle;
	EditBoxStyle.CornerSetAll(3.0f);
	//TODO: add corners

	EditBoxStyle.EditBoxSetHidden(false);
	EditBoxStyle.BackgroundSetColor(vec4(0, 0, 0, 0));
	EditBoxStyle.TextSetFontColor(vec4(1, 1, 1, 1.0f));
	EditBoxStyle.TextSetFontSize(15.0f);
	EditBoxStyle.MarginSetLeft(15.0f);


	if(UI()->DoInputTabLogic(&g_Config.m_GalaxyUser, &s_aPassword) ||
			DoEditBox2(&g_Config.m_GalaxyUser, &InputUsername, g_Config.m_GalaxyUser, sizeof(g_Config.m_GalaxyUser), &s_UsernameOffset, "Username", EditBoxStyle))
	{
		s_UsernameError = str_length(g_Config.m_GalaxyUser) == 0;
		s_Error = false;
	}

	vec4 UsernameColor = ButtonColor;

	if (s_UsernameError || s_Error) {
		UsernameColor = ButtonColorError;
	}

	UsernameColor = ColorSwitch(g_Config.m_GalaxyUser, UI()->LastActiveItem() == &g_Config.m_GalaxyUser ?  Brightness(UsernameColor, 0.7f) : UsernameColor, Brightness(UsernameColor, 1.2f), Brightness(UsernameColor, 0.5f));
	DoBorder(&InputUsername, UsernameColor, 1.5f);

	//PASSWORD
	LoginForm.HSplitTop(8.0f, 0, &LoginForm);
	LoginForm.HSplitTop(50.0f, &InputPassword, 0);

	EditBoxStyle.EditBoxSetHidden(true);

	if(UI()->DoInputTabLogic(&s_aPassword, 0, &g_Config.m_GalaxyUser) ||
			DoEditBox2(&s_aPassword, &InputPassword, s_aPassword, sizeof(s_aPassword), &s_PasswordOffset, "Password", EditBoxStyle))
	{
		s_PasswordError = str_length(s_aPassword) == 0;
		s_Error = false;
	}

	vec4 PasswordColor = ButtonColor;

	if (s_PasswordError || s_Error) {
		PasswordColor = ButtonColorError;
	}

	PasswordColor = ColorSwitch(s_aPassword, UI()->LastActiveItem() == &s_aPassword ?  Brightness(PasswordColor, 0.7f) : PasswordColor, Brightness(PasswordColor, 1.2f), Brightness(PasswordColor, 0.5f));
	DoBorder(&InputPassword, PasswordColor, 1.5f);

	//SUBMIT
	LoginSubmit.HSplitTop(50.0f, &Button, &LoginSubmit);


    static CUIButton SubmitButton;
    SubmitButton.SetEnabled(!Submitted);
    SubmitButton.SetLoading(s_AccountStatus == IAccount::ACCOUNT_STATUS_WAITING || Client()->Galaxy()->Updater()->GetState() == IUpdater::DOWNLOADING);
    SubmitButton.SetErroneous(s_AccountStatus == IAccount::ACCOUNT_STATUS_ERROR);
	SubmitButton.SetHotkeys()
			.Add(KEY_RETURN)
			.Add(KEY_KP_ENTER);

	const char *pDownloadLabelString = 0;
    const char *pCurrent = Client()->Galaxy()->Updater()->GetCurrentFile();

	switch (Client()->Galaxy()->Updater()->GetState()) {
		case IUpdater::NEED_UPDATE:
			str_format(aBuf, sizeof(aBuf), "Install Updates (%s)", Client()->Galaxy()->Updater()->NextVersion());
            SubmitButton.SetText(aBuf);
			break;
		case IUpdater::DOWNLOADING:
            SubmitButton.SetText("Downloading ...");
            if (pCurrent != nullptr) {
                str_format(aBuf, sizeof(aBuf),"%d%% (%s)", Client()->Galaxy()->Updater()->GetCurrentPercent(), pCurrent);
            } else {
                str_format(aBuf, sizeof(aBuf),"%d%%", Client()->Galaxy()->Updater()->GetCurrentPercent());
            }
			pDownloadLabelString = aBuf;
			break;
		case IUpdater::CHECKING:
            SubmitButton.SetText("Checking for updates");
			break;
		case IUpdater::FAIL:
            SubmitButton.SetText("An error occured!");
            SubmitButton.SetErroneous(true);
			break;
		default:
			SubmitButton.SetText("Play");
			break;
	}

	SubmitButton.Create(Button, RenderTools());

	if(SubmitButton.Pressed(Input()))
	{
		if (Client()->Galaxy()->Updater()->GetState() == IUpdater::NEED_UPDATE || Client()->Galaxy()->Updater()->GetState() == IUpdater::FAIL ) {
			Client()->Galaxy()->Updater()->Update();
		}
		else {
			//check if input is available
			if (str_length(g_Config.m_GalaxyUser) == 0) {
				s_UsernameError = true;
			}

			if(str_length(s_aPassword) == 0) {
				s_PasswordError = true;
			}

			if(!s_Error && str_length(g_Config.m_GalaxyUser) != 0 && str_length(s_aPassword) != 0) {
				Client()->Galaxy()->Account()->Login(g_Config.m_GalaxyUser, s_aPassword);
				s_AccountStatus = IAccount::ACCOUNT_STATUS_WAITING;
				s_Error = false;
			}
		}
	}

	// visualize update progress
	CUIRect DownloadLabel, DownloadProgress;

	LoginSubmit.HSplitTop(8.0f, 0, &DownloadProgress);
	DownloadProgress.HSplitTop(5.0f, &DownloadProgress, &DownloadLabel);
	DownloadLabel.HSplitTop(20.0f, &DownloadLabel, 0);

	if (Client()->Galaxy()->Updater()->GetState() == IUpdater::DOWNLOADING) {
		RenderTools()->DrawUIRect(&DownloadProgress, Brightness(ButtonColor, 0.5f), 0, 0.0f);

		DownloadProgress.VSplitPerc(Client()->Galaxy()->Updater()->GetProgress(), &DownloadProgress, 0);
		RenderTools()->DrawUIRect(&DownloadProgress, ButtonColor, 0, 0.0f);
	}

	if (pDownloadLabelString) {
		UI()->DoLabel(&DownloadLabel, pDownloadLabelString, 12.0f, CUI::ALIGN_CENTER);
	}

	// do forgot password link
	{
		CUIRect Rect;
		LoginSubmit.HSplitTop(35.0f, 0, &Rect);
		Rect.HSplitTop(20.0f, &Rect, 0);

		if(Client()->Galaxy()->Updater()->GetState() == IUpdater::OK)
		{
			static CUILink RecoverLink;
			RecoverLink.GetStyle().TextSetFontSize(15.0f);
			RecoverLink.SetEnabled(!Submitted);
			RecoverLink.GetTooltip().SetText(Localize("Forgot your password?"));
			RecoverLink.SetText(Localize("Recover Account"));
			RecoverLink.Create(Rect, RenderTools());
			if(RecoverLink.Pressed())
			{
				g_Config.m_UiPage = PAGE_RECOVER;
				s_UsernameError = false;
				s_PasswordError = false;
			}
		}
	}

	// do register link
	{
		CUIRect Rect;
		LoginSubmit.HSplitTop(55.0f, 0, &Rect);
		Rect.HSplitTop(20.0f, &Rect, 0);

		if(Client()->Galaxy()->Updater()->GetState() == IUpdater::OK)
		{
			static CUILink RegisterLink;
			RegisterLink.GetStyle().TextSetFontSize(15.0f);
			RegisterLink.SetEnabled(!Submitted);
			RegisterLink.GetTooltip().SetText(Localize("Join the Galaxy"));
			RegisterLink.SetText(Localize("Register now!"));
			RegisterLink.Create(Rect, RenderTools());
			if(RegisterLink.Pressed())
			{
				g_Config.m_UiPage = PAGE_REGISTER;
				s_UsernameError = false;
				s_PasswordError = false;
			}
		}
	}
}

void CMenus::RenderRecoverForm(CUIRect LoginForm)
{
	char aBuf[128];

	CUIRect RecoverSubmit, InputEmail, Button;
	vec4 ButtonColor = vec4(0.078f, 0.612f, 0.902f, 1.0f);
	vec4 ButtonColorError = vec4(0.917f, 0.2078f, 0.2745f, 1.0f);

	int AccountStatus = Client()->Galaxy()->Account()->GetStatus();

	static float s_UsernameOffset = 0.0f;

	static bool s_UsernameError = false;
	static bool s_Error = false;

	bool Submitted = 	AccountStatus == IAccount::ACCOUNT_STATUS_WAITING ||
						Client()->Galaxy()->Updater()->GetState() == IUpdater::CHECKING ||
						Client()->Galaxy()->Updater()->GetState() == IUpdater::FAIL ||
						Client()->Galaxy()->Updater()->GetState() == IUpdater::DOWNLOADING;


	//FORM
	LoginForm.HSplitPerc(0.5f, &LoginForm, &RecoverSubmit);

	//USERNAME
	LoginForm.HSplitTop(50.0f, &InputEmail, &LoginForm);

	CUIEditBoxStyle EditBoxStyle;
	EditBoxStyle.CornerSetAll(3.0f);
	//TODO: add corners

	EditBoxStyle.EditBoxSetHidden(false);
	EditBoxStyle.BackgroundSetColor(vec4(0, 0, 0, 0));
	EditBoxStyle.TextSetFontColor(vec4(1, 1, 1, 1.0f));
	EditBoxStyle.TextSetFontSize(15.0f);
	EditBoxStyle.EditBoxSetPlaceholderColor(vec4(1, 1, 1, 0.6f));
	EditBoxStyle.MarginSetLeft(15.0f);

	static char aEmail[256] = {0};
	if(UI()->DoInputTabLogic(&aEmail, 0) ||
	   DoEditBox2(&aEmail, &InputEmail, aEmail, sizeof(aEmail), &s_UsernameOffset, "E-Mail", EditBoxStyle))
	{
		s_UsernameError = str_length(aEmail) == 0;
		s_Error = false;
	}

	vec4 UsernameColor = ButtonColor;

	if (s_UsernameError || s_Error) {
		UsernameColor = ButtonColorError;
	}

	UsernameColor = ColorSwitch(aEmail, UI()->LastActiveItem() == &aEmail ?  Brightness(UsernameColor, 0.7f) : UsernameColor, Brightness(UsernameColor, 1.2f), Brightness(UsernameColor, 0.5f));
	DoBorder(&InputEmail, UsernameColor, 1.5f);


	// print the error if there was any
	if(Client()->Galaxy()->Account()->GetFormError("0"))
	{
		CUIRect Rect;
		InputEmail.HSplitTop(60.0f, 0, &Rect);
		Rect.HSplitTop(17.0f, &Rect, 0);

		UI()->DoLabelScaled(&Rect, Client()->Galaxy()->Account()->GetFormError("0"), 12.0f, 0);
	}


	//SUBMIT
	RecoverSubmit.HSplitTop(50.0f, &Button, &RecoverSubmit);

	static CUIButton SubmitButton;
	SubmitButton.SetText("Request Password");
	SubmitButton.GetStyle().TextSetFontSize(15.0f);
	SubmitButton.SetErroneous(AccountStatus == IAccount::ACCOUNT_STATUS_ERROR);
	SubmitButton.SetEnabled(!Submitted);
	SubmitButton.SetHotkeys()
			.Add(KEY_RETURN)
			.Add(KEY_KP_ENTER);

	SubmitButton.Create(Button, RenderTools());
	if(SubmitButton.Pressed(Input()))
	{
		if (Client()->Galaxy()->Updater()->GetState() == IUpdater::NEED_UPDATE) {
			Client()->Galaxy()->Updater()->Update();
		} else {
			//check if input is available
			if (str_length(aEmail) == 0) {
				s_UsernameError = true;
			}

			if(!s_Error && str_length(aEmail) != 0)
			{
				Client()->Galaxy()->Account()->RequestPassword(aEmail);
				s_Error = false;
			}
		}
	}

	// do login password link
	{
		CUIRect Rect;
		RecoverSubmit.HSplitTop(35.0f, 0, &Rect);
		Rect.HSplitTop(20.0f, &Rect, 0);

		if(Client()->Galaxy()->Updater()->GetState() == IUpdater::OK)
		{
			static CUILink LoginLink;
			LoginLink.GetStyle().TextSetFontSize(15.0f);
			LoginLink.SetEnabled(!Submitted);
			LoginLink.GetTooltip().SetText(Localize("Login into your Galaxy account"));
			LoginLink.SetText(Localize("Login"));
			LoginLink.Create(Rect, RenderTools());
			if(LoginLink.Pressed())
				g_Config.m_UiPage = PAGE_LOGIN;
		}
	}

	// do register link
	{
		CUIRect Rect;
		RecoverSubmit.HSplitTop(55.0f, 0, &Rect);
		Rect.HSplitTop(20.0f, &Rect, 0);

		if(Client()->Galaxy()->Updater()->GetState() == IUpdater::OK)
		{
			static CUILink RegisterLink;
			RegisterLink.GetStyle().TextSetFontSize(15.0f);
			RegisterLink.SetEnabled(!Submitted);
			RegisterLink.GetTooltip().SetText(Localize("Join the Galaxy"));
			RegisterLink.SetText(Localize("Register now!"));
			RegisterLink.Create(Rect, RenderTools());
			if(RegisterLink.Pressed())
				g_Config.m_UiPage = PAGE_REGISTER;
		}
	}
}

void CMenus::RenderRegisterForm(CUIRect RegisterForm)
{
	char aBuf[128];

	CUIRect RegisterSubmit, LoginLink, InputUsername, InputEmail, InputPassword, InputPasswordRepeat, Button, ButtonLabel;
	vec4 ButtonColor = vec4(0.078f, 0.612f, 0.902f, 1.0f);
	vec4 ButtonColorError = vec4(0.917f, 0.2078f, 0.2745f, 1.0f);

	static int s_AccountStatus = Client()->Galaxy()->Account()->GetStatus();

	static float s_UsernameOffset = 0.0f;

	static bool s_UsernameError = false;
	static bool s_Error = false;

	static char s_aPassword[64] = {0};
	static char s_aPasswordRepeat[64] = {0};
	static float s_PasswordOffset = 0.0f;
	static float s_PasswordRepeatOffset = 0.0f;
	static bool s_PasswordError = false;

	static char s_aEmail[128] = {0};
	static float s_EmailOffset = 0.0f;
	static float s_EmailError = false;

	if(s_AccountStatus != Client()->Galaxy()->Account()->GetStatus())
	{
		s_AccountStatus = Client()->Galaxy()->Account()->GetStatus();

		if(s_AccountStatus == IAccount::ACCOUNT_STATUS_ERROR)
		{
			s_Error = true;
		} else if(s_AccountStatus == IAccount::ACCOUNT_STATUS_OK)
		{
			g_Config.m_UiPage = PAGE_NEWS;
			ServerBrowser()->Refresh(IServerBrowser::TYPE_INTERNET);
			m_Popup = POPUP_NEWS;
			Render();
			return;
		}
	}

	bool Submitted = s_AccountStatus == IAccount::ACCOUNT_STATUS_WAITING;

	//REGISTER FORM
	RegisterForm.HSplitPerc(0.5f, &RegisterForm, &RegisterSubmit);

	//USERNAME
	RegisterForm.HSplitTop(40.0f, &InputUsername, &RegisterForm);
	RegisterForm.HSplitTop(8.0f, 0, &RegisterForm);


	CUIRect UserNameError = InputUsername;
	UserNameError.x += InputUsername.w + 5.0f;

	vec4 UserNameErrorColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);
	const char *pUserNameError = Client()->Galaxy()->Account()->GetFormError("name");
	if(pUserNameError)
	{
		UI()->DoLabel(&UserNameError, pUserNameError, 12.0f, CUI::ALIGN_CENTER | CUI::ALIGN_LEFT, -1, &UserNameErrorColor);
	}


	CUIEditBoxStyle EditBoxStyle;
	EditBoxStyle.CornerSetAll(3.0f);
	//TODO: add corners

	EditBoxStyle.EditBoxSetHidden(false);
	EditBoxStyle.BackgroundSetColor(vec4(0, 0, 0, 0));
	EditBoxStyle.TextSetFontColor(vec4(1, 1, 1, 1.0f));
	EditBoxStyle.TextSetFontSize(15.0f);
	EditBoxStyle.EditBoxSetPlaceholderColor(vec4(1, 1, 1, 0.6f));
	EditBoxStyle.MarginSetLeft(15.0f);

	if(UI()->DoInputTabLogic(&g_Config.m_GalaxyUser, &s_aEmail) ||
	   DoEditBox2(&g_Config.m_GalaxyUser, &InputUsername, g_Config.m_GalaxyUser, sizeof(g_Config.m_GalaxyUser), &s_UsernameOffset, "Name", EditBoxStyle))
	{
		s_UsernameError = str_length(g_Config.m_GalaxyUser) == 0;
		s_Error = false;
	}

	vec4 UsernameColor = ButtonColor;

	if(s_UsernameError || s_Error)
	{
		UsernameColor = ButtonColorError;
	}

	UsernameColor = ColorSwitch(g_Config.m_GalaxyUser, UI()->LastActiveItem() == &g_Config.m_GalaxyUser ? Brightness(UsernameColor, 0.7f) : UsernameColor, Brightness(UsernameColor, 1.2f), Brightness(UsernameColor, 0.5f));
	DoBorder(&InputUsername, UsernameColor, 1.5f);

	//EMAIL
	RegisterForm.HSplitTop(40.0f, &InputEmail, &RegisterForm);


	CUIRect EmailError = InputEmail;
	EmailError.x += InputEmail.w + 5.0f;

	vec4 EmailErrorColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);
	const char *pEmailError = Client()->Galaxy()->Account()->GetFormError("email");
	if(pEmailError)
	{
		UI()->DoLabel(&EmailError, pEmailError, 12.0f, CUI::ALIGN_CENTER | CUI::ALIGN_LEFT, -1, &EmailErrorColor);
	}

	if(UI()->DoInputTabLogic(&s_aEmail, &s_aPassword, &g_Config.m_GalaxyUser) ||
	   DoEditBox2(&s_aEmail, &InputEmail, s_aEmail, sizeof(s_aEmail), &s_EmailOffset, "Email", EditBoxStyle))
	{
		s_EmailError = str_length(s_aEmail) == 0;
		s_Error = false;
	}

	vec4 EmailColor = ButtonColor;

	if(s_EmailError || s_Error)
	{
		EmailColor = ButtonColorError;
	}

	EmailColor = ColorSwitch(s_aEmail, UI()->LastActiveItem() == &s_aEmail ? Brightness(EmailColor, 0.7f) : EmailColor, Brightness(EmailColor, 1.2f), Brightness(EmailColor, 0.5f));
	DoBorder(&InputEmail, EmailColor, 1.5f);

	//PASSWORD
	RegisterForm.HSplitTop(8.0f, 0, &RegisterForm);
	RegisterForm.HSplitTop(40.0f, &InputPassword, &RegisterForm);
	RegisterForm.HSplitTop(8.0f, 0, &RegisterForm);
	RegisterForm.HSplitTop(40.0f, &InputPasswordRepeat, 0);

	EditBoxStyle.EditBoxSetHidden(true);


    vec4 PWErrorColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);
    CUIRect PWError = InputPassword;
    PWError.x += InputPassword.w + 5.0f;

    const char *pPWError = Client()->Galaxy()->Account()->GetFormError("password");
    if(pPWError)
    {
        UI()->DoLabel(&PWError, pPWError, 12.0f, CUI::ALIGN_CENTER | CUI::ALIGN_LEFT, -1, &PWErrorColor);
    }

	if(UI()->DoInputTabLogic(&s_aPassword, s_aPasswordRepeat, &s_aEmail) ||
	   DoEditBox2(&s_aPassword, &InputPassword, s_aPassword, sizeof(s_aPassword), &s_PasswordOffset, "Password", EditBoxStyle))
	{
		s_PasswordError = str_length(s_aPassword) == 0;
		s_Error = false;
	}
	vec4 PasswordColor = ButtonColor;

	if(s_PasswordError || s_Error)
	{
		PasswordColor = ButtonColorError;
	}
	PasswordColor = ColorSwitch(s_aPassword, UI()->LastActiveItem() == &s_aPassword ? Brightness(PasswordColor, 0.7f) : PasswordColor, Brightness(PasswordColor, 1.2f), Brightness(PasswordColor, 0.5f));

	DoBorder(&InputPassword, PasswordColor, 1.5f);

	if(UI()->DoInputTabLogic(&s_aPasswordRepeat, 0, &s_aPassword) ||
	   DoEditBox2(&s_aPasswordRepeat, &InputPasswordRepeat, s_aPasswordRepeat, sizeof(s_aPasswordRepeat), &s_PasswordRepeatOffset, "Repeat password", EditBoxStyle))
	{
		s_PasswordError = str_length(s_aPasswordRepeat) == 0;
		s_Error = false;
	}

	vec4 PasswordColorRepeat = ButtonColor;

	if(s_PasswordError || s_Error)
	{
		PasswordColorRepeat = ButtonColorError;
	}
	PasswordColorRepeat = ColorSwitch(s_aPasswordRepeat, UI()->LastActiveItem() == &s_aPasswordRepeat ? Brightness(PasswordColorRepeat, 0.7f) : PasswordColorRepeat, Brightness(PasswordColorRepeat, 1.2f), Brightness(PasswordColorRepeat, 0.5f));

	DoBorder(&InputPasswordRepeat, PasswordColorRepeat, 1.5f);

	//SUBMIT
	RegisterSubmit.HSplitTop(50.0f, &Button, &RegisterSubmit);

	static CUIButton SubmitButton;
	SubmitButton.SetText("Register");
	SubmitButton.GetStyle().TextSetFontSize(15.0f);
	SubmitButton.SetErroneous(s_AccountStatus == IAccount::ACCOUNT_STATUS_ERROR);
	SubmitButton.SetEnabled(!Submitted);
	SubmitButton.SetHotkeys()
			.Add(KEY_RETURN)
			.Add(KEY_KP_ENTER);

	SubmitButton.Create(Button, RenderTools());
	if(SubmitButton.Pressed(Input()))
	{
		// check if input is available
		if(str_length(g_Config.m_GalaxyUser) == 0)
		{
			s_UsernameError = true;
		}

		if(str_length(s_aPassword) == 0 || str_comp(s_aPassword, s_aPasswordRepeat))
		{
			s_PasswordError = true;
		}

		if(!s_UsernameError && !s_PasswordError && !s_Error && str_length(s_aEmail) != 0 && str_length(s_aPassword) != 0)
		{
			Client()->Galaxy()->Account()->Register(g_Config.m_GalaxyUser, s_aEmail, s_aPassword);
			s_AccountStatus = IAccount::ACCOUNT_STATUS_WAITING;
			mem_zero(s_aPassword, sizeof(s_aPassword));
			mem_zero(s_aPasswordRepeat, sizeof(s_aPasswordRepeat));
		}
	}

	RegisterSubmit.HSplitTop(50.0f, 0, &LoginLink);
	LoginLink.HSplitTop(20.0f, &LoginLink, 0);

	static CUILink LinkElement;
	LinkElement.GetStyle().TextSetFontSize(15.0f);
	LinkElement.SetEnabled(!Submitted);
	LinkElement.GetTooltip().SetText(Localize("Login into your Galaxy account"));
	LinkElement.SetText(Localize("Login"));
	LinkElement.Create(LoginLink, RenderTools());
	if(LinkElement.Pressed())
	{
		mem_zero(s_aPassword, sizeof(s_aPassword));
		mem_zero(s_aPasswordRepeat, sizeof(s_aPasswordRepeat));
		g_Config.m_UiPage = PAGE_LOGIN;
	}
}

void CMenus::RenderLogin(CUIRect MainView)
{
	CUIRect LoginSection, LoginForm;
	CUIRect Logo, SectionLeft, SectionRight;

	MainView.VSplitPerc(0.33f, &SectionLeft, &LoginSection);
	LoginSection.VSplitPerc(0.5f, &LoginSection, &SectionRight);


	LoginSection.HSplitPerc(0.33f, &Logo, &LoginForm);

	LoginForm.VMargin(60.0f, &LoginForm);

	// render particles
	static CLightParticle aParticles[24];

	static bool s_Init = true;
	if(s_Init)
	{
		GenerateLightParticles(&Logo, aParticles, 6);
		GenerateLightParticles(&LoginForm, &aParticles[6], 6);
		GenerateLightParticles(&SectionLeft, &aParticles[12], 6);
		GenerateLightParticles(&SectionRight, &aParticles[18], 6);
		s_Init = false;
	}
	else
	{
		for(int i = 0; i < 24; ++i)
		{
			aParticles[i].Update(Client()->RenderFrameTime(), Client()->LocalTime());
			RenderLightParticle(&aParticles[i]);
		}
	}

	// draw logo
	RenderTools()->DrawUIRectTexture(&Logo, IMAGE_BANNER, vec4(1.0f, 1.0f, 1.0f, 1.0f), STYLE_LOGO_SIZE.x, STYLE_LOGO_SIZE.y);
	if(g_Config.m_UiPage == PAGE_LOGIN)
		RenderLoginForm(LoginForm);
	else if(g_Config.m_UiPage == PAGE_RECOVER)
		RenderRecoverForm(LoginForm);
	else
		RenderRegisterForm(LoginForm);

	//Draw top right menu
	CUIRect Menu = SectionRight;

	Menu.Margin(20.0f, &Menu);

	CUIRect MenuLanguage, MenuQuit;
	Menu.VSplitPerc(0.5f, &MenuLanguage, &MenuQuit);

	MenuLanguage.h = 15.0f;
	MenuQuit.h = 15.0f;

	static CUILink LanguageLink;
	LanguageLink.GetTooltip().SetText(Localize("Change the language"));
	LanguageLink.GetStyle().TextSetFontSize(15.0f);
	LanguageLink.SetText(Localize("Language"));
	LanguageLink.Create(MenuLanguage, RenderTools());
	if(LanguageLink.Pressed())
		m_Popup = POPUP_LANGUAGE;

	static CUILink QuitLink;
	QuitLink.GetTooltip().SetText(Localize("Leave the Galaxy"));
	QuitLink.GetStyle().TextSetFontSize(15.0f);
	QuitLink.SetText(Localize("Quit"));
	QuitLink.Create(MenuQuit, RenderTools());
	if(QuitLink.Pressed())
		m_Popup = POPUP_QUIT;
}

void CMenus::OnLogout()
{
	g_Config.m_UiPage = PAGE_LOGIN;
	m_Popup = POPUP_NONE;
	dbg_msg("DEBUG", "logged out!!");
}
