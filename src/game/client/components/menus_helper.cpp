#include <engine/config.h>
#include <engine/friends.h>
#include <engine/graphics.h>
#include <engine/keys.h>
#include <engine/serverbrowser.h>
#include <engine/textrender.h>
#include <engine/shared/config.h>
#include <engine/galaxy_client.h>

#include <game/generated/client_data.h>
#include <game/generated/protocol.h>

#include <game/localization.h>
#include <game/version.h>
#include <game/client/render.h>
#include <game/client/ui.h>
#include <game/client/components/countryflags.h>


#include <game/client/animstate.h>
#include <game/localization.h>
#include <game/client/ui/elements/link.h>
#include <game/client/ui/elements/button.h>
#include <game/client/ui/constants.h>
#include "skins.h"

#include "menus.h"
#include "../ui.h"
#include "../../../engine/client/serverbrowser.h"


void CMenus::RenderLightParticle(CLightParticle *pParticle) {

	vec4 OutlineColor = vec4(1.0f, 1.0f, 1.0f, 0.0f);
	vec4 InnerColor = vec4(1.0f, 1.0f, 1.0f, 0.2f*pParticle->m_Transparency);
	float dR = frandom()/10.0f;
	float dB = frandom()/10.0f;
	RenderTools()->DrawCircle(pParticle->m_Pos.x, pParticle->m_Pos.y, pParticle->m_CurrentRadius , vec4(1.0f+dR, 1.0f, 1.0f+dB, 0.4f*pParticle->m_Transparency), &OutlineColor);
	RenderTools()->DrawCircle(pParticle->m_Pos.x, pParticle->m_Pos.y, pParticle->m_InnerRadius, vec4(1.0f+dR, 1.0f, 1.0f+dB, 0.7f*pParticle->m_Transparency), &InnerColor);
}

void CMenus::GenerateLightParticles(CUIRect *pArea, CLightParticle aParticles[], int Num) {

	for(int i = 0; i < Num; ++i) {
		aParticles[i].m_StartPos.x = pArea->x + rand()% (int) pArea->w;
		aParticles[i].m_StartPos.y = pArea->y + rand()% (int) pArea->h;
		aParticles[i].m_Speed = mix(0.5f, 1.0f, frandom());
		aParticles[i].m_InnerRadius = mix(1.5f, 3.0f, frandom());
		aParticles[i].m_MinRadius = aParticles[i].m_InnerRadius + mix(1.5f, 5.0f, frandom());
		aParticles[i].m_MaxRadius = aParticles[i].m_MinRadius + mix(2.0f, 3.0f, frandom());
		aParticles[i].m_CurrentRadius = mix(aParticles[i].m_InnerRadius, aParticles[i].m_MaxRadius, frandom());
		aParticles[i].m_Transparency = -0.5f;
		aParticles[i].Update(Client()->RenderFrameTime(), Client()->LocalTime());
	}
}