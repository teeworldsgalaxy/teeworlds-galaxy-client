/* (c) Magnus Auvinen. See licence.txt in the root of the distribution for more information. */
/* If you are missing that file, acquire a complete release at teeworlds.com.                */
#include <math.h>

#include <base/system.h>
#include <base/math.h>
#include <base/vmath.h>

#include <engine/config.h>
#include <engine/editor.h>
#include <engine/engine.h>
#include <engine/friends.h>
#include <engine/graphics.h>
#include <engine/keys.h>
#include <engine/serverbrowser.h>
#include <engine/storage.h>
#include <engine/textrender.h>
#include <engine/shared/config.h>
#include <engine/galaxy_client.h>

#include <game/version.h>
#include <game/generated/protocol.h>

#include <game/generated/client_data.h>
#include <game/client/components/sounds.h>
#include <game/client/gameclient.h>
#include <game/client/lineinput.h>
#include <game/localization.h>
#include <mastersrv/mastersrv.h>
#include <game/client/ui/elements/link.h>
#include <game/client/ui/elements/box.h>
#include <game/client/ui/elements/edit_box.h>
#include <game/client/ui/elements/scrollbar.h>
#include <game/client/ui/elements/icon.h>
#include <game/client/ui/constants.h>

#include "countryflags.h"
#include "menus.h"
#include "skins.h"




vec4 CMenus::ms_GuiColor;
vec4 CMenus::ms_ColorTabbarInactiveOutgame;
vec4 CMenus::ms_ColorTabbarActiveOutgame;
vec4 CMenus::ms_ColorTabbarInactive;
vec4 CMenus::ms_ColorTabbarActive = vec4(0,0,0,0.8f);
vec4 CMenus::ms_ColorTabbarInactiveIngame;
vec4 CMenus::ms_ColorTabbarActiveIngame;

float CMenus::ms_ButtonHeight = 25.0f;
float CMenus::ms_ListheaderHeight = 17.0f;
float CMenus::ms_FontmodHeight = 0.8f;

IInput::CEvent CMenus::m_aInputEvents[MAX_INPUTEVENTS];
int CMenus::m_NumInputEvents;


CMenus::CMenus()
{
	m_Popup = POPUP_NONE;
	m_LastPopup = POPUP_NONE;
	m_ActivePage = PAGE_INTERNET;
	m_GamePage = PAGE_GAME;

	m_NeedRestartGraphics = false;
	m_NeedRestartSound = false;
	m_NeedSendinfo = false;
	m_MenuActive = true;
	m_UseMouseButtons = true;

	m_EscapePressed = false;
	m_EnterPressed = false;
	m_DeletePressed = false;
	m_NumInputEvents = 0;

	m_LastInput = time_get();

	str_copy(m_aCurrentDemoFolder, "demos", sizeof(m_aCurrentDemoFolder));
	m_aCallvoteReason[0] = 0;

	m_FriendlistSelectedIndex = -1;
	m_FriendActiveActionsIndex = -1;
}

vec4 CMenus::ButtonColorMul(const void *pID)
{
	return ColorSwitch(pID, vec4(1,1,1,1), vec4(1,1,1,1.5f), vec4(1,1,1,0.5f));
}

vec4 CMenus::ButtonColorMul(const void *pID, vec4 DefaultColor)
{
	return ButtonColorMul(pID)*DefaultColor;
}

vec4 CMenus::ColorSwitch(const void *pID, vec4 DefaultColor, vec4 HoverColor, vec4 ClickColor) {
	if(UI()->ActiveItem() == pID)
		return ClickColor;
	else if(UI()->HotItem() == pID)
		return HoverColor;
	return DefaultColor;
}

int CMenus::DoButton_Icon(int ImageId, int SpriteId, const CUIRect *pRect)
{
	Graphics()->TextureSet(g_pData->m_aImages[ImageId].m_Id);

	Graphics()->QuadsBegin();
	RenderTools()->SelectSprite(SpriteId);
	IGraphics::CQuadItem QuadItem(pRect->x, pRect->y, pRect->w, pRect->h);
	Graphics()->QuadsDrawTL(&QuadItem, 1);
	Graphics()->QuadsEnd();

	return 0;
}

int CMenus::DoButton_Toggle(const void *pID, int Checked, const CUIRect *pRect, bool Active)
{
	Graphics()->TextureSet(g_pData->m_aImages[IMAGE_GUIBUTTONS].m_Id);
	Graphics()->QuadsBegin();
	if(!Active)
		Graphics()->SetColor(1.0f, 1.0f, 1.0f, 0.5f);
	RenderTools()->SelectSprite(Checked?SPRITE_GUIBUTTON_ON:SPRITE_GUIBUTTON_OFF);
	IGraphics::CQuadItem QuadItem(pRect->x, pRect->y, pRect->w, pRect->h);
	Graphics()->QuadsDrawTL(&QuadItem, 1);
	if(UI()->HotItem() == pID && Active)
	{
		RenderTools()->SelectSprite(SPRITE_GUIBUTTON_HOVER);
		IGraphics::CQuadItem QuadItem(pRect->x, pRect->y, pRect->w, pRect->h);
		Graphics()->QuadsDrawTL(&QuadItem, 1);
	}
	Graphics()->QuadsEnd();

	return Active ? UI()->DoButtonLogic(pID, "", Checked, pRect) : 0;
}

int CMenus::DoButton_Menu(const void *pID, const char *pText, int Checked, const CUIRect *pRect)
{
	RenderTools()->DrawUIRect(pRect, vec4(1,1,1,0.5f)*ButtonColorMul(pID));
	CUIRect Temp;
	pRect->HMargin(pRect->h>=20.0f?2.0f:1.0f, &Temp);
	UI()->DoLabel(&Temp, pText, Temp.h*ms_FontmodHeight, 0);
	return UI()->DoButtonLogic(pID, pText, Checked, pRect);
}

void CMenus::DoButton_KeySelect(const void *pID, const char *pText, int Checked, const CUIRect *pRect)
{
	RenderTools()->DrawUIRect(pRect, vec4(1,1,1,0.5f)*ButtonColorMul(pID));
	CUIRect Temp;
	pRect->HMargin(1.0f, &Temp);
	UI()->DoLabel(&Temp, pText, Temp.h*ms_FontmodHeight, 0);
}

int CMenus::DoButton_MenuTab(const void *pID, const char *pText, int Checked, const CUIRect *pRect, int Corners, float Radius)
{

    int HoverOrClick = UI()->DoRectLogic(pID, pRect, !Checked);

    if(Checked)
		RenderTools()->DrawUIRect(pRect, ms_ColorTabbarActive, Corners, Radius);
    else if (HoverOrClick == -1)        //OnHover
        RenderTools()->DrawUIRect(pRect, STYLE_COLOR_TAB_HOVER, Corners, Radius);
	else
		RenderTools()->DrawUIRect(pRect, ms_ColorTabbarInactive, Corners, Radius);
	CUIRect Temp;
	pRect->HMargin(2.0f, &Temp);
	UI()->DoLabel(&Temp, pText, Temp.h*ms_FontmodHeight, 0);

	return HoverOrClick == 1;
}

int CMenus::DoButton_GridHeader(const void *pID, const char *pText, int Checked, const CUIRect *pRect)
//void CMenus::ui_draw_grid_header(const void *id, const char *text, int checked, const CUIRect *r, const void *extra)
{
	CUIRect t;
	pRect->VSplitLeft(5.0f, 0, &t);
	vec4 Color = STYLE_COLOR_TEXT_LIGHT;
	UI()->DoLabel(&t, pText, pRect->h*ms_FontmodHeight, CUI::ALIGN_LEFT|CUI::ALIGN_TOP, -2, Checked ? &Color : 0);
	return UI()->DoButtonLogic(pID, pText, Checked, pRect);
}

int CMenus::DoButton_CheckBox_Common(const void *pID, const char *pText, const char *pBoxText, const CUIRect *pRect, bool HoverSound)
//void CMenus::ui_draw_checkbox_common(const void *id, const char *text, const char *boxtext, const CUIRect *r, const void *extra)
{
	CUIRect c = *pRect;
	CUIRect t = *pRect;
	c.w = c.h;
	t.x += c.w;
	t.w -= c.w;
	t.VSplitLeft(5.0f, 0, &t);

	c.Margin(2.0f, &c);
	RenderTools()->DrawUIRect(&c, vec4(1,1,1,0.25f)*ButtonColorMul(pID), CUI::CORNER_ALL, 3.0f);
	c.y += 2;
	UI()->DoLabel(&c, pBoxText, pRect->h*ms_FontmodHeight*0.6f, 0);
	UI()->DoLabel(&t, pText, pRect->h*ms_FontmodHeight*0.8f, -1);
	return UI()->DoButtonLogic(pID, pText, 0, pRect, HoverSound);
}

int CMenus::DoButton_CheckBox(const void *pID, const char *pText, int Checked, const CUIRect *pRect, int Alignment, float BoxMargin, bool HoverSound)
{
	  CUIRect CheckboxRect;
	  CheckboxRect.w = 128;
	  CheckboxRect.h = 128;

	pRect->Align(&CheckboxRect, Alignment, BoxMargin, BoxMargin);
	CUIRect CheckboxLabel = *pRect;

	CheckboxLabel.x += CheckboxRect.w;
	CheckboxLabel.w -= CheckboxRect.w;
	CheckboxLabel.VSplitLeft(5.0f, 0, &CheckboxLabel);

	Graphics()->TextureSet(g_pData->m_aImages[IMAGE_GUIBUTTONS].m_Id);
	Graphics()->QuadsBegin();

	Graphics()->SetColor(1.0f, 1.0f, 1.0f, 1.0f);

	RenderTools()->SelectSprite(SPRITE_GUI_CHECKBOX_BORDER);
	IGraphics::CQuadItem QuadItem(CheckboxRect.x, CheckboxRect.y, CheckboxRect.w, CheckboxRect.h);
	Graphics()->QuadsDrawTL(&QuadItem, 1);

  if(UI()->HotItem() == pID)
	{
    Graphics()->SetColor(1.0f, 1.0f, 1.0f, 0.5f);
    RenderTools()->SelectSprite(SPRITE_GUI_CHECKBOX_HOVER);
    IGraphics::CQuadItem QuadItem(CheckboxRect.x, CheckboxRect.y, CheckboxRect.w, CheckboxRect.h);
    Graphics()->QuadsDrawTL(&QuadItem, 1);
  }

  if (Checked) {
    Graphics()->SetColor(1.0f, 1.0f, 1.0f, 1.0f);
    RenderTools()->SelectSprite(SPRITE_GUI_CHECKBOX_CHECK);
    IGraphics::CQuadItem QuadItem(CheckboxRect.x, CheckboxRect.y, CheckboxRect.w, CheckboxRect.h);
    Graphics()->QuadsDrawTL(&QuadItem, 1);
  }

  Graphics()->QuadsEnd();
  UI()->DoLabel(&CheckboxLabel, pText, pRect->h*ms_FontmodHeight*0.8f, CUI::ALIGN_LEFT);
  return UI()->DoButtonLogic(pID, "", Checked, &CheckboxRect, HoverSound);
}

int CMenus::DoIcon(const void *pID, int Icon, const CUIRect *pRect, int Alignment, float BoxMargin, bool HoverSound, vec4 *pColor)
{
    static CUIIcon IconElement;
    IconElement.SetID(0);
    IconElement.GetStyle().IconSetIcon(Icon);
    IconElement.GetStyle().AlignmentSetLegacyValue(Alignment);

    int HoverOrClick = UI()->DoRectLogic(pID, pRect, HoverSound);

    if (pColor) {
        IconElement.GetStyle().IconSetColor(*pColor);
    } else {
        //default
        if (HoverOrClick == -1) {
            IconElement.GetStyle().IconSetColor(STYLE_COLOR_BG_WHITE);
        } else {
            IconElement.GetStyle().IconSetColor(STYLE_COLOR_BG_LIGHT);
        }
    }

    IconElement.Create(*pRect, RenderTools());
    return HoverOrClick;
}

int CMenus::DoButton_CheckBox_Number(const void *pID, const char *pText, int Checked, const CUIRect *pRect, bool HoverSound)
{
	char aBuf[16];
	str_format(aBuf, sizeof(aBuf), "%d", Checked);
	return DoButton_CheckBox_Common(pID, pText, aBuf, pRect, HoverSound);
}

int CMenus::DoEditBox(void *pID, const CUIRect *pRect, char *pStr, unsigned StrSize, float FontSize, float *Offset, bool Hidden, int Corners)
{
	CUIEditBoxStyle Style;
	Style.CornerSetAll(3.0f);
	//TODO: add corners

	Style.EditBoxSetHidden(Hidden);
	Style.TextSetFontSize(FontSize);
	Style.MarginSetLeft(10.0f);

	return DoEditBox2(pID, pRect, pStr, StrSize, Offset, 0, Style);
}
int CMenus::DoEditBox2(void *pID, const CUIRect *pRect, char *pStr, unsigned StrSize, float *Offset, const char *pPlaceholder, const CUIEditBoxStyle &Style)
{
	//
	static CUIEditBox EditBox;
	EditBox.CopyStyle(Style);
	EditBox.SetID(pID);
	EditBox.SetText(pStr, StrSize);
	EditBox.SetPlaceholderText(pPlaceholder);
	EditBox.SetOffset(Offset);
	EditBox.Create(*pRect, RenderTools());

	return EditBox.WasUpdated();
}


//TODO: make me faster
void CMenus::DoBorder(const CUIRect *pRect, vec4 Color, float Strength, int Sides, int Corners, float Rounding) {

	CUIRect Top, Right, Bottom, Left;

	if(Sides&1) {
		pRect->HSplitTop(Strength, &Top, 0);
		RenderTools()->DrawUIRect(&Top, Color, Corners&CUI::CORNER_T, Rounding); // TOP
	}

	if(Sides&2) {
		pRect->VSplitRight(Strength, 0, &Right);
		RenderTools()->DrawUIRect(&Right, Color, Corners&CUI::CORNER_R, Rounding); // RIGHT
	}

	if(Sides&4) {
		pRect->HSplitBottom(Strength, 0, &Bottom);
		RenderTools()->DrawUIRect(&Bottom, Color, Corners&CUI::CORNER_B, Rounding); // BOTTOM
	}

	if(Sides&8) {
		pRect->VSplitLeft(Strength, &Left, 0);
		RenderTools()->DrawUIRect(&Left, Color, Corners&CUI::CORNER_L, Rounding); // LEFT
	}
}

float CMenus::DoScrollbarV(const void *pID, const CUIRect *pRect, float Current, float Percent)
{
	CUIScrollbar bar;
	bar.ScrollbarSetValue(Current);
	bar.ScrollbarSetPercent(Percent);
	bar.SetID(pID);
	bar.Create(*pRect, RenderTools());
	return bar.ScrollbarGetValue();
}



float CMenus::DoScrollbarH(const void *pID, const CUIRect *pRect, float Current)
{

	return DoScrollbarV(pID, pRect, Current);
}

int CMenus::DoKeyReader(void *pID, const CUIRect *pRect, int Key)
{
	// process
	static void *pGrabbedID = 0;
	static bool MouseReleased = true;
	static int ButtonUsed = 0;
	int Inside = UI()->MouseInside(pRect);
	int NewKey = Key;

	if(!UI()->MouseButton(0) && !UI()->MouseButton(1) && pGrabbedID == pID)
		MouseReleased = true;

	if(UI()->ActiveItem() == pID)
	{
		if(m_Binder.m_GotKey)
		{
			// abort with escape key
			if(m_Binder.m_Key.m_Key != KEY_ESCAPE)
				NewKey = m_Binder.m_Key.m_Key;
			m_Binder.m_GotKey = false;
			UI()->SetActiveItem(0);
			MouseReleased = false;
			pGrabbedID = pID;
		}

		if(ButtonUsed == 1 && !UI()->MouseButton(1))
		{
			if(Inside)
				NewKey = 0;
			UI()->SetActiveItem(0);
		}
	}
	else if(UI()->HotItem() == pID)
	{
		if(MouseReleased)
		{
			if(UI()->MouseButton(0))
			{
				m_Binder.m_TakeKey = true;
				m_Binder.m_GotKey = false;
				UI()->SetActiveItem(pID);
				ButtonUsed = 0;
			}

			if(UI()->MouseButton(1))
			{
				UI()->SetActiveItem(pID);
				ButtonUsed = 1;
			}
		}
	}

	if(Inside)
		UI()->SetHotItem(pID, true);

	// draw
	if (UI()->ActiveItem() == pID && ButtonUsed == 0)
		DoButton_KeySelect(pID, "???", 0, pRect);
	else
	{
		if(Key == 0)
			DoButton_KeySelect(pID, "", 0, pRect);
		else
			DoButton_KeySelect(pID, Input()->KeyName(Key), 0, pRect);
	}
	return NewKey;
}


int CMenus::RenderMenubar(CUIRect r)
{
	CUIRect Box = r;
	CUIRect Button;

	m_ActivePage = g_Config.m_UiPage;
	int NewPage = -1;

	if(Client()->State() != IClient::STATE_OFFLINE)
		m_ActivePage = m_GamePage;

	if(Client()->State() == IClient::STATE_OFFLINE)
	{

		Box.VSplitLeft(90.0f, &Button, &Box);
		static int s_NewsButton=0;
		if (DoButton_MenuTab(&s_NewsButton, Localize("News"), m_ActivePage==PAGE_NEWS, &Button, 0))
			NewPage = PAGE_NEWS;
		Box.VSplitLeft(30.0f, 0, &Box);

		Box.VSplitLeft(100.0f, &Button, &Box);
		static int s_InternetButton=0;
		if(DoButton_MenuTab(&s_InternetButton, Localize("Internet"), m_ActivePage==PAGE_INTERNET, &Button, CUI::CORNER_TL))
		{
			ServerBrowser()->Refresh(IServerBrowser::TYPE_INTERNET);
			NewPage = PAGE_INTERNET;
		}

		//Box.VSplitLeft(4.0f, 0, &Box);
		Box.VSplitLeft(80.0f, &Button, &Box);
		static int s_LanButton=0;
		if(DoButton_MenuTab(&s_LanButton, Localize("LAN"), m_ActivePage==PAGE_LAN, &Button, 0))
		{
			ServerBrowser()->Refresh(IServerBrowser::TYPE_LAN);
			NewPage = PAGE_LAN;
		}

		//box.VSplitLeft(4.0f, 0, &box);
		Box.VSplitLeft(110.0f, &Button, &Box);
		static int s_FavoritesButton=0;
		if(DoButton_MenuTab(&s_FavoritesButton, Localize("Favorites"), m_ActivePage==PAGE_FAVORITES, &Button, CUI::CORNER_TR))
		{
			ServerBrowser()->Refresh(IServerBrowser::TYPE_FAVORITES);
			NewPage = PAGE_FAVORITES;
		}

		Box.VSplitLeft(4.0f*5, 0, &Box);
		Box.VSplitLeft(100.0f, &Button, &Box);
		static int s_DemosButton=0;
		if(DoButton_MenuTab(&s_DemosButton, Localize("Demos"), m_ActivePage==PAGE_DEMOS, &Button, CUI::CORNER_T))
		{
			DemolistPopulate();
			NewPage = PAGE_DEMOS;
		}
	}

	/*
	box.VSplitRight(110.0f, &box, &button);
	static int system_button=0;
	if (UI()->DoButton(&system_button, "System", g_Config.m_UiPage==PAGE_SYSTEM, &button))
		g_Config.m_UiPage = PAGE_SYSTEM;

	box.VSplitRight(30.0f, &box, 0);
	*/

	Box.VSplitRight(90.0f, &Box, &Button);
	static int s_QuitButton=0;
	if(DoButton_MenuTab(&s_QuitButton, Localize("Quit"), 0, &Button, CUI::CORNER_T))
		m_Popup = POPUP_QUIT;

	Box.VSplitRight(10.0f, &Box, &Button);
	Box.VSplitRight(130.0f, &Box, &Button);
	static int s_SettingsButton=0;
	if(DoButton_MenuTab(&s_SettingsButton, Localize("Settings"), m_ActivePage==PAGE_SETTINGS, &Button, CUI::CORNER_T))
		NewPage = PAGE_SETTINGS;

	if(NewPage != -1)
	{
		if(Client()->State() == IClient::STATE_OFFLINE)
			g_Config.m_UiPage = NewPage;
		else
			m_GamePage = NewPage;
	}

	return 0;
}

void CMenus::RenderLoading()
{
	// TODO: not supported right now due to separate render thread

	static int64 LastLoadRender = 0;
	float Percent = m_LoadCurrent++/(float)m_LoadTotal;

	// make sure that we don't render for each little thing we load
	// because that will slow down loading if we have vsync
	if(time_get()-LastLoadRender < time_freq()/60)
		return;

	LastLoadRender = time_get();

	// need up date this here to get correct
	vec3 Rgb = HslToRgb(vec3(g_Config.m_UiColorHue/255.0f, g_Config.m_UiColorSat/255.0f, g_Config.m_UiColorLht/255.0f));
	ms_GuiColor = vec4(Rgb.r, Rgb.g, Rgb.b, g_Config.m_UiColorAlpha/255.0f);

	CUIRect Screen = *UI()->Screen();
	Graphics()->MapScreen(Screen.x, Screen.y, Screen.w, Screen.h);

	RenderBackground();

	float w = 700;
	float h = 200;
	float x = Screen.w/2-w/2;
	float y = Screen.h/2-h/2;

	Graphics()->BlendNormal();

	UI()->DoSpinner(&Screen);

	Graphics()->Swap();
}

void CMenus::OnInit()
{
	if(g_Config.m_ClShowWelcome)
		m_Popup = POPUP_LANGUAGE;
	g_Config.m_ClShowWelcome = 0;

	Console()->Chain("add_favorite", ConchainServerbrowserUpdate, this);
	Console()->Chain("remove_favorite", ConchainServerbrowserUpdate, this);
	Console()->Chain("add_friend", ConchainFriendlistUpdate, this);
	Console()->Chain("remove_friend", ConchainFriendlistUpdate, this);

	// setup load amount
	m_LoadCurrent = 0;
	m_LoadTotal = g_pData->m_NumImages;
	if(!g_Config.m_ClThreadsoundloading)
		m_LoadTotal += g_pData->m_NumSounds;

	// we start with the login screen every time
	g_Config.m_UiPage = PAGE_LOGIN;

	m_pClient->m_pSounds->Enqueue(CSounds::CHN_MUSIC, SOUND_MENU);
}

void CMenus::PopupMessage(const char *pTopic, const char *pBody, const char *pButton)
{
	// reset active item
	UI()->SetActiveItem(0);

	str_copy(m_aMessageTopic, pTopic, sizeof(m_aMessageTopic));
	str_copy(m_aMessageBody, pBody, sizeof(m_aMessageBody));
	str_copy(m_aMessageButton, pButton, sizeof(m_aMessageButton));
	m_Popup = POPUP_MESSAGE;
}


int CMenus::Render()
{
	CUIRect Screen = *UI()->Screen();
	Graphics()->MapScreen(Screen.x, Screen.y, Screen.w, Screen.h);

	/*static bool s_First = true;
	if(s_First)
	{
		if(g_Config.m_UiPage == PAGE_INTERNET)
			ServerBrowser()->Refresh(IServerBrowser::TYPE_INTERNET);
		else if(g_Config.m_UiPage == PAGE_LAN)
			ServerBrowser()->Refresh(IServerBrowser::TYPE_LAN);
		else if(g_Config.m_UiPage == PAGE_FAVORITES)
			ServerBrowser()->Refresh(IServerBrowser::TYPE_FAVORITES);

		s_First = false;
	}*/

	

	if(Client()->State() == IClient::STATE_ONLINE)
	{
	    if (m_GamePage != PAGE_GAME) {
            RenderBackground(120.0f);
        }
		ms_ColorTabbarInactive = ms_ColorTabbarInactiveIngame;
		ms_ColorTabbarActive = ms_ColorTabbarActiveIngame;
	}
	else
	{
		RenderBackground();
		ms_ColorTabbarInactive = ms_ColorTabbarInactiveOutgame;
		ms_ColorTabbarActive = ms_ColorTabbarActiveOutgame;
	}

	CUIRect TabBar;
	CUIRect MainView;

	static bool s_SoundCheck = false;
	if(!s_SoundCheck && m_Popup == POPUP_NONE)
	{
		if(Client()->SoundInitFailed())
			m_Popup = POPUP_SOUNDERROR;
		s_SoundCheck = true;
	}
	// make sure we don't go to the wrong page
	if(!PageIsValid(g_Config.m_UiPage))
	{
		g_Config.m_UiPage = PAGE_LOGIN;
		if(m_Popup == POPUP_LOGOUT)
			m_Popup = POPUP_NONE;
	}

	// render current page
	if(PageIsLoginscreen(g_Config.m_UiPage))
	{
		RenderLogin(Screen);
	}
	else
	{
		RenderMain(Screen);

		// do tab bar
		/*Screen.HSplitTop(24.0f, &TabBar, &MainView);
		TabBar.VMargin(20.0f, &TabBar);
		RenderMenubar(TabBar);
		if(Client()->State() != IClient::STATE_OFFLINE)
		{
		  if(m_GamePage == PAGE_SETTINGS)
			RenderSettings(MainView);
		}
		else if(g_Config.m_UiPage == PAGE_NEWS)
		  RenderNews(MainView);
		else if(g_Config.m_UiPage == PAGE_INTERNET)
		  RenderServerbrowser(MainView);
		else if(g_Config.m_UiPage == PAGE_LAN)
		  RenderServerbrowser(MainView);
		else if(g_Config.m_UiPage == PAGE_DEMOS)
		  RenderDemoList(MainView);
		else if(g_Config.m_UiPage == PAGE_FAVORITES)
		  RenderServerbrowser(MainView);
		else if(g_Config.m_UiPage == PAGE_SETTINGS)
		  RenderSettings(MainView);*/
	}

	if(m_Popup != POPUP_NONE)
	{

		//render big overlay
		RenderTools()->DrawUIRect(&Screen, vec4(1.0f, 1.0f, 1.0f, 0.7f), 0, 0.0f);

		//make logical overlay too
		static int LogicalOverlay = 0;
		UI()->DoRectLogic(&LogicalOverlay, &Screen);

		if(m_LastPopup != m_Popup)
		{
			// make sure that other windows doesn't do anything funny!
			UI()->SetHotItem(0);
			UI()->SetActiveItem(0);
			m_LastPopup = m_Popup;
		}

		switch(m_Popup)
		{
			// Simple popups
			case POPUP_DISCONNECTED:
				RenderPopupSimple(Screen, Localize("Disconnected"), Client()->ErrorString(), Localize("Ok"), -1);
				break;
			case POPUP_MESSAGE:
				RenderPopupSimple(Screen, m_aMessageTopic, m_aMessageBody, m_aMessageButton, 0);
				break;
			case POPUP_PURE:
				RenderPopupSimple(Screen, Localize("Disconnected"), Localize("The server is running a non-standard tuning on a pure game type."), Localize("Ok"), -1);
				break;
			case POPUP_SOUNDERROR:
				RenderPopupSimple(Screen, Localize("Sound error"), Localize("The audio device couldn't be initialised."), Localize("Ok"), -1);
				break;
				// More complicated ones
			case POPUP_QUIT:
				RenderPopupQuit(Screen);
				break;
			case POPUP_LOGOUT:
				RenderPopupLogout(Screen);
				break;
			case POPUP_PASSWORD:
				RenderPopupPassword(Screen);
				break;
			case POPUP_CONNECTING:
				RenderPopupConnecting(Screen);
				break;
			case POPUP_LANGUAGE:
				RenderPopupLanguage(Screen);
				break;
			case POPUP_COUNTRY:
				RenderPopupCountry(Screen);
				break;
			case POPUP_DELETE_DEMO:
				RenderPopupDeleteDemo(Screen);
				break;
			case POPUP_RENAME_DEMO:
				RenderPopupRenameDemo(Screen);
				break;
			case POPUP_REMOVE_FRIEND:
				RenderPopupRemoveFriend(Screen);
				break;
			case POPUP_FIRST_LAUNCH:
				RenderPopupFirstLaunch(Screen);
				break;
			case POPUP_SETTINGS_MAIN:
				RenderPopupSettings(Screen);
				break;
			case POPUP_SETTINGS_TEE:
				RenderPopupSettingsTee(Screen);
				break;
			case POPUP_NEWS:
				RenderPopupNews(Screen);
				break;
			case POPUP_SEARCH_PLAYER:
				RenderPopupSearchPlayer(Screen);
				break;
			case POPUP_PLAYER:
				RenderPopupPlayer(Screen);
                break;
            case POPUP_SERVER_PLAYERS:
                RenderPopupServerPlayers(Screen);
                break;
			case POPUP_SERVER_INFO:
				RenderPopupServerInfo(Screen);
				break;
			case POPUP_SERVER_VOTES:
				RenderPopupServerVotes(Screen);
				break;
		}

		if(m_Popup == POPUP_NONE)
			UI()->SetActiveItem(0);
	}

	return 0;
}


void CMenus::SetActive(bool Active)
{
	m_MenuActive = Active;
	if(!m_MenuActive)
	{
		if(m_NeedSendinfo)
		{
			m_pClient->SendInfo(false);
			m_NeedSendinfo = false;
		}

		if(Client()->State() == IClient::STATE_ONLINE)
		{
			m_pClient->OnRelease();
		}
	}
	else if(Client()->State() == IClient::STATE_DEMOPLAYBACK)
	{
		m_pClient->OnRelease();
	}
}

void CMenus::OnReset()
{
}

bool CMenus::OnMouseMove(float x, float y)
{
	m_LastInput = time_get();

	if(!m_MenuActive)
		return false;

	UI()->ConvertMouseMove(&x, &y);
	m_MousePos.x += x;
	m_MousePos.y += y;
	if(m_MousePos.x < 0) m_MousePos.x = 0;
	if(m_MousePos.y < 0) m_MousePos.y = 0;
	if(m_MousePos.x > Graphics()->ScreenWidth()) m_MousePos.x = Graphics()->ScreenWidth();
	if(m_MousePos.y > Graphics()->ScreenHeight()) m_MousePos.y = Graphics()->ScreenHeight();

	return true;
}

bool CMenus::OnInput(IInput::CEvent e)
{
	m_LastInput = time_get();

	// special handle esc and enter for popup purposes
	if(e.m_Flags&IInput::FLAG_PRESS)
	{
		if(e.m_Key == KEY_ESCAPE)
		{
			m_EscapePressed = true;
			SetActive(!IsActive());
			return true;
		}
	}

	if(IsActive())
	{
		if(e.m_Flags&IInput::FLAG_PRESS)
		{
			// special for popups
			if(e.m_Key == KEY_RETURN || e.m_Key == KEY_KP_ENTER)
				m_EnterPressed = true;
			else if(e.m_Key == KEY_DELETE)
				m_DeletePressed = true;
		}

		if(m_NumInputEvents < MAX_INPUTEVENTS)
			m_aInputEvents[m_NumInputEvents++] = e;
		return true;
	}
	return false;
}

void CMenus::OnStateChange(int NewState, int OldState)
{
	// reset active item
	UI()->SetActiveItem(0);

	if(NewState == IClient::STATE_OFFLINE)
	{
		if(OldState >= IClient::STATE_ONLINE && NewState < IClient::STATE_QUITING)
			m_pClient->m_pSounds->Play(CSounds::CHN_MUSIC, SOUND_MENU, 1.0f);
		m_Popup = POPUP_NONE;
		if(Client()->ErrorString() && Client()->ErrorString()[0] != 0)
		{
			if(str_find(Client()->ErrorString(), "password"))
			{
				m_Popup = POPUP_PASSWORD;
				UI()->SetHotItem(&g_Config.m_Password);
				UI()->SetActiveItem(&g_Config.m_Password);
			}
			else
				m_Popup = POPUP_DISCONNECTED;
		}
	}
	else if(NewState == IClient::STATE_LOADING)
	{
		m_Popup = POPUP_CONNECTING;
		m_DownloadLastCheckTime = time_get();
		m_DownloadLastCheckSize = 0;
		m_DownloadSpeed = 0.0f;
		//client_serverinfo_request();
	}
	else if(NewState == IClient::STATE_CONNECTING) {
        m_Popup = POPUP_CONNECTING;
        m_GamePage = PAGE_GAME;
    }
	else if (NewState == IClient::STATE_ONLINE || NewState == IClient::STATE_DEMOPLAYBACK)
	{
		m_Popup = POPUP_NONE;
		SetActive(false);
	}
}

extern "C" void font_debug_render();

void CMenus::OnRender()
{
	/*
	// text rendering test stuff
	render_background();

	CTextCursor cursor;
	TextRender()->SetCursor(&cursor, 10, 10, 20, TEXTFLAG_RENDER);
	TextRender()->TextEx(&cursor, "ようこそ - ガイド", -1);

	TextRender()->SetCursor(&cursor, 10, 30, 15, TEXTFLAG_RENDER);
	TextRender()->TextEx(&cursor, "ようこそ - ガイド", -1);

	//Graphics()->TextureSet(-1);
	Graphics()->QuadsBegin();
	Graphics()->QuadsDrawTL(60, 60, 5000, 5000);
	Graphics()->QuadsEnd();
	return;*/

	if(Client()->State() != IClient::STATE_ONLINE && Client()->State() != IClient::STATE_DEMOPLAYBACK)
		SetActive(true);

	if(Client()->State() == IClient::STATE_DEMOPLAYBACK)
	{
		CUIRect Screen = *UI()->Screen();
		Graphics()->MapScreen(Screen.x, Screen.y, Screen.w, Screen.h);
		RenderDemoPlayer(Screen);
	}

	if(Client()->State() == IClient::STATE_ONLINE && m_pClient->m_ServerMode == m_pClient->SERVERMODE_PUREMOD)
	{
		Client()->Disconnect();
		SetActive(true);
		m_Popup = POPUP_PURE;
	}

	if(!IsActive())
	{
		m_EscapePressed = false;
		m_EnterPressed = false;
		m_DeletePressed = false;
		m_NumInputEvents = 0;
		return;
	}

	// update colors
	vec3 Rgb = HslToRgb(vec3(g_Config.m_UiColorHue/255.0f, g_Config.m_UiColorSat/255.0f, g_Config.m_UiColorLht/255.0f));
	ms_GuiColor = vec4(Rgb.r, Rgb.g, Rgb.b, g_Config.m_UiColorAlpha/255.0f);

	ms_ColorTabbarInactiveOutgame = STYLE_COLOR_TAB_INACTIVE;
	ms_ColorTabbarActiveOutgame = STYLE_COLOR_TAB_ACTIVE;

  	ms_ColorTabbarInactiveOutgame.a = 0.75f;
  	ms_ColorTabbarActiveOutgame.a = 0.9f;

	float ColorIngameScaleI = 0.5f;
	float ColorIngameAcaleA = 0.2f;
	ms_ColorTabbarInactiveIngame = vec4(
		ms_GuiColor.r*ColorIngameScaleI,
		ms_GuiColor.g*ColorIngameScaleI,
		ms_GuiColor.b*ColorIngameScaleI,
		ms_GuiColor.a*0.8f);

	ms_ColorTabbarActiveIngame = vec4(
		ms_GuiColor.r*ColorIngameAcaleA,
		ms_GuiColor.g*ColorIngameAcaleA,
		ms_GuiColor.b*ColorIngameAcaleA,
		ms_GuiColor.a);

	// update the ui
	CUIRect *pScreen = UI()->Screen();
	float mx = (m_MousePos.x/(float)Graphics()->ScreenWidth())*pScreen->w;
	float my = (m_MousePos.y/(float)Graphics()->ScreenHeight())*pScreen->h;

	int Buttons = 0;
	if(m_UseMouseButtons)
	{
		if(Input()->KeyPressed(KEY_MOUSE_1)) Buttons |= 1;
		if(Input()->KeyPressed(KEY_MOUSE_2)) Buttons |= 2;
		if(Input()->KeyPressed(KEY_MOUSE_3)) Buttons |= 4;
	}

	UI()->Update(mx,my,mx*3.0f,my*3.0f,Buttons);

	// render
	if(Client()->State() != IClient::STATE_DEMOPLAYBACK)
		Render();

	// render cursor
	Graphics()->TextureSet(g_pData->m_aImages[IMAGE_CURSOR].m_Id);
	Graphics()->QuadsBegin();
	Graphics()->SetColor(1,1,1,1);
	IGraphics::CQuadItem QuadItem(mx, my, 24, 24);
	Graphics()->QuadsDrawTL(&QuadItem, 1);
	Graphics()->QuadsEnd();

	// render debug information
	if(g_Config.m_Debug)
	{
		CUIRect Screen = *UI()->Screen();
		Graphics()->MapScreen(Screen.x, Screen.y, Screen.w, Screen.h);

		char aBuf[512];
		str_format(aBuf, sizeof(aBuf), "%p %p %p", UI()->HotItem(), UI()->ActiveItem(), UI()->LastActiveItem());
		CTextCursor Cursor;
		TextRender()->SetCursor(&Cursor, 10, 10, 10, TEXTFLAG_RENDER);
		TextRender()->TextEx(&Cursor, aBuf, -1);
	}

	m_EscapePressed = false;
	m_EnterPressed = false;
	m_DeletePressed = false;
	m_NumInputEvents = 0;
}

static int gs_TextureBlob = -1;

void CMenus::RenderBackground(float Alpha)
{
  CUIRect MainView;
  Graphics()->GetScreen(&MainView.x, &MainView.y, &MainView.w, &MainView.h);

  vec4 Colors[4] = {
    vec4(0.0f, 0.0f, 0.0f, Alpha),
    vec4(0.0f, 0.0f, 0.0f, Alpha),
    vec4(16.0f, 32.0f, 50.0f, Alpha),
    vec4(16.0f, 32.0f, 50.0f, Alpha)
  };
  RenderTools()->DrawUIRectGradient(&MainView, Colors, 0, 0.0f);
}

bool CMenus::PageIsValid(int Page)
{
	if(!PageIsLoginscreen(Page) && (Page > PAGE_SETTINGS || (Client()->State() == IClient::STATE_OFFLINE && Page >= PAGE_GAME && Page <= PAGE_CALLVOTE)))
		return false;

	if(Client()->Galaxy()->Account()->GetStatus() != IAccount::ACCOUNT_STATUS_OK && !PageIsLoginscreen(Page))
		return false;

	return true;

}

bool CMenus::PageIsLoginscreen(int Page)
{
	return Page == PAGE_LOGIN || Page == PAGE_REGISTER || Page == PAGE_RECOVER;
}



void CMenus::DoDivider(const CUIRect *pRect)
{
	float Width = 0.5f;
	float Margin = 10.0f;

	bool Vertical = pRect->w < pRect->h;

	CUIRect Box;

	if (Vertical) {
		pRect->HMargin(Margin, &Box);
		Box.w = Width;
	} else {
		pRect->VMargin(Margin, &Box);
		Box.h = Width;
	}
	pRect->Align(&Box, CUI::ALIGN_CENTER);
	RenderTools()->DrawUIRect(&Box, STYLE_COLOR_BG_LIGHT, 0, 0.0f);
}