#include <engine/config.h>
#include <engine/friends.h>
#include <engine/graphics.h>
#include <engine/keys.h>
#include <engine/serverbrowser.h>
#include <engine/textrender.h>
#include <engine/shared/config.h>
#include <engine/galaxy_client.h>

#include <game/generated/client_data.h>
#include <game/generated/protocol.h>

#include <game/localization.h>
#include <game/version.h>
#include <game/client/render.h>
#include <game/client/ui.h>
#include <game/client/components/countryflags.h>


#include <game/client/animstate.h>
#include <game/localization.h>
#include <game/client/ui/elements/link.h>
#include <game/client/ui/elements/button.h>
#include <game/client/ui/constants.h>
#include "skins.h"

#include "menus.h"
#include "../ui.h"
#include "../../../engine/client/serverbrowser.h"


void CMenus::RenderPlayerSection(CUIRect MainView) {


    MainView.Margin(5.0f, &MainView);
    MainView.HSplitBottom(80.0f, 0, &MainView);

    CUIRect PlayerInfo, TeeBox;
    MainView.VSplitLeft(50.0f, &TeeBox, &PlayerInfo);

    PlayerInfo.w = 90.0f;

    // Handle Tee Rendering
    {
        static float WalkTime = 0.0f;
        static vec2 TargetPos = vec2(0.0f,0.0f);
        static vec2 CurrentPos = TargetPos;

        if (distance(CurrentPos, TargetPos) < 0.001f) {
            TargetPos = vec2((float) (rand() %  (int) UI()->Screen()->w), (float) (rand() % (int)UI()->Screen()->h));
        } else {
            CurrentPos += normalize(TargetPos - CurrentPos) * 200.0f * Client()->RenderFrameTime();
        }

        const float WalkTimeMagic = 100.0f;
        WalkTime = fmodf(WalkTime+Client()->RenderFrameTime()*3.0f, 1.0f);

        static float NextChange = 0;
        static bool ShallReturn = 0;


        static vec2 Position;
        static vec2 Vel;
        static int Emote = EMOTE_HAPPY;

        NextChange--;

        if (ShallReturn && Vel.x < 0) {
            Vel.x = -Vel.x;
        }

        if (NextChange <= 0 && Position.x == 0) {

            if (rand() % 10 == 0) {
                //Vel.x = -50;
                //ShallReturn = 0;
            } else {
                Vel.x = 0;
            }

            NextChange = (((float) (rand() % 20000))/1000.0f+0.5f) / Client()->RenderFrameTime();

            int r = rand() % 100;
            if (r < 5) {
                Emote = EMOTE_ANGRY;
                NextChange *= 0.2;
            } else if (r < 30) {
                Emote = EMOTE_SURPRISE;
                NextChange *= 0.3;
            } else if (r < 60) {
                Emote = EMOTE_HAPPY;
                NextChange *= 0.3;
            } else {
                Emote = 0;
            }
        }

        CUIRect Tee;
        float m = min(TeeBox.w, TeeBox.h);
        Tee.w = Tee.h = 50.0f;

        TeeBox.Align(&Tee, CUI::ALIGN_BOTTOM);


        CAnimState State;
        State.Set(&g_pData->m_aAnimations[ANIM_BASE], 0);

        if(Vel.x != 0) {
            Position += Vel*Client()->RenderFrameTime();

            const float WalkTimeMagic = 100.0f;
            float WalkTime =
                    ((Position.x >= 0)
                     ? fmod(Position.x, WalkTimeMagic)
                     : WalkTimeMagic - fmod(-Position.x, WalkTimeMagic))
                    / WalkTimeMagic;

            if (Position.x != 0) {
                State.Add(&g_pData->m_aAnimations[ANIM_WALK], WalkTime, 1.0f);
            }

            if (Vel.x < 0) {

            }
        }

        if (Position.x >= 0.0f) {
            ShallReturn = 0;
            Vel.x = 0;
            Position.x = 0;
        }
        vec2 DisplayPos = vec2(Tee.x + Tee.w/2, Tee.y + Tee.h/2) + Position;

        // Do not reset, when the tee settings are active
        if (DisplayPos.x < -Tee.w) {
            Position.x = -Tee.w-Tee.x - Tee.w/2;

            if (m_Popup != POPUP_SETTINGS_TEE) {
                ShallReturn = 1;
            }
        }


        if(Vel.x <= 1 && Vel.x >= -1) {
            State.Add(&g_pData->m_aAnimations[ANIM_IDLE], 0, 1.0f);
        }

        const CSkins::CSkin *pOwnSkin = m_pClient->m_pSkins->Get(m_pClient->m_pSkins->Find(g_Config.m_PlayerSkin));
        CTeeRenderInfo OwnSkinInfo;
        if(g_Config.m_PlayerUseCustomColor)
        {
            OwnSkinInfo.m_Texture = pOwnSkin->m_ColorTexture;
            OwnSkinInfo.m_ColorBody = m_pClient->m_pSkins->GetColorV4(g_Config.m_PlayerColorBody);
            OwnSkinInfo.m_ColorFeet = m_pClient->m_pSkins->GetColorV4(g_Config.m_PlayerColorFeet);
        }
        else
        {
            OwnSkinInfo.m_Texture = pOwnSkin->m_OrgTexture;
            OwnSkinInfo.m_ColorBody = vec4(1.0f, 1.0f, 1.0f, 1.0f);
            OwnSkinInfo.m_ColorFeet = vec4(1.0f, 1.0f, 1.0f, 1.0f);
        }
        OwnSkinInfo.m_Size = 50.0f*UI()->Scale();


        //Render TeeBox

        static int s_TeeSettings = 0;
        int HoverOrClick = UI()->DoRectLogic(&s_TeeSettings, &TeeBox, true);

        if (HoverOrClick && (Client()->State() == IClient::STATE_OFFLINE || m_GamePage == PAGE_MAIN)) {
            RenderTools()->DrawUIRect(&Tee, vec4(0.5f, 0.5f,0.5f,1.0f), CUI::CORNER_ALL, 3.0f);
        }


        if (HoverOrClick > 0) {
            m_Popup = POPUP_SETTINGS_TEE;
            Vel.x = -150;
        }

        TeeBox.Align(&Tee, CUI::ALIGN_BOTTOM);

        vec2 Dir = normalize(CurrentPos - DisplayPos);
        if (Vel.x < 0)
            Dir.x *= -1;
        RenderTools()->RenderTee(&State, &OwnSkinInfo, Emote, Dir, DisplayPos);
    }


    // Render Player Info
    {
        float H = 15.0f;
        float M = 4.0f;

        CUIRect AlignBox;
        AlignBox.w = PlayerInfo.w;
        AlignBox.h = H*2+M+12.5f;
        PlayerInfo.Align(&AlignBox, CUI::ALIGN_BOTTOM, 3.0f);

        CUIRect Username, Clan;
        AlignBox.HSplitTop(H, &Username, &AlignBox);
        AlignBox.HSplitTop(M, 0, &AlignBox);
        AlignBox.HSplitTop(H, &Clan, &AlignBox);
        UI()->DoLabelScaled(&Username, Client()->Galaxy()->Account()->Name(), 14.0, CUI::ALIGN_LEFT|CUI::ALIGN_TOP);

        // Clan editing
        CUIEditBoxStyle EditBoxStyle;
        EditBoxStyle.CornerSetAll(3.0f);
        //TODO: add corners
        EditBoxStyle.EditBoxSetHidden(false);
        EditBoxStyle.BackgroundSetColor(vec4(0, 0, 0, 0));
        EditBoxStyle.TextSetFontColor(STYLE_COLOR_BTN_PRIMARY);
        EditBoxStyle.TextSetFontSize(10.0f);
        EditBoxStyle.MarginSetLeft(2.0f);

        static float s_OffsetClan = 0.0f;
        if(DoEditBox2(&g_Config.m_PlayerClan, &Clan, g_Config.m_PlayerClan, sizeof(g_Config.m_PlayerClan), &s_OffsetClan, Localize("Clan"), EditBoxStyle))
        {
            m_NeedSendinfo = true;
        }
        DoBorder(&Clan, STYLE_COLOR_BTN_PRIMARY, 1.5f);
    }
}

void CMenus::RenderMain(CUIRect MainView) {

    //set page
    m_ActivePage = PAGE_INTERNET;

    CUIRect SectionLeft, SectionLeftTop, SectionLeftBottom;
    CUIRect SectionMiddle;
    CUIRect SectionRight;

    CUIRect ServerFilter, ServerList;
    CUIRect ServerDetail; // server details
    CUIRect Menu;
    CUIRect Tabs, TabContent;
    CUIRect PlayerSection;
    CUIRect Logo;

    // Layout
    MainView.VSplitPerc(0.2f, &SectionLeft, &SectionMiddle);
    SectionMiddle.VSplitPerc(0.75f, &SectionMiddle, &SectionRight);
    SectionLeft.HSplitPerc(0.333f, &SectionLeftTop, &SectionLeftBottom);
    SectionLeftTop.HSplitPerc(0.333f, &Logo, &PlayerSection);
    SectionLeftBottom.HSplitTop(25.0f, &Tabs, &TabContent);
    SectionMiddle.HSplitTop(50.0f, &ServerFilter, &ServerList);
    SectionRight.HSplitTop(50.0f, &Menu, &ServerDetail);


    if ((Client()->State() == IClient::STATE_OFFLINE || Client()->State() == IClient::STATE_CONNECTING || Client()->State() == IClient::STATE_LOADING) || m_GamePage == PAGE_MAIN) {
        // Only render if we are offline or have the main page active
        RenderPlayerSection(PlayerSection);

        static bool s_NewsTab = true;
        static bool s_FriendsTab = false;

        CUIRect TabNews, TabFriends;
        Tabs.VSplitPerc(0.5f, &TabNews, &TabFriends);

        RenderSectionLeftBottom(SectionLeftBottom);

        //Draw Logo
        Logo.VMargin(10.0f, &Logo);
        RenderTools()->DrawUIRectTexture(&Logo, IMAGE_BANNER, vec4(1.0f, 1.0f, 1.0f, 1.0f), STYLE_LOGO_SIZE.x, STYLE_LOGO_SIZE.y);

        RenderServerbrowserServerList(ServerList);

        RenderServerbrowserServerDetail(ServerDetail);

        RenderServerbrowserFilters(ServerFilter);
    } else  {
        RenderGame(MainView);
    }
    // Hide menu when loading
    if (Client()->State() != IClient::STATE_LOADING) {
        RenderMainMenu(Menu);
    }
}

void CMenus::RenderMainMenu(CUIRect Menu) {
    CUIRect Item;

    const int NumBtns = 4;
    CUIRect BtnBoxes[NumBtns];

    Menu.Margin(5.0f, &Menu);

    // From right to left
    for(int i = 0; i < NumBtns; ++i) {
        Menu.VSplitPerc(1.0f-1.0f/(float)(NumBtns-i), &Menu, &BtnBoxes[i]);
    }


    // Quit Button
    Item = BtnBoxes[0];
    static int s_Quit = 0;
    if(Client()->State() == IClient::STATE_ONLINE) {
        if (DoMainMenuItem(&s_Quit, Item, SPRITE_GUIICON_LOG_OUT) > 0) {
            Client()->Disconnect();
        }
    } else {
        if (DoMainMenuItem(&s_Quit, Item, SPRITE_GUIICON_QUIT) > 0) {
            m_Popup = POPUP_LOGOUT;
        }
    }

    // Settings Button
    Item = BtnBoxes[1];    
    static int s_Settings = 0;
    if (DoMainMenuItem(&s_Settings, Item, SPRITE_GUIICON_GEAR) > 0) {
        m_Popup = POPUP_SETTINGS_MAIN;
    }

    // Browser / Game switch Button
    Item = BtnBoxes[2]; 
    
    if (Client()->State() != IClient::STATE_OFFLINE && Client()->State() != IClient::STATE_CONNECTING) {

        if (m_GamePage == PAGE_MAIN) {
            // We are ingame but displaying the browser
            static int s_SwitchToGame = 0;
            if (DoMainMenuItem(&s_SwitchToGame, Item, SPRITE_GUIICON_CONTROLLER) > 0) {
                m_GamePage = PAGE_GAME;
            }
        } else {
            // We are ingame and displaying the ingame menu
            static int s_SwitchToBrowser = 0;
            if (DoMainMenuItem(&s_SwitchToBrowser, Item, SPRITE_GUIICON_WORLD) > 0) {
                m_GamePage = PAGE_MAIN;
            }
        }        
    }   

}

int CMenus::DoMainMenuItem(void *pId, CUIRect ItemView, int Icon) {
    vec4 HoverColor = vec4(0.078f, 0.612f, 0.902f, 1.0f);
    int HoverOrClick = UI()->DoRectLogic(pId, &ItemView, true);
    RenderTools()->DrawCircle(ItemView.x+ItemView.w/2, ItemView.y+ItemView.h/2, min(ItemView.w, ItemView.h)*0.5f, HoverOrClick < 0 ? HoverColor : vec4(1.0f, 1.0f, 1.0f, 1.0f));

    CUIRect IconView;

    IconView.w = IconView.h = 16;

    ItemView.Align(&IconView);
    vec4 IconColor = HoverOrClick < 0 ?  vec4(0, 0, 0, 1.0f) : vec4(0.4f, 0.4f, 0.4f, 1.0f);
    DoIcon(0, Icon, &IconView, CUI::ALIGN_CENTER, 30.0f, false, &IconColor);
    return HoverOrClick;
}

void CMenus::RenderNews(CUIRect MainView)
{
    //RenderTools()->DrawUIRect(&MainView, vec4(255.0f, 255.0f, 255.0f, 255.0f), 0, 0.0f);

    UI()->ClipEnable(&MainView);

    CUIRect NewsSection;

    //Render News Entries
    MainView.Margin( 0.0f, &NewsSection);
    NewsSection.HSplitTop( 15.0f, 0, &NewsSection);

    static float s_ScrollValue = 0;

    NewsSection.y -= s_ScrollValue;

    bool EndReached = false;

    vec4 HeadlineColor = vec4(0.8, 0.8,  1, 1.0f);
    vec4 AuthorColor = vec4(0.6f, 0.6f, 0.8f, 1.0f);
    vec4 DateColor = vec4(0.8f, 0.8, 0.8f, 1.0f);
    vec4 ContentColor = vec4(1, 1, 1, 1.0f);
    vec4 OutlineColor = vec4(0, 0, 0, 0.3f);

    float Heights = 0.0f;

    if (Client()->Galaxy()->News()->GetStatus() == INews::NEWS_STATUS_WAITING) {
        UI()->DoSpinner(&NewsSection);
    }
    else if (Client()->Galaxy()->News()->GetStatus() == INews::NEWS_STATUS_OK)
    {

        int size = Client()->Galaxy()->News()->NumEntries();

        if (size > 0) {
            for(int i = 0; i < size; ++i)
            {

                const CNewsEntry *pEntry = Client()->Galaxy()->News()->GetEntry(i);

                CUIRect NewsItem, Headline, Author, Date, Content;

                float HeadSize = 25.0f;
                float HeadHeight = HeadSize * TextRender()->TextLineCount(0, HeadSize, pEntry->m_pHeadline, NewsSection.w);

                float ContentSize = 18.0f;
                float ContentHeight = ContentSize * TextRender()->TextLineCount(0, ContentSize, pEntry->m_pContent, NewsSection.w);

                float AuthorSize = 18.0f;
                float DateSize = 16.0f;
                float AuthorHeight = ContentSize * TextRender()->TextLineCount(0, ContentSize, pEntry->m_pAuthor, NewsSection.w);

                float OverallHeight = HeadHeight + ContentHeight + AuthorHeight + 30.0f;
                Heights += OverallHeight;

                NewsSection.HSplitTop(OverallHeight, &NewsItem, &NewsSection);
                NewsItem.HSplitTop(HeadHeight + 5.0f, &Headline, &NewsItem);
                NewsItem.HSplitTop(AuthorHeight + 25.0f, &Author, &Content);

                Headline.VSplitRight(TextRender()->TextWidth(0, DateSize, pEntry->m_pDate, -1) + 10.0f, &Headline, &Date);

                Date.VSplitLeft(5.0f, 0, &Date);

                UI()->DoLabel(&Headline, pEntry->m_pHeadline, HeadSize, CUI::ALIGN_LEFT | CUI::ALIGN_TOP, -2, &HeadlineColor, &OutlineColor);

                UI()->DoLabel(&Author, pEntry->m_pAuthor, AuthorSize, CUI::ALIGN_RIGHT | CUI::ALIGN_TOP, -2, &AuthorColor, &OutlineColor);
                UI()->DoLabel(&Date, pEntry->m_pDate, DateSize, CUI::ALIGN_LEFT, -2, &DateColor, &OutlineColor);
                UI()->DoLabel(&Content, pEntry->m_pContent, ContentSize, CUI::ALIGN_LEFT | CUI::ALIGN_TOP, -2, &ContentColor, &OutlineColor);


                EndReached = NewsSection.y < MainView.h;
            }

            float LastScrollValue = s_ScrollValue;
            s_ScrollValue = UI()->DoScrollLogic(&s_ScrollValue, &MainView, s_ScrollValue, Heights * 0.01f, false);

            if(s_ScrollValue < 0.0f)
            {
                s_ScrollValue = 0.0f;
            }
            else if(EndReached && s_ScrollValue > LastScrollValue)
            {
                s_ScrollValue = LastScrollValue;
            }
        }
    } else if (Client()->Galaxy()->News()->GetStatus() == INews::NEWS_STATUS_ALREAD_READ) {
        m_Popup = POPUP_NONE; // close the popup!
    }



    UI()->ClipDisable();
}


