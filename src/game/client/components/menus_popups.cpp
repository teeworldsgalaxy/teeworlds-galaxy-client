#include <engine/config.h>
#include <engine/friends.h>
#include <engine/graphics.h>
#include <engine/keys.h>
#include <engine/serverbrowser.h>
#include <engine/textrender.h>
#include <engine/shared/config.h>
#include <engine/editor.h>
#include <engine/storage.h>

#include <game/generated/client_data.h>
#include <game/generated/protocol.h>

#include <game/localization.h>
#include <game/version.h>
#include <game/client/render.h>
#include <game/client/ui.h>
#include <game/client/ui/constants.h>
#include <game/client/ui/elements/button.h>
#include <game/client/ui/views/chat.h>
#include <game/client/components/countryflags.h>

#include <engine/galaxy_client.h>

#include "menus.h"

void CMenus::RenderPopupBox(CUIRect &Box, const char *pTitle, const char *pExtraText, int ExtraAlign, CUIRect *pPart,
                            bool CloseButton) {

    CUIRect Part, CloseButtonRect;

    Box.VMargin(150.0f, &Box);
    Box.HMargin(150.0f, &Box);

    // render the box
    RenderTools()->DrawUIRect(&Box, STYLE_COLOR_POPUP_BG,0, 0.0f);
    Box.HSplitTop(10.f, 0, &Box);

    Box.HSplitTop(24.f, &Part, &Box);
    UI()->DoLabelScaled(&Part, pTitle, 24.f, 0, Part.w, &STYLE_COLOR_TEXT_LIGHT);

    if (CloseButton) {
        CloseButtonRect = Part;
        CloseButtonRect.VSplitRight(6.0f, &CloseButtonRect, 0);

        static int s_CloseButton = 0;
        int HoverOrClick = DoIcon(&s_CloseButton, SPRITE_GUIICON_TIMES, &CloseButtonRect,
                                  CUI::ALIGN_CENTER | CUI::ALIGN_RIGHT, 3.0f);

        if (HoverOrClick > 0) {
            m_Popup = POPUP_NONE;
        }
    }

    if (pExtraText) {
        Box.VMargin(20.f , &Part);
        Part.HSplitTop(24.f,0,  &Part);
        if (ExtraAlign == -1)
            UI()->DoLabelScaled(&Part, pExtraText, 20.f, CUI::ALIGN_TOP, (int) Part.w);
        else
            UI()->DoLabelScaled(&Part, pExtraText, 20.f, 0, -1);
    }

    if (pPart) {
        Box.HSplitBottom(20.f, &Box, 0);
        Box.HSplitBottom(24.f, &Box, &Part);
        *pPart = Part;
    }
}

void CMenus::RenderPopupSimple(CUIRect Screen, const char *pTitle, const char *pExtraText, const char *pButtonText,
                               int ExtraAlign, bool CloseButton) {

    CUIRect Box, Part;
    Box = Screen;
    RenderPopupBox(Box, pTitle, pExtraText, ExtraAlign, &Part, CloseButton);

    Part.VMargin(120.0f, &Part);
    static int s_Button = 0;
    if ((pButtonText && DoButton_Menu(&s_Button, pButtonText, 0, &Part)) || m_EscapePressed || m_EnterPressed)
        m_Popup = POPUP_NONE;
}


void CMenus::RenderPopupQuit(CUIRect Screen) {
    CUIRect Box, Part;
    Box = Screen;
    RenderPopupBox(Box, Localize("Quit"), Localize("Are you sure that you want to quit?"), -1, &Part);

    CUIRect Yes, No;

    // additional info
    Box.HSplitTop(10.0f, 0, &Box);
    Box.VMargin(20.f / UI()->Scale(), &Box);
    if (m_pClient->Editor()->HasUnsavedData()) {
        char aBuf[256];
        str_format(aBuf, sizeof(aBuf), "%s\n%s", Localize(
                "There's an unsaved map in the editor, you might want to save it before you quit the game."),
                   Localize("Quit anyway?"));
        UI()->DoLabelScaled(&Box, aBuf, 20.f, -1, Part.w - 20.0f);
    }

    // buttons
    Part.VMargin(80.0f, &Part);
    Part.VSplitMid(&No, &Yes);
    Yes.VMargin(20.0f, &Yes);
    No.VMargin(20.0f, &No);

    static int s_ButtonAbort = 0;
    if (DoButton_Menu(&s_ButtonAbort, Localize("No"), 0, &No) || m_EscapePressed)
        m_Popup = POPUP_NONE;

    static int s_ButtonTryAgain = 0;
    if (DoButton_Menu(&s_ButtonTryAgain, Localize("Yes"), 0, &Yes) || m_EnterPressed)
        Client()->Quit();
}

void CMenus::RenderPopupLogout(CUIRect Screen) {
    CUIRect Box, Part;
    Box = Screen;
    RenderPopupBox(Box, Localize("Logout"), Localize("Do you really want to logout?"), -1, &Part);

    CUIRect Yes, No;

    // additional info
    Box.HSplitTop(10.0f, 0, &Box);
    Box.VMargin(20.f / UI()->Scale(), &Box);

    // buttons
    Part.VMargin(80.0f, &Part);
    Part.VSplitMid(&No, &Yes);
    Yes.VMargin(20.0f, &Yes);
    No.VMargin(20.0f, &No);

    static int s_ButtonAbort = 0;
    if (DoButton_Menu(&s_ButtonAbort, Localize("No"), 0, &No) || m_EscapePressed)
        m_Popup = POPUP_NONE;

    static int s_ButtonTryAgain = 0;
    if (DoButton_Menu(&s_ButtonTryAgain, Localize("Yes"), 0, &Yes) || m_EnterPressed) {
        m_FriendActiveActionsIndex = -1; // TODO: put this into some kind of OnLogout function or so
        Client()->Galaxy()->Account()->Logout();
    }
}


void CMenus::RenderPopupConnecting(CUIRect Screen) {
    CUIRect Box, Part;
    Box = Screen;
    char aBuf[128];

    const char *pTitle = Localize("Connecting to");
    const char *pExtraText = g_Config.m_UiServerAddress; // TODO: query the client about the address
    const char *pButtonText = Localize("Abort");
    if (Client()->MapDownloadTotalsize() > 0) {
        pTitle = Localize("Downloading map");
        pExtraText = "";
    }
    RenderPopupBox(Box, pTitle, pExtraText, -1, &Part);

    Part.VMargin(120.0f, &Part);

    static int s_Button = 0;
    if (DoButton_Menu(&s_Button, pButtonText, 0, &Part) || m_EscapePressed || m_EnterPressed) {
        Client()->Disconnect();
        m_Popup = POPUP_NONE;
    }

    if (Client()->MapDownloadTotalsize() > 0) {
        int64 Now = time_get();
        if (Now - m_DownloadLastCheckTime >= time_freq()) {
            if (m_DownloadLastCheckSize > Client()->MapDownloadAmount()) {
                // map downloaded restarted
                m_DownloadLastCheckSize = 0;
            }

            // update download speed
            float Diff = (Client()->MapDownloadAmount() - m_DownloadLastCheckSize) /
                         ((int) ((Now - m_DownloadLastCheckTime) / time_freq()));
            float StartDiff = m_DownloadLastCheckSize - 0.0f;
            if (StartDiff + Diff > 0.0f)
                m_DownloadSpeed = (Diff / (StartDiff + Diff)) * (Diff / 1.0f) +
                                  (StartDiff / (Diff + StartDiff)) * m_DownloadSpeed;
            else
                m_DownloadSpeed = 0.0f;
            m_DownloadLastCheckTime = Now;
            m_DownloadLastCheckSize = Client()->MapDownloadAmount();
        }

        Box.HSplitTop(64.f, 0, &Box);
        Box.HSplitTop(24.f, &Part, &Box);
        str_format(aBuf, sizeof(aBuf), "%d/%d KiB (%.1f KiB/s)", Client()->MapDownloadAmount() / 1024,
                   Client()->MapDownloadTotalsize() / 1024, m_DownloadSpeed / 1024.0f);
        UI()->DoLabel(&Part, aBuf, 20.f, 0, -1);

        // time left
        const char *pTimeLeftString;
        int TimeLeft = max(1, m_DownloadSpeed > 0.0f ? static_cast<int>(
                (Client()->MapDownloadTotalsize() - Client()->MapDownloadAmount()) / m_DownloadSpeed) : 1);
        if (TimeLeft >= 60) {
            TimeLeft /= 60;
            pTimeLeftString = TimeLeft == 1 ? Localize("%i minute left") : Localize("%i minutes left");
        } else
            pTimeLeftString = TimeLeft == 1 ? Localize("%i second left") : Localize("%i seconds left");
        Box.HSplitTop(20.f, 0, &Box);
        Box.HSplitTop(24.f, &Part, &Box);
        str_format(aBuf, sizeof(aBuf), pTimeLeftString, TimeLeft);
        UI()->DoLabel(&Part, aBuf, 20.f, 0, -1);

        // progress bar
        Box.HSplitTop(20.f, 0, &Box);
        Box.HSplitTop(24.f, &Part, &Box);
        Part.VMargin(40.0f, &Part);
        RenderTools()->DrawUIRect(&Part, vec4(1.0f, 1.0f, 1.0f, 0.25f), CUI::CORNER_ALL, 5.0f);
        Part.w = max(10.0f, (Part.w * Client()->MapDownloadAmount()) / Client()->MapDownloadTotalsize());
        RenderTools()->DrawUIRect(&Part, vec4(1.0f, 1.0f, 1.0f, 0.5f), CUI::CORNER_ALL, 5.0f);
    }
}


void CMenus::RenderPopupDeleteDemo(CUIRect Screen) {
    CUIRect Box, Part;
    Box = Screen;
    RenderPopupBox(Box, Localize("Delete demo"), Localize("Are you sure that you want to delete the demo?"), -1, &Part);
    CUIRect Yes, No;
    Part.VMargin(80.0f, &Part);
    Part.VSplitMid(&No, &Yes);

    Yes.VMargin(20.0f, &Yes);
    No.VMargin(20.0f, &No);

    static int s_ButtonAbort = 0;
    if (DoButton_Menu(&s_ButtonAbort, Localize("No"), 0, &No) || m_EscapePressed)
        m_Popup = POPUP_NONE;

    static int s_ButtonTryAgain = 0;
    if (DoButton_Menu(&s_ButtonTryAgain, Localize("Yes"), 0, &Yes) || m_EnterPressed) {
        m_Popup = POPUP_NONE;
        // delete demo
        if (m_DemolistSelectedIndex >= 0 && !m_DemolistSelectedIsDir) {
            char aBuf[512];
            str_format(aBuf, sizeof(aBuf), "%s/%s", m_aCurrentDemoFolder,
                       m_lDemos[m_DemolistSelectedIndex].m_aFilename);
            if (Storage()->RemoveFile(aBuf, m_lDemos[m_DemolistSelectedIndex].m_StorageType)) {
                DemolistPopulate();
                DemolistOnUpdate(false);
            } else
                PopupMessage(Localize("Error"), Localize("Unable to delete the demo"), Localize("Ok"));
        }
    }
}


void CMenus::RenderPopupRenameDemo(CUIRect Screen) {
    CUIRect Box, Part;
    Box = Screen;
    RenderPopupBox(Box, Localize("Rename demo"), 0, -1, &Part);

    CUIRect Label, TextBox, Ok, Abort;
    Part.VMargin(80.0f, &Part);

    Part.VSplitMid(&Abort, &Ok);

    Ok.VMargin(20.0f, &Ok);
    Abort.VMargin(20.0f, &Abort);

    static int s_ButtonAbort = 0;
    if (DoButton_Menu(&s_ButtonAbort, Localize("Abort"), 0, &Abort) || m_EscapePressed)
        m_Popup = POPUP_NONE;

    static int s_ButtonOk = 0;
    if (DoButton_Menu(&s_ButtonOk, Localize("Ok"), 0, &Ok) || m_EnterPressed) {
        m_Popup = POPUP_NONE;
        // rename demo
        if (m_DemolistSelectedIndex >= 0 && !m_DemolistSelectedIsDir) {
            char aBufOld[512];
            str_format(aBufOld, sizeof(aBufOld), "%s/%s", m_aCurrentDemoFolder,
                       m_lDemos[m_DemolistSelectedIndex].m_aFilename);
            int Length = str_length(m_aCurrentDemoFile);
            char aBufNew[512];
            if (Length <= 4 || m_aCurrentDemoFile[Length - 5] != '.' ||
                str_comp_nocase(m_aCurrentDemoFile + Length - 4, "demo"))
                str_format(aBufNew, sizeof(aBufNew), "%s/%s.demo", m_aCurrentDemoFolder, m_aCurrentDemoFile);
            else
                str_format(aBufNew, sizeof(aBufNew), "%s/%s", m_aCurrentDemoFolder, m_aCurrentDemoFile);
            if (Storage()->RenameFile(aBufOld, aBufNew, m_lDemos[m_DemolistSelectedIndex].m_StorageType)) {
                DemolistPopulate();
                DemolistOnUpdate(false);
            } else
                PopupMessage(Localize("Error"), Localize("Unable to rename the demo"), Localize("Ok"));
        }
    }

    Box.HSplitBottom(60.f, &Box, &Part);
    Box.HSplitBottom(24.f, &Box, &Part);

    Part.VSplitLeft(60.0f, 0, &Label);
    Label.VSplitLeft(120.0f, 0, &TextBox);
    TextBox.VSplitLeft(20.0f, 0, &TextBox);
    TextBox.VSplitRight(60.0f, &TextBox, 0);
    UI()->DoLabel(&Label, Localize("New name:"), 18.0f, -1);
    static float Offset = 0.0f;
    DoEditBox(&Offset, &TextBox, m_aCurrentDemoFile, sizeof(m_aCurrentDemoFile), 12.0f, &Offset);
}


void CMenus::RenderPopupPassword(CUIRect Screen) {
    CUIRect Box, Part;
    Box = Screen;
    RenderPopupBox(Box, Localize("Password incorrect"), 0, -1, &Part);

    CUIRect Label, TextBox, TryAgain, Abort;
    Part.VMargin(80.0f, &Part);

    Part.VSplitMid(&Abort, &TryAgain);

    TryAgain.VMargin(20.0f, &TryAgain);
    Abort.VMargin(20.0f, &Abort);

    static int s_ButtonAbort = 0;
    if (DoButton_Menu(&s_ButtonAbort, Localize("Abort"), 0, &Abort) || m_EscapePressed)
        m_Popup = POPUP_NONE;

    static int s_ButtonTryAgain = 0;
    if (DoButton_Menu(&s_ButtonTryAgain, Localize("Try again"), 0, &TryAgain) || m_EnterPressed) {
        Client()->Connect(g_Config.m_UiServerAddress);
    }

    Box.HSplitBottom(60.f, &Box, &Part);
    Box.HSplitBottom(24.f, &Box, &Part);

    Part.VSplitLeft(60.0f, 0, &Label);
    Label.VSplitLeft(100.0f, 0, &TextBox);
    TextBox.VSplitLeft(20.0f, 0, &TextBox);
    TextBox.VSplitRight(60.0f, &TextBox, 0);
    UI()->DoLabel(&Label, Localize("Password"), 18.0f, -1);
    static float Offset = 0.0f;
    DoEditBox(&g_Config.m_Password, &TextBox, g_Config.m_Password, sizeof(g_Config.m_Password), 12.0f, &Offset, true);
}

void CMenus::RenderPopupRemoveFriend(CUIRect Screen) {

    CUIRect Box, Part;
    Box = Screen;
    RenderPopupBox(Box, Localize("Remove friend"),
                   Localize("Are you sure that you want to remove the player from your friends list?"), -1, &Part);

    CUIRect Yes, No;
    Part.VMargin(80.0f, &Part);

    Part.VSplitMid(&No, &Yes);

    Yes.VMargin(20.0f, &Yes);
    No.VMargin(20.0f, &No);

    static int s_ButtonAbort = 0;
    if (DoButton_Menu(&s_ButtonAbort, Localize("No"), 0, &No) || m_EscapePressed)
        m_Popup = POPUP_NONE;

    static int s_ButtonTryAgain = 0;
    if (DoButton_Menu(&s_ButtonTryAgain, Localize("Yes"), 0, &Yes) || m_EnterPressed) {
        m_Popup = POPUP_NONE;
        // remove friend
        if (m_FriendlistSelectedIndex >= 0) {
            //TODO: m_pClient->Friends()->RemoveFriend(m_lFriends[m_FriendlistSelectedIndex].m_pFriendInfo->m_pName);
            FriendlistOnUpdate();
            Client()->ServerBrowserUpdate();
        }
    }
}

void CMenus::RenderPopupFirstLaunch(CUIRect Screen) {
    m_Popup = POPUP_NONE;
    //TODO: implement me with welcome message

    /* pTitle = Localize("Welcome to Teeworlds");
              pExtraText = Localize("As this is the first time you launch the game, please enter your nick name below. It's recommended that you check the settings to adjust them to your liking before joining a server.");
              pButtonText = Localize("Ok");
              ExtraAlign = -1;
        CUIRect Label, TextBox;

              Box.HSplitBottom(20.f, &Box, &Part);
              Box.HSplitBottom(24.f, &Box, &Part);
              Part.VMargin(80.0f, &Part);

              static int s_EnterButton = 0;
              if(DoButton_Menu(&s_EnterButton, Localize("Enter"), 0, &Part) || m_EnterPressed)
                  m_Popup = POPUP_NONE;

              Box.HSplitBottom(40.f, &Box, &Part);
              Box.HSplitBottom(24.f, &Box, &Part);

              Part.VSplitLeft(60.0f, 0, &Label);
              Label.VSplitLeft(100.0f, 0, &TextBox);
              TextBox.VSplitLeft(20.0f, 0, &TextBox);
              TextBox.VSplitRight(60.0f, &TextBox, 0);
              UI()->DoLabel(&Label, Localize("Nickname"), 18.0f, -1);
              static float Offset = 0.0f;
              DoEditBox(&g_Config.m_PlayerName, &TextBox, g_Config.m_PlayerName, sizeof(g_Config.m_PlayerName), 12.0f, &Offset);
        */
}

void CMenus::RenderPopupLanguage(CUIRect Screen) {
    CUIRect Box = Screen;
    CUIRect Part;

    RenderPopupBox(Box, Localize("Language"), 0, -1, &Part, 0);
    Box.HMargin(5.0f, &Box);
    RenderLanguageSelection(Box);

    static int s_Button = 0;
    Part.VMargin(80.0f, &Part);
    if (DoButton_Menu(&s_Button, Localize("Ok"), 0, &Part) || m_EscapePressed || m_EnterPressed)
        m_Popup = POPUP_NONE;
}

void CMenus::RenderPopupCountry(CUIRect Screen) {
    CUIRect Box = Screen;
    CUIRect Part;
    Box.VMargin(150.0f, &Box);
    Box.HMargin(150.0f, &Box);
    Box.HSplitTop(20.f, &Part, &Box);
    Box.HSplitBottom(20.f, &Box, &Part);
    Box.HSplitBottom(24.f, &Box, &Part);
    Box.HSplitBottom(20.f, &Box, 0);
    Box.VMargin(20.0f, &Box);

    static int ActSelection = -2;
    if (ActSelection == -2)
        ActSelection = g_Config.m_BrFilterCountryIndex;
    static float s_ScrollValue = 0.0f;
    int OldSelected = -1;
    UiDoListboxStart(&s_ScrollValue, &Box, 50.0f, Localize("Country"), "", m_pClient->m_pCountryFlags->Num(), 6,
                     OldSelected, s_ScrollValue);

    for (int i = 0; i < m_pClient->m_pCountryFlags->Num(); ++i) {
        const CCountryFlags::CCountryFlag *pEntry = m_pClient->m_pCountryFlags->GetByIndex(i);
        if (pEntry->m_CountryCode == ActSelection)
            OldSelected = i;

        CListboxItem Item = UiDoListboxNextItem(&pEntry->m_CountryCode, OldSelected == i);
        if (Item.m_Visible) {
            CUIRect Label;
            Item.m_Rect.Margin(5.0f, &Item.m_Rect);
            Item.m_Rect.HSplitBottom(10.0f, &Item.m_Rect, &Label);
            float OldWidth = Item.m_Rect.w;
            Item.m_Rect.w = Item.m_Rect.h * 2;
            Item.m_Rect.x += (OldWidth - Item.m_Rect.w) / 2.0f;
            vec4 Color(1.0f, 1.0f, 1.0f, 1.0f);
            m_pClient->m_pCountryFlags->Render(pEntry->m_CountryCode, &Color, Item.m_Rect.x, Item.m_Rect.y,
                                               Item.m_Rect.w, Item.m_Rect.h);
            UI()->DoLabel(&Label, pEntry->m_aCountryCodeString, 10.0f, 0);
        }
    }

    const int NewSelected = UiDoListboxEnd(&s_ScrollValue, 0);
    if (OldSelected != NewSelected)
        ActSelection = m_pClient->m_pCountryFlags->GetByIndex(NewSelected)->m_CountryCode;

    Part.VMargin(120.0f, &Part);

    static int s_Button = 0;
    if (DoButton_Menu(&s_Button, Localize("Ok"), 0, &Part) || m_EnterPressed) {
        g_Config.m_BrFilterCountryIndex = ActSelection;
        Client()->ServerBrowserUpdate();
        m_Popup = POPUP_NONE;
    }

    if (m_EscapePressed) {
        ActSelection = g_Config.m_BrFilterCountryIndex;
        m_Popup = POPUP_NONE;
    }
}


void CMenus::RenderPopupSettings(CUIRect Screen) {
    static int s_SettingsPage = 0;
    CUIRect Content = Screen;

    Content.Margin(50.0f, &Content);

    // render background
    CUIRect Temp, TabBar, RestartWarning;
    Content.HSplitBottom(15.0f, &Content, &RestartWarning);
    Content.VSplitRight(120.0f, &Content, &TabBar);
    RenderTools()->DrawUIRect(&Content, ms_ColorTabbarActive, CUI::CORNER_B | CUI::CORNER_TL, 3.0f);
    //TabBar.HSplitTop(50.0f, &Temp, &TabBar);
    //RenderTools()->DrawUIRect(&Temp, ms_ColorTabbarActive, CUI::CORNER_R, 10.0f);

    Content.HSplitTop(10.0f, 0, &Content);

    CUIRect Button;

    const char *aTabs[] = {
            Localize("Language"),
            Localize("General"),
            Localize("Country"),
            Localize("Controls"),
            Localize("Graphics"),
            Localize("Sound")};

    int NumTabs = (int) (sizeof(aTabs) / sizeof(*aTabs));

    // TODO: Add scroll events, when mouse is hovering tabs ;)

    for (int i = 0; i < NumTabs; i++) {
        TabBar.HSplitTop(26, &Button, &TabBar);
        if (DoButton_MenuTab(aTabs[i], aTabs[i], s_SettingsPage == i, &Button, 0))
            s_SettingsPage = i;
        TabBar.HSplitTop(4, &Button, &TabBar);
    }

    Content.Margin(10.0f, &Content);

    if (s_SettingsPage == 0)
        RenderLanguageSelection(Content);
    else if (s_SettingsPage == 1)
        RenderSettingsGeneral(Content);
    else if (s_SettingsPage == 2)
        RenderSettingsCountry(Content);
    else if (s_SettingsPage == 3)
        RenderSettingsControls(Content);
    else if (s_SettingsPage == 4)
        RenderSettingsGraphics(Content);
    else if (s_SettingsPage == 5)
        RenderSettingsSound(Content);

    if (m_NeedRestartGraphics || m_NeedRestartSound)
        UI()->DoLabel(&RestartWarning, Localize("You must restart the game for all settings to take effect."), 15.0f,
                      -1);

    if (m_EscapePressed) {
        m_Popup = POPUP_NONE;
    }
}

void CMenus::RenderPopupSettingsTee(CUIRect Screen) {
    CUIRect Content = Screen;

    Content.Margin(50.0f, &Content);
    // render background
    RenderTools()->DrawUIRect(&Content, ms_ColorTabbarActive, CUI::CORNER_B | CUI::CORNER_TL, 3.0f);

    Content.HSplitTop(10.0f, 0, &Content);
    Content.Margin(10.0f, &Content);
    RenderSettingsTee(Content);

    if (m_EscapePressed) {
        m_Popup = POPUP_NONE;
    }
}

void CMenus::RenderPopupNews(CUIRect Screen) {
    CUIRect Content = Screen;
    CUIRect CloseButtonRect;
    Content.Margin(50.0f, &Content);

    RenderTools()->DrawUIRect(&Content, ms_ColorTabbarActive, CUI::CORNER_ALL, 3.0f);

    Content.Margin(10.0f, &Content);

    Content.HSplitTop(24.f, &CloseButtonRect, &Content);
	CloseButtonRect.VSplitRight(6.0f, &CloseButtonRect, 0);

	static int s_CloseButton = 0;
    int HoverOrClick = DoIcon(&s_CloseButton, SPRITE_GUIICON_TIMES, &CloseButtonRect,
                              CUI::ALIGN_CENTER | CUI::ALIGN_RIGHT, 3.0f);
    if (HoverOrClick > 0) {
        m_Popup = POPUP_NONE;
    }


    RenderNews(Content);
    if (m_EscapePressed) {
        m_Popup = POPUP_NONE;
    }
}

void CMenus::RenderPopupSearchPlayer(CUIRect Screen) {

    CUIRect Box;
    Box = Screen;

    RenderPopupBox(Box, Localize("Search Players"), 0, -1, 0, false);
    CUIRect Results, SearchBox;

    Box.Margin(6.0f, &Box);


    const float LineSize = 30.0f;
    const float FontSize = 12.0f;

    Box.HSplitTop(LineSize, &SearchBox, &Results);
    SearchBox.w = 120.0f;

    Results.HSplitTop(20.0f, 0, &Results);

    static float NameOffset = 0.0f;
    static char s_aName[64] = {0};

    CUIEditBoxStyle EditBoxStyle;
    EditBoxStyle.EditBoxSetHidden(false);
    EditBoxStyle.BackgroundSetColor(vec4(0, 0, 0, 0));
    EditBoxStyle.TextSetFontColor(vec4(1, 1, 1, 1.0f));
    EditBoxStyle.TextSetFontSize(12.0f);
    EditBoxStyle.EditBoxSetPlaceholderColor(STYLE_COLOR_BG_LIGHT);
    EditBoxStyle.MarginSetLeft(5.0f);
    EditBoxStyle.BorderSetColor(STYLE_COLOR_BG_LIGHT);
    EditBoxStyle.BorderSetSize(1.0f);
    EditBoxStyle.EditBoxSetShowClear(true);

    SearchBox.VSplitLeft(6.0f, 0, &SearchBox);
    if(DoEditBox2(&s_aName, &SearchBox,  s_aName, sizeof(s_aName), &NameOffset, Localize("Search"), EditBoxStyle)) {
        if (str_length(s_aName) > 0) {
            Client()->Galaxy()->Players()->Search(s_aName);
        } else {
            Client()->Galaxy()->Players()->Reset();
        }
    }

    static float s_ScrollValue = 0.0f;
    if (Client()->Galaxy()->Players()->GetStatus() == IApiService::STATUS_OK) {
        UiDoListboxStart(&s_ScrollValue, &Results, 50.0f, Localize("Search"), "", Client()->Galaxy()->Players()->NumEntries(), 6,
                         -1, s_ScrollValue);

        for (int i = 0; i < Client()->Galaxy()->Players()->NumEntries(); ++i) {
            const CApiPlayer *pPlayer = Client()->Galaxy()->Players()->GetEntry(i);
            CListboxItem Item = UiDoListboxNextItem(pPlayer, false);
            if (Item.m_Visible) {
                CUIRect Label;
                Item.m_Rect.Margin(5.0f, &Item.m_Rect);
                Item.m_Rect.HSplitBottom(10.0f, &Item.m_Rect, &Label);
                float OldWidth = Item.m_Rect.w;
                Item.m_Rect.w = Item.m_Rect.h * 2;
                Item.m_Rect.x += (OldWidth - Item.m_Rect.w) / 2.0f;
                vec4 Color(1.0f, 1.0f, 1.0f, 1.0f);
                //m_pClient->m_pCountryFlags->Render(pEntry->m_CountryCode, &Color, Item.m_Rect.x, Item.m_Rect.y, Item.m_Rect.w, Item.m_Rect.h);

                RenderTee(Item.m_Rect, pPlayer->m_pSkin, pPlayer->m_UseColor, pPlayer->m_ColorBody,
                          pPlayer->m_ColorFeet);
                if (pPlayer->m_pName) {
                    UI()->DoLabel(&Label, pPlayer->m_pName, 10.0f, 0);
                }
            }
        }
        const int Selected = UiDoListboxEnd(&s_ScrollValue, 0);

        if (Selected != -1) {
            const CApiPlayer *pPlayer = Client()->Galaxy()->Players()->GetEntry(Selected);
            if (pPlayer->m_ID) {
                Client()->Galaxy()->Players()->Detail(pPlayer->m_ID);
                m_Popup = POPUP_PLAYER;
            } else {
                m_Popup = POPUP_NONE;
            }
        }
    } else {
        UI()->DoSpinner(&Results);
    }

    if (m_EscapePressed) {
        m_Popup = POPUP_NONE;
    }
}


void CMenus::RenderPopupPlayer(CUIRect Screen) {

    static int64 s_LastChecked = 0;
    const CApiPlayer *pPlayer = Client()->Galaxy()->Players()->GetDetailPlayer();
    bool Ready = Client()->Galaxy()->Players()->GetStatus() == IApiService::STATUS_OK && pPlayer != 0;


    CUIRect Box;
    Box = Screen;

    if (m_EscapePressed) {
        m_Popup = POPUP_NONE;
    }

    Box.VMargin(150.0f, &Box);
    Box.HMargin(150.0f, &Box);
    RenderTools()->DrawUIRect(&Box, STYLE_COLOR_POPUP_BG,0, 0.0f);

    if (!Ready) {
        // We did not finished loading, so we show a simple spinner and return
        UI()->DoSpinner(&Box);
        s_LastChecked = 0;
        return;
    }

    CUIRect InfoRect, ChatRect;

    CUIRect TopBar;
    //Box.HSplitTop(24.f, &TopBar, &Box);

    /*CUIRect CloseButtonRect = TopBar;
    CloseButtonRect.VSplitRight(6.0f, &CloseButtonRect, 0);

    static int s_CloseButton = 0;
    int HoverOrClick = DoIcon(&s_CloseButton, SPRITE_GUIICON_TIMES, &CloseButtonRect,
                              CUI::ALIGN_CENTER | CUI::ALIGN_RIGHT, 3.0f);
    if (HoverOrClick > 0) {
        m_Popup = POPUP_NONE;
    }*/

    CUIRect PlayerInfo, PlayerChat;
    Box.VSplitPerc(0.5f, &PlayerChat, &PlayerInfo);


    CUIRect Label;
    PlayerInfo.HSplitTop(24.f, &Label, &PlayerInfo);
    // We render the playername
    UI()->DoLabelScaled(&Label, pPlayer->m_pName, 24.f, 0, -1, &STYLE_COLOR_TEXT_LIGHT);

    PlayerInfo.Margin(20.0f, &PlayerInfo);
    PlayerInfo.VMargin(40.0f, &PlayerInfo);
    PlayerChat.Margin(5.0f, &PlayerChat);

    //Render player info
    {
        CUIRect Info, Actions, SkinBox;

        PlayerInfo.HSplitPerc(0.5f, &PlayerInfo, &Info);

        PlayerInfo.HSplitTop(30.0f, &SkinBox, &Actions);
        Info.HSplitTop(30.0f, &Info, 0);

        CUIRect InfoTop, InfoBottom;
        Info.HSplitPerc(0.5f, &InfoTop, &InfoBottom);
        InfoBottom.HSplitTop(2.0f, 0, &InfoBottom);

        if(pPlayer->m_pClanName)
        {
            vec4 Color = STYLE_COLOR_BG_LIGHT;
            UI()->DoLabelScaled(&InfoBottom, pPlayer->m_pClanName, InfoBottom.h, CUI::ALIGN_LEFT,InfoBottom.w, &Color);
        }

        //Render Information

        //m_pClient->m_pCountryFlags->Render(pEntry->m_CountryCode, &Color, Item.m_Rect.x, Item.m_Rect.y, Item.m_Rect.w, Item.m_Rect.h);

        RenderTee(SkinBox, pPlayer->m_pSkin, pPlayer->m_UseColor, pPlayer->m_ColorBody, pPlayer->m_ColorFeet);


        //Render Actions

        //check if the player is already a friend, (a) Add Friend, (b) Remove Friend, (c) Accept Request, (d) Request sent
        CUIRect ButtonBox;

        Actions.HSplitTop(5.0f, 0, &Actions);
        Actions.HSplitTop(20.0f, &ButtonBox, & Actions);

        ButtonBox.VMargin(20.0f, &ButtonBox);


        static CUIButton SubmitButton;


        static CApiFriend::FriendStatus FriendStatus = Client()->Galaxy()->Friends()->GetFriendStatus(pPlayer->m_ID);

        int64 now = time_get();
        //check only at most once per second
        if (!s_LastChecked || s_LastChecked + time_freq() < now)  {
            FriendStatus = Client()->Galaxy()->Friends()->GetFriendStatus(pPlayer->m_ID);
            s_LastChecked = now;
        }

        SubmitButton.SetEnabled(true);
        SubmitButton.SetErroneous(false);
        switch (FriendStatus) {
            case CApiFriend::STATUS_FRIEND:
                SubmitButton.SetText(Localize("Remove Friend"));
                SubmitButton.SetErroneous(true);
                break;

            case CApiFriend::STATUS_UNKNOWN:
                SubmitButton.SetText(Localize("Send Friend Request"));
                break;
            case CApiFriend::STATUS_REQUEST:
                SubmitButton.SetText(Localize("Accept Request"));
                break;
            case CApiFriend::STATUS_OWN_REQUEST:
                SubmitButton.SetText(Localize("Request sent"));
                SubmitButton.SetEnabled(false);
                break;
        }

        SubmitButton.Create(ButtonBox, RenderTools());

        if(SubmitButton.Pressed(Input()))
        {
            switch (FriendStatus) {
                case CApiFriend::STATUS_FRIEND:
                    Client()->Galaxy()->Friends()->RemoveFriend(*pPlayer);
                    break;
                case CApiFriend::STATUS_UNKNOWN:
                    Client()->Galaxy()->Friends()->AddFriend(*pPlayer);
                    break;
                case CApiFriend::STATUS_REQUEST:
                    Client()->Galaxy()->Friends()->AddFriend(*pPlayer);
                    break;
            }

            //TODO: dont close
            m_Popup = POPUP_NONE;
        }
    }

    // Render Player chat
    {

        static CTopicWebsocketConnection *pWS = nullptr;

        static CUIChatView ChatView;

        if (pWS == nullptr) {
            pWS = new CTopicWebsocketConnection();

            pWS->m_pUser = this;
            pWS->SetPath("wss://phxchat.herokuapp.com/socket/websocket?token=undefined&vsn=2.0.0");
            pWS->m_Secure = false;
            Client()->Galaxy()->Api()->Handle(pWS);
            ChatView.SetTopicWebsocketConnection(pWS);
            ChatView.SetKernel(m_pClient->Kernel());
        }


        ChatView.Create(PlayerChat, RenderTools());

        //m_pConnection->Send(R"json(["chats", null,"room:lobby","phx_join",{}])json");
        //m_pConnection->Send(R"json(["2","4","room:lobby","shout",{"name":"asd","message":"asd"}])json");



    }
}