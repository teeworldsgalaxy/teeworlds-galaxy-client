/* (c) Magnus Auvinen. See licence.txt in the root of the distribution for more information. */
/* If you are missing that file, acquire a complete release at teeworlds.com.                */
#ifndef GAME_CLIENT_COMPONENTS_MENUS_H
#define GAME_CLIENT_COMPONENTS_MENUS_H

#include <base/vmath.h>
#include <base/tl/sorted_array.h>

#include <engine/demo.h>
#include <engine/friends.h>


#include <game/voting.h>
#include <game/client/component.h>
#include <game/client/ui.h>

#include <game/client/ui/styles/background.h>
#include <game/client/ui/styles/text.h>
#include <game/client/ui/styles/border.h>
#include <game/client/ui/styles/margin.h>
#include <game/client/ui/styles/corner.h>
#include <game/client/ui/styles/edit_box.h>
#include <game/client/ui/styles/link.h>


// compnent to fetch keypresses, override all other input
class CMenusKeyBinder : public CComponent
{
public:
	bool m_TakeKey;
	bool m_GotKey;
	IInput::CEvent m_Key;
	CMenusKeyBinder();
	virtual bool OnInput(IInput::CEvent Event);
};

class CMenus : public CComponent
{
	static vec4 ms_GuiColor;
	static vec4 ms_ColorTabbarInactiveOutgame;
	static vec4 ms_ColorTabbarActiveOutgame;
	static vec4 ms_ColorTabbarInactiveIngame;
	static vec4 ms_ColorTabbarActiveIngame;
	static vec4 ms_ColorTabbarInactive;
	static vec4 ms_ColorTabbarActive;

	vec4 ButtonColorMul(const void *pID);
	vec4 ButtonColorMul(const void *pID, vec4 DefaultColor);
	vec4 ColorSwitch(const void *pID, vec4 DefaultColor, vec4 HoverColor, vec4 ActiveColor);
	void DoBorder(const CUIRect *pRect, vec4 Color, float Strength, int Sides = 15, int Corners = 0, float Rounding = 0.0f);

	static vec4 Brightness(vec4 Color, float Brightness) { return CUI::Brightness(Color, Brightness); }; // TODO: only keep either one

	int DoButton_DemoPlayer(const void *pID, const char *pText, int Checked, const CUIRect *pRect);
	int DoButton_Sprite(const void *pID, int ImageID, int SpriteID, int Checked, const CUIRect *pRect, int Corners);
	int DoButton_Toggle(const void *pID, int Checked, const CUIRect *pRect, bool Active);
	int DoButton_Menu(const void *pID, const char *pText, int Checked, const CUIRect *pRect);
	int DoButton_MenuTab(const void *pID, const char *pText, int Checked, const CUIRect *pRect, int Corners, float Radius = 4.0f);

	int DoButton_CheckBox_Common(const void *pID, const char *pText, const char *pBoxText, const CUIRect *pRect, bool HoverSound = true);
	int DoButton_CheckBox(const void *pID, const char *pText, int Checked, const CUIRect *pRect, int Alignment = 12, float BoxMargin = 2.0f, bool HoverSound = true);
	int DoButton_CheckBox_Number(const void *pID, const char *pText, int Checked, const CUIRect *pRect, bool HoverSound = true);
	int DoIcon(const void *pID, int Icon, const CUIRect *pRect, int Alignment = 0, float BoxMargin = 0.0f, bool HoverSound = false, vec4 *pColor = 0);
	/*static void ui_draw_menu_button(const void *id, const char *text, int checked, const CUIRect *r, const void *extra);
	static void ui_draw_keyselect_button(const void *id, const char *text, int checked, const CUIRect *r, const void *extra);
	static void ui_draw_menu_tab_button(const void *id, const char *text, int checked, const CUIRect *r, const void *extra);
	static void ui_draw_settings_tab_button(const void *id, const char *text, int checked, const CUIRect *r, const void *extra);
	*/

	int DoButton_Icon(int ImageId, int SpriteId, const CUIRect *pRect);
	int DoButton_GridHeader(const void *pID, const char *pText, int Checked, const CUIRect *pRect);

	//static void ui_draw_browse_icon(int what, const CUIRect *r);
	//static void ui_draw_grid_header(const void *id, const char *text, int checked, const CUIRect *r, const void *extra);

	/*static void ui_draw_checkbox_common(const void *id, const char *text, const char *boxtext, const CUIRect *r, const void *extra);
	static void ui_draw_checkbox(const void *id, const char *text, int checked, const CUIRect *r, const void *extra);
	static void ui_draw_checkbox_number(const void *id, const char *text, int checked, const CUIRect *r, const void *extra);
	*/
	int DoEditBox(void *pID, const CUIRect *pRect, char *pStr, unsigned StrSize, float FontSize, float *Offset, bool Hidden=false, int Corners=CUI::CORNER_ALL);
	int DoEditBox2(void *pID, const CUIRect *pRect, char *pStr, unsigned StrSize,  float *Offset, const char *pPlaceholder, const CUIEditBoxStyle &Style);
	//static int ui_do_edit_box(void *id, const CUIRect *rect, char *str, unsigned str_size, float font_size, bool hidden=false);

	float DoScrollbarV(const void *pID, const CUIRect *pRect, float Current, float Percent = 0.3f);
	float DoScrollbarH(const void *pID, const CUIRect *pRect, float Current);
	void DoButton_KeySelect(const void *pID, const char *pText, int Checked, const CUIRect *pRect);
	int DoKeyReader(void *pID, const CUIRect *pRect, int Key);


	void DoDivider(const CUIRect *pRect);

	//static int ui_do_key_reader(void *id, const CUIRect *rect, int key);
	void UiDoGetButtons(int Start, int Stop, CUIRect View);

	struct CListboxItem
	{
		int m_Visible;
		int m_Selected;
		CUIRect m_Rect;
		CUIRect m_HitRect;
	};

	void UiDoListboxStart(const void *pID, const CUIRect *pRect, float RowHeight, const char *pTitle, const char *pBottomText, int NumItems,
						int ItemsPerRow, int SelectedIndex, float ScrollValue);
	CListboxItem UiDoListboxNextItem(const void *pID, bool Selected = false);
	CListboxItem UiDoListboxNextRow();
	int UiDoListboxEnd(float *pScrollValue, bool *pItemActivated);

	//static void demolist_listdir_callback(const char *name, int is_dir, void *user);
	//static void demolist_list_callback(const CUIRect *rect, int index, void *user);



	int64 m_LastInput;

	// loading
	int m_LoadCurrent;
	int m_LoadTotal;

	//
	char m_aMessageTopic[512];
	char m_aMessageBody[512];
	char m_aMessageButton[512];

	void PopupMessage(const char *pTopic, const char *pBody, const char *pButton);



	// some settings
	static float ms_ButtonHeight;
	static float ms_ListheaderHeight;
	static float ms_FontmodHeight;

	// for settings
	bool m_NeedRestartGraphics;
	bool m_NeedRestartSound;
	bool m_NeedSendinfo;
	int m_SettingPlayerPage;

	//
	bool m_EscapePressed;
	bool m_EnterPressed;
	bool m_DeletePressed;

	// for map download popup
	int64 m_DownloadLastCheckTime;
	int m_DownloadLastCheckSize;
	float m_DownloadSpeed;

	// for call vote
	int m_CallvoteSelectedOption;
	int m_CallvoteSelectedPlayer;
	char m_aCallvoteReason[VOTE_REASON_LENGTH];

	// demo
	struct CDemoItem
	{
		char m_aFilename[128];
		char m_aName[128];
		bool m_IsDir;
		int m_StorageType;

		bool m_InfosLoaded;
		bool m_Valid;
		CDemoHeader m_Info;

		bool operator<(const CDemoItem &Other) { return !str_comp(m_aFilename, "..") ? true : !str_comp(Other.m_aFilename, "..") ? false :
														m_IsDir && !Other.m_IsDir ? true : !m_IsDir && Other.m_IsDir ? false :
														str_comp_filenames(m_aFilename, Other.m_aFilename) < 0; }
	};

	sorted_array<CDemoItem> m_lDemos;
	char m_aCurrentDemoFolder[256];
	char m_aCurrentDemoFile[64];
	int m_DemolistSelectedIndex;
	bool m_DemolistSelectedIsDir;
	int m_DemolistStorageType;

	void DemolistOnUpdate(bool Reset);
	void DemolistPopulate();
	static int DemolistFetchCallback(const char *pName, int IsDir, int StorageType, void *pUser);

	struct CLightParticle {

		vec2 m_Pos;
		vec2 m_StartPos;
		float m_InnerRadius;
		float m_MaxRadius;
		float m_MinRadius;
		float m_CurrentRadius;
		float m_CurrentSpeed;
		float m_Speed;
		float m_Transparency;

		void Update(float DeltaFrameTime, float ContinuousTime)
		{
			DeltaFrameTime *= 60.0f;

			// radius
			m_CurrentRadius += (m_Speed/50.0f) * DeltaFrameTime;

			if(m_CurrentRadius > m_MaxRadius)
			{
				m_CurrentRadius = m_MaxRadius;
				m_Speed = -m_Speed;
			}
			else if(m_CurrentRadius < m_MinRadius)
			{
				m_CurrentRadius = m_MinRadius;
				m_Speed = -m_Speed;
			}

			// position
			m_Pos.x = m_StartPos.x + (sinf(fabsf(m_Speed*2)*ContinuousTime/16.0f) * 30.0f * 2);
			m_Pos.y = m_StartPos.y + (cosf(fabsf(m_Speed)*ContinuousTime/8.0f) * 30.0f * tanf(fabsf(m_Speed)));

			// transparency
//			m_Transparency = max(0.5f, min((float)min((int64)1, time_get()-m_StartTime), sinf(TimeDiff*2.0f*3.1415f)*cosf(TimeDiff*3.0f*3.1415f)));
			m_Transparency = min(1.0f, (m_Transparency+DeltaFrameTime*frandom()/100.0f));
		}
	};
	int m_FriendlistSelectedIndex;

	void FriendlistOnUpdate();

	// found in menus.cpp
	int Render();
	//void render_background();
	//void render_loading(float percent);
	int RenderMenubar(CUIRect r);
	void RenderNews(CUIRect MainView);

	// found in menus_demo.cpp
	void RenderDemoPlayer(CUIRect MainView);
	void RenderDemoList(CUIRect MainView);

	// found in menus_ingame.cpp
	void RenderGame(CUIRect Screen);
	void RenderPopupServerPlayers(CUIRect Screen);
	void RenderPopupServerInfo(CUIRect Screen);
	void RenderPopupServerVotes(CUIRect Screen);
	void RenderServerControlKick(CUIRect MainView, bool FilterSpectators);
	void RenderServerControlServer(CUIRect MainView);

	// found in menus_browser.cpp
	int m_SelectedIndex;
	int m_ScrollOffset;
	void RenderServerbrowserServerList(CUIRect View);
	void RenderServerbrowserServerDetail(CUIRect View);
	void RenderServerbrowserServerDetailMap(CUIRect View, const CServerInfo *pSelectedServer);
	void RenderServerbrowserFilters(CUIRect View);
	void RenderServerbrowserFriends(CUIRect View);
	void RenderServerbrowser(CUIRect MainView);
	static void ConchainFriendlistUpdate(IConsole::IResult *pResult, void *pUserData, IConsole::FCommandCallback pfnCallback, void *pCallbackUserData);
	static void ConchainServerbrowserUpdate(IConsole::IResult *pResult, void *pUserData, IConsole::FCommandCallback pfnCallback, void *pCallbackUserData);

	// found in menus_settings.cpp
	void RenderLanguageSelection(CUIRect MainView);
	void RenderSettingsGeneral(CUIRect MainView);
	void RenderSettingsCountry(CUIRect MainView);
	void RenderSettingsTee(CUIRect MainView);
	void RenderSettingsControls(CUIRect MainView);
	void RenderSettingsGraphics(CUIRect MainView);
	void RenderSettingsSound(CUIRect MainView);
	void RenderSettings(CUIRect MainView);

	//found in menus_login.cpp
	int m_FriendActiveActionsIndex;
	void RenderMain(CUIRect MainView);
	void RenderMainMenu(CUIRect Menu);
	int DoMainMenuItem(void *pId, CUIRect ItemView, int Icon);
	void OnLogout();
	bool PageIsValid(int Page);
	bool PageIsLoginscreen(int Page);
	void RenderLogin(CUIRect MainView);
	void RenderLoginForm(CUIRect LoginForm);
	void RenderRecoverForm(CUIRect LoginForm);
    void RenderRegisterForm(CUIRect RegisterForm);
	void RenderPlayerSection(CUIRect MainView);
	void RenderSectionLeftBottom(CUIRect SectionView);
	void RenderFriendListEntry(CListboxItem Item, const CApiFriend *pInfo, int i, bool Request);
	void RenderFriendList(CUIRect UpperBox);
	void RenderSectionLeftBottomFriendsAction(CUIRect View);
	void RenderSectionLeftBottomFriendTee(CUIRect& Tee, const class CApiFriend *pInfo);
	void RenderLightParticle(CLightParticle *pParticle);
	void RenderTee(CUIRect View, const char *pSkin, bool UseColor, int ColorBody, int ColorFeet);
	void GenerateLightParticles(CUIRect *pArea, CLightParticle aParticles[], int Num);




	// popups found in menus_popups.cpp
	void RenderPopupBox(CUIRect& Box, const char *pTitle = 0, const char *pExtraText = 0, int ExtraAlign = 0, CUIRect *pPart = 0, bool CloseButton = true);
	void RenderPopupSimple(CUIRect Screen, const char *pTitle, const char *pExtraText, const char *pButtonText, int ExtraAlign = 0, bool CloseButton = true);
	void RenderPopupQuit(CUIRect Screen);
	void RenderPopupLogout(CUIRect Screen);
	void RenderPopupConnecting(CUIRect Screen);
	void RenderPopupDeleteDemo(CUIRect Screen);
	void RenderPopupRenameDemo(CUIRect Screen);
	void RenderPopupPassword(CUIRect Screen);
	void RenderPopupRemoveFriend(CUIRect Screen);
	void RenderPopupFirstLaunch(CUIRect Screen);
	void RenderPopupLanguage(CUIRect Screen);
	void RenderPopupCountry(CUIRect Screen);
	void RenderPopupSettings(CUIRect Screen);
	void RenderPopupSettingsTee(CUIRect Screen);
	void RenderPopupNews(CUIRect Screen);
	void RenderPopupSearchPlayer(CUIRect Screen);
	void RenderPopupPlayer(CUIRect Screen);


 public:
    enum
    {
        POPUP_NONE = 0,
        POPUP_FIRST_LAUNCH,
        POPUP_CONNECTING,
        POPUP_MESSAGE,
        POPUP_DISCONNECTED,
        POPUP_PURE,
        POPUP_LANGUAGE,
        POPUP_COUNTRY,
        POPUP_DELETE_DEMO,
        POPUP_RENAME_DEMO,
        POPUP_REMOVE_FRIEND,
        POPUP_SOUNDERROR,
        POPUP_PASSWORD,
        POPUP_LOGOUT,
        POPUP_QUIT,
        POPUP_SETTINGS_MAIN,
        POPUP_SETTINGS_TEE,
        POPUP_NEWS,
        POPUP_SEARCH_PLAYER,
        POPUP_PLAYER,
        POPUP_SERVER_PLAYERS,
        POPUP_SERVER_INFO,
        POPUP_SERVER_VOTES,
    };

    enum
    {
        PAGE_NEWS = 0,
        PAGE_MAIN,
        PAGE_GAME,
        PAGE_PLAYERS,
        PAGE_SERVER_INFO,
        PAGE_CALLVOTE,
        PAGE_INTERNET,
        PAGE_LAN,
        PAGE_FAVORITES,
        PAGE_DEMOS,
        PAGE_SETTINGS,
        PAGE_SYSTEM,
        PAGE_LOGIN,
        PAGE_REGISTER,
        PAGE_RECOVER,
        NUM_PAGES
    };

    int m_GamePage;
    int m_Popup;
    int m_LastPopup;
    int m_ActivePage;
    bool m_MenuActive;
    bool m_UseMouseButtons;
    vec2 m_MousePos;

    void SetActive(bool Active);
	void RenderBackground(float Alpha = 255.0f);

	void UseMouseButtons(bool Use) { m_UseMouseButtons = Use; }

	static CMenusKeyBinder m_Binder;

	CMenus();

	void RenderLoading();

	bool IsActive() const { return m_MenuActive; }

	virtual void OnInit();

	virtual void OnStateChange(int NewState, int OldState);
	virtual void OnReset();
	virtual void OnRender();
	virtual bool OnInput(IInput::CEvent Event);
	virtual bool OnMouseMove(float x, float y);

	// TODO: this is a bit ugly but.. well.. yeah
	enum { MAX_INPUTEVENTS = 32 };
	static IInput::CEvent m_aInputEvents[MAX_INPUTEVENTS];
	static int m_NumInputEvents;
};
#endif
