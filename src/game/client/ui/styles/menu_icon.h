#ifndef GAME_CLIENT_UI_STYLES_ICON_H
#define GAME_CLIENT_UI_STYLES_ICON_H

#include "background.h"
#include "text.h"

/**
 * TODO: WIP
 */
class CUIIconStyleTemplate : public virtual CUIBackgroundStyle {
	vec4 m_IconColor;
	int m_Icon;

	CUIIconStyleTemplate() : CUIBackgroundStyle() {

	}
};

class CUIIconStyle : public virtual CUIStatefulStyle<CUIIconStyleTemplate>
{
	vec4 m_HotColor;
	vec4 m_ClickedColor;
	vec4 m_InactiveColor;

	void InitLinkStyle()
	{
		LinkSetHotColor(vec4(0.078f, 0.612f, 0.902f, 1.0f));
		LinkSetClickedColor(vec4(0.902f, 0.612f, 0.078f, 0.9f));
		LinkSetInactiveColor(vec4(0.53f, 0.53f, 0.53f, 0.9f));
	}

public:
	CUILinkStyle() : CUITextStyle()
	{
		InitLinkStyle();
	}

	CUILinkStyle(const vec4& HotColor, const vec4& ClickedColor) : CUITextStyle()
	{
		InitLinkStyle();
		LinkSetHotColor(HotColor);
		LinkSetClickedColor(ClickedColor);
	}

	CUILinkStyle(const vec4& HotColor, const vec4& ClickedColor, const vec4& InactiveColor) : CUITextStyle()
	{
		InitLinkStyle();
		LinkSetHotColor(HotColor);
		LinkSetClickedColor(ClickedColor);
		LinkSetInactiveColor(InactiveColor);
	}

	const vec4& LinkGetHotColor() const
	{
		return m_HotColor;
	}

	void LinkSetHotColor(const vec4& HotColor)
	{
		m_HotColor = HotColor;
	}

	const vec4& LinkGetClickedColor() const
	{
		return m_ClickedColor;
	}

	void LinkSetClickedColor(const vec4& ClickedColor)
	{
		m_ClickedColor = ClickedColor;
	}

	const vec4& LinkGetInactiveColor() const
	{
		return m_InactiveColor;
	}

	void LinkSetInactiveColor(const vec4& InactiveColor)
	{
		m_InactiveColor = InactiveColor;
	}
};

#endif
