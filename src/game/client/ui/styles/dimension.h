/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_STYLES_DIMENSION_H
#define GAME_CLIENT_UI_STYLES_DIMENSION_H
#include "style.h"
class CUIDimensionStyle : public virtual CUIStyle
{
private:
    float m_Width;
    float m_Height;
    float m_MaxWidth;
    float m_MaxHeight;
    float m_MinWidth;
    float m_MinHeight;
public:
    CUIDimensionStyle() : CUIStyle() {
        m_Width = 0.0f;
        m_Height = 0.0f;
        m_MaxWidth = 0.0f;
        m_MaxHeight = 0.0f;
        m_MinWidth = 0.0f;
        m_MinHeight = 0.0f;
    }
    virtual ~CUIDimensionStyle() {};

    void DimensionSetWidth(float Width) {
        m_Width = Width;
    }

    void DimensionSetHeight(float Height) {
        m_Height = Height;
    }

    void DimensionSetMaxWidth(float Width) {
        m_MaxWidth = Width;
    }

    void DimensionSetMaxHeight(float Height) {
        m_MaxHeight = Height;
    }

    void DimensionSetMinWidth(float Width) {
        m_MinWidth = Width;
    }

    void DimensionSetMinHeight(float Height) {
        m_MinHeight = Height;
    }

    float DimensionGetWidth()  const {
        return m_Width;
    }

    float DimensionGetHeight()  const {
        return m_Height;
    }

    float DimensionGetMaxWidth()  const {
        if (m_MaxWidth != 0) {
            return m_MaxWidth;
        } else {
            return DimensionGetWidth();
        }
    }

    float DimensionGetMaxHeight()  const {
        if (m_MaxHeight != 0) {
            return m_MaxHeight;
        } else {
            return DimensionGetHeight();
        }
    }

    float DimensionGetMinWidth()  const {
        if (m_MinWidth != 0) {
            return m_MinWidth;
        } else {
            return DimensionGetWidth();
        }
    }

    float DimensionGetMinHeight()  const {
        if (m_MinHeight != 0) {
            return m_MinHeight;
        } else {
            return DimensionGetHeight();
        }
    }
};
#endif
