/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_STYLES_BACKGROUND_H
#define GAME_CLIENT_UI_STYLES_BACKGROUND_H
#include "style.h"
#include <base/vmath.h>
class CUIBackgroundStyle : public virtual CUIStyle
{

	private:
		vec4 m_Color;

	public:
		CUIBackgroundStyle() : CUIStyle() {
			BackgroundSetColor(vec4(1, 1, 1, 0.33f));
		}

		CUIBackgroundStyle(vec4 Color) : CUIStyle() {
			BackgroundSetColor(Color);
		}

        virtual ~CUIBackgroundStyle() {};

		void BackgroundSetColor(vec4 Color) {
			m_Color = Color;
		}

		vec4 BackgroundGetColor() const {
			return m_Color;
		}
};
#endif
