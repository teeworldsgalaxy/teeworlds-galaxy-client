/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_STYLES_ALIGNMENT_H
#define GAME_CLIENT_UI_STYLES_ALIGNMENT_H
#include "style.h"
#include "../../ui.h"
class CUIAlignmentStyle : public virtual CUIStyle
{
    enum Alignment {
        UI_ALIGN_CENTER = 0,
        UI_ALIGN_TOP = 2,
        UI_ALIGN_LEFT = 4,
        UI_ALIGN_RIGHT = 8,
        UI_ALIGN_BOTTOM = 16,
    };
	private:
		int m_Value;
	public:
		CUIAlignmentStyle() : CUIStyle() {
            m_Value = 0;
		}

        virtual ~CUIAlignmentStyle() {};

        int AlignmentGetValue() const {
            return m_Value;
        }

        int AlignmentGetLegacyValue() const {

            int Value = 2;

            if (m_Value&UI_ALIGN_LEFT) {
                Value |= CUI::ALIGN_LEFT;
            } else if(m_Value&UI_ALIGN_RIGHT) {
                Value |= CUI::ALIGN_RIGHT;
            }

            if (m_Value&UI_ALIGN_TOP) {
                Value |= CUI::ALIGN_TOP;
            } else if (m_Value&UI_ALIGN_BOTTOM) {
                Value |= CUI::ALIGN_BOTTOM;
            }
            return Value;
        }

        void AlignmentSetLegacyValue(int Value) {
            m_Value = 0;

            if (Value == CUI::ALIGN_LEGACY_LEFT) {
                m_Value = UI_ALIGN_LEFT;
            }

            if (Value&CUI::ALIGN_RIGHT) {
                m_Value |= UI_ALIGN_RIGHT;
            } else if(Value&CUI::ALIGN_LEFT) {
                m_Value |= UI_ALIGN_LEFT;
            }

            if (Value&CUI::ALIGN_TOP) {
                m_Value |= UI_ALIGN_TOP;
            } else if (Value&CUI::ALIGN_BOTTOM) {
                m_Value |= UI_ALIGN_BOTTOM;
            }
        }

        /** Reset the alignment to center **/
        void AlignmentAlignCenter() {
            m_Value = UI_ALIGN_CENTER;
        }

        void AlignmentAlignTop() {
            m_Value |= UI_ALIGN_TOP;
        }

        void AlignmentAlignRight() {
            m_Value |= UI_ALIGN_RIGHT;
        }

        void AlignmentAlignBottom() {
            m_Value |= UI_ALIGN_BOTTOM;
        }

        void AlignmentAlignLeft() {
            m_Value |= UI_ALIGN_LEFT;
        }
        void AlignmentSetValue(int Value) {
            m_Value = Value;
        }
};
#endif
