/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_STYLES_TOOLTIP_H
#define GAME_CLIENT_UI_STYLES_TOOLTIP_H
#include "box.h"
class CUITooltipStyle : public virtual CUIBoxStyle
{
	public:
	CUITooltipStyle() :  CUIBoxStyle() {
		BackgroundSetColor(vec4(0.5, 0.5, 0.5, 0.3));
		TextSetFontSize(10.0f);
		DimensionSetMaxWidth(250);
	}

	virtual ~CUITooltipStyle(){};
};
#endif
