/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_STYLES_TEXT_H
#define GAME_CLIENT_UI_STYLES_TEXT_H
#include "style.h"
#include "margin.h"
#include "alignment.h"
#include "dimension.h"
#include <base/vmath.h>


class CUITextStyle : public virtual CUIMarginStyle,
					 public virtual CUIAlignmentStyle,
					 public virtual CUIDimensionStyle
{
private:
	float m_FontSize;
	float m_OutlineSize;
	vec4 m_FontColor;
	vec4 m_OutlineColor;

	void InitTextStyle()
	{
		TextSetFontSize(12.0f);
		TextSetFontColor(vec4(1.0f, 1.0f, 1.0f, 1.0f));
		TextSetOutlineSize(0.05f);
		TextSetOutlineColor(vec4(0.0f, 0.0f, 0.0f, 0.3f));
	};

public:

	CUITextStyle() : CUIStyle()
	{
		InitTextStyle();
	};

	CUITextStyle(float Size) : CUIStyle()
	{
		InitTextStyle();
		TextSetFontSize(Size);
	};

	CUITextStyle(float Size, vec4 Color) : CUIStyle()
	{
		InitTextStyle();
		TextSetFontSize(Size);
		TextSetFontColor(Color);
	};

	void TextSetFontSize(float Size)
	{
		if(Size > 0.0f)
			m_FontSize = Size;
	}

	float TextGetFontSize() const
	{
		return m_FontSize;
	}

	void TextSetOutlineSize(float Size)
	{
		if(Size >= 0.0f)
			m_OutlineSize = Size;
	}

	float TextGetOutlineSize() const
	{
		return m_OutlineSize;
	}

	void TextSetFontColor(vec4 Color)
	{
		m_FontColor = Color;
	}

	vec4 TextGetFontColor() const
	{
		return m_FontColor;
	}

	void TextSetOutlineColor(vec4 Color)
	{
		m_OutlineColor = Color;
	}

	vec4 TextGetOutlineColor() const
	{
		return m_OutlineColor;
	}
};
#endif
