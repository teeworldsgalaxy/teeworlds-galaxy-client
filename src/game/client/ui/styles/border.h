/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_STYLES_BORDER_H
#define GAME_CLIENT_UI_STYLES_BORDER_H
#include "style.h"
#include <base/vmath.h>
class CUIBorderStyle : public virtual CUIStyle
{
	private:
		/*float m_SizeTop;
		float m_SizeRight;
		float m_SizeBottom;
		float m_SizeLeft;*/
		float m_Size;
		vec4 m_Color;
		
		//TODO: we could define an enum here
		void Init() {
			//m_SizeTop = m_SizeRight = m_SizeBottom = m_SizeLeft = 0;
			m_Size = 0;
		}
		
	public:
		
		CUIBorderStyle() : CUIStyle() {
			Init();
		}
		
		CUIBorderStyle(float Size, vec4 Color) : CUIStyle() {
			BorderSetSize(Size);
			BorderSetColor(Color);
		}

		virtual ~CUIBorderStyle(){};
		
		void BorderSetSize(float Size) {
			if (Size >= 0.0f) {
				m_Size = Size;
			}
		}
		
		float BorderGetSize() const {
			return m_Size;
		}
		
		void BorderSetColor(vec4 Color) {
			m_Color = Color;
		}
		
		vec4 BorderGetColor() const {
			return m_Color;
		}
};
#endif
