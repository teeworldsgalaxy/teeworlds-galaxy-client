/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_STYLES_BOX_H
#define GAME_CLIENT_UI_STYLES_BOX_H
#include "border.h"
#include "corner.h"
#include "margin.h"
#include "text.h"
#include "dimension.h"
class CUIBoxStyle : public virtual CUIBorderStyle,
                    public virtual CUIMarginStyle,
                    public virtual CUITextStyle, /* TODO: this does not fit here */
                    public virtual CUICornerStyle,
                    public virtual CUIBackgroundStyle,
                    public virtual CUIDimensionStyle
{
	public:
        CUIBoxStyle() :  CUIBorderStyle(), CUIMarginStyle(), CUITextStyle(), CUICornerStyle(), CUIBackgroundStyle(), CUIDimensionStyle() {

		}

        virtual ~CUIBoxStyle(){};
};
#endif
