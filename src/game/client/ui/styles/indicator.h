/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_STYLES_INDICATOR_H
#define GAME_CLIENT_UI_STYLES_INDICATOR_H
#include "background.h"
#include "alignment.h"
class CUIIndicatorStyle : public virtual CUIBackgroundStyle, public virtual CUIAlignmentStyle
{
public:

    float m_Radius;

    CUIIndicatorStyle() : CUIBackgroundStyle(), CUIAlignmentStyle() {
        m_Radius = 1.0f;
    }

    float IndicatorSetRadius(float radius) {
        m_Radius = radius;
    }

    const float IndicatorGetRadius() const {
        return m_Radius;
    }

    virtual ~CUIIndicatorStyle(){};
};
#endif
