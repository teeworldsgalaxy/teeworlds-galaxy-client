/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_STYLES_SCROLLBAR_H
#define GAME_CLIENT_UI_STYLES_SCROLLBAR_H

#include "box.h"

class CUIScrollbarStyle : public virtual CUIStyle
{

	bool m_AutoHide;
public:
	CUIScrollbarStyle() : CUIStyle() {
		m_AutoHide = false;
	}

	virtual ~CUIScrollbarStyle(){};

	bool ScrollbarGetAutoHide() const {
		return m_AutoHide;
	}

	void ScrollbarSetAutoHide(bool AutoHide) {
		m_AutoHide = AutoHide;
	}
};
#endif
