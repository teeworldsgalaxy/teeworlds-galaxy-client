/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_STYLES_BUTTON_H
#define GAME_CLIENT_UI_STYLES_BUTTON_H
#include "box.h"
#include "icon.h"
class CUIButtonStyle : public virtual CUIBoxStyle, public virtual CUIIconStyle
{
	public:
		CUIButtonStyle() :  CUIBoxStyle(), CUIIconStyle() {
            TextSetFontSize(15.0f);
		}

        virtual ~CUIButtonStyle(){};
};
#endif
