/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_STYLES_STATEFUL_H
#define GAME_CLIENT_UI_STYLES_STATEFUL_H

#include "style.h"

template <typename S>
class CUIStatefulStyleHolder : public virtual CUIStyle
{
protected:
	S m_Default;
	S m_Hover;
	S m_Active;
	S m_Inactive;
	S m_Disabled;
	S m_Clicked;
	S m_Click;

public:
	CUIStatefulStyleHolder() : CUIStyle() {}

	CUIStatefulStyleHolder(S Default) : m_Default(Default), CUIStyle() {
		StatefulSetAll(Default);
	}

	virtual ~CUIStatefulStyleHolder(){};

	/*
	 * TODO: Add more states
	 */

	void StatefulSetDefault(const S &Default) {
		m_Default = Default;
	}

	const S& StatefulGetDefault() const {
		return m_Default;
	}

    S& StatefulGetDefault() {
        return m_Default;
    }

	void StatefulSetHover(const S &Hover) {
		m_Hover = Hover;
	}

	const S& StatefulGetHover() const {
		return m_Hover;
	}

    S& StatefulGetHover() {
        return m_Hover;
    }

	void StatefulSetActive(const S &Active) {
		m_Active = Active;
	}

	const S& StatefulGetActive() const {
		return m_Active;
	}

    S& StatefulGetActive() {
        return m_Active;
    }

    void StatefulSetDisabled(const S &Disabled) {
		m_Disabled = Disabled;
	}

	const S& StatefulGetDisabled() const {
		return m_Disabled;
	}

    S& StatefulGetDisabled() {
        return m_Disabled;
    }

    void StatefulSetClicked(const S &Clicked) {
        m_Clicked = Clicked;
    }

    const S& StatefulGetClicked() const {
        return m_Clicked;
    }

    S& StatefulGetClicked() {
        return m_Clicked;
    }

	void StatefulSetClick(const S &Click) {
		m_Click = Click;
	}

	const S& StatefulGetClick() const {
		return m_Click;
	}

	S& StatefulGetClick() {
		return m_Click;
	}

	void StatefulSetAll(S Style) {
		m_Default = m_Hover = m_Active = m_Inactive = m_Disabled = m_Clicked = m_Click = Style;
	}
};


template <typename S>
class CUIStatefulStyle : public CUIStatefulStyleHolder<S>
{
protected:
	CUIStatefulStyleHolder<S> m_Error;

public:
	CUIStatefulStyle() : CUIStatefulStyleHolder<S>() {}
	CUIStatefulStyle(S Default) : CUIStatefulStyleHolder<S>(Default) {}

	virtual ~CUIStatefulStyle() {}


	void StatefulSetError(const CUIStatefulStyleHolder<S> &Error) {
		m_Error = Error;
	}

	const CUIStatefulStyleHolder<S>& StatefulGetError() const {
		return m_Error;
	}

	CUIStatefulStyleHolder<S>& StatefulGetError() {
		return m_Error;
	}
};

#endif
