/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_STYLES_EDIT_BOX_H
#define GAME_CLIENT_UI_STYLES_EDIT_BOX_H
#include "box.h"
#include "corner.h"
#include "margin.h"
#include <base/vmath.h>
class CUIEditBoxStyle : public virtual CUIBoxStyle
{
	private:
		bool m_Hidden;
		vec4 m_PlaceholderColor;
		bool m_ShowClear;

	void Init() {
		m_Hidden = false;
		m_ShowClear = false;
	}

	public:


		CUIEditBoxStyle() :  CUIBoxStyle() {
			Init();

			m_PlaceholderColor = vec4(1, 1, 1, 0.5f);
		}

		CUIEditBoxStyle(bool Hidden, const vec4& PlaceholderColor) : CUIBoxStyle() {
			Init();
			m_Hidden = Hidden;
			m_PlaceholderColor = PlaceholderColor;
		}

        virtual ~CUIEditBoxStyle(){};

		void EditBoxSetHidden(bool Hidden) {
			m_Hidden = Hidden;
		}

		bool EditBoxIsHidden() const {
			return m_Hidden;
		}

		void EditBoxSetPlaceholderColor(const vec4& PlaceholderColor) {
			m_PlaceholderColor = PlaceholderColor;
		}

		const vec4& EditBoxGetPlaceholderColor() const {
			return m_PlaceholderColor;
		}

	void EditBoxSetShowClear(bool Show) {
		m_ShowClear = Show;
	}

	bool EditBoxGetShowClear() const {
		return m_ShowClear;
	}
};
#endif
