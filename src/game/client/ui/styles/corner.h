/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_STYLES_CORNER_H
#define GAME_CLIENT_UI_STYLES_CORNER_H
#include "style.h"
class CUICornerStyle : public virtual CUIStyle
{
	private:
		float m_Radius[4];
		//TODO: we could define an enum here
		void Init() {
			m_Radius[0] = 0; //TL
			m_Radius[1] = 0; //TR
			m_Radius[2] = 0; //BR
			m_Radius[3] = 0; //BL
		}

	public:
		CUICornerStyle() : CUIStyle() {
			Init();
		}

		CUICornerStyle(float Radius) : CUIStyle() {
			m_Radius[0] = m_Radius[1] = m_Radius[2] = m_Radius[3] = Radius;
		}

		CUICornerStyle(float RadiusTop, float RadiusBot) : CUIStyle() {
			m_Radius[0] = RadiusTop;
			m_Radius[1] = RadiusTop;
			m_Radius[2] = RadiusBot;
			m_Radius[3] = RadiusBot;
		}

		CUICornerStyle(float TL, float TR, float BR, float BL) : CUIStyle() {
			m_Radius[0] = TL;
			m_Radius[1] = TR;
			m_Radius[2] = BR;
			m_Radius[3] = BL;
		}

	 	virtual ~CUICornerStyle(){};

		void CornerSetAll(float Value) {
			if (Value >= 0.0f) {
				m_Radius[0] = m_Radius[1] = m_Radius[2] = m_Radius[3] = Value;
			}
		}

		void CornerSetRadiusTL(float Value) {
			if (Value >= 0.0f) {
				m_Radius[0] = Value;
			}
		}
		float CornerGetRadiusTL() const {
			return m_Radius[0];
		}

		void CornerSetRadiusTR(float Value) {
			if (Value >= 0.0f) {
				m_Radius[1] = Value;
			}
		}
		float CornerGetRadiusTR() const {
			return m_Radius[1];
		}

		void CornerSetRadiusBR(float Value) {
			if (Value >= 0.0f) {
				m_Radius[2] = Value;
			}
		}
		float CornerGetRadiusBR() const {
			return m_Radius[2];
		}

		void CornerSetRadiusBL(float Value) {
			if (Value >= 0.0f) {
				m_Radius[3] = Value;
			}
		}
		float CornerGetRadiusBL() const {
			return m_Radius[3];
		}
};
#endif
