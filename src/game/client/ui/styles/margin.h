/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_STYLES_MARGIN_H
#define GAME_CLIENT_UI_STYLES_MARGIN_H
#include "style.h"
class CUIMarginStyle : public virtual CUIStyle
{
	private:
		float m_Top;
		float m_Right;
		float m_Bottom;
		float m_Left;
		
		void Init() {
			m_Top = 0;
			m_Right = 0;
			m_Bottom = 0;
			m_Left = 0;
		}
	
	public:
		CUIMarginStyle() : CUIStyle() {
			Init();
		}
		
		CUIMarginStyle(float All) : CUIStyle() {
			m_Top = m_Right = m_Bottom = m_Left = All;
		}
		
		CUIMarginStyle(float Vertical, float Horizontal) : CUIStyle(){
			m_Top = Vertical;
			m_Right = Horizontal;
			m_Bottom = Vertical;
			m_Left = Horizontal;
		}
		
		CUIMarginStyle(float Top, float Horizontal, float Bottom) : CUIStyle() {
			m_Top = Top;
			m_Right = Horizontal;
			m_Bottom = Bottom;
			m_Left = Horizontal;
		}
		
		CUIMarginStyle(float Top, float Right, float Bottom, float Left) : CUIStyle() {
			m_Top = Top;
			m_Right = Right;
			m_Bottom = Bottom;
			m_Left = Left;
		}

		virtual ~CUIMarginStyle(){};
		
		void MarginSetTop(float Value) {
			m_Top = Value;
		}	
		float MarginGetTop() const {
			return m_Top;
		}	
		
		void MarginSetRight(float Value) {
			m_Right = Value;
		}	
		float MarginGetRight() const {
			return m_Right;
		}	
		
		void MarginSetBottom(float Value) {
			m_Bottom = Value;
		}	
		float MarginGetBottom() const {
			return m_Bottom;
		}	
		
		void MarginSetLeft(float Value) {
			m_Left = Value;
		}	
		float MarginGetLeft() const {
			return m_Left;
		}

		void MarginSetAll(float All) {
			m_Top = m_Right = m_Bottom = m_Left = All;
		}

		float MarginGetMax() const {
			return max(max(max(m_Top, m_Right), m_Bottom), m_Left);
		}
};
#endif
