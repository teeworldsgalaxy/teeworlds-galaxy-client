/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_STYLES_ICON_H
#define GAME_CLIENT_UI_STYLES_ICON_H

#include "margin.h"
#include "background.h"
#include "alignment.h"
#include "../../ui.h"
class CUIIconStyle : public virtual CUIBackgroundStyle, public virtual CUIMarginStyle, public virtual CUIAlignmentStyle
{
	private:
		int m_Icon;
        float m_Size;
        vec4 m_Color;
	public:
        CUIIconStyle() : CUIBackgroundStyle(), CUIMarginStyle() {
            m_Icon = 0;
            m_Size = 0;
            MarginSetAll(0);
            AlignmentAlignCenter();
		}

        virtual ~CUIIconStyle() {};

        int IconGetIcon() const {
            return m_Icon;
        }
        void IconSetIcon(int Icon) {
            m_Icon = Icon;
        }

        float IconGetSize() const {
            return m_Size;
        }

        void IconSetSize(float Size) {
            m_Size = Size;
        }

        bool IconHasIcon() const {
            return m_Icon != 0;
        }

        vec4 IconGetColor() const {
            return m_Color;
        }

        void IconSetColor(vec4 Color) {
            m_Color = Color;
        }
};
#endif
