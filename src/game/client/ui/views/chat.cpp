/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */

#include "chat.h"
#include "../constants.h"
#include <engine/keys.h>
#include "../elements/box.h"
#include "../elements/edit_box.h"
#include "../elements/button.h"


#include <engine/engine.h>
#include <engine/graphics.h>
#include <engine/textrender.h>
#include <engine/keys.h>
#include <engine/shared/config.h>

#include <game/generated/protocol.h>
#include <game/generated/client_data.h>

#include <game/client/gameclient.h>

#include <game/client/components/scoreboard.h>
#include <game/client/components/sounds.h>
#include <game/client/ui/constants.h>
#include <game/localization.h>

#include "../elements/scrollbar.h"

void CUIChatView::OnRender(const CUIRect &View, CRenderTools *pRT, float Delta) const {



}

void CUIChatView::OnUpdate(const CUIRect &View, CRenderTools *pRT, float Delta) {


    CheckNewMessages();

    // Render Whole Chatbox
    CUIBox BorderBox;
    BorderBox.GetStyle().BackgroundSetColor(STYLE_COLOR_BG_DARK);
    BorderBox.GetStyle().BorderSetSize(1.0f);
    BorderBox.GetStyle().BorderSetColor(STYLE_COLOR_BG_LIGHT);
    BorderBox.OnRender(View, pRT, Delta);

    CUIRect Content = View;
    View.VSplitRight(1.0f, &Content, 0);

    CUIRect Chat, ActionBar;

    Content.HSplitBottom(25.0f, &Chat, &ActionBar);

    // Render Chat

    Chat.Margin(2.0f, &Chat);
    RenderChat(Chat, pRT, Delta);

    /*CUIRect ChatRow = Chat;
    for (int i = 0; i < m_apMessages.size(); ++i) {

        const CChatMessage *pMsg = m_apMessages[i];
        CUIRect RowAuthor, RowText;
        ChatRow.VSplitLeft(30.0f, &RowAuthor, &RowText);
        RowText.Margin(2.0f, &RowText);

        pRT->UI()->DoLabelScaled(&RowAuthor, pMsg->m_pAuthor, 14.0f, -1);
        pRT->UI()->DoLabelScaled(&RowText, pMsg->m_pText, 12.0f, -1);
        // new line
        ChatRow.HSplitTop(15.0f, nullptr, &ChatRow);
    }*/


    CUIRect InputBox, SendBtn;
    ActionBar.VSplitRight(60.0f, &InputBox, &SendBtn);
    SendBtn.x--;
    SendBtn.w++;


    static char aBuf[256];

    static float Offset;


    static CUIEditBox Input;
    Input.GetStyle().EditBoxSetHidden(false);
    Input.GetStyle().BackgroundSetColor(vec4(0, 0, 0, 0));
    Input.GetStyle().BackgroundSetColor(vec4(0, 0, 0, 0));
    Input.GetStyle().TextSetFontColor(vec4(1, 1, 1, 1.0f));
    Input.GetStyle().TextSetFontSize(12.0f);
    Input.GetStyle().EditBoxSetPlaceholderColor(STYLE_COLOR_BG_LIGHT);
    Input.GetStyle().MarginSetLeft(2.0f);
    Input.GetStyle().BorderSetColor(STYLE_COLOR_BG_LIGHT);
    Input.GetStyle().BorderSetSize(1.0f);
    Input.GetStyle().EditBoxSetShowClear(true);

    Input.SetOffset(&Offset);
    Input.SetPlaceholderText(Localize("Message"));
    Input.SetText(aBuf, sizeof(aBuf)-1);
    Input.SetHotkeys()
            .Add(KEY_RETURN)
            .Add(KEY_KP_ENTER);



    Input.Create(InputBox, pRT, Delta);


    static CUIButton SubmitButton;
    SubmitButton.SetText(Localize("Submit"));
    SubmitButton.Create(SendBtn, pRT, Delta);


    if ((SubmitButton.Pressed() || Input.Pressed(m_pInput)) && Input.GetText() != nullptr && str_length(Input.GetText()) > 0) {
        SendMessage(Input.GetText());
        aBuf[0] = '\0'; // Reset Text!
        pRT->UI()->SetActiveItem(Input.GetID());
        pRT->UI()->SetActiveItem(0);
    }

}



void CUIChatView::RenderChat(CUIRect View, CRenderTools *pRT, float Delta) {

    pRT->UI()->ClipEnable(&View);

    CUIRect Scroll, Content;
    View.VSplitRight(11.0f, &View, &Scroll);

    Scroll.HMargin(3.0f, &Scroll);
    View.Margin( 0.0f, &Content);

    CUIScrollbar Bar;
    Bar.ScrollbarSetValue(1.0f-m_State.m_ScrollValue);

    float Percent = m_State.m_OverallHeight > 0.0f ? Content.h / m_State.m_OverallHeight : 1.0f;
    Bar.ScrollbarSetPercent(max(Percent, 0.3f));
    Bar.GetStyle().ScrollbarSetAutoHide(true);
    Bar.Create(Scroll, pRT);
    m_State.m_ScrollValue = 1.0f-Bar.ScrollbarGetValue();


    float x = Content.x;


    int s_InputLineCount = 1;

    float NumberOfLines = 25.0f;

    float LineWidth = Content.w;// m_pClient->m_pScoreboard->Active() ? 90.0f : m_Show || m_Mode != MODE_NONE ? 200.0f : 120.0f;
    float HeightLimit = Content.h;
    float StartOffset = 0.0f;(s_InputLineCount-1)*-8.0f; //: 8.0f;
    float Opacity = 0.0f; //m_Mode != MODE_NONE || m_pClient->m_pScoreboard->Active() || m_Show ? 50.0f : 0.0f;

    float s_DrawHeight = Content.y;
    float s_DrawWidth = LineWidth;
    float s_DrawOffset = StartOffset;
    float s_DrawOpacity = 1.0f;

    // we first need to calculate the heights!
    // these will be saved as well

    float Begin = x;
    float FontSize = Content.h/NumberOfLines;
    CTextCursor Cursor;
    int OffsetType = 0;


    for(int i = m_State.m_apMessages.size()-1; i >= 0; i--) {
        auto &Line = m_State.m_apMessages[i]->m_Line;

        // get the y offset (calculate it if we haven't done that yet)
        if (Line.m_YOffset[OffsetType] < 0.0f || abs(s_DrawWidth - LineWidth) > 0.1f) {
            m_pTextRender->SetCursor(&Cursor, Begin, 0.0f, FontSize, 0);
            Cursor.m_LineWidth = s_DrawWidth;
            m_pTextRender->TextEx(&Cursor, Line.m_aName, -1);
            m_pTextRender->TextEx(&Cursor, Line.m_aText, -1);
            Line.m_YOffset[OffsetType] = Cursor.m_Y + Cursor.m_FontSize;
            m_State.m_OverallHeight += Line.m_YOffset[OffsetType];
        } else {
            break; // all others are already calculated, no need to check further...
        }
    }


    // we see Content.h of m_State.m_OverallHeight values

    Content.y += m_State.m_ScrollValue*(m_State.m_OverallHeight-Content.h);
    float y = Content.y+Content.h;

    bool EndReached = false;

    /*smooth_set(&s_DrawHeight, HeightLimit, 20.0f, m_pClient->RenderFrameTime());
    smooth_set(&s_DrawWidth, LineWidth, 20.0f, m_pClient->RenderFrameTime());
    smooth_set(&s_DrawOffset, StartOffset, 20.0f, m_pClient->RenderFrameTime());

    smooth_set(&s_DrawOpacity, Opacity, 15.0f, m_pClient->RenderFrameTime());

    y += s_DrawOffset;

    //draw the box
    if (s_DrawOpacity > 0.1f) {
        m_pGraphics->BlendNormal();
        m_pGraphics->TextureSet(-1);
        m_pGraphics->QuadsBegin();
        m_pGraphics->SetColor(STYLE_COLOR_BG_DARK.r,STYLE_COLOR_BG_DARK.g, STYLE_COLOR_BG_DARK.b, s_DrawOpacity*0.01f);
        pRT->DrawRoundRectExt(2.0f, 300.0f, s_DrawWidth+5.0f, s_DrawHeight-300.0f, 0.0f, 0);
        m_pGraphics->QuadsEnd();
    }

    // render chat input
    CTextCursor Cursor;
    m_pTextRender->SetCursor(&Cursor, x, y, 8.0f, TEXTFLAG_RENDER);
    Cursor.m_LineWidth = s_DrawWidth;
    Cursor.m_MaxLines = 3;

    if(m_Mode == MODE_ALL)
        m_pTextRender->TextEx(&Cursor, Localize("All"), -1);
    else if(m_Mode == MODE_TEAM)
        m_pTextRender->TextEx(&Cursor, Localize("Team"), -1);
    else
        m_pTextRender->TextEx(&Cursor, Localize("Chat"), -1);

    m_pTextRender->TextEx(&Cursor, ": ", -1);

    // check if the visible text has to be moved
    if(m_InputUpdate)
    {
        if(m_ChatStringOffset > 0 && m_Input.GetLength() < m_OldChatStringLength)
            m_ChatStringOffset = max(0, m_ChatStringOffset-(m_OldChatStringLength-m_Input.GetLength()));

        if(m_ChatStringOffset > m_Input.GetCursorOffset())
            m_ChatStringOffset -= m_ChatStringOffset-m_Input.GetCursorOffset();
        else
        {
            CTextCursor Temp = Cursor;
            Temp.m_Flags = 0;
            m_pTextRender->TextEx(&Temp, m_Input.GetString()+m_ChatStringOffset, m_Input.GetCursorOffset()-m_ChatStringOffset);
            m_pTextRender->TextEx(&Temp, "|", -1);
            while(Temp.m_LineCount > 3)
            {
                ++m_ChatStringOffset;
                Temp = Cursor;
                Temp.m_Flags = 0;
                m_pTextRender->TextEx(&Temp, m_Input.GetString()+m_ChatStringOffset, m_Input.GetCursorOffset()-m_ChatStringOffset);
                m_pTextRender->TextEx(&Temp, "|", -1);
            }
            s_InputLineCount = Temp.m_LineCount;
        }
        m_InputUpdate = false;
    }

    m_pTextRender->TextEx(&Cursor, m_Input.GetString()+m_ChatStringOffset, m_Input.GetCursorOffset()-m_ChatStringOffset);
    static float MarkerOffset = m_pTextRender->TextWidth(0, 8.0f, "|", -1)/3;
    CTextCursor Marker = Cursor;
    Marker.m_X -= MarkerOffset;
    m_pTextRender->TextEx(&Marker, "|", -1);
    m_pTextRender->TextEx(&Cursor, m_Input.GetString()+m_Input.GetCursorOffset(), -1);
    */

    y -= 2.0f;
    int64 Now = time_get();

    bool ShowAll = true; //m_pClient->m_pScoreboard->Active() || m_Show || m_Mode != MODE_NONE;

    for(int i = m_State.m_apMessages.size()-1; i >= 0; i--)
    {
        int Type = m_State.m_apMessages[i]->m_Type;
        auto& Line = m_State.m_apMessages[i]->m_Line;

        if(Now > Line.m_Time+16*time_freq() && !ShowAll)
            break;

        y -= Line.m_YOffset[OffsetType];

        // cut off if msgs waste too much space
        //if(y < s_DrawHeight)
        //   break;

        float Blend = Now > Line.m_Time+14*time_freq() &&  !ShowAll ? 1.0f-(Now-Line.m_Time-14*time_freq())/(2.0f*time_freq()) : 1.0f;

        // reset the cursor
        m_pTextRender->SetCursor(&Cursor, Begin, y, FontSize, TEXTFLAG_RENDER);
        Cursor.m_LineWidth = s_DrawWidth;


        if (Type == CChatMessage::TYPE_SELF) {
            m_pTextRender->TextColor(0.75f, 0.75f, 0.75f, Blend); // spectator
        }
        // render name
        else if(Type == CChatMessage::TYPE_NOTIFICATION  | Type == CChatMessage::TYPE_SERVER || Type == CChatMessage::TYPE_HIGHLIGHT)
            m_pTextRender->TextColor(1.0f, 1.0f, 0.5f, Blend); // system
        else
            m_pTextRender->TextColor(0.8f, 0.8f, 0.8f, Blend);

        m_pTextRender->TextEx(&Cursor, Line.m_aName, -1);

        // render line
        if (Type == CChatMessage::TYPE_SELF) {
            m_pTextRender->TextColor(0.75f, 0.75f, 0.75f, Blend); // spectator
        }// render name
        else if(Type == CChatMessage::TYPE_NOTIFICATION  | Type == CChatMessage::TYPE_SERVER || Type == CChatMessage::TYPE_HIGHLIGHT)
            m_pTextRender->TextColor(1.0f, 1.0f, 0.5f, Blend); // system
        else
            m_pTextRender->TextColor(1.0f, 1.0f, 1.0f, Blend);

        m_pTextRender->TextEx(&Cursor, Line.m_aText, -1);

        EndReached = y < View.h;
    }

    float Heights = (Content.y+Content.h)-y;

    m_pTextRender->TextColor(1.0f, 1.0f, 1.0f, 1.0f);

    float LastScrollValue = m_State.m_ScrollValue;
    m_State.m_ScrollValue = pRT->UI()->DoScrollLogic(&m_State.m_ScrollValue, &View, m_State.m_ScrollValue, Heights * 0.01f, false);

    if(m_State.m_ScrollValue < 0.0f)
    {
        m_State.m_ScrollValue = 0.0f;
    }
    else if(EndReached && m_State.m_ScrollValue > LastScrollValue)
    {
        m_State.m_ScrollValue = LastScrollValue;
    }

    pRT->UI()->ClipDisable();
}

CChatMessage* CUIChatView::OnNewMessage(int Type, const char *pLine, const char *pAuthor) {

    auto Msg = new CChatMessage(Type, pLine, pAuthor);

    const char *pOwnName = m_pClient->Galaxy()->Account()->Name();
    const int OwnNameLength = str_length(pOwnName);

    auto& Line = Msg->m_Line;

    // trim right and set maximum length to 128 utf8-characters
    int Length = 0;
    const char *pStr = pLine;
    const char *pEnd = 0;
    while(*pStr)
    {
        const char *pStrOld = pStr;
        int Code = str_utf8_decode(&pStr);

        // check if unicode is not empty
        if(Code > 0x20 && Code != 0xA0 && Code != 0x034F && (Code < 0x2000 || Code > 0x200F) && (Code < 0x2028 || Code > 0x202F) &&
           (Code < 0x205F || Code > 0x2064) && (Code < 0x206A || Code > 0x206F) && (Code < 0xFE00 || Code > 0xFE0F) &&
           Code != 0xFEFF && (Code < 0xFFF9 || Code > 0xFFFC))
        {
            pEnd = 0;
        }
        else if(pEnd == 0)
            pEnd = pStrOld;

        if(++Length >= 127)
        {
            *(const_cast<char *>(pStr)) = 0;
            break;
        }
    }
    if(pEnd != 0)
        *(const_cast<char *>(pEnd)) = 0;

    bool Highlighted = false;
    char *p = const_cast<char*>(pLine);
    while(*p)
    {
        Highlighted = false;
        pLine = p;
        // find line seperator and strip multiline
        while(*p)
        {
            if(*p++ == '\n')
            {
                *(p-1) = 0;
                break;
            }
        }

        Line.m_Time = time_get();
        Line.m_YOffset[0] = -1.0f;
        Line.m_YOffset[1] = -1.0f;
        Line.m_NameColor = -2;

        // check for highlighted name
        const char *pHL = str_find_nocase(pLine, pOwnName);
        if(pHL)
        {
            if((pLine == pHL || pHL[-1] == ' ') && (pHL[OwnNameLength] == 0 || pHL[OwnNameLength] == ' ' || (pHL[OwnNameLength] == ':' && pHL[OwnNameLength+1] == ' ')))
                Highlighted = true;
        }
        Line.m_Highlighted = Highlighted;

        if(Type == CChatMessage::TYPE_SERVER || Type == CChatMessage::TYPE_HIGHLIGHT  || Type == CChatMessage::TYPE_NOTIFICATION) // server message
        {
            str_copy(Line.m_aName, "*** ", sizeof(Line.m_aName));
            str_format(Line.m_aText, sizeof(Line.m_aText), "%s", pLine);
        } else {
            // So now we need to check if the Author is the player itself :)
            if (str_comp(pOwnName, pAuthor) == 0) {
                str_format(Line.m_aName, sizeof(Line.m_aName), "You");
                Msg->m_Type = CChatMessage::TYPE_SELF;
            } else {
                str_copy(Line.m_aName, pAuthor, sizeof(Line.m_aName));
            };

            str_format(Line.m_aText, sizeof(Line.m_aText), ": %s", pLine);
        }

        char aBuf[1024];
        str_format(aBuf, sizeof(aBuf), "%s%s", Line.m_aName, Line.m_aText);
    }

    // play sound
    int64 Now = time_get();
    if(Type == -1)
    {
        if(Now-m_State.m_aLastSoundPlayed[CChatMessage::TYPE_SERVER] >= time_freq()*3/10)
        {
            m_pGameClient->m_pSounds->Play(CSounds::CHN_GUI, SOUND_CHAT_SERVER, 0);
            m_State.m_aLastSoundPlayed[CChatMessage::TYPE_SERVER] = Now;
        }
    }
    else if(Highlighted)
    {
        if(Now-m_State.m_aLastSoundPlayed[CChatMessage::TYPE_HIGHLIGHT] >= time_freq()*3/10)
        {
            m_pGameClient->m_pSounds->Play(CSounds::CHN_GUI, SOUND_CHAT_HIGHLIGHT, 0);
            m_State.m_aLastSoundPlayed[CChatMessage::TYPE_HIGHLIGHT] = Now;
        }
    }
    else
    {
        if(Now-m_State.m_aLastSoundPlayed[CChatMessage::TYPE_CLIENT] >= time_freq()*3/10)
        {
            m_pGameClient->m_pSounds->Play(CSounds::CHN_GUI, SOUND_CHAT_CLIENT, 0);
            m_State.m_aLastSoundPlayed[CChatMessage::TYPE_CLIENT] = Now;
        }
    }

    return Msg;
}