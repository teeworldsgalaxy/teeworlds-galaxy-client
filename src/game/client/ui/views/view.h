/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_VIEWS_VIEW_H
#define GAME_CLIENT_UI_VIEWS_VIEW_H

#include "../styles/style.h"
#include "../elements/element.h"
#include "../../render.h"
#include "../../components/menus.h"


/*
 * For now the view is basically an UI Element, but views are logically distinct from elements in the way that they are being used
 * For example a view may handle data manipulation or api requests, so they may be seen as active parts of the ui, the ui elements
 * are mostly passively used in other components (like e.g. a button).
 * We defined a ViewState and a ViewStyle here, the main idea is that rendering only depends on the state and style,
 */
class CUIView: public virtual CUIElement
{
};
#endif
