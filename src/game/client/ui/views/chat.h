/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_VIEWS_CHAT_H
#define GAME_CLIENT_UI_VIEWS_CHAT_H
#include "../views/view.h"
#include "../constants.h"
#include <engine/api/meta_include.h>
#include <base/system.h>

#include <engine/textrender.h>
#include <engine/client/galaxy_client.h>

struct CChatMessage {
    const char *m_pAuthor = nullptr;
    const char *m_pText = nullptr;

    CChatMessage(int Type, const char *pText, const char *pAuthor) {
        m_Type = Type;
        m_pAuthor = pAuthor;
        m_pText = pText;
    }

    ~CChatMessage() {
        mem_free((void*)m_pAuthor);
        mem_free((void*)m_pText);
    }

    int m_Type;

    enum
    {
        TYPE_SELF = 0,
        TYPE_CLIENT,
        TYPE_FRIEND,
        TYPE_SERVER,
        TYPE_HIGHLIGHT,
        TYPE_NOTIFICATION,
        NUM_TYPES
    };

    // This line struct is used for cached rendering
    struct CLine
    {
        int64 m_Time;
        float m_YOffset[2];
        int m_ClientID;
        int m_Team;
        int m_NameColor;
        char m_aName[64];
        char m_aText[512];
        bool m_Highlighted;
    } m_Line;



};




struct CChatState {

    struct CChatInputState {
        bool m_InputUpdate;
        int m_ChatStringOffset;
        int m_OldChatStringLength;
        int m_CompletionChosen;
        char m_aCompletionBuffer[256];
        int m_PlaceholderOffset;
        int m_PlaceholderLength;
    } m_Input;

    array<CChatMessage*> m_apMessages;

    //CHistoryEntry *m_pHistoryEntry;
    //TStaticRingBuffer<CHistoryEntry, 64*1024, CRingBufferBase::FLAG_RECYCLE> m_History;

    int64 m_LastChatSend;
    int64 m_aLastSoundPlayed[CChatMessage::NUM_TYPES];

    float m_ScrollValue = 0.0f;
    float m_OverallHeight = 0.0f;
};


class CUIChatView : public CUIView
{

public:

    CTopicWebsocketConnection *m_pWS;

    IInput *m_pInput;
    IClient *m_pClient;
    CGameClient *m_pGameClient;
    IGraphics *m_pGraphics;
    ITextRender *m_pTextRender;

    CChatState m_State;

    CUIChatView() : CUIView() {}

    void SetKernel(IKernel *pKernel) {
        m_pClient = pKernel->RequestInterface<IClient>();
        m_pGameClient = static_cast<CGameClient *>(pKernel->RequestInterface<IGameClient>());
        m_pInput = pKernel->RequestInterface<IInput>();
        m_pGraphics = pKernel->RequestInterface<IGraphics>();
        m_pTextRender = pKernel->RequestInterface<ITextRender>();
    }

    void CheckNewMessages() {
        const CTopicWebsocketConnection::CTopicMessage *pMsg = m_pWS->GetNextReceiveTopicMessage();

        if(pMsg != nullptr) {
            if (str_comp(pMsg->m_pTopic, "room:lobby") == 0 && str_comp(pMsg->m_pEvent, "shout")  == 0) {

                const json_value *pAuthor = json_object_get(pMsg->m_pData, "name");
                const json_value *pText = json_object_get(pMsg->m_pData, "message");

                if (pAuthor != &json_value_none && pText != &json_value_none) {
                    CChatMessage *pMsg = OnNewMessage(CChatMessage::TYPE_CLIENT, str_create_source(json_string_get(pText)), str_create_source(json_string_get(pAuthor)));
                    m_State.m_apMessages.add(pMsg);
                }
            }
            delete pMsg;
        }
    }


    void SendMessage(const char *pText) {
        CTopicWebsocketConnection::CTopicMessage Msg;
        Msg.m_pTopic = str_create_source("room:lobby");
        Msg.m_pEvent = str_create_source("shout");
        Msg.m_pSendData = json_object_new(0);
        json_object_push(Msg.m_pSendData, "name", json_string_new(m_pClient->Galaxy()->Account()->Name()));
        json_object_push(Msg.m_pSendData, "message", json_string_new(pText));
        m_pWS->SendTopicMessage(&Msg);
    }

    void SetTopicWebsocketConnection(CTopicWebsocketConnection *pConn) {
        m_State = CChatState();
        m_pWS = pConn;

        // Join chat
        CTopicWebsocketConnection::CTopicMessage Msg;
        Msg.m_pMessageRef = str_create_source("chats");
        Msg.m_pTopic = str_create_source("room:lobby");
        Msg.m_pEvent = str_create_source("phx_join");
        Msg.m_pSendData = json_object_new(0);
        m_pWS->SendTopicMessage(&Msg);
    }

    virtual void OnUpdate(const CUIRect &View, CRenderTools *pRT, float Delta = 0.0f);
    virtual void OnRender(const CUIRect &View, CRenderTools *pRT, float Delta = 0.0f) const;


    void RenderChat(CUIRect View, CRenderTools *pRT, float Delta);

    CChatMessage* OnNewMessage(int Type, const char *pLine, const char *pAuthor);
};
#endif
