/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_ELEMENTS_BOX_H
#define GAME_CLIENT_UI_ELEMENTS_BOX_H
#include "styled.h"
#include "../styles/text.h"

class CUIText : public CUIStyled<CUITextStyle>
{
    const char *m_pText;
public:

    void Init() {
        m_pText = 0;
    }

    CUIText() : CUIStyled() {
        Init();
    }

    CUIText(const CUITextStyle Style) : CUIStyled(Style) {
        Init();
    }

    virtual void OnUpdate(const CUIRect &View, CRenderTools *pRT, float Delta = 0.0f) {

    };

    virtual void OnRender(const CUIRect &View,  CRenderTools *pRT, float Delta = 0.0f) const;

    void SetText(const char *pText) {
        m_pText = pText;
    }

    const char *GetText() {
        return m_pText;
    }
};
#endif
