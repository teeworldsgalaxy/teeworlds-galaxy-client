/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_ELEMENTS_ELEMENT_H
#define GAME_CLIENT_UI_ELEMENTS_ELEMENT_H

#include "../styles/style.h"
#include "../../render.h"
#include "../../components/menus.h"
class CUIElement
{
protected:

public:
    CUIElement() {
    }

    /**
     * Handle animations, view updates
     */
    virtual void OnUpdate(const CUIRect &View, CRenderTools *pRT, float Delta = 0.0f) = 0;

    /**
     * Render this element to the screen
     */
    virtual void OnRender(const CUIRect &View, CRenderTools *pRT, float Delta = 0.0f) const = 0;

	/**
	 * Easy method to dispatch the element and all of its logic
	 */
    virtual void Create(const CUIRect &View, CRenderTools *pRT, float Delta = 0.0f)
	{
		OnUpdate(View, pRT, Delta);
		OnRender(View, pRT, Delta);
	}
};
#endif
