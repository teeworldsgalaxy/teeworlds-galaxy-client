/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_ELEMENTS_ICON_BUTTON_H
#define GAME_CLIENT_UI_ELEMENTS_ICON_BUTTON_H
#include "stateful.h"
#include "clickable.h"
#include "tooltip.h"
#include "../styles/icon.h"
#include "interactable.h"
#include "../constants.h"
#include "icon.h"
class CUIIconButton : public CUIIcon
{

public:

	CUIIconButton() : CUIInteractable() {
        SetEnabled(true);
		StatefulStylesSetDefault();
    }

    virtual void OnRender(const CUIRect &View, CRenderTools *pRT, float Delta = 0.0f) const;

	virtual void StatefulStylesSetDefault()
	{
		float DefaultMargin = 8.0f;

		GetStatefulStyles().StatefulGetDefault().BackgroundSetColor(STYLE_COLOR_BG_WHITE);
		GetStatefulStyles().StatefulGetDefault().IconSetColor(STYLE_COLOR_BG_DARK);
		GetStatefulStyles().StatefulGetDefault().MarginSetAll(DefaultMargin);

		GetStatefulStyles().StatefulGetDisabled().BackgroundSetColor(STYLE_COLOR_BG_GREY);
		GetStatefulStyles().StatefulGetDisabled().MarginSetAll(DefaultMargin);
		GetStatefulStyles().StatefulGetDisabled().IconSetColor(STYLE_COLOR_BG_DARK);

		GetStatefulStyles().StatefulGetHover().BackgroundSetColor(STYLE_COLOR_BG_LIGHT);
		GetStatefulStyles().StatefulGetHover().IconSetColor(STYLE_COLOR_BG_WHITE);
		GetStatefulStyles().StatefulGetHover().MarginSetAll(DefaultMargin);

		GetStatefulStyles().StatefulGetClicked().BackgroundSetColor(STYLE_COLOR_BG_WHITE);
		GetStatefulStyles().StatefulGetClicked().IconSetColor(STYLE_COLOR_BG_LIGHT);
		GetStatefulStyles().StatefulGetClicked().MarginSetAll(DefaultMargin);


		GetStatefulStyles().StatefulGetClick().BackgroundSetColor(STYLE_COLOR_BG_WHITE);
		GetStatefulStyles().StatefulGetClick().IconSetColor(STYLE_COLOR_BG_LIGHT);
		GetStatefulStyles().StatefulGetClick().MarginSetAll(DefaultMargin);

		/*const vec4 ButtonColorError = STYLE_COLOR_BTN_ERROR;
		GetStatefulStyles().StatefulGetError().StatefulGetDefault().BackgroundSetColor(ButtonColorError);
		GetStatefulStyles().StatefulGetError().StatefulGetDisabled().BackgroundSetColor(CUI::Brightness(ButtonColorError, 0.5f));
		GetStatefulStyles().StatefulGetError().StatefulGetDisabled().IconSetColor(STYLE_COLOR_TEXT_DANGER_DISABLED);
		GetStatefulStyles().StatefulGetError().StatefulGetHover().BackgroundSetColor(CUI::Brightness(ButtonColorError, 1.2f));
		GetStatefulStyles().StatefulGetError().StatefulGetClicked().BackgroundSetColor(CUI::Brightness(ButtonColorError, 0.7f));
		GetStatefulStyles().StatefulGetError().StatefulGetClick().BackgroundSetColor(CUI::Brightness(ButtonColorError, 0.7f));*/

	}
};
#endif
