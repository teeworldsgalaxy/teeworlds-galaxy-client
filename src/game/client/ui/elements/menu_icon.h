#ifndef GAME_CLIENT_UI_ELEMENTS_LINK_H
#define GAME_CLIENT_UI_ELEMENTS_LINK_H

#include "styled.h"
#include "clickable.h"

class CUIMenuIcon : public CUIStyled<CUILinkStyle>, public CUIClickable
{
	const char *m_pText;

	void Init()
	{
		SetEnabled(true);
		m_pText = "";
	}

public:
	CUIMenuIcon(const void *pID = 0): CUIStyled(), CUIClickable(pID)
	{
		Init();
	}

	CUIMenuIcon(bool Enabled, const void *pID = 0) : CUIStyled(), CUIClickable(pID)
	{
		Init();
		SetEnabled(Enabled);
	}

	CUIMenuIcon(bool Enabled, const CUILinkStyle& Style, const void *pID = 0) : CUIStyled(Style), CUIClickable(pID)
	{
		Init();
		SetEnabled(Enabled);
	}

	virtual void OnRender(const CUIRect& View, CRenderTools *pRT, float Delta = 0.0f) const;

	const char *GetText() const
	{
		return m_pText;
	}

	void SetText(const char *pText)
	{
		m_pText = pText;
	}
};

#endif
