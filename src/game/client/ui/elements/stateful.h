/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_ELEMENTS_STATEFUL_H
#define GAME_CLIENT_UI_ELEMENTS_STATEFUL_H

#include "styled.h"
#include "../styles/stateful.h"

template <typename  S, typename T>
class CUIStateful : public CUIStyled<S>
{
protected:

	enum {
		UI_STATE_DEFAULT,
		UI_STATE_DISABLED,
		UI_STATE_HOVER,
		UI_STATE_CLICK, /// being set while the button is down
		UI_STATE_CLICKED, /// being set on release
		UI_STATE_ACTIVE, // isn't this the same as 'CLICK'...?
		//UI_STATE_ERROR // not quite ideal - you could also click an erroneous button
	};

	int m_State;

	enum {
		UI_PROPERTY_NONE,
		UI_PROPERTY_ERROR,
		UI_PROPERTY_LOADING,
	};

	int m_Property; // TODO: this should maybe be used as a flags storage instead, depending on what properties 'll get added

	CUIStatefulStyle<T> m_StatefulStyles;

	const T& GetCurrentStyle() const {
		const CUIStatefulStyleHolder<T> *pStatefulStyles = &m_StatefulStyles;

		if(m_Property == UI_PROPERTY_ERROR)
			pStatefulStyles = &m_StatefulStyles.StatefulGetError();

		//TODO: support other states
		switch (m_State) {
			case UI_STATE_HOVER:
				return pStatefulStyles->StatefulGetHover();
			case UI_STATE_ACTIVE:
				return pStatefulStyles->StatefulGetActive();
			case UI_STATE_DISABLED:
				return pStatefulStyles->StatefulGetDisabled();
			case UI_STATE_DEFAULT:
				return pStatefulStyles->StatefulGetDefault();
			case UI_STATE_CLICKED:
				return pStatefulStyles->StatefulGetClicked();
			case UI_STATE_CLICK:
				return pStatefulStyles->StatefulGetClick();
		}
		//TODO: add missing
		return pStatefulStyles->StatefulGetDefault();
	}

	T& GetCurrentStyle() {
		const CUIStatefulStyle<T> *pStatefulStyles = &m_StatefulStyles;

		if(m_Property == UI_PROPERTY_ERROR)
			pStatefulStyles = &m_StatefulStyles.StatefulGetError();

		//TODO: support other states
		switch (m_State) {
			case UI_STATE_HOVER:
				return pStatefulStyles->StatefulGetHover();
			case UI_STATE_ACTIVE:
				return pStatefulStyles->StatefulGetActive();
			case UI_STATE_DISABLED:
				return pStatefulStyles->StatefulGetDisabled();
			case UI_STATE_DEFAULT:
				return pStatefulStyles->StatefulGetDefault();
			case UI_STATE_CLICKED:
				return pStatefulStyles->StatefulGetClicked();
			case UI_STATE_CLICK:
				return pStatefulStyles->StatefulGetClick();
		}
		//TODO: add missing
		return pStatefulStyles->StatefulGetDefault();
	}

	CUIStateful() : CUIStyled<S>(), m_State(UI_STATE_DEFAULT){

	}

	CUIStateful(S Style) : CUIStyled<S>(Style), m_State(UI_STATE_DEFAULT)
	{
	}

public:

	CUIStatefulStyle<T>& GetStatefulStyles() {
		return m_StatefulStyles;
	};

	const CUIStatefulStyle<const T>& GetStatefulStyles() const {
		return m_StatefulStyles;
	};

	void SetState(int State) {
		m_State = State;
	}

	int GetState() const {
		return m_State;
	}

	void SetProperty(int Property) {
		m_Property = Property;
	}

	int GetProperty() const {
		return m_Property;
	}

	virtual void StatefulStylesSetDefault() = 0;
};
#endif
