/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */

#include "box.h"

void CUIBox::OnRender(const CUIRect &View, CRenderTools *pRT, float Delta) const {
    pRT->DrawUIRect(&View, GetStyle().BackgroundGetColor(), 0, 0.0f);

    vec4 Color = GetStyle().BorderGetColor();
    float Size = GetStyle().BorderGetSize();
    CUIRect Side;
    View.HSplitTop(Size, &Side, 0);
    pRT->DrawUIRect(&Side, Color); // TOP
    View.VSplitRight(Size, 0, &Side);
    pRT->DrawUIRect(&Side, Color); // RIGHT
    View.HSplitBottom(Size, 0, &Side);
    pRT->DrawUIRect(&Side, Color); // BOTTOM
    View.VSplitLeft(Size, &Side, 0);
    pRT->DrawUIRect(&Side, Color); // LEFT
}



void CUIBox::OnUpdate(const CUIRect &View, CRenderTools *pRT, float Delta){


}
