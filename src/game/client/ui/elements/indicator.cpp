/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */

#include "indicator.h"
#include <engine/textrender.h>

void CUIIndicator::OnRender(const CUIRect &View, CRenderTools *pRT, float Delta) const {

    CUIRect Circle;
    Circle.w = Circle.h = GetStyle().IndicatorGetRadius();
    View.Align(&Circle, GetStyle().AlignmentGetLegacyValue());
    pRT->UI()->RenderTools()->DrawCircle(Circle.x+Circle.w/2.0f, Circle.y+Circle.h/2.0f, GetStyle().IndicatorGetRadius(), GetStyle().BackgroundGetColor());
    m_Tooltip.OnRender(View, pRT, Delta);
}

void CUIIndicator::OnUpdate(const CUIRect &View, CRenderTools *pRT, float Delta) {
    CUIRect Circle;
    Circle.w = Circle.h = GetStyle().IndicatorGetRadius();
    View.Align(&Circle, GetStyle().AlignmentGetLegacyValue());
    CUI* UI = pRT->UI();
    UI->DoClickableRectLogic(this, &Circle);
    // handle tooltip
    if (UI->HotItem() == GetID()) {
        m_Tooltip.SetActive(true);
    } else {
        m_Tooltip.SetActive(false);
    }
}