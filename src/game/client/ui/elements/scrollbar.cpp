/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */

#include "box.h"
#include "../constants.h"
#include "scrollbar.h"

void CUIScrollbar::OnRender(const CUIRect &View, CRenderTools *pRT, float Delta) const {

    CUIRect Border, Handle;

    //TODO: We could also render this scrollbar as disabled
    bool Vertical;
    CalculateRects(pRT, View, &Border, &Handle, &Vertical);


    if (GetStyle().ScrollbarGetAutoHide() && m_Percent >= 1.0f) {
        return;
    }

    CUIBoxStyle InnerStyle, OuterStyle;
    OuterStyle.BackgroundSetColor(vec4(0, 0, 0, 0));
    OuterStyle.BorderSetColor(STYLE_COLOR_SCROLLBAR_OUTER);
    OuterStyle.BorderSetSize(1.0f);
    InnerStyle.BackgroundSetColor(STYLE_COLOR_SCROLLBAR_INNER);
    InnerStyle.BorderSetColor(vec4(0, 0, 0, 0));


    CUIBox Inner(InnerStyle);
    CUIBox Outer(OuterStyle);
    Outer.Create(Border, pRT);
    Inner.Create(Handle, pRT);
}



void CUIScrollbar::OnUpdate(const CUIRect &View, CRenderTools *pRT, float Delta){

    CUI *pUI = pRT->UI();
    static float Offset;

    CUIRect Handle;
    bool Vertical;
    bool Inside;
    CalculateRects(pRT, View, 0, &Handle, &Vertical, &Inside);

    // TODO: the scollbar handle collision box does not match the drawn rect. Is that intended? Because if it would, it would be incredibly hard to grab.

//    pRT->DrawUIRect(&Handle, vec4(1,0,0,1), 0, 0);

	// logic
    float Value = m_Value;

    if(pUI->ActiveItem() == GetID())
    {
        if(!pUI->MouseButton(0))
            pUI->SetActiveItem(0);

        float Min = Vertical ? View.y : View.x;
        float Max = Vertical ?  View.h-Handle.h : View.w-Handle.w;
        float Cur = Vertical ? (pUI->MouseY()-Offset) : pUI->MouseX()-Offset;
        Value = clamp((Cur-Min)/Max, 0.0f, 1.0f);
    }
    else if(pUI->HotItem() == GetID())
    {
        if(pUI->MouseButton(0))
        {
            pUI->SetActiveItem(GetID());
            Offset = Vertical ? (pUI->MouseY()-Handle.y) : pUI->MouseX()-Handle.x;
        }
    }

    if(Inside)
        pUI->SetHotItem(GetID());

    m_Value = Value;
}

void CUIScrollbar::CalculateRects(const CRenderTools *pRT, const CUIRect& View, CUIRect *pBorder, CUIRect *pHandle, bool *pOutVertical, bool *pOutInsideBar, bool *pOutInsideHandle) const
{
    bool Vertical = View.h > View.w;

    bool InsideBar = pRT->UI()->MouseInside(&View) || pRT->UI()->ActiveItem() == GetID();

    float Margin = InsideBar ? 1.5f : 2.5f;
    float Size = 6.0f;

    float Width = m_Percent == 0.0f ? 1.0f : clamp(m_Percent, 0.05f, 1.0f);


    CUIRect Border = View;
    CUIRect Handle;
    if (Vertical) {

        Border.w = min(Border.w, Size);

        Border.HSplitTop(33, &Handle, 0);
        Border.Margin(Margin, &Handle);

        Handle.h *= Width;

        Handle.y =  mix(Border.y+Margin , Border.y+Border.h-Margin-Handle.h, m_Value);
    } else {
        Border.h = min(Border.h, Size);
        Border.VSplitLeft(33, &Handle, 0);
        Border.Margin(Margin, &Handle);

        Handle.w *= Width;
        Handle.x = mix(Border.x+Margin , Border.x+Border.w-Margin-Handle.w, m_Value);
    }

    if(pBorder) *pBorder = Border;
    if(pHandle) *pHandle = Handle;
    if(pOutVertical) *pOutVertical = Vertical;
    if(pOutInsideBar) *pOutInsideBar = InsideBar;
    if(pOutInsideHandle) *pOutInsideHandle = pRT->UI()->MouseInside(&Handle) || pRT->UI()->ActiveItem() == GetID();
}
