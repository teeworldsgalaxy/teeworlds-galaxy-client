/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */

#include "text.h"
#include <engine/textrender.h>

void CUIText::OnRender(const CUIRect &View, CRenderTools *pRT, float Delta) const {

    if (!m_pText) {
        return;
    }

    pRT->UI()->TextRender()->TextColor(	m_Style.TextGetFontColor().r,
										m_Style.TextGetFontColor().g,
										m_Style.TextGetFontColor().b,
										m_Style.TextGetFontColor().a );

//    if (m_Style.TextGetOutlineSize() > 0.0f) {
//		pRT->UI()->TextRender()->TextOutlineSize(m_Style.TextGetOutlineSize); TODO!
        pRT->UI()->TextRender()->TextOutlineColor(
				m_Style.TextGetOutlineColor().r,
				m_Style.TextGetOutlineColor().g,
				m_Style.TextGetOutlineColor().b,
				m_Style.TextGetOutlineColor().a
		);
//    }

    const float MaxWidth = m_Style.DimensionGetMaxWidth();
    const float tw = pRT->UI()->TextRender()->TextWidth(0, m_Style.TextGetFontSize(), m_pText, -1, MaxWidth);

    int Lines = 1;
	if (MaxWidth == -2)
		Lines = pRT->UI()->TextRender()->TextLineCount(0, m_Style.TextGetFontSize(), m_pText, View.w);
	else if(MaxWidth > 0)
		Lines = pRT->UI()->TextRender()->TextLineCount(0, m_Style.TextGetFontSize(), m_pText, MaxWidth);

    CUIRect Inner;
    Inner.w = MaxWidth == -2 ? View.w : tw;
    Inner.h = (float)Lines * m_Style.TextGetFontSize(); //TODO, the font size is not really equal to the rendered size

    View.Align(&Inner, m_Style.AlignmentGetLegacyValue());

    pRT->UI()->TextRender()->Text(0, Inner.x, Inner.y, MaxWidth == -2 ? Inner.h / (float)Lines :  m_Style.TextGetFontSize(), m_pText, MaxWidth == -2 ? Inner.w : MaxWidth);
}
