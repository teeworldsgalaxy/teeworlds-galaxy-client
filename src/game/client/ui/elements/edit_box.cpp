/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */

#include "edit_box.h"
#include "box.h"
#include "icon.h"
#include <engine/textrender.h>

#include <game/client/lineinput.h>
#include <game/generated/client_data.h>
#include <engine/keys.h>

void CUIEditBox::OnRender(const CUIRect &View, CRenderTools *pRT, float Delta) const {


    CUIBox BorderBox;
    BorderBox.GetStyle().BackgroundSetColor(GetStyle().BackgroundGetColor());
    BorderBox.GetStyle().BorderSetSize(GetStyle().BorderGetSize());
    BorderBox.GetStyle().BorderSetColor(GetStyle().BorderGetColor());
    BorderBox.OnRender(View, pRT, Delta);


    float FontSize = GetStyle().TextGetFontSize()*pRT->UI()->Scale();
    vec4 PlaceholderColor = GetStyle().EditBoxGetPlaceholderColor();
    float MarginLeft = GetStyle().MarginGetLeft()*pRT->UI()->Scale();

    CUIRect EditBox = View;

    if (GetStyle().EditBoxGetShowClear() && m_pText[0] != 0) {
        CUIRect ClearButton;
        EditBox.VSplitRight(14.0f, &EditBox, &ClearButton);
        EditBox.VSplitRight(2.0f, &EditBox, 0); //Margin Right
        ClearButton.VSplitRight(5.0f, &ClearButton, 0); //Margin Right
        m_ClearIcon.OnRender(ClearButton, pRT);
    }

    pRT->UI()->ClipEnable(&EditBox);



    CUIRect Textbox = EditBox;
    Textbox.VMargin(2.0f, &Textbox);
    Textbox.HMargin(2.0f, &Textbox);

    Textbox.x -= *m_Offset;
    Textbox.x += GetStyle().MarginGetLeft();

    vec4 color = m_ShowPlaceholder ? GetStyle().EditBoxGetPlaceholderColor() : GetStyle().TextGetFontColor();
    pRT->UI()->TextRender()->TextColor(color.r, color.g, color.b, color.a);

    const char * pDisplayStr = m_pDisplayStr;
    // Hide Input with stars if hidden and no placeholder set
    if(GetStyle().EditBoxIsHidden() && !m_ShowPlaceholder)
    {
        char aHiddenInput[128] = {0};

        int count = min((int) sizeof(aHiddenInput)-1, str_length(m_pDisplayStr));
        for(int i = 0; i < count ; ++i)
            aHiddenInput[i] = '*';
        aHiddenInput[count] = '\0';
        pDisplayStr = aHiddenInput;
    }

    pRT->UI()->DoLabel(&Textbox, pDisplayStr, FontSize, CUI::ALIGN_LEFT, -1, &color);

    // Reset Textcolor
    pRT->UI()->TextRender()->TextColor(1.0f, 1.0f, 1.0f, 1.0f);
    // render the cursor
    if(pRT->UI()->LastActiveItem() == GetID() /*&& !JustGotActive*/)
    {
        float w = m_AtIndex > 0 ? pRT->UI()->TextRender()->TextWidth(0, FontSize, pDisplayStr, m_AtIndex): 0.0f;
        Textbox = EditBox;
        Textbox.VSplitLeft(2.0f, 0, &Textbox);
        Textbox.x += (w-*m_Offset-pRT->UI()->TextRender()->TextWidth(0, FontSize, "|", -1)/2)+MarginLeft;

        if((2*time_get()/time_freq()) % 2)	// make it blink
            pRT->UI()->DoLabel(&Textbox, "|", FontSize, CUI::ALIGN_LEFT);
    }
    pRT->UI()->ClipDisable();
}

void CUIEditBox::OnUpdate(const CUIRect &View, CRenderTools *pRT, float Delta) {

    float FontSize = GetStyle().TextGetFontSize();
    float MarginLeft = GetStyle().MarginGetLeft();

    float ClearButtonWidth = 14.0f;
    float Width = View.w-MarginLeft-2.0f;

    bool ReturnValue = false;


    CUIRect EditBox = View;


    if (GetStyle().EditBoxGetShowClear() && m_pText[0] != 0) {
        CUIRect ClearButton;
        EditBox.VSplitRight(14.0f, &EditBox, &ClearButton);
        EditBox.VSplitRight(2.0f, &EditBox, 0); //Margin Right
        CUIRect ClearArea = ClearButton;
        ClearButton.VSplitRight(5.0f, &ClearButton, 0); //Margin Right

        int HoverOrClick = pRT->UI()->DoRectLogic(( char *) GetID()+1, &ClearArea, true);

        if (HoverOrClick == -1) {
            m_ClearIcon.GetStyle().IconSetColor(GetStyle().TextGetFontColor());
        } else {
            m_ClearIcon.GetStyle().IconSetColor(GetStyle().BorderGetColor());
        }

        if (HoverOrClick > 0) {
            m_pText[0] = 0;
            *m_Offset = 0.0f;
            ReturnValue = true;
            if (pRT->UI()->ActiveItem() == GetID()) {
                pRT->UI()->SetActiveItem(0);
            }
        }
        Width = EditBox.w-2.0f-MarginLeft;
    }

    int Inside = pRT->UI()->MouseInside(&EditBox);

    bool UpdateOffset = false;

    if(pRT->UI()->LastActiveItem() == GetID())
    {
        int Len = str_length(m_pText);
        if(Len == 0) {
            m_AtIndex = 0;
        }

        if(Inside && pRT->UI()->MouseButton(0))
        {
            m_DoScroll = true;
            m_ScrollStart = pRT->UI()->MouseX();
            int MxRel = (int)(pRT->UI()->MouseX() - View.x-MarginLeft);

            for(int i = 1; i <= Len; i++)
            {
                if(pRT->UI()->TextRender()->TextWidth(0, FontSize, m_pText, i) - *m_Offset > MxRel)
                {
                    m_AtIndex = i - 1;
                    break;
                }

                if(i == Len)
                    m_AtIndex = Len;
            }
        }
        else if(!pRT->UI()->MouseButton(0))
            m_DoScroll = false;
        else if(m_DoScroll)
        {
            // do scrolling
            if(pRT->UI()->MouseX() < EditBox.x && m_ScrollStart-pRT->UI()->MouseX() > 10.0f)
            {
                m_AtIndex = max(0, m_AtIndex-1);
                m_ScrollStart = pRT->UI()->MouseX();
                UpdateOffset = true;
            }
            else if(pRT->UI()->MouseX() > EditBox.x+EditBox.w && pRT->UI()->MouseX()-m_ScrollStart > 10.0f)
            {
                m_AtIndex = min(Len, m_AtIndex+1);
                m_ScrollStart = pRT->UI()->MouseX();
                UpdateOffset = true;
            }
        }

        for(int i = 0; i < CMenus::m_NumInputEvents; i++)
        {
            Len = str_length(m_pText);
            int NumChars = Len;
            ReturnValue |= CLineInput::Manipulate(CMenus::m_aInputEvents[i], m_pText, m_TextMaxLength, m_TextMaxLength, &Len, &m_AtIndex, &NumChars);
        }
    }

    bool JustGotActive = false;

    if(pRT->UI()->ActiveItem() == GetID())
    {
        if(!pRT->UI()->MouseButton(0))
        {
            m_AtIndex = min(m_AtIndex, str_length(m_pText));
            m_DoScroll = false;
            pRT->UI()->SetActiveItem(0);
        }
    }
    else if(pRT->UI()->HotItem() == GetID())
    {

        if(pRT->UI()->MouseButton(0))
        {
            if (pRT->UI()->LastActiveItem() != GetID())
                JustGotActive = true;
            pRT->UI()->SetActiveItem(GetID());
        }
    }

    if(Inside)
        pRT->UI()->SetHotItem(GetID());

    m_ShowPlaceholder = str_length(m_pText) == 0 && m_pPlaceholderText && str_length(m_pPlaceholderText) > 0 && (pRT->UI()->LastActiveItem() != GetID() || JustGotActive);

    m_pDisplayStr =  m_ShowPlaceholder ? m_pPlaceholderText : m_pText;

    float TBWidth = Width;

    // check if the text has to be moved
    if(pRT->UI()->LastActiveItem() == GetID() && !JustGotActive && (UpdateOffset || CMenus::m_NumInputEvents))
    {
        float w = pRT->UI()->TextRender()->TextWidth(0, FontSize, m_pDisplayStr, m_AtIndex);
        if(w-*m_Offset > TBWidth)
        {
            // move to the left
            float wt = pRT->UI()->TextRender()->TextWidth(0, FontSize, m_pDisplayStr, -1);
            do
            {
                *m_Offset += min(wt-*m_Offset-TBWidth, TBWidth/3);
            }
            while(w-*m_Offset > TBWidth);
        }
        else if(w-*m_Offset < 0.0f)
        {
            // move to the right
            do
            {
                *m_Offset = max(0.0f, *m_Offset-TBWidth);
            }
            while(w-*m_Offset < 0.0f);
        }
    }


    // Check if buttons were pressed too
    m_WasUpdated = ReturnValue
                   || pRT->UI()->DoInputTabLogic(GetID())
                   || pRT->UI()->DoInputEscapeLogic(GetID())
                   || pRT->UI()->DoInputReturnLogic(GetID());
}
