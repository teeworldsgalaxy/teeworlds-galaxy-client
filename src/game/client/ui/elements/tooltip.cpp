/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */

#include <base/math.h>
#include <game/client/render.h>
#include <engine/textrender.h>
#include "tooltip.h"

void CUITooltip::OnRender(const CUIRect &View, CRenderTools *pRT, float Delta) const {

	if (!m_pText) {
		return;
	}

	if(m_pText[0] == '\0')
		return;

    if(!m_Active) {
        return;
    }

	const float LINE_WIDTH = m_Style.DimensionGetMaxWidth() * pRT->UI()->Scale();
	const int LINE_COUNT = pRT->UI()->TextRender()->TextLineCount(0, m_Style.TextGetFontSize(), m_pText, LINE_WIDTH);

	CUIRect Rect;

	CUIRect Text;
	Text.h = (m_Style.TextGetFontSize() * (float)LINE_COUNT);
	Text.w = pRT->UI()->TextRender()->TextWidth(0, m_Style.TextGetFontSize(), m_pText, -1, LINE_WIDTH);


	Text.Margin(-3.0f, &Rect); // outsize it a bit to give it a little padding to the text
	Rect.x = pRT->UI()->MouseX() + 30.0f;
	Rect.y = pRT->UI()->MouseY() + 5.0f;

	// clamp the tooltip to the screen bounds (at the right)
	Rect.x = clamp(Rect.x, 0.0f, pRT->UI()->Screen()->w - Rect.w);

	// make sure we don't hide the cursor -> move the tooltip down if we would
	if(pRT->UI()->MouseX() + 20.0f > Rect.x) {
		Rect.y += 20.0f * pRT->UI()->Scale();
	}

	// clamp the tooltip to the screen bounds (at the bottom)
	Rect.y = clamp(Rect.y, 0.0f, pRT->UI()->Screen()->h - Rect.h);
	Rect.Align(&Text, CUI::ALIGN_CENTER);

	//TODO: use corner style here
    pRT->DrawUIRect(&Rect, m_Style.BackgroundGetColor(), CUI::CORNER_ALL, 0);
	const vec4& c = m_Style.TextGetFontColor();
    pRT->UI()->TextRender()->TextColor(c.r, c.g, c.b, c.a);
    pRT->UI()->TextRender()->Text(0, Text.x, Text.y-2.5f, m_Style.TextGetFontSize(), m_pText, LINE_WIDTH);

    //TODO: use text element here
}
