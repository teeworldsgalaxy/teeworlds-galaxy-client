/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_ELEMENTS_INDICATOR_H
#define GAME_CLIENT_UI_ELEMENTS_INDICATOR_H
#include "styled.h"
#include "tooltip.h"
#include "clickable.h"
#include "../styles/indicator.h"

class CUIIndicator : virtual public CUIStyled<CUIIndicatorStyle>, virtual public CUIClickable
{
public:

    CUIIndicator() : CUIStyled<CUIIndicatorStyle>(),CUIClickable()
    {

    }

    CUIIndicator(const CUIIndicatorStyle Style) : CUIStyled<CUIIndicatorStyle>(Style), CUIClickable()
    {

    }

    CUITooltip& GetTooltip() {
        return m_Tooltip;
    }

    const CUITooltip& GetTooltip() const {
        return m_Tooltip;
    }

    virtual void OnUpdate(const CUIRect &View, CRenderTools *pRT, float Delta = 0.0f);

    /**
     * Render this element to the screen
     */
    virtual void OnRender(const CUIRect &View, CRenderTools *pRT, float Delta = 0.0f) const;

protected:
    CUITooltip m_Tooltip;


};
#endif
