#ifndef GAME_CLIENT_UI_ELEMENTS_CLICKABLE_H
#define GAME_CLIENT_UI_ELEMENTS_CLICKABLE_H

class CUIClickable
{
	bool m_Enabled = true;
	const void *m_pID;

protected:
	CUIClickable() : m_pID(this)
	{
	}

public:
	void SetID(const void *pID) { m_pID = pID; }
	const void* GetID() const { return m_pID; }

	virtual bool IsEnabled() const { return m_Enabled; }
	virtual void SetEnabled(bool Enabled) { m_Enabled = Enabled; }
};

#endif
