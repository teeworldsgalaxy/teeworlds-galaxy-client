/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_ELEMENTS_STYLED_H
#define GAME_CLIENT_UI_ELEMENTS_STYLED_H

#include "element.h"
template <typename  S>
class CUIStyled : public CUIElement
{
protected:
    S m_Style;
    S *m_pStyle;
public:

    CUIStyled() : m_pStyle(0), CUIElement() {
    }

    CUIStyled(const S Style) : m_pStyle(0), CUIElement() {
        m_Style = Style;
    }

    CUIStyled(S *pStyle) : m_pStyle(pStyle), CUIElement() {
    }

    /**
     * Binds the own style to another one
     * @param pStyle
     */
    void Bind(S *pStyle) {
        m_pStyle = pStyle;
    }

    /**
     *  Unbinds this styling from another one
     */
    void Unbind(bool copy = true) {
        //copy style
        if (copy)
            CopyStyle(*m_pStyle);
        //release binding
        m_pStyle = 0;
    }

    S& GetStyle() {
        return m_pStyle ? *m_pStyle : m_Style;
    };

    const S GetStyle() const {
        return m_pStyle ? *m_pStyle : m_Style;
    };

    void CopyStyle(S Style) {
        m_Style = Style;
    }
};
#endif
