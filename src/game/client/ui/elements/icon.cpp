/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */

#include "icon.h"
#include <game/generated/client_data.h>

void CUIIcon::OnRender(const CUIRect &View, CRenderTools *pRT, float Delta) const {

    if (!GetStyle().IconHasIcon()) {
        return;
    }


    CUIRect IconView;
    IconView.w = IconView.h = GetCurrentStyle().IconGetSize() == 0.0f ? 0.75f*min(View.w, View.h) : GetCurrentStyle().IconGetSize();
    View.Align(&IconView);

    CUIRect IconRect;
    // Specify maximum size
    IconRect.w = 64;
    IconRect.h = 64;

    View.Align(&IconRect, GetStyle().AlignmentGetLegacyValue(), GetStyle().MarginGetMax(), GetStyle().MarginGetMax());

    pRT->Graphics()->BlendNormal();
    pRT->Graphics()->TextureSet(g_pData->m_aImages[IMAGE_GUIICONSNEW].m_Id);
    pRT->Graphics()->QuadsBegin();

    vec4 C = GetStyle().IconGetColor();
    pRT->Graphics()->SetColor(C.r, C.g, C.b, C.a);

    pRT->SelectSprite(GetStyle().IconGetIcon());
    IGraphics::CQuadItem QuadItem(IconRect.x, IconRect.y, IconRect.w, IconRect.h);
    pRT->Graphics()->QuadsDrawTL(&QuadItem, 1);
    pRT->Graphics()->QuadsEnd();

    m_Tooltip.OnRender(View, pRT, Delta);
}

void CUIIcon::OnUpdate(const CUIRect &View, CRenderTools *pRT, float Delta) {

    if (!GetID()) {
        return;
    }

    CUI* UI = pRT->UI();
    UI->DoClickableRectLogic(this, &View);

    if (!IsEnabled()) {
        m_State = UI_STATE_DISABLED;
    } else {
        if(m_State == UI_STATE_CLICK && UI->ActiveItem() != GetID() && UI->MouseInside(&View)) // on mouse button release IN the rect
            m_State = UI_STATE_CLICKED;
        else if (UI->ActiveItem() == GetID() && UI->MouseInside(&View)) // while the mouse button is pressed and the mouse is IN the rect
            m_State = UI_STATE_CLICK;
        else if (UI->HotItem() == GetID()) // if the mouse is on the element
            m_State = UI_STATE_HOVER;
        else if(m_State == UI_STATE_CLICKED) // the 'clicked' state shall only be set for one tick
            m_State = UI_STATE_HOVER;
        else
            m_State = UI_STATE_DEFAULT;
    }

    // handle tooltip
    if (UI->HotItem() == GetID()) {
        // render tooltip
        // TODO: Add Tooltip delay
        m_Tooltip.SetActive(true);
    } else {
        m_Tooltip.SetActive(false);
    }
}
