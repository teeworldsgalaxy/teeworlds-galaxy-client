#ifndef GAME_CLIENT_UI_ELEMENTS_LINK_H
#define GAME_CLIENT_UI_ELEMENTS_LINK_H

#include "tooltip.h"
#include "stateful.h"
#include "clickable.h"
#include "../styles/link.h"
#include "interactable.h"

class CUILink : public CUIInteractable<CUILinkStyle, CUITextStyle>
{
	const char *m_pText;

	void Init()
	{
		m_pText = "";
		SetEnabled(true);
		StatefulStylesSetDefault();
	}

public:
	CUILink() : CUIInteractable()
	{
		Init();
	}

	CUILink(bool Enabled) : CUIInteractable()
	{
		Init();
		SetEnabled(Enabled);
	}

	CUILink(bool Enabled, const CUILinkStyle& Style) : CUIInteractable(Style)
	{
		Init();
		SetEnabled(Enabled);
	}

	virtual void OnUpdate(const CUIRect &View, CRenderTools *pRT, float Delta = 0.0f);

	virtual void OnRender(const CUIRect& View, CRenderTools *pRT, float Delta = 0.0f) const;

	virtual void StatefulStylesSetDefault()
	{
		GetStatefulStyles().StatefulGetHover().TextSetFontColor(vec4(0.078f, 0.612f, 0.902f, 1.0f));
		GetStatefulStyles().StatefulGetClick().TextSetFontColor(vec4(0.902f, 0.612f, 0.078f, 0.9f));
		GetStatefulStyles().StatefulGetClicked().TextSetFontColor(vec4(0.902f, 0.612f, 0.078f, 0.9f));
		GetStatefulStyles().StatefulGetDisabled().TextSetFontColor(vec4(0.53f, 0.53f, 0.53f, 0.9f));
	}

	const char *GetText() const
	{
		return m_pText;
	}

	void SetText(const char *pText)
	{
		m_pText = pText;
	}
};

#endif
