/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_ELEMENTS_ICON_H
#define GAME_CLIENT_UI_ELEMENTS_ICON_H
#include "stateful.h"
#include "clickable.h"
#include "tooltip.h"
#include "../styles/icon.h"
#include "interactable.h"
#include "../constants.h"
#include <game/generated/client_data.h>

class CUIIcon : public virtual CUIInteractable<CUIIconStyle, CUIIconStyle>
{


public:

	CUIIcon() : CUIInteractable() {
        SetEnabled(true);
    }

    virtual void OnUpdate(const CUIRect &View, CRenderTools *pRT, float Delta = 0.0f);

    virtual void OnRender(const CUIRect &View, CRenderTools *pRT, float Delta = 0.0f) const;

	virtual void StatefulStylesSetDefault()
	{
        //GetStatefulStyles().StatefulSetAll(GetStyle());
	}
};
#endif