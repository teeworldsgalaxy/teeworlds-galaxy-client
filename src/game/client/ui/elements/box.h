/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_ELEMENTS_BOX_H
#define GAME_CLIENT_UI_ELEMENTS_BOX_H
#include "styled.h"
#include "../styles/box.h"

class CUIBox : public virtual CUIStyled<CUIBoxStyle>
{
public:

    CUIBox() : CUIStyled() {
    }

    CUIBox(CUIBoxStyle Style) : CUIStyled(Style) {

    }

    virtual void OnUpdate(const CUIRect &View, CRenderTools *pRT, float Delta = 0.0f);

    virtual void OnRender(const CUIRect &View, CRenderTools *pRT, float Delta = 0.0f) const;
};
#endif
