/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_ELEMENTS_TOOLTIP_H
#define GAME_CLIENT_UI_ELEMENTS_TOOLTIP_H
#include "styled.h"
#include "../styles/tooltip.h"
class CUITooltip : public virtual CUIStyled<CUITooltipStyle>
{
    const char *m_pText;
    bool m_Active;
public:

    void Init() {
        m_pText = 0;
        m_Active = false;
    }
    CUITooltip() : CUIStyled() {
        Init() ;
    }

    CUITooltip(const CUITooltipStyle Style) : CUIStyled(Style) {
        Init();
    }

    void SetText(const char *pText) {
        m_pText = pText;
    }

    const char* GetText() const {
        return m_pText;
    }

    void SetActive(bool Value) {
        m_Active = Value;
    }

    bool IsActive() const {
        return m_Active;
    }

    virtual void OnUpdate(const CUIRect &View, CRenderTools *pRT, float Delta = 0.0f) {};

    virtual void OnRender(const CUIRect &View, CRenderTools *pRT, float Delta = 0.0f) const;
};
#endif
