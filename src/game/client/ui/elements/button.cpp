/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */

#include "button.h"

void CUIButton::OnRender(const CUIRect &View, CRenderTools *pRT, float Delta) const {

    pRT->DrawUIRect(&View, GetCurrentStyle().BackgroundGetColor(), 0, 0.0f);

    vec4 Color = GetStyle().BorderGetColor();
    float Size = GetStyle().BorderGetSize();
    CUIRect Side;
    View.HSplitTop(Size, &Side, 0);
    pRT->DrawUIRect(&Side, Color); // TOP
    View.VSplitRight(Size, 0, &Side);
    pRT->DrawUIRect(&Side, Color); // RIGHT
    View.HSplitBottom(Size, 0, &Side);
    pRT->DrawUIRect(&Side, Color); // BOTTOM
    View.VSplitLeft(Size, &Side, 0);
    pRT->DrawUIRect(&Side, Color); // LEFT

    vec4 TextColor = GetCurrentStyle().TextGetFontColor();

    if (IsLoading()) {
        pRT->UI()->DoSpinner(&View);
    } else {
        pRT->UI()->DoLabel(&View, GetText(), GetCurrentStyle().TextGetFontSize(), CUI::ALIGN_CENTER, -1, &TextColor, 0/* TODO: Style.TextGetOutlineColor() */);
    }
    m_Tooltip.OnRender(View, pRT, Delta);
}

void CUIButton::OnUpdate(const CUIRect &View, CRenderTools *pRT, float Delta) {
    CUI* UI = pRT->UI();
    UI->DoClickableRectLogic(this, &View);

    if (!IsEnabled()) {
        m_State = UI_STATE_DISABLED;
//    } else if (IsErroneous()) {
//        m_State = UI_STATE_ERROR;
    } else {
		if(m_State == UI_STATE_CLICK && UI->ActiveItem() != GetID() && UI->MouseInside(&View)) // on mouse button release IN the rect
			m_State = UI_STATE_CLICKED;
		else if (UI->ActiveItem() == GetID() && UI->MouseInside(&View)) // while the mouse button is pressed and the mouse is IN the rect
			m_State = UI_STATE_CLICK;
		else if (UI->HotItem() == GetID()) // if the mouse is on the element
			m_State = UI_STATE_HOVER;
		else if(m_State == UI_STATE_CLICKED) // the 'clicked' state shall only be set for one tick
			m_State = UI_STATE_HOVER;
		else
			m_State = UI_STATE_DEFAULT;
    }

    // handle tooltip
    if (UI->HotItem() == GetID()) {
        // render tooltip
        // TODO: Add Tooltip delay
        m_Tooltip.SetActive(true);
    } else {
        m_Tooltip.SetActive(false);
    }
}
