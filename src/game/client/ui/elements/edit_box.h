/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_ELEMENTS_EDIT_BOX_H
#define GAME_CLIENT_UI_ELEMENTS_EDIT_BOX_H
#include "interactable.h"
#include "../styles/edit_box.h"
#include "icon.h"

class CUIEditBox : public CUIInteractable<CUIEditBoxStyle, CUIEditBoxStyle>
{

    char *m_pText;
    int m_TextMaxLength;
    const char *m_pPlaceholderText;
    float *m_Offset;

    bool m_ShowPlaceholder;
    bool m_WasUpdated;

    int m_AtIndex;
    bool m_DoScroll;
    float m_ScrollStart;


    const char *m_pDisplayStr;

    CUIIcon m_ClearIcon;


public:

    virtual void StatefulStylesSetDefault()
    {
    }


    void Init() {
        SetText(0);
        SetPlaceholderText(0);
        m_Offset = 0;
        m_WasUpdated = false;
        m_AtIndex = 0;
        m_DoScroll = false;
        m_ScrollStart = 0.0f;
        m_pDisplayStr = 0;
        m_ClearIcon.GetStyle().IconSetIcon(SPRITE_GUIICON_TIMES);
        m_ClearIcon.SetID(0); // deactivate own logic
    }

    CUIEditBox() : CUIInteractable() {
        Init();
    }

    CUIEditBox(const CUIEditBoxStyle Style) : CUIInteractable(Style) {
        Init();
    }

    virtual void OnUpdate(const CUIRect &View, CRenderTools *pRT, float Delta = 0.0f);
    virtual void OnRender(const CUIRect &View,  CRenderTools *pRT, float Delta = 0.0f) const;


    void SetText(char *pText, int MaxLength = 0) {
        m_pText = pText;
        m_TextMaxLength = MaxLength;
    }

    const char *GetText() {
        return m_pText;
    }

    void SetPlaceholderText(const char *pText) {
        m_pPlaceholderText = pText;
    }

    const char *GetPlaceholderText() {
        return m_pPlaceholderText;
    }

    void SetOffset(float *Offset) {
        m_Offset = Offset;
    }

    bool WasUpdated() {
        return m_WasUpdated;
    }
};
#endif
