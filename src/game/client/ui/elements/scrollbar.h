/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_ELEMENTS_SCROLLBAR_H
#define GAME_CLIENT_UI_ELEMENTS_SCROLLBAR_H
#include "styled.h"
#include "../styles/scrollbar.h"
#include "../elements/clickable.h"

class CUIScrollbar :  public virtual CUIStyled<CUIScrollbarStyle>, public virtual CUIClickable
{
    void CalculateRects(const CRenderTools *pRT, const CUIRect& View, CUIRect *pBorder, CUIRect *pHandle,
                        bool *pOutVertical = 0, bool *pOutInsideBar = 0, bool *pOutInsideHandle = 0) const;

	float m_Value;
	float m_Percent;

	void Init() {
		m_Percent = 0.2f;
	}

public:


    CUIScrollbar() : CUIStyled(), CUIClickable() {
        Init();
    }

    CUIScrollbar(CUIScrollbarStyle Style) : CUIStyled(Style), CUIClickable() {
        Init();
    }


    void ScrollbarSetValue(float Value) {
        m_Value = Value;
    }

    float ScrollbarGetValue() {
        return m_Value;
    }

    void ScrollbarSetPercent(float Percent) {
        m_Percent = Percent;
    }

    float ScrollbarGetPercent() {
        return m_Percent;
    }

    virtual void OnUpdate(const CUIRect &View, CRenderTools *pRT, float Delta = 0.0f);

    /**
     * Render this element to the screen
     */
    virtual void OnRender(const CUIRect &View, CRenderTools *pRT, float Delta = 0.0f) const;
};
#endif
