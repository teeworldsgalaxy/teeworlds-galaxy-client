/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */

#include "icon_button.h"
#include <game/generated/client_data.h>

void CUIIconButton::OnRender(const CUIRect &View, CRenderTools *pRT, float Delta) const {


    if (!GetStyle().IconHasIcon()) {
        return;
    }

    float radius = min(View.w, View.h)*0.5f;
    pRT->DrawCircle(View.x+View.w/2, View.y+View.h/2, radius, GetCurrentStyle().BackgroundGetColor());
    CUIRect IconView;
    IconView.w = IconView.h = GetCurrentStyle().IconGetSize() == 0.0f ? 0.75f*min(View.w, View.h) : GetCurrentStyle().IconGetSize();
    View.Align(&IconView);


    CUIRect IconRect;
    IconRect.w = 64;
    IconRect.h = 64;

    IconView.Align(&IconRect, CUI::ALIGN_CENTER, GetCurrentStyle().MarginGetMax(), GetCurrentStyle().MarginGetMax());

    pRT->Graphics()->BlendNormal();

    pRT->Graphics()->TextureSet(g_pData->m_aImages[IMAGE_GUIICONSNEW].m_Id);
    pRT->Graphics()->QuadsBegin();

    vec4 C = GetCurrentStyle().IconGetColor();
    pRT->Graphics()->SetColor(C.r, C.g, C.b, C.a);

    pRT->SelectSprite(GetStyle().IconGetIcon());
    IGraphics::CQuadItem QuadItem(IconRect.x, IconRect.y, IconRect.w, IconRect.h);
    pRT->Graphics()->QuadsDrawTL(&QuadItem, 1);
    pRT->Graphics()->QuadsEnd();

    m_Tooltip.OnRender(View, pRT, Delta);
}