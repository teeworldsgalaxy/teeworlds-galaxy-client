/* (c) Henritees, Patrick Rathje <tw@patrickrathje.de> */
#ifndef GAME_CLIENT_UI_ELEMENTS_BUTTON_H
#define GAME_CLIENT_UI_ELEMENTS_BUTTON_H
#include "stateful.h"
#include "clickable.h"
#include "tooltip.h"
#include "../styles/button.h"
#include "interactable.h"
#include "../constants.h"

class CUIButton : public CUIInteractable<CUIButtonStyle, CUIButtonStyle>
{
    /**
     * TODO: Use LabelElement here
     */
    const char *m_pText;

public:

    CUIButton() : CUIInteractable() {
        SetEnabled(true);
		StatefulStylesSetDefault();
    }

    virtual void OnUpdate(const CUIRect &View, CRenderTools *pRT, float Delta = 0.0f);

    virtual void OnRender(const CUIRect &View, CRenderTools *pRT, float Delta = 0.0f) const;

	virtual void StatefulStylesSetDefault()
	{
		const vec4 ButtonColor = STYLE_COLOR_BTN_PRIMARY;
		GetStatefulStyles().StatefulGetDefault().BackgroundSetColor(ButtonColor);
		GetStatefulStyles().StatefulGetDisabled().BackgroundSetColor(CUI::Brightness(ButtonColor, 0.5f));
		GetStatefulStyles().StatefulGetDisabled().TextSetFontColor(vec4(0.53f, 0.53f, 0.53f, 0.9f));
		GetStatefulStyles().StatefulGetHover().BackgroundSetColor(CUI::Brightness(ButtonColor, 1.2f));
		GetStatefulStyles().StatefulGetClicked().BackgroundSetColor(CUI::Brightness(ButtonColor, 0.7f));
		GetStatefulStyles().StatefulGetClick().BackgroundSetColor(CUI::Brightness(ButtonColor, 0.7f));

		const vec4 ButtonColorError = STYLE_COLOR_BTN_ERROR;
		GetStatefulStyles().StatefulGetError().StatefulGetDefault().BackgroundSetColor(ButtonColorError);
		GetStatefulStyles().StatefulGetError().StatefulGetDisabled().BackgroundSetColor(CUI::Brightness(ButtonColorError, 0.5f));
		GetStatefulStyles().StatefulGetError().StatefulGetDisabled().TextSetFontColor(STYLE_COLOR_TEXT_DANGER_DISABLED);
		GetStatefulStyles().StatefulGetError().StatefulGetHover().BackgroundSetColor(CUI::Brightness(ButtonColorError, 1.2f));
		GetStatefulStyles().StatefulGetError().StatefulGetClicked().BackgroundSetColor(CUI::Brightness(ButtonColorError, 0.7f));
		GetStatefulStyles().StatefulGetError().StatefulGetClick().BackgroundSetColor(CUI::Brightness(ButtonColorError, 0.7f));

	}

    bool IsErroneous() const
    {
        return GetProperty() == UI_PROPERTY_ERROR;
    }

    void SetErroneous(bool Erroneous)
    {
        if (Erroneous != IsErroneous()) {
            // we need to update things
            if (IsErroneous()) {
                SetProperty(UI_PROPERTY_NONE);
            } else {
                SetProperty(UI_PROPERTY_ERROR);
            }
        }
    }

    const char *GetText() const
    {
        return m_pText;
    }

    void SetText(const char *pText)
    {
        m_pText = pText;
    }

	bool IsLoading() const
	{
		return GetProperty() == UI_PROPERTY_LOADING;
	}

	void SetLoading(bool Loading)
	{
        if (Loading != IsLoading()) {
            // we need to update things
            if (IsLoading()) {
                SetProperty(UI_PROPERTY_NONE);
            } else {
                SetProperty(UI_PROPERTY_LOADING);
            }
        }
	}

};
#endif
