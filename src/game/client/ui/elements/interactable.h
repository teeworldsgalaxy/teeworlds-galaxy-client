#ifndef GAME_CLIENT_UI_ELEMENTS_INTERACTABLE_H
#define GAME_CLIENT_UI_ELEMENTS_INTERACTABLE_H

#include <vector>
#include "clickable.h"
#include "stateful.h"
#include "tooltip.h"

template <typename  S, typename T>
class CUIInteractable : public CUIStateful<S, T>, public CUIClickable
{
public:
	class CHotkey // a neat helper class
	{
		friend class CUIInteractable;
		std::vector<int> m_Hotkeys;

	public:
		CHotkey& Add(int KeyID) { m_Hotkeys.push_back(KeyID); return *this; }
	};


private:
	CHotkey m_Hotkeys;

	void Init()
	{
		m_Hotkeys.m_Hotkeys.clear();
	}


protected:
	CUITooltip m_Tooltip;

	CUIInteractable() : CUIStateful<S, T>(), CUIClickable()
	{
		Init();
	}

	CUIInteractable(const S Style) : CUIStateful<S, T>(Style), CUIClickable()
	{
		Init();
	}

	bool CheckHotkeys(IInput *pInput) const
	{
		if(!pInput)
			return false;

		for(std::vector<int>::const_iterator it = m_Hotkeys.m_Hotkeys.begin(); it != m_Hotkeys.m_Hotkeys.end(); it++)
			if(pInput->KeyDown(*it))
				return true;
		return false;
	}


public:
	bool Pressed(class IInput *pInput = 0) const {
		if(!IsEnabled())
			return false;
		if(CheckHotkeys(pInput))
			return true;
		return CUIStateful<S,T>::m_State == CUIStateful<S,T>::UI_STATE_CLICKED;
	}

	CHotkey& SetHotkeys() // TODO: maybe cache these? (Something like 'SetHotkeysStatic()'... will only be done once...)
	{
		m_Hotkeys.m_Hotkeys.clear();
		return m_Hotkeys;
	}

	CUITooltip& GetTooltip() {
		return m_Tooltip;
	}

	const CUITooltip& GetTooltip() const {
		return m_Tooltip;
	}

};

#endif
