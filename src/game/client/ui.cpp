/* (c) Magnus Auvinen. See licence.txt in the root of the distribution for more information. */
/* If you are missing that file, acquire a complete release at teeworlds.com.                */
#include <base/system.h>
#include <base/math.h>

#include <engine/shared/config.h>
#include <engine/graphics.h>
#include <engine/sound.h>
#include <engine/textrender.h>
#include <engine/keys.h>

#include <engine/engine.h>
#include <engine/sound.h>
#include <game/client/components/sounds.h>

#include <game/generated/client_data.h>
#include <game/client/ui/elements/clickable.h>
#include "ui.h"
#include "ui/styles/text.h"
#include "ui/elements/text.h"
#include "ui/constants.h"

/********************************************************
 UI
*********************************************************/

CUI::CUI()
{
	m_pHotItem = 0;
	m_pActiveItem = 0;
	m_pLastActiveItem = 0;
	m_pBecommingHotItem = 0;

	m_MouseX = 0;
	m_MouseY = 0;
	m_MouseWorldX = 0;
	m_MouseWorldY = 0;
	m_MouseButtons = 0;
	m_LastMouseButtons = 0;

	m_Screen.x = 0;
	m_Screen.y = 0;
	m_Screen.w = 848.0f;
	m_Screen.h = 480.0f;

  m_pSounds = 0;
  m_HoverSound = false;
  m_HoverSoundTime = 0;
}

int CUI::Update(float Mx, float My, float Mwx, float Mwy, int Buttons)
{
	m_MouseX = Mx;
	m_MouseY = My;
	m_MouseWorldX = Mwx;
	m_MouseWorldY = Mwy;
	m_LastMouseButtons = m_MouseButtons;
	m_MouseButtons = Buttons;
	m_pHotItem = m_pBecommingHotItem;
	if(m_pActiveItem)
		m_pHotItem = m_pActiveItem;
	m_pBecommingHotItem = 0;


  //handle mouse hovering
  if (m_HoverSound && m_pSounds && m_HoverSoundTime < time_get() - time_freq()/20) {
    m_pSounds->Play(CSounds::CHN_GUI, SOUND_GUI_HOVER, 0);
    m_HoverSound = false;
    m_HoverSoundTime = time_get();
  }
	return 0;
}

int CUI::MouseInside(const CUIRect *r)
{
	if(m_MouseX >= r->x && m_MouseX <= r->x+r->w && m_MouseY >= r->y && m_MouseY <= r->y+r->h)
		return 1;
	return 0;
}


CUIRect CUI::ConvertToUICoordinates(const CUIRect ScreenMapping, const CUIRect Rect) {

	CUIRect Converted;
	// TODO: Support translations as well
	Converted.x = Rect.x*(m_Screen.w / ScreenMapping.w);
	Converted.w = Rect.w*(m_Screen.w / ScreenMapping.w);
	Converted.y = Rect.y *(m_Screen.h / ScreenMapping.h);
	Converted.h = Rect.h *(m_Screen.h / ScreenMapping.h);
	return Converted;
}

void CUI::ConvertMouseMove(float *x, float *y)
{
	float Fac = (float)(g_Config.m_UiMousesens)/g_Config.m_InpMousesens;
	*x = *x*Fac;
	*y = *y*Fac;
}

CUIRect *CUI::Screen()
{
	float Aspect = Graphics()->ScreenAspect();
	float w, h;

	h = 600;
	w = Aspect*h;

	m_Screen.w = w;
	m_Screen.h = h;

	return &m_Screen;
}

float CUI::PixelSize()
{
	return Screen()->w/Graphics()->ScreenWidth();
}

void CUI::SetScale(float s)
{
	g_Config.m_UiScale = (int)(s*100.0f);
}

float CUI::Scale()
{
	return g_Config.m_UiScale/100.0f;
}

float CUIRect::Scale() const
{
	return g_Config.m_UiScale/100.0f;
}

void CUI::ClipEnable(const CUIRect *r)
{
	float XScale = Graphics()->ScreenWidth()/Screen()->w;
	float YScale = Graphics()->ScreenHeight()/Screen()->h;
	Graphics()->ClipEnable((int)(r->x*XScale), (int)(r->y*YScale), (int)(r->w*XScale), (int)(r->h*YScale));
}

void CUI::ClipDisable()
{
	Graphics()->ClipDisable();
}

void CUIRect::HSplitMid(CUIRect *pTop, CUIRect *pBottom) const
{
	HSplitPerc(0.5f, pTop, pBottom);
}

void CUIRect::HSplitPerc(float perc, CUIRect *pTop, CUIRect *pBottom) const
{
	CUIRect r = *this;
	float Cut = r.h*perc;

	if(pTop)
	{
		pTop->x = r.x;
		pTop->y = r.y;
		pTop->w = r.w;
		pTop->h = Cut;
	}

	if(pBottom)
	{
		pBottom->x = r.x;
		pBottom->y = r.y + Cut;
		pBottom->w = r.w;
		pBottom->h = r.h - Cut;
	}
}

void CUIRect::HSplitTop(float Cut, CUIRect *pTop, CUIRect *pBottom) const
{
	CUIRect r = *this;
	Cut *= Scale();

	if (pTop)
	{
		pTop->x = r.x;
		pTop->y = r.y;
		pTop->w = r.w;
		pTop->h = Cut;
	}

	if (pBottom)
	{
		pBottom->x = r.x;
		pBottom->y = r.y + Cut;
		pBottom->w = r.w;
		pBottom->h = r.h - Cut;
	}
}

void CUIRect::HSplitBottom(float Cut, CUIRect *pTop, CUIRect *pBottom) const
{
	CUIRect r = *this;
	Cut *= Scale();

	if (pTop)
	{
		pTop->x = r.x;
		pTop->y = r.y;
		pTop->w = r.w;
		pTop->h = r.h - Cut;
	}

	if (pBottom)
	{
		pBottom->x = r.x;
		pBottom->y = r.y + r.h - Cut;
		pBottom->w = r.w;
		pBottom->h = Cut;
	}
}


void CUIRect::VSplitMid(CUIRect *pLeft, CUIRect *pRight) const
{
	VSplitPerc(0.5f, pLeft, pRight);
}

void CUIRect::VSplitPerc(float perc, CUIRect *pLeft, CUIRect *pRight) const
{
	CUIRect r = *this;
	float Cut = r.w*perc;

	if (pLeft)
	{
		pLeft->x = r.x;
		pLeft->y = r.y;
		pLeft->w = Cut;
		pLeft->h = r.h;
	}

	if (pRight)
	{
		pRight->x = r.x + Cut;
		pRight->y = r.y;
		pRight->w = r.w - Cut;
		pRight->h = r.h;
	}
}

void CUIRect::VSplitLeft(float Cut, CUIRect *pLeft, CUIRect *pRight) const
{
	CUIRect r = *this;
	Cut *= Scale();

	if (pLeft)
	{
		pLeft->x = r.x;
		pLeft->y = r.y;
		pLeft->w = Cut;
		pLeft->h = r.h;
	}

	if (pRight)
	{
		pRight->x = r.x + Cut;
		pRight->y = r.y;
		pRight->w = r.w - Cut;
		pRight->h = r.h;
	}
}

void CUIRect::VSplitRight(float Cut, CUIRect *pLeft, CUIRect *pRight) const
{
	CUIRect r = *this;
	Cut *= Scale();

	if (pLeft)
	{
		pLeft->x = r.x;
		pLeft->y = r.y;
		pLeft->w = r.w - Cut;
		pLeft->h = r.h;
	}

	if (pRight)
	{
		pRight->x = r.x + r.w - Cut;
		pRight->y = r.y;
		pRight->w = Cut;
		pRight->h = r.h;
	}
}

void CUIRect::Margin(float Cut, CUIRect *pOtherRect) const
{
	CUIRect r = *this;
	Cut *= Scale();

	pOtherRect->x = r.x + Cut;
	pOtherRect->y = r.y + Cut;
	pOtherRect->w = r.w - 2*Cut;
	pOtherRect->h = r.h - 2*Cut;
}

void CUIRect::VMargin(float Cut, CUIRect *pOtherRect) const
{
	CUIRect r = *this;
	Cut *= Scale();

	pOtherRect->x = r.x + Cut;
	pOtherRect->y = r.y;
	pOtherRect->w = r.w - 2*Cut;
	pOtherRect->h = r.h;
}

void CUIRect::HMargin(float Cut, CUIRect *pOtherRect) const
{
	CUIRect r = *this;
	Cut *= Scale();

	pOtherRect->x = r.x;
	pOtherRect->y = r.y + Cut;
	pOtherRect->w = r.w;
	pOtherRect->h = r.h - 2*Cut;
}



float CUI::DoScrollLogic(const void *pID, CUIRect *pView, float Current, float Step, bool Relative) {

	if (MouseInside(pView)) {
		if (Input()->KeyPresses(KEY_MOUSE_WHEEL_UP)) {
			Current -= Step;
		} else if (Input()->KeyPresses(KEY_MOUSE_WHEEL_DOWN)) {
			Current += Step;
		}
	}

	if (Relative) {
		return clamp(Current, 0.0f, 1.0f);
	} else {
		return Current;
	}
}


int CUI::DoLinkLogic(const void *pID, const char *pUrl, const CUIRect *pRect) {

	int Click = DoButtonLogic(pID, "", false, pRect);
	if (Click) {
		browser_launch(pUrl);
	}
	return Click;
}

int CUI::DoRectLogic(const void *pID, const CUIRect *pRect, bool HoverSound)
{
	if (!pID) {
		return 0;
	}
	// logic for mouse hover and clicks
	int ReturnValue = 0;
	int Inside = MouseInside(pRect);
	static int ButtonUsed = 0;

	if(ActiveItem() == pID)
	{
		if(!MouseButton(ButtonUsed))
		{
			if(Inside)
				ReturnValue = 1+ButtonUsed;
			SetActiveItem(0);
		}
	}
	else if(HotItem() == pID)
	{
		if(MouseButton(0))
		{
			SetActiveItem(pID);
			ButtonUsed = 0;
		}

		if(MouseButton(1))
		{
			SetActiveItem(pID);
			ButtonUsed = 1;
		}

    	ReturnValue = -1;
	}

	if(Inside)
		SetHotItem(pID, HoverSound);

	return ReturnValue;
}

int CUI::DoClickableRectLogic(const CUIClickable *pElem, const CUIRect *pRect, bool HoverSound)
{
	if(pElem->IsEnabled())
		return DoRectLogic(pElem->GetID(), pRect, HoverSound);
	return 0;
}

int CUI::DoButtonLogic(const void *pID, const char *pText, int Checked, const CUIRect *pRect, bool HoverSound)
{
	// logic
	int ReturnValue = DoRectLogic(pID, pRect, HoverSound);
  if (ReturnValue == -1) {  //no hover for buttons
    ReturnValue = 0;
  } else if(Checked < 0) {
    ReturnValue = 0;
  }
	return ReturnValue;
}


bool CUI::DoInputTabLogic(const void *pCurrent, const void *pNext, const void *pPrevious) {

	static bool s_AlreadyPressed = false;
	if (!s_AlreadyPressed && Input()->KeyPressed(KEY_TAB)) {

		if (LastActiveItem() == pCurrent) {
			s_AlreadyPressed = true;
			if (Input()->KeyPressed(KEY_LSHIFT)) {
				if (pPrevious) {
					SetActiveItem(pPrevious);
					return true;
				}
			} else if (pNext) {
				SetActiveItem(pNext);
				return true;
			}

			ClearLastActiveItem();
			SetActiveItem(0);
			return true;
		}
	} else if (!Input()->KeyPressed(KEY_TAB)) {
		s_AlreadyPressed = false;
	}


	return false;
}

bool CUI::DoInputReturnLogic(const void *pCurrent, const void *pNext) {

	static bool s_AlreadyPressed = false;

	if (!s_AlreadyPressed && Input()->KeyPressed(KEY_RETURN)) {

		if (LastActiveItem() == pCurrent) {
			s_AlreadyPressed = true;
			if (pNext) {
				SetActiveItem(pNext);
				return true;
			}
			ClearLastActiveItem();
			SetActiveItem(0);
			return true;
		}
	} else if (!Input()->KeyPressed(KEY_RETURN)) {
		s_AlreadyPressed = false;
	}
	return false;
}

bool CUI::DoInputEscapeLogic(const void *pCurrent, const void *pNext) {

	static bool s_AlreadyPressed = false;

	if (!s_AlreadyPressed && Input()->KeyPressed(KEY_ESCAPE)) {

		if (LastActiveItem() == pCurrent) {
			s_AlreadyPressed = true;
			if (pNext) {
				SetActiveItem(pNext);
				return true;
			}
			ClearLastActiveItem();
			SetActiveItem(0);
			return true;
		}
	} else if (!Input()->KeyPressed(KEY_ESCAPE)) {
		s_AlreadyPressed = false;
	}
	return false;
}

bool CUIRect::Align(CUIRect *pInner, int Align, float MarginX, float MarginY, bool KeepRatio) const {

	//check if margins are possible else lower margins
	MarginX = min(MarginX, max(w*0.5f-MarginX, 0.0f));
	MarginY = min(MarginY, max(h*0.5f-MarginY, 0.0f));


	float DoubleMarginX = 2.0f*MarginX;
	float DoubleMarginY = 2.0f*MarginY;

	float Width = pInner->w;
	float Height = pInner->h;

	float Ratio = Width / Height;

	bool Resized = false;

	if (Width > w - DoubleMarginX) {
		Width = w - DoubleMarginX;
		if (KeepRatio) {
			Height = Width / Ratio;
			Resized = true;
		}
	}

	if (Height > h - DoubleMarginY) {
		Height = h - DoubleMarginY;
		if (KeepRatio) {
			Width = Height*Ratio;
			Resized = true;
		}
	}

	//Alignment

	float PosX = x + (w-Width)*0.5f;
	float PosY = y + (h-Height)*0.5f;

	if(Align != CUI::ALIGN_CENTER)
	{
		if(Align&CUI::ALIGN_TOP || Align == CUI::ALIGN_LEGACY_TOP) {
			PosY = y+MarginY;
		} else if(Align&CUI::ALIGN_BOTTOM) {
			PosY = y + h - Height - MarginY;
		}

		if(Align&CUI::ALIGN_LEFT || Align == CUI::ALIGN_LEGACY_LEFT) {
			PosX = x + MarginX;
		} else if(Align&CUI::ALIGN_RIGHT || Align == CUI::ALIGN_LEGACY_RIGHT) {
			PosX = x + w - Width - MarginX;
		}
	}

	//set values
	pInner->x = PosX;
	pInner->y = PosY;
	pInner->w = Width;
	pInner->h = Height;

	return Resized;
}

void CUI::DoLabel(const CUIRect *r, const char *pText, float Size, int Align, int MaxWidth, const vec4 *pColor, const vec4 *pOutlineColor) {
	// TODO: FIX ME!!!!
	//Graphics()->BlendNormal();

	CUITextStyle Style;
	Style.TextSetFontSize(Size);
	Style.DimensionSetMaxWidth(MaxWidth);

	if (pOutlineColor) {
		Style.TextSetOutlineColor(*pOutlineColor);
	}

	if (pColor) {
		Style.TextSetFontColor(*pColor);
	}

	Style.AlignmentSetLegacyValue(Align);

	CUIText TextEl(Style);
    CUIRect Copy = *r;
    TextEl.SetText(pText);
	TextEl.OnRender(Copy, RenderTools(),0.0f);
}

void CUI::DoLabelScaled(const CUIRect *r, const char *pText, float Size, int Align, int MaxWidth, const vec4 *pColor, const vec4 *pOutlineColor)
{
	DoLabel(r, pText, Size*Scale(), Align, MaxWidth, pColor, pOutlineColor);
}


void CUI::DoSpinner(const CUIRect *pRect, int Align, vec4 *pColor) {

	const int NumSpinner = 3;
	CUIRect Circles;

	Circles.h = min(pRect->h, 15.0f);
	Circles.w = NumSpinner * Circles.h;

	pRect->Align(&Circles, Align);	

	vec4 Color = STYLE_COLOR_SPINNER;
	vec4 CurrentColor =  STYLE_COLOR_SPINNER_CURRENT;
	
	
	float MaxRadius = Circles.h*0.3f;


	float Speed = 1.5f;
	const float PostTime = 0.16f;
	const float OverlappingTime = 0.16f;
	const float StepTime = 1.80f;
	float OverAllTime = StepTime+(NumSpinner-1)*OverlappingTime+PostTime;

	static float s_Step = 0.0f;

	if (s_Step >= OverAllTime-0.001f) {
		s_Step = 0.0f;
	} else {
		s_Step += Speed*Client()->RenderFrameTime();
	}

	CEnvPoint Start;
	Start.m_aValues[0] = f2fx(0.0f);
	Start.m_Curvetype = CURVETYPE_SMOOTH;
	Start.m_Time = 0;
	CEnvPoint Mid;
	Mid.m_aValues[0] = f2fx(1.0f);
	Mid.m_Curvetype = CURVETYPE_SMOOTH;
	Mid.m_Time = 500;
	CEnvPoint End;
	End.m_aValues[0] = f2fx(0.0f);
	End.m_Curvetype = CURVETYPE_SMOOTH;
	End.m_Time = 1000;
	CEnvPoint Points[3] = {Start, Mid, End};


	for(int i = 0; i < NumSpinner; ++i) {

		CUIRect CircleBox;
		Circles.VSplitPerc(1.0f / (float) (NumSpinner-i), &CircleBox, &Circles);
		CircleBox.Margin(2.0f, &CircleBox);

		float CircleStep = s_Step-i*OverlappingTime;		
		CircleStep = CircleStep / StepTime;
		CircleStep = clamp(CircleStep, 0.0f, 1.0f);
		
		float Step = 0;
		RenderTools()->RenderEvalEnvelope(Points, 3, 1, CircleStep, &Step);

		float Radius = mix(0.0f, MaxRadius, Step);
		CUIRect Circle;
		Circle.w = Circle.h = Radius;
		CircleBox.Align(&Circle, ALIGN_CENTER);
		RenderTools()->DrawCircle(Circle.x+Circle.w/2.0f, Circle.y+Circle.h/2.0f, Radius, STYLE_COLOR_SPINNER_CURRENT);
	}


	/* Old version
		float Step = 1.0f-2.0f*abs(CircleStep-0.5f);
		Step = Step*Step;

		dbg_msg("asd", "%d %f", i, Step);

		float Radius = mix(MinRadius, MaxRadius, Step);

		CUIRect Circle;
		Circle.w = Circle.h = Radius;
		CircleBox.Align(&Circle, ALIGN_CENTER);
		
		vec4 C = mix(STYLE_COLOR_SPINNER, STYLE_COLOR_SPINNER_CURRENT, Step);
		RenderTools()->DrawCircle(Circle.x+Circle.w/2.0f, Circle.y+Circle.h/2.0f, Radius, C);
	*/
	

 /* if (g_pData->m_aImages[IMAGE_GUISPINNER].m_Id == -1) {
    return; //Image not loaded
  }

  CUIRect Image;
  Image.x = Image.y = 0;
  Image.w = Image.h = 128;
  pRect->Align(&Image, Align);

  Graphics()->TextureSet(g_pData->m_aImages[IMAGE_GUISPINNER].m_Id);
	Graphics()->QuadsBegin();

  if (pColor) {
    Graphics()->SetColor(pColor->r, pColor->g, pColor->b, pColor->a);
  } else {
    Graphics()->SetColor(1.0f, 1.0f, 1.0f, 0.8f);
  }

  float Angle = 6.2832f *  ((float)(time_get()%time_freq())  / (float)time_freq());
	IGraphics::CQuadItem QuadItem = IGraphics::CQuadItem(Image.x, Image.y, Image.w, Image.h);
  Graphics()->QuadsSetRotation(-Angle);
	Graphics()->QuadsDrawTL(&QuadItem, 1);
	Graphics()->QuadsEnd();*/
}


vec4 CUI::Brightness(vec4 Color, float Brightness) {
	Color.r *= Brightness;
	Color.g *= Brightness;
	Color.b *= Brightness;
	return Color;
}
