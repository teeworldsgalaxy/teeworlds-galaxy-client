/*
	Created by Patrick Rathje, little tested use carefully
*/
#ifndef BASE_TL_LIST_H
#define BASE_TL_LIST_H

#include <base/system.h>

template <class T>
class list
{
protected:	
		struct CEntry {
			CEntry *m_pNext;
			CEntry *m_pPrevious;
			T m_Value;		
		};
		
	CEntry *m_pFirst;
	CEntry *m_pLast;
	int m_Size;

public:

	/*
		Function: list constructor
	*/
	list()
	{
		//init pointers
		
		m_pFirst = 0;
		m_pLast = 0;
		
		//clear size
		m_Size = 0;
	}

	/*
		Function: list destructor
	*/
	~list()
	{	
		clear();
	}


	/*
		Function: clear
	*/
	void clear()
	{	
		while(m_pFirst) {
			delete_entry(m_pFirst);	
		}
		m_Size = 0;
	}

	/*
		Function: size
	*/
	int size() const
	{
		return m_Size;
	}

	/*
		Function: remove
	*/
	bool remove(const T& Item)
	{
		CEntry *pCurrent = m_pFirst;
		
		while(pCurrent) {
			if(pCurrent->m_Value == Item) {
				delete_entry(pCurrent);							
				return true;
			}
			pCurrent = pCurrent->m_pNext;
		}
			
		return false;
	}
	
	/*
		Function: remove_index
	*/
	bool remove_index(int index) {
		//TODO optimize: we know the size!
		
		CEntry *pCurrent = m_pFirst;
		
		if (index < 0) {
			return false;
		}
		
		while(pCurrent) {
			if (index == 0) {
				delete_entry(pCurrent);						
				return true;
			}
			index--;
			pCurrent = pCurrent->m_pNext;
		}
		
		return false;
	}

	/*
		Function: add
			Adds an item to the array.

		Arguments:
			item - Item to add.

	*/
	void add(const T& item)
	{
		push_back(item);
	}
	
	/*
		Function: push_back
			Adds an item to the array.

		Arguments:
			item - Item to add.

	*/
	void push_back(const T& item)
	{
		CEntry *pNew = new CEntry();
		pNew->m_pPrevious = m_pLast;
		pNew->m_pNext = 0;
		pNew->m_Value = item;
		m_pLast = pNew;
		m_Size++;
		
		//update pointer of previous element
		if (pNew->m_pPrevious) {
			pNew->m_pPrevious->m_pNext = pNew;
		}
		
		//update start of list
		if (m_pFirst == 0) {
			m_pFirst = pNew;
		}
	}
	
	void push_front(const T& item)
	{
		CEntry *pNew = new CEntry();
		pNew->m_pPrevious = 0;
		pNew->m_pNext = m_pFirst;
		pNew->m_Value = item;
		m_pFirst = pNew;
		m_Size++;
		
		//update pointer of next element
		if (pNew->m_pNext) {
			pNew->m_pNext->m_pPrevious = pNew;
		}
		
		//update end of list
		if (m_pLast == 0) {
			m_pLast = pNew;
		}
	}

	T front() {
		return m_pFirst->m_Value;
	}
	
	T back() {
		return m_pLast->m_Value;
	}

	T pop_front() {
		T Value = m_pFirst->m_Value;
		delete_entry(m_pFirst);
		return Value;
	}	
	
	T pop_back() {
		T Value = m_pLast->m_Value;
		delete_entry(m_pLast);
		return Value;
	}	
	
	/*
		Function: operator[]
	*/
	T& operator[] (int index) {
		return get_entry(index)->m_Value;	
	}
	
	/*
		Function: const operator[]
	*/
	const T& operator[] (int index) const {
		return get_entry(index)->m_Value;
	}
protected:

	CEntry* get_entry(int index) {
		//TODO optimize: we know the size!
		
		CEntry *pCurrent = m_pFirst;
		
		if (index < 0) {
			return 0;
		}
		
		while(pCurrent) {
			if (index == 0) {
				return pCurrent;
			}
			index--;
			pCurrent = pCurrent->m_pNext;
		}
		return 0;
	}

	
	void delete_entry(CEntry *pEntry) {
		CEntry *pPrevious = pEntry->m_pPrevious;
		CEntry *pNext = pEntry->m_pNext;		
		
		//restore connection:
		if (pPrevious) {
			pPrevious->m_pNext = pNext;
		}
		
		if (pNext) {
			pNext->m_pPrevious = pPrevious;
		}
		
		//restore main pointers:
		if (m_pFirst == pEntry) {
			m_pFirst = pNext;
		}
		
		if (m_pLast == pEntry) {
			m_pLast = pPrevious;
		}
		
		//delete entry
		delete pEntry;
		
		//decrease Size
		m_Size--;
	}
};

#endif
