/*
	Created by Patrick Rathje, little tested use carefully
*/

#ifndef BASE_TL_PROMISE_H
#define BASE_TL_PROMISE_H


//promises
//we might want to add synchronization later
template<typename T>
class promise
{
  protected:
    T* m_pData;
    bool m_Resolved;
    bool m_CascadeDeletion;
    bool m_Rejected;
    
    typedef void (*callback)(promise<T> &promise, void *pUser);
    
    callback m_pfSuccessHandler;
    void *m_pfSuccessUser;
    
    callback m_pfErrorHandler;
    void *m_pfErrorUser;
    
    callback m_pfFinallyHandler;
    void *m_pfFinallyUser;
  
  public: 
    promise() {
      m_Resolved = false;
      m_Rejected = false;
      m_pData = 0;
      
      m_pfSuccessHandler = 0;
      m_pfSuccessUser = 0;
      m_pfErrorHandler = 0;
      m_pfErrorUser = 0;
      m_pfFinallyHandler = 0;
      m_pfFinallyUser = 0;
    }
    
    ~promise() {
      if (m_CascadeDeletion && m_pData) {
        delete m_pData;
      }      
    }
    
    bool resolved() {
      return m_Resolved;
    }
    
    bool rejected() {
      return m_Rejected;
    }
    
    T* value() {
      if (m_Rejected || !m_Resolved) {
        return 0;
      } else {
        return m_pData;
      }
    }
    
    // we might want to add support for multiple handlers
    void finally(callback pCallback, void *pUser) {
      m_pfFinallyUser = pUser;
      m_pfFinallyHandler = pCallback;
    }
     // we might want to add support for multiple handlers
    void success(callback pCallback, void *pUser) {
      m_pfSuccessUser = pUser;
      m_pfSuccessHandler = pCallback;
    }
    
     // we might want to add support for multiple handlers
    void error(callback pCallback, void *pUser) {
      m_pfErrorUser = pUser;
      m_pfErrorHandler = pCallback;
    }
    
    void resolve(T *pData, bool CascadeDeletion = false) {
      m_pData = pData;
      m_CascadeDeletion = CascadeDeletion;
      m_Resolved = true;
      
      //call success handler
      if (m_pfSuccessHandler) {
        m_pfSuccessHandler(this, m_pfSuccessUser);
      }
      //call finally handler
      if (m_pfFinallyHandler) {
        m_pfFinallyHandler(this, m_pfFinallyUser);
      }
    }
    
    void reject() {
      m_Rejected = true;
      m_CascadeDeletion = false;
      
      //call error handler
      if (m_pfErrorHandler) {
        m_pfErrorHandler(this, m_pfErrorUser);
      }
      //call finally handler
      if (m_pfFinallyHandler) {
        m_pfFinallyHandler(this, m_pfFinallyUser);
      }
    }    
};
#endif