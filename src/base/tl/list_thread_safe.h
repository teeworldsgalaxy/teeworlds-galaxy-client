/*
	Created by Patrick Rathje, little tested use carefully
*/
#ifndef BASE_TL_LIST_THREAD_SAFE_H
#define BASE_TL_LIST_THREAD_SAFE_H

#include "list.h"

template <class T>
struct Maybe
{
    bool Nothing;
    T    Value;

    Maybe( const T& value ): Nothing( false ), Value( value ) { }
    Maybe(): Nothing ( true ), Value( T() ) { }
    bool IsEmpty() { return Nothing;}
    bool IsFull() {return !Nothing;}
};

/*
	Thread safe implementation of the linked list using locks
*/template <class T>
class list_ts : public list<T>
{

protected:
	LOCK m_pLock;
public:

	list_ts() {
		m_pLock = sys_lock_create();
	}
	
	~list_ts() {
		sys_lock_destroy(m_pLock);
	}

	void clear()
	{	
		wait();
		list<T>::clear();
		release();
	}


	int size()
	{
		wait();
		int size = list<T>::size();
		release();
		return size;
	}


	bool remove(const T& Item)
	{
		wait();
		bool result = list<T>::remove(Item);
		release();
		return result;
	}
	

	bool remove_index(int index) {
		wait();
		bool result = list<T>::remove_index(index);
		release();
		return result;
	}
	
	void add(const T& item)
	{
		push_back(item);
	}

	void push_back(const T& item)
	{
		wait();
		list<T>::push_back(item);
		release();
	}
	
	void push_front(const T& item)
	{
		wait();
		list<T>::push_front(item);
		release();
	}

	T front() {
		wait();
		T Value = list<T>::front();
		release();
		return Value;
	}
	
	T back() {
		wait();
		T Value = list<T>::back();
		release();
		return Value;
	}

	T pop_front() {
		wait();
		T Value = list<T>::pop_front();
		release();
		return Value;
	}

    Maybe<T> get_front() {
		wait();
		int size = list<T>::size();

        if (size > 0) {
            T Value = list<T>::pop_front();
            release();
            return Maybe<T>(Value);
        } else {
            release();
            return Maybe<T>();
        }
	}
	
	
	T pop_back() {
		wait();
		T Value = list<T>::pop_back();
		release();
		return Value;
	}
	
	
	/*
		Function: operator[]
	*/
	T& operator[] (int index) {
		wait();
		T Value = list<T>::get_entry(index)->m_Value;
		release();
		return Value;
	}
	
	/*
		Function: const operator[]
	*/
	const T& operator[] (int index) const {
		wait();
		T Value = list<T>::get_entry(index)->m_Value;
		release();
		return Value;
	}
	
protected:
	
	void wait() {
		sys_lock_wait(m_pLock);
	}
	
	void release() {
		sys_lock_release(m_pLock);
	}
};

#endif
